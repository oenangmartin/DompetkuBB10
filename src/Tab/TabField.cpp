/*
 * TabField.cpp
 *
 *  Created on: Dec 11, 2014
 *      Author: yogi
 */

#include "TabField.h"

TabField::TabField() {
	tabItem = NULL;
	tabContent = NULL;
}

TabField::~TabField() {
	// TODO Auto-generated destructor stub
}

TabItem* TabField::getTabItem() {
	return tabItem;
}

void TabField::setTabItem(TabItem* tabItem) {
	this->tabItem = tabItem;
	connect(tabItem, SIGNAL(clicked()), this,
				SLOT(onSelected()));
}

void TabField::onSelected(){
	//qDebug() << ">>TabField::onSelected";
	emit activeTabChanged(this);
}

Container* TabField::getTabContent(){
	return tabContent;
}

void TabField::setTabContent(Container *content)
{
	tabContent = content;
}
