/*
 * TabField.h
 *
 *  Created on: Dec 11, 2014
 *      Author: yogi
 */

#ifndef TABFIELD_H_
#define TABFIELD_H_

#include "TabItem.h"
#include <bb/cascades/Container>
#include <bb/cascades/CustomControl>

using namespace bb::cascades;

class TabField : public bb::cascades::CustomControl {
	Q_OBJECT
public:
	TabField();
	virtual ~TabField();

	TabItem* getTabItem() ;
	void setTabItem( TabItem* tabItem);

	Container* getTabContent();
	void setTabContent(Container *content);


private slots:
	void onSelected();
protected:
	TabItem *tabItem;
	Container *tabContent;

signals:
	void activeTabChanged(TabField* field);
};

#endif /* TABFIELD_H_ */
