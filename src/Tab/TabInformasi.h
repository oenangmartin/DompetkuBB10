/*
 * TabInformasi.h
 *
 *  Created on: Dec 16, 2014
 *      Author: yogi
 */

/*
 * TabInformasi.h
 *
 *  Created on: Dec 16, 2014
 *      Author: yogi
 */

#ifndef TabInformasi_H_
#define TabInformasi_H_


#include "TabField.h"
#include <bb/cascades/Container>
#include <bb/cascades/CustomControl>

using namespace bb::cascades;

class TabInformasi : public TabField {
	Q_OBJECT
public:
	TabInformasi(QString title);
	virtual ~TabInformasi();
};

#endif /* TabInformasi_H_ */

