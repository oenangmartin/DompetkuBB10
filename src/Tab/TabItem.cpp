/*
 * TabItem.cpp
 *
 *  Created on: Dec 11, 2014
 *      Author: yogi
 */

#include "TabItem.h"

/*
 Container {
    property alias tabtitleLabel: thetitleLabel.text
    property alias tabImage: theBG.imageSource
    property alias tabIcon: theIcon.imageSource
    layout: DockLayout {

    }
    ImageView {
        id: theBG
    }
    ImageView {
        id: theIcon
        verticalAlignment: VerticalAlignment.Center
        translationX: 30.0
    }
    Label {
        id: thetitleLabel
        text: ""
        verticalAlignment: VerticalAlignment.Center
        translationX: 200.0
        textStyle.fontSize: FontSize.Large
        textStyle.color: Color.White
    }
}
 */
TabItem::TabItem() {

	//Container *base = Container::create().layout(DockLayout::create());
	Container *base = Container::create();
	int padding = 30;
	base->setTopPadding(padding);
	base->setLeftPadding(padding);
	base->setRightPadding(padding);
	base->setBottomPadding(padding);
	setRoot(base);

	tabBg = ImageView::create();
	base->add(tabBg);

	tabIcon = ImageView::create();
	tabIcon->setVerticalAlignment(VerticalAlignment::Center);
	base->add(tabIcon);

	titleLabel = Label::create();
	//titleLabel->setTranslationX(200);
	titleLabel->setVerticalAlignment(VerticalAlignment::Center);
	titleLabel->textStyle()->setFontSizeValue(FontSize::XXLarge);
	titleLabel->textStyle()->setColor(Color::White);
	base->add(titleLabel);

	connect(this, SIGNAL(touch(bb::cascades::TouchEvent *)), this, SLOT(onClicked(bb::cascades::TouchEvent *)));

}

QString TabItem::title(){
	return titleLabel->text();
}

void TabItem::onClicked(bb::cascades::TouchEvent *touchEvent){
	if (touchEvent->touchType() == TouchType::Up ){
		//qDebug() << ">>TabItem::onClicked";
		emit clicked();
	}
	else if (touchEvent->touchType() == TouchType::Down){
	}
	else if (touchEvent->touchType() == TouchType::Cancel){
	}
}

void TabItem::setBackground(QUrl bgUrl){
	tabBg->setImageSource(bgUrl);
}
void TabItem::setIcon(QUrl iconUrl){
	tabIcon->setImageSource(iconUrl);
}
void TabItem::setTitle(QString title){
	titleLabel->setText(title);
}

TabItem::~TabItem() {
	// TODO Auto-generated destructor stub
}

