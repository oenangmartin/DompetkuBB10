/*
 * InboxTab.cpp
 *
 *  Created on: Dec 16, 2014
 *      Author: yogi
 */

#include "InboxTab.h"
#include "UiUtils.h"

InboxTab::InboxTab(QString title) {
	setImplicitLayoutAnimationsEnabled(false);
	QmlDocument *qml = UiUtils::LoadQml("tab/TabInbox.qml", this);
	qml->setContextProperty("page", this);
	Container *container = qml->createRootObject<Container>();
	setRoot(container);

	TabItem *tabItem = new TabItem;
	tabItem->setTitle(title);
	setTabItem(tabItem);

}

InboxTab::~InboxTab() {
	// TODO Auto-generated destructor stub
}

