/*
 * TabItem.h
 *
 *  Created on: Dec 11, 2014
 *      Author: yogi
 */

#ifndef TABITEM_H_
#define TABITEM_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Container>
#include <bb/cascades/CustomControl>
#include <bb/cascades/DockLayout>
#include <bb/cascades/ImageView>
#include <bb/cascades/Label>

using namespace bb::cascades;

class TabItem  : public bb::cascades::CustomControl {
	Q_OBJECT
public:
	TabItem();
	virtual ~TabItem();

	void setBackground(QUrl bgUrl);
	void setIcon(QUrl iconUrl);
	void setTitle(QString title);
	QString title();

private:
	ImageView *tabBg;
	ImageView *tabIcon;
	Label *titleLabel;

private slots:
	void onClicked(bb::cascades::TouchEvent *pTouchEvent);

signals:
	void clicked();
};

#endif /* TABITEM_H_ */
