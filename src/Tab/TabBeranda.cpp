/*
 * TabBeranda.cpp
 *
 *  Created on: Dec 16, 2014
 *      Author: yogi
 */

#include "TabBeranda.h"
#include "UiUtils.h"

TabBeranda::TabBeranda(QString title) {
	QmlDocument *qml = UiUtils::LoadQml("tab/TabBeranda.qml", this);
	qml->setContextProperty("page", this);
	Container *container = qml->createRootObject<Container>();
	setRoot(container);

	TabItem *tabItem = new TabItem;
	tabItem->setTitle(title);
	setTabItem(tabItem);

}

TabBeranda::~TabBeranda() {
	// TODO Auto-generated destructor stub
}

