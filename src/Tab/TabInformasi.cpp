/*
 * TabInformasi.cpp
 *
 *  Created on: Dec 16, 2014
 *      Author: yogi
 */

#include "TabInformasi.h"
#include "UiUtils.h"

TabInformasi::TabInformasi(QString title) {
	QmlDocument *qml = UiUtils::LoadQml("tab/TabInformasi.qml", this);
	qml->setContextProperty("page", this);
	Container *container = qml->createRootObject<Container>();
	setRoot(container);

	TabItem *tabItem = new TabItem;
	tabItem->setTitle(title);
	setTabItem(tabItem);

}

TabInformasi::~TabInformasi() {
	// TODO Auto-generated destructor stub
}

