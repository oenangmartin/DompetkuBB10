/*
 * InboxTab.h
 *
 *  Created on: Dec 16, 2014
 *      Author: yogi
 */

#ifndef INBOXTAB_H_
#define INBOXTAB_H_


#include "TabField.h"
#include <bb/cascades/Container>
#include <bb/cascades/CustomControl>

using namespace bb::cascades;

class InboxTab : public TabField {
	Q_OBJECT
public:
	InboxTab(QString title);
	virtual ~InboxTab();
};

#endif /* INBOXTAB_H_ */
