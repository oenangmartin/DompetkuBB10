/*
 * TabControl.h
 *
 *  Created on: Dec 11, 2014
 *      Author: yogi
 */

#ifndef TABCONTROL_H_
#define TABCONTROL_H_

#include <bb/cascades/Container>
#include <bb/cascades/CustomControl>
#include "TabField.h"

using namespace bb::cascades;

class TabControl : public bb::cascades::CustomControl {
	Q_OBJECT
public:
	TabControl();
	virtual ~TabControl();

	void addTab(TabField *tabField);

protected:
	QVector<TabField*> tabVector;

};

#endif /* TABCONTROL_H_ */
