/*
 * TabBeranda.h
 *
 *  Created on: Dec 21, 2014
 *      Author: yogi
 */

/*
 * TabBeranda.h
 *
 *  Created on: Dec 16, 2014
 *      Author: yogi
 */

/*
 * TabBeranda.h
 *
 *  Created on: Dec 16, 2014
 *      Author: yogi
 */

#ifndef TabBeranda_H_
#define TabBeranda_H_


#include "TabField.h"
#include <bb/cascades/Container>
#include <bb/cascades/CustomControl>

using namespace bb::cascades;

class TabBeranda : public TabField {
	Q_OBJECT
public:
	TabBeranda(QString title);
	virtual ~TabBeranda();
};

#endif /* TabBeranda_H_ */

