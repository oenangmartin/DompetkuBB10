/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "applicationui.hpp"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/LocaleHandler>
#include "LoginViewController.h"
#include "MerchantItem.h"
#include "FavoritItem.h"
#include "InboxItem.h"
#include "UiUtils.h"
#include "RegisterViewController.h"
#include "WebImage.h"
#include "TabController.h"
#include "VerifikasiNoViewController.h"

using namespace bb::cascades;

NavigationPane* ApplicationUI::nav;
AccountManager* ApplicationUI::accManager;
FavoritController* ApplicationUI::favoritController;
InboxController* ApplicationUI::inboxController;
InboxViewController* ApplicationUI::inboxViewController;
FavoritViewController* ApplicationUI::favoritViewController;
HomeViewController* ApplicationUI::homeViewController;

ApplicationUI::ApplicationUI(bb::cascades::Application *app) :
		QObject(app) {

	qmlRegisterType<MerchantItem>("com.dompetku.MerchantItem", 1, 0,
			"MerchantItem");
	qmlRegisterType<FavoritItem>("com.dompetku.FavoritItem", 1, 0,
			"FavoritItem");
	qmlRegisterType<InboxItem>("com.dompetku.InboxItem", 1, 0,
			"InboxItem");
	qmlRegisterType<WebImage>("com.dompetku.WebImage", 1, 0, "WebImage");

	accManager = NULL;
	// prepare the localization
	m_pTranslator = new QTranslator(this);
	m_pLocaleHandler = new LocaleHandler(this);

	bool res = QObject::connect(m_pLocaleHandler,
			SIGNAL(systemLanguageChanged()), this,
			SLOT(onSystemLanguageChanged()));
	// This is only available in Debug builds
	Q_ASSERT(res);
	// Since the variable is not used in the app, this is added to avoid a
	// compiler warning
	Q_UNUSED(res);

	// initial load
	onSystemLanguageChanged();

	// Create scene document from main.qml asset, the parent is set
	// to ensure the document gets destroyed properly at shut down.
	QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);

	// Create root object for the UI
	nav = qml->createRootObject<NavigationPane>();
	// Set created root object as the application scene
	app->setScene(nav);
	nav->setBackButtonsVisible(false);
	nav->setPeekEnabled(false);

	QSettings settings;

	if (settings.value(SHOW_OTP).isNull())
	{
		VerifikasiNoViewController *verifikasiNo = new VerifikasiNoViewController;
		nav->push(verifikasiNo);
		//settings.setValue(SHOW_OTP, QVariant("1"));
	}
	else
	{
		LoginViewController *loginView = new LoginViewController;
		nav->push(loginView);
	}


	//nav->push(new TabController);

	/*ApplicationUI::accManager = AccountManager::getInstance();
	AccountManager *accManager = ApplicationUI::accManager;

	accManager->getUser()->setPin("123456");
	accManager->getUser()->setNoTlp("0816775009");
	accManager->getUser()->setUserId("mobileapps_elasitas");
	homeViewController = new HomeViewController;
	nav->push(homeViewController);*/
	//nav->push(new TabController);

	/*if(accManager->hasExisting())
	 {
	 accManager->load();
	 qDebug() << ">>user: " << accManager->getUser()->getNoTlp();
	 nav->push(new HomeViewController);
	 }
	 else
	 {
	 LoginViewController *loginView = new LoginViewController;
	 nav->push(loginView);
	 }*/

	inboxController = new InboxController;
	inboxController->createTable();

	favoritController = new FavoritController;
	favoritController->createFavoritTable();

}

AccountManager* ApplicationUI::getAccManager() {
	if (accManager == NULL)
		accManager = new AccountManager;
	return accManager;
}

void ApplicationUI::showBerandaPage(QString message){
	while(nav->count() > 1)
	{
		nav->remove(nav->top());
	}
	UiUtils::alert(message, homeViewController);
}
void ApplicationUI::showBerandaPage(){
	while(nav->count() > 1)
	{
		nav->remove(nav->top());
	}
}
void ApplicationUI::popAllPage(){
	while(nav->count() > 0)
	{
		nav->remove(nav->top());
	}
}

void ApplicationUI::onSystemLanguageChanged() {
	QCoreApplication::instance()->removeTranslator(m_pTranslator);
	// Initiate, load and install the application translation files.
	QString locale_string = QLocale().name();
	QString file_name = QString("dompetku_%1").arg(locale_string);
	if (m_pTranslator->load(file_name, "app/native/qm")) {
		QCoreApplication::instance()->installTranslator(m_pTranslator);
	}
}
