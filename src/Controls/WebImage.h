/*
 * WebImage.h
 *
 *  Created on: Feb 7, 2013
 *      Author: ASUS
 */

#ifndef WEBIMAGE_H_
#define WEBIMAGE_H_

#include <bb/cascades/CustomControl>
#include <bb/cascades/ImageView>
#include <bb/cascades/StackLayout>
#include <bb/cascades/DockLayout>
#include <bb/cascades/Container>
#include "ImageRetriever.h"
#include "Global.h"

class WebImage: public bb::cascades::CustomControl {
	Q_OBJECT
	Q_PROPERTY(QString imagePath WRITE setImagePath NOTIFY imagePathChanged)
	Q_PROPERTY(QString borderNormal WRITE setBorderNormal)
	Q_PROPERTY(float width WRITE setImageWidth)
	Q_PROPERTY(float height WRITE setImageHeight)
	Q_PROPERTY(QString noImage WRITE setNoImage)
	Q_PROPERTY(QString cache WRITE setCache)
	Q_PROPERTY(bool noPadding WRITE setNoPadding)
	Q_PROPERTY(bool loadEffect WRITE setLoadEffect)
	Q_PROPERTY(bool placeholder WRITE setPlaceholder)
public:
	WebImage();
	void setImagePath(QString imagePath);
	void setImageWidth(float w);
	void setImageHeight(float h);
	void setBorder(QString border, QString borderPressed);
	void setBorderNormal(QString border);
	void setNoImage(QString noImage);
	void setCache(QString cache);
	void showBorder(bool useborder);
	void setNoPadding(bool padding);
	void setLoadEffect(bool effect);
	void setPlaceholder(bool placeholder);
	bool isImageLoaded();
	int tag;
	virtual ~WebImage();

private slots:
	void onImageLoaded(QString imgpath);
	void onTouch(bb::cascades::TouchEvent *);

	signals:
	void imageLoaded(QString imgpath);
	void imagePathChanged(QString imgpath);
	void onClicked();
	void onClicked(int);

private:
	ImageRetriever *imageRetriever;
	QString currImgPath;
	ImageView *imgView;
	ImageView *border;
	bool _imageLoaded;
	Container *contentContainer;
	Container *photoCont;
	QString borderNormal;
	QString borderPressed;
	QString noImage;
	QString cachePath;
};

#endif /* WEBIMAGE_H_ */
