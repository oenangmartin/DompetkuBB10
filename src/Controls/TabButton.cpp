/*
 * TabButton.cpp
 *
 *  Created on: Jan 17, 2013
 *      Author: ASUS
 */

#include "TabButton.h"

TabButton::TabButton() {
	QmlDocument *qml =
			QmlDocument::create("asset:///TabButton.qml").parent(
					this);

	borderNormal = "asset:///images/border/menubox_off.amd";
	borderSelection = "asset:///images/border/menubox_on.amd";

	root = qml->createRootObject<Container>();

	labelText = root->findChild<Label*>("text");
	border = root->findChild<ImageView*>("border");
	icon = root->findChild<ImageView*>("icon");

	selected = false;
	setRoot(root);

	connect(this, SIGNAL(touch(bb::cascades::TouchEvent *)), this, SLOT(onClicked(bb::cascades::TouchEvent *)));

}

void TabButton::setTextColor(const bb::cascades::ColorPaint &paint){
	labelText->textStyle()->setColor(paint);
}

void TabButton::setBorder(QString borderNormal, QString borderSelection){
	this->borderNormal = borderNormal;
	this->borderSelection = borderSelection;
	border->setImageSource(QUrl(borderNormal));
}

void TabButton::setPadding (int padding){
	Container *contentContainer = root->findChild<Container*>("contentContainer");
	contentContainer->setTopPadding(padding);
	contentContainer->setBottomPadding(padding);
}

void TabButton::setText(QString text){
	this->text = text;
	Label *labelName = root->findChild<Label*>("text");
	labelName->setText(text);
	labelName->setTopMargin(20);
}

QString TabButton::getText(){
	return text;
}

bool TabButton::isSelected() const {
	return selected;
}

void TabButton::setSelected(bool selected) {
	this->selected = selected;
	if(selected){
		border->setImageSource(QUrl(borderSelection));
		icon->setImageSource(QUrl(selectedIconUrl));
	}
	else{
		border->setImageSource(QUrl(borderNormal));
		icon->setImageSource(QUrl(iconUrl));
	}
}

const QString& TabButton::getSelectedIconUrl() const {
	return selectedIconUrl;
}

void TabButton::setSelectedIconUrl(const QString& selectedIconUrl) {
	this->selectedIconUrl = selectedIconUrl;
}

const QString& TabButton::getIconUrl() const {
	return iconUrl;
}

void TabButton::setIconUrl(const QString& iconUrl) {
	this->iconUrl = iconUrl;
	icon->setImageSource(QUrl(iconUrl));
}

void TabButton::onClicked(bb::cascades::TouchEvent *touchEvent){
	if (touchEvent->touchType() == TouchType::Up ){
		border->setImageSource(QUrl(borderSelection));
		emit clicked(text);
	}
	else if (touchEvent->touchType() == TouchType::Down){
		if(selected) border->setImageSource(QUrl(borderSelection));
		else border->setImageSource(QUrl(borderNormal));
	}
	else if (touchEvent->touchType() == TouchType::Cancel){
		if(selected) border->setImageSource(QUrl(borderSelection));
				else border->setImageSource(QUrl(borderNormal));
	}
}


TabButton::~TabButton() {
	// TODO Auto-generated destructor stub
}

