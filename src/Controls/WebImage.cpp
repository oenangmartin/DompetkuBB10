/*
 * WebImage.cpp
 *
 *  Created on: Feb 7, 2013
 *      Author: ASUS
 */

#include "WebImage.h"
#include "Global.h"

WebImage::WebImage() {
	tag = -1;
	cachePath = QDir::homePath() + "/cache/";
	if (!QDir(cachePath).exists()) {
		if (!QDir().mkdir(cachePath))
			qDebug() << "failed create cache folder";
	}

	//noImage = "asset:///images/noBitmap360.png";
	noImage = "";
	currImgPath = "";
	_imageLoaded = false;
	//borderNormal = Global::BORDER_TRANSPARANT;
	//borderPressed = Global::BORDER_PHOTO_PRESS;
	borderNormal = "";
	borderPressed = "";

	border = ImageView::create().imageSource(QUrl(borderNormal));
	border->setParent(this);
	border->setVerticalAlignment(VerticalAlignment::Fill);
	border->setHorizontalAlignment(HorizontalAlignment::Fill);

	contentContainer = new Container();
	contentContainer->setParent(this);
	contentContainer->setLayout(DockLayout::create());
	contentContainer->setTopPadding(2);
	contentContainer->setBottomPadding(2);
	contentContainer->setRightPadding(2);
	contentContainer->setLeftPadding(2);
	//contentContainer->add(border);

	photoCont = Container::create().layout(DockLayout::create());
	photoCont->setParent(this);
	photoCont->setVerticalAlignment(VerticalAlignment::Center);
	photoCont->setHorizontalAlignment(HorizontalAlignment::Center);
	/* photoCont->setPreferredHeight(160);
	 photoCont->setPreferredWidth(160);*/

	imgView = ImageView::create();
	imgView->setParent(this);
	imgView->setVerticalAlignment(VerticalAlignment::Fill);
	imgView->setHorizontalAlignment(HorizontalAlignment::Fill);
	imgView->setScalingMethod(ScalingMethod::AspectFit);

	photoCont->add(imgView);
	contentContainer->add(photoCont);

	setRoot(contentContainer);

	imageRetriever = new ImageRetriever;
	imageRetriever->setParent(this);
	connect(imageRetriever, SIGNAL(imageLoaded(QString)), this,
			SLOT(onImageLoaded(QString)));

	connect(this, SIGNAL(touch(bb::cascades::TouchEvent *)), this,
			SLOT(onTouch(bb::cascades::TouchEvent *)));
}

void WebImage::setImageHeight(float h) {
	imgView->setPreferredHeight(h);
	photoCont->setPreferredHeight(h);
}

void WebImage::setImageWidth(float w) {
	imgView->setPreferredWidth(w);
	//imgView->setPreferredHeight(w);
	photoCont->setPreferredWidth(w);
	//photoCont->setPreferredHeight(w);
}

void WebImage::setBorder(QString _border, QString _borderPressed) {
	if (_border == NULL) {
		contentContainer->removeAll();
		contentContainer->add(imgView);
		borderNormal.clear();
		borderPressed.clear();
	} else {
		borderNormal = _border;
		borderPressed = _borderPressed;
		border->setImageSource(borderNormal);
	}
}

void WebImage::setBorderNormal(QString _border) {
	borderNormal = _border;
	border->setImageSource(borderNormal);
}

void WebImage::setCache(QString cache) {
	if(cache.compare("sticker") == 0){
		cachePath = "";
		imageRetriever->setCachePath(cachePath);
		qDebug() << ">>cache path: " << "";
	}
}

void WebImage::onImageLoaded(QString imgpath) {
	//qDebug() << ">>image load: " << imgpath;
	imgView->setVisible(true);
    if(imgpath == NULL) return;
    _imageLoaded = true;
	QString ext = ".JPG";
	//if (imgpath.indexOf(".PNG", Qt::CaseInsensitive) > 0) {
	if (imgpath.right(4).compare(".png") == 0 || imgpath.right(4).compare(".PNG") == 0 ){
		ext = ".PNG";
	}
	QString diskPath = cachePath + QString::number(qHash(currImgPath)) + ext;

	//qDebug() << ">>diskpath: " << diskPath;

	if (diskPath == imgpath) {
		imgView->setImageSource(QUrl("file://" + imgpath));
		emit imageLoaded(imgpath);
	}


}

void WebImage::setImagePath(QString imagePath) {
	//if (noImage.length() > 0 && currImgPath.length() > 0)
	if (noImage.size() > 0)
			imgView->setImageSource(QUrl(noImage));
	else
		imgView->setVisible(false);

	currImgPath = imagePath;
	_imageLoaded = false;
	imageRetriever->setImagePath(imagePath);
}

void WebImage::setNoImage(QString _noImage) {
	noImage = _noImage;
	imgView->setImageSource(QUrl(noImage));
}

void WebImage::onTouch(bb::cascades::TouchEvent *touchEvent) {
	if (touchEvent->touchType() == TouchType::Up) {
		//if (border && !borderNormal.isNull())border->setImageSource(QUrl(borderNormal));
		imgView->setOpacity(1);
		if (tag > -1)
			emit onClicked(tag);
		else
			emit onClicked();
	} else if (touchEvent->touchType() == TouchType::Down) {
		imgView->setOpacity(0.25);
		//if (border && !borderPressed.isNull()) border->setImageSource(QUrl(borderPressed));
	} else if (touchEvent->touchType() == TouchType::Cancel) {
		imgView->setOpacity(1);
		//if (border && !borderNormal.isNull()) border->setImageSource(QUrl(borderNormal));
	}
}

void WebImage::showBorder(bool visible){
	//border->setVisible(visible);
}

void WebImage::setPlaceholder(bool placeholder){
	//if(placeholder) photoCont->setBackground(Color::LightGray);
	//else photoCont->resetBackground();
}
void WebImage::setLoadEffect(bool effect){
	if(!effect) imgView->setLoadEffect(ImageViewLoadEffect::None);
	else imgView->setLoadEffect(ImageViewLoadEffect::Default);
}

void WebImage::setNoPadding(bool padding){
	if(!padding){
		photoCont->setTopPadding(0);
			photoCont->setBottomPadding(0);
			photoCont->setRightPadding(0);
			photoCont->setLeftPadding(0);
	}

}

bool WebImage::isImageLoaded(){
	return _imageLoaded;
}

WebImage::~WebImage() {
	// TODO Auto-generated destructor stub
}

