/*
 * TabButton.h
 *
 *  Created on: Jan 17, 2013
 *      Author: ASUS
 */

#ifndef TabButton_H_
#define TabButton_H_


#include <bb/cascades/CustomControl>
#include <bb/cascades/controls/container.h>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/Container>
#include <bb/cascades/Button>
#include <bb/cascades/Label>
#include <bb/cascades/ImageView>

using namespace bb::cascades;

class TabButton : public bb::cascades::CustomControl {
	Q_OBJECT
public:
	TabButton();
	virtual ~TabButton();
	void setText(QString text);
	void setPadding (int padding);
	QString getText();

	void setTextColor(const bb::cascades::ColorPaint &paint);
	void setBorder(QString border, QString borderSelection);
	bool isSelected() const;
	void setSelected(bool selected);
	const QString& getSelectedIconUrl() const;
	void setSelectedIconUrl(const QString& selectedIconUrl);
	const QString& getIconUrl() const;
	void setIconUrl(const QString& iconUrl);

private slots:
	void onClicked(bb::cascades::TouchEvent *pTouchEvent);

private:
	Container *root;
	ImageView *border;
	ImageView *icon;
	Label *labelText;
	QString text;
	QString borderNormal;
	QString borderSelection;
	QString iconUrl;
	QString selectedIconUrl;

	bool selected;


	signals:
	void clicked(QString text);
};

#endif /* TabButton_H_ */
