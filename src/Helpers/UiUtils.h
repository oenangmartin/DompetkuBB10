/*
 * UiUtils.h
 *
 *  Created on: Sep 11, 2014
 *      Author: yogi
 */

#ifndef UiUtils_H_
#define UiUtils_H_

#include <bb/device/DisplayInfo>
#include <bb/cascades/QmlDocument>
#include <bb/system/SystemToast>
#include <bb/system/SystemDialog>
#include <bb/cascades/TextStyleDefinition>

using namespace bb::device;
using namespace bb::cascades;
using namespace bb::system;

class UiUtils : public QObject{
Q_OBJECT
public:
	UiUtils();

	static QmlDocument* LoadQml(QString qmlName, QObject *parent);
	static QmlDocument* LoadQml(QString qmlName, QObject *parent, QVariantList variableMap);
	static bool IsNeedToSetASMainScroll();
	static int DisplayW;
	static int DisplayH;
	static QVariant getDefaultFontStyle();
	static QString AddThousandSeparator(QString text);
	static void toast(QString text, QObject *parent);
	static void alert(const QString &message, QObject *parent);
	virtual ~UiUtils();
};

#endif /* UiUtils_H_ */
