/*
 * InboxController.cpp
 *
 *  Created on: Dec 25, 2014
 *      Author: yogi
 */

#include "InboxController.h"
#include "UiUtils.h"

InboxController::InboxController() {
	// TODO Auto-generated constructor stub

}

//refId, tanggal, action, merchant, tujuan, jumlah, token, expired
//refId TEXT, tanggal TEXT, action TEXT, merchant TEXT, tujuan TEXT, jumlah INTEGER, token TEXT, expired TEXT
bool InboxController::createTable()
{
    const QString query("CREATE TABLE IF NOT EXISTS inbox (seqnum INTEGER PRIMARY KEY AUTOINCREMENT, refId TEXT, tanggal TEXT, action TEXT, merchant TEXT, tujuan TEXT, jumlah TEXT, token TEXT, expired TEXT);");

    // Execute the query.
    QSqlQuery sqlQuery(query, SQLConnection());

    const QSqlError err = sqlQuery.lastError();

    if (err.isValid()) {
        qWarning() << "Error executing SQL statement: " << query << ". ERROR: " << err.text();
        return false;
    }

    return true;
}

int InboxController::add(const InboxData &data)
{
    int insertId = -1;
    QSqlQuery sqlQuery(SQLConnection());

    const QString query("INSERT INTO inbox (refId, tanggal, action, merchant, tujuan, jumlah, token, expired) "
                        "VALUES(:refId, :tanggal, :action, :merchant, :tujuan, :jumlah, :token, :expired)");

    sqlQuery.prepare(query);

    sqlQuery.bindValue(":refId", data.getRefId());
    sqlQuery.bindValue(":tanggal",data.getTanggal());
    sqlQuery.bindValue(":action", data.getAction());
    sqlQuery.bindValue(":merchant", data.getMerchant());
    sqlQuery.bindValue(":tujuan", data.getTujuan());
    sqlQuery.bindValue(":jumlah", data.getJumlah());
    sqlQuery.bindValue(":token", data.getToken());
    sqlQuery.bindValue(":expired", data.getExpired());
    sqlQuery.exec();

    const QSqlError err = sqlQuery.lastError();

    if (err.isValid()) {
        qWarning() << "Error executing SQL statement: " << query << ". ERROR: " << err.text();
    } else {
        if (sqlQuery.lastInsertId().isValid()) {
            insertId = sqlQuery.lastInsertId().toInt();
        }
    }

    return insertId;
}

bool InboxController::remove(int pushSeqNum)
{

	deleteId = pushSeqNum;

	prompt = new SystemDialog("Yes","No");

	prompt->setTitle("Hapus");
	prompt->setBody("Hapus dari Inbox?");

	bool success = QObject::connect(prompt,
	  SIGNAL(finished(bb::system::SystemUiResult::Type)),
	  this,
	  SLOT(onPromptFinished(bb::system::SystemUiResult::Type)));

	if (success) {

		prompt->show();

	} else {
		prompt->deleteLater();
	}

    return true;
}

void InboxController::onPromptFinished(
    bb::system::SystemUiResult::Type type)
{
    if (type == SystemUiResult::ConfirmButtonSelection)
    {
		QSqlQuery sqlQuery(SQLConnection());

		const QString query("DELETE FROM inbox WHERE seqnum = :seqNum;");

		sqlQuery.prepare(query);

		sqlQuery.bindValue(":seqNum", deleteId);
		sqlQuery.exec();

		const QSqlError err = sqlQuery.lastError();

		if (err.isValid()) {
			qWarning() << "Error executing SQL statement: " << query << ". ERROR: " << err.text();
		}

		emit inboxDeleted(deleteId);
    }
    else {
        qDebug() << "Prompt Rejected";
    }
}

bool InboxController::removeAll()
{
    const QString query("DROP TABLE inbox;");

    // Execute the query.
    QSqlQuery sqlQuery(query, SQLConnection());

    const QSqlError err = sqlQuery.lastError();

    if (err.isValid()) {
        qWarning() << "Error executing SQL statement: " << query << ". ERROR: " << err.text();
        return false;
    }

    return true;
}


QVariantList InboxController::getInboxList()
{
    QVariantList data;

    const QString query("SELECT seqnum, refId, tanggal, action, merchant, tujuan, jumlah, token, expired FROM inbox ORDER BY seqnum DESC;");

    QSqlQuery sqlQuery(query, SQLConnection());

    const QSqlError err = sqlQuery.lastError();

    if (err.isValid()) {
        qWarning() << "Error executing SQL statement: " << query << ". ERROR: " << err.text();
    } else {
        while (sqlQuery.next()){
            data.append(retrieveInboxData(sqlQuery).toMap());
        }
    }

    return data;
}

InboxData InboxController::retrieveInboxData(const QSqlQuery& sqlQuery)
{
	//refId, tanggal, action, merchant, tujuan, jumlah, token, expired
	InboxData inboxData;
	inboxData.setId(sqlQuery.value(0).toInt());
	inboxData.setRefId(sqlQuery.value(1).toString());
	inboxData.setTanggal(sqlQuery.value(2).toString());
	inboxData.setAction(sqlQuery.value(3).toString());
	inboxData.setMerchant(sqlQuery.value(4).toString());
	inboxData.setTujuan(sqlQuery.value(5).toString());
	inboxData.setJumlah(sqlQuery.value(6).toString());
	inboxData.setToken(sqlQuery.value(7).toString());
	inboxData.setExpired(sqlQuery.value(8).toString());
    return inboxData;
}


InboxController::~InboxController() {
	// TODO Auto-generated destructor stub
}

