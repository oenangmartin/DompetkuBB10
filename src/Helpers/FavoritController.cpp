/*
 * FavoritController.cpp
 *
 *  Created on: Dec 25, 2014
 *      Author: yogi
 */

#include "FavoritController.h"
#include "UiUtils.h"

FavoritController::FavoritController() {
	// TODO Auto-generated constructor stub

}

void FavoritController::addToFavorit(FavoritData *favoritData, QString defaultLabel){
	tempFavoritData = favoritData;
	prompt = new SystemPrompt();
	prompt->setTitle("Masukan nama favorit:");
	prompt->inputField()->setDefaultText(defaultLabel);
	prompt->setDismissAutomatically(true);

	// Connect the finished() signal
	// to the onPromptFinished() slot.

	bool success = QObject::connect(prompt,
	  SIGNAL(finished(bb::system::SystemUiResult::Type)),
	  this,
	  SLOT(onPromptFinished(bb::system::SystemUiResult::Type)));

	if (success) {

		prompt->show();

	} else {
		prompt->deleteLater();
	}
}

bool FavoritController::createFavoritTable()
{
	/*
	 * QString nama;
	QString value;
	QString typeLabel;
	int type;
	QVariantMap qmap, int formType, QString namaItem
	 */
    const QString query("CREATE TABLE IF NOT EXISTS favorit (seqnum INTEGER PRIMARY KEY AUTOINCREMENT, nama TEXT, number TEXT, typeLabel TEXT, type INTEGER, jsonData TEXT, namaItem TEXT);");

    // Execute the query.
    QSqlQuery sqlQuery(query, SQLConnection());

    const QSqlError err = sqlQuery.lastError();

    if (err.isValid()) {
        qWarning() << "Error executing SQL statement: " << query << ". ERROR: " << err.text();
        return false;
    }

    return true;
}

int FavoritController::add(const FavoritData &data)
{
    int insertId = -1;
    QSqlQuery sqlQuery(SQLConnection());

    const QString query("INSERT INTO favorit (nama, number, typeLabel, type, jsonData, namaItem) "
                        "VALUES(:nama, :number, :typeLabel, :type, :jsonData, :namaItem)");

    sqlQuery.prepare(query);

    sqlQuery.bindValue(":nama", data.getNama());
    sqlQuery.bindValue(":number", data.getValue());
    sqlQuery.bindValue(":typeLabel",data.getTypeLabel());
    sqlQuery.bindValue(":type", data.getType());
    sqlQuery.bindValue(":jsonData", data.getJsonData());
    sqlQuery.bindValue(":namaItem", data.getNamaItem());
    sqlQuery.exec();

    const QSqlError err = sqlQuery.lastError();

    if (err.isValid()) {
        qWarning() << "Error executing SQL statement: " << query << ". ERROR: " << err.text();
    } else {
        if (sqlQuery.lastInsertId().isValid()) {
            insertId = sqlQuery.lastInsertId().toInt();
        }
    }

    return insertId;
}

bool FavoritController::remove(int pushSeqNum)
{
    QSqlQuery sqlQuery(SQLConnection());

    const QString query("DELETE FROM favorit WHERE seqnum = :seqNum;");

    sqlQuery.prepare(query);

    sqlQuery.bindValue(":seqNum", pushSeqNum);
    sqlQuery.exec();

    const QSqlError err = sqlQuery.lastError();

    if (err.isValid()) {
        qWarning() << "Error executing SQL statement: " << query << ". ERROR: " << err.text();
        return false;
    }

    return true;
}

bool FavoritController::removeAll()
{
    const QString query("DROP TABLE favorit;");

    // Execute the query.
    QSqlQuery sqlQuery(query, SQLConnection());

    const QSqlError err = sqlQuery.lastError();

    if (err.isValid()) {
        qWarning() << "Error executing SQL statement: " << query << ". ERROR: " << err.text();
        return false;
    }

    return true;
}


QVariantList FavoritController::getFavoritList()
{
    QVariantList data;

    const QString query("SELECT seqnum, nama, number, typeLabel, type, jsonData, namaItem FROM favorit ORDER BY seqnum;");

    QSqlQuery sqlQuery(query, SQLConnection());

    const QSqlError err = sqlQuery.lastError();

    if (err.isValid()) {
        qWarning() << "Error executing SQL statement: " << query << ". ERROR: " << err.text();
    } else {
        while (sqlQuery.next()){
            data.append(retrieveFavoritData(sqlQuery).toMap());
        }
    }

    return data;
}

FavoritData FavoritController::retrieveFavoritData(const QSqlQuery& sqlQuery)
{
	FavoritData favoritData;
	favoritData.setId(sqlQuery.value(0).toInt());
	favoritData.setNama(sqlQuery.value(1).toString());
	favoritData.setValue(sqlQuery.value(2).toString());
	favoritData.setTypeLabel(sqlQuery.value(3).toString());
	favoritData.setType(sqlQuery.value(4).toInt());
	favoritData.setJsonData(sqlQuery.value(5).toString());
	favoritData.setNamaItem(sqlQuery.value(6).toString());
    return favoritData;
}

void FavoritController::onPromptFinished(
    bb::system::SystemUiResult::Type type)
{
    if (type == SystemUiResult::ConfirmButtonSelection)
    {
        qDebug() << "Prompt Accepted:" << prompt->inputFieldTextEntry();
        // The user accepted the prompt.
        tempFavoritData->setNama(prompt->inputFieldTextEntry());
        if(add(*tempFavoritData) > -1) UiUtils::alert("Berhasil menambahkan ke favorit.", this);
    }
    else {
        qDebug() << "Prompt Rejected";
        // The user rejected the prompt.
    }
}

FavoritController::~FavoritController() {
	// TODO Auto-generated destructor stub
}

