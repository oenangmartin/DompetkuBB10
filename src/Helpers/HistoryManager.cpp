/*
 * HistoryManager.cpp
 *
 *  Created on: Nov 30, 2014
 *      Author: yogi
 */

#include "HistoryManager.h"
#include "Global.h"
#include "AccountManager.h"
#include "applicationui.hpp"
#include "UiUtils.h"

HistoryManager* HistoryManager::instance = NULL;

HistoryManager::HistoryManager() {
	httpRequest = new HttpRequest(this);
		connect(httpRequest, SIGNAL(httpReply(QByteArray)), this,
				SLOT(httpReply(QByteArray)));

}

HistoryManager* HistoryManager::getInstance(){
	if(instance == NULL) instance = new HistoryManager;
	return instance;
}

void HistoryManager::getHistory(QString count){
	AccountManager *accManager = ApplicationUI::accManager;
	/*QString url = API_HISTORY_TRANSACTION + "?signature=" +QUrl::toPercentEncoding(accManager->getSignature()) + "&userid=" + accManager->getUser()->getUserId() + "&to=" + accManager->getUser()->getNoTlp() + "&count=" + count;
	qDebug() << ">>url: " << url;
	httpRequest->get(url);*/

	QUrl postData;
		postData.addQueryItem("userid", accManager->getUser()->getUserId());
		postData.addQueryItem("signature", accManager->getSignature());
		postData.addQueryItem("to", accManager->getUser()->getNoTlp());
		postData.addQueryItem("count", count);
		httpRequest->post(API_HISTORY_TRANSACTION, postData);
}


void HistoryManager::httpReply(QByteArray data) {
	if (!data.isNull()) {

		QString dataStr(data);
		qDebug() << ">>data: " << dataStr;

		JsonDataAccess jda;
		QVariantMap qmap = jda.loadFromBuffer(data).value<QVariantMap>();
		emit historyLoaded(qmap);

	}
	else {
		qDebug() << ">>data HistoryManager null ";
		UiUtils::toast(WORDING_KONEKSI_GAGAL, this);
		emit failedLoadHistory();
	}
}

HistoryManager::~HistoryManager() {
	// TODO Auto-generated destructor stub
}

