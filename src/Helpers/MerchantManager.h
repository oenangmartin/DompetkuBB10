/*
 * MerchantManager.h
 *
 *  Created on: Nov 8, 2014
 *      Author: yogi
 */

#ifndef MERCHANTMANAGER_H_
#define MERCHANTMANAGER_H_

#include <bb/cascades/QmlDocument>
#include "HttpRequest.h"

#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>

using namespace bb::data;

class MerchantManager : public QObject{
	Q_OBJECT
public:
	MerchantManager();
	virtual ~MerchantManager();

	static MerchantManager* getInstance();
	void getMerchantList();
	const QVariantList& getBayarList() const;
	const QVariantList& getBeliList() const;
	bool isDataLoaded();

private:
	static MerchantManager *instance;
	HttpRequest *httpRequest;
	QVariantList beliList;
	QVariantList bayarList;
	bool _isDataLoaded;

private slots:
	void httpReply(QByteArray data);


signals:
	void dataLoaded(bool success);

};

#endif /* MERCHANTMANAGER_H_ */
