/*
 * HttpRequest.cpp
 *
 *  Created on: Feb 25, 2013
 *      Author: ASUS
 */

#include "HttpRequest.h"


HttpRequest::HttpRequest(QObject *parent) {
	setParent(parent);
	mNetworkAccessManager = new QNetworkAccessManager(this);
	request = QNetworkRequest();
	request.setAttribute( QNetworkRequest::CacheLoadControlAttribute, QNetworkRequest::AlwaysNetwork );
	request.setAttribute( QNetworkRequest::CacheSaveControlAttribute, false );
	connect(mNetworkAccessManager,
			SIGNAL(finished(QNetworkReply*)), this,
			SLOT(requestFinished(QNetworkReply*)));


	/*
	 connect(httpRequest,
				SIGNAL(httpReply(QByteArray )), this,
				SLOT(httpReply(QByteArray)));
	 */
}

void HttpRequest::requestFinished(QNetworkReply* reply) {
	QVariant statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
	if (statusCode.isValid()) {
		int status = statusCode.toInt();
		if(status != 200)
		{
			qDebug() << ">>req finish: " << QString::number(status);
			//emit httpReply(NULL);
			//return;
		}
	}
	if (reply->error() == QNetworkReply::NoError) {
		QByteArray data = reply->readAll();
		emit httpReply(data);
	}
	else{
		emit httpReply(NULL);
	}
}

void HttpRequest::get(QString url) {
	QUrl qUrl;
	qUrl.setEncodedUrl(url.toUtf8());


	request.setUrl(qUrl);

	//request.setUrl(QUrl(url));
	mNetworkAccessManager->get(request);

}

/*!
	  @param url : url api
	  @param postData: QUrl postData;
		postData.addQueryItem("param1", "data1");
		postData.addQueryItem("param2", "data2");
	 */
void HttpRequest::post(QString url, QUrl postData) {
	request.setUrl(url);
	request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");

	mNetworkAccessManager->post(request, postData.encodedQuery());
}


HttpRequest::~HttpRequest() {
	// TODO Auto-generated destructor stub
}

