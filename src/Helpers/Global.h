/*
 * Global.h
 *
 *  Created on: Nov 8, 2014
 *      Author: yogi
 */

#ifndef GLOBAL_H_
#define GLOBAL_H_

QT_BEGIN_NAMESPACE
class QString;
QT_END_NAMESPACE

#define TYPE_BELI 1
#define TYPE_BAYAR 2


extern qreal g_some_double;
extern QString USER_ID ;
extern QString LAST_NO_AKUN ;
extern QString FIRST_OPEN_APPS ;
extern QString SHOW_OTP ;
extern QString API_ROOT ;
extern QString API_BILLER_ROOT ;

extern QString API_MERCHANT;
extern QString API_INFORMASI_MERCHANT;
extern QString API_FIELD;
extern QString API_LOGIN;
extern QString API_REGISTER;
extern QString API_GET_BALANCE;
extern QString API_HISTORY_TRANSACTION;
extern QString API_GET_TOKEN;
extern QString API_PROMO;
extern QString API_CHANGE_PIN;
extern QString API_MONEY_TRANSFER;
extern QString API_BANK_TRANSFER;
extern QString API_TRANSFER_OP_LAIN;
extern QString API_TRANSFER_DOMPETKU;

extern QString API_OTP;

//wording
extern QString WORDING_KONEKSI_GAGAL;



#endif /* GLOBAL_H_ */
