/*
 * PromoManager.h
 *
 *  Created on: Nov 8, 2014
 *      Author: yogi
 */

#ifndef PromoManager_H_
#define PromoManager_H_

#include <bb/cascades/QmlDocument>
#include "HttpRequest.h"

#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>

using namespace bb::data;

class PromoManager : public QObject{
	Q_OBJECT
public:
	PromoManager();
	virtual ~PromoManager();

	static PromoManager* getInstance();
	void getPromo();

private:
	static PromoManager *instance;
	HttpRequest *httpRequest;

private slots:
	void httpReply(QByteArray data);


signals:
	void promoLoaded(bool success, QVariantList promoList);

};

#endif /* PromoManager_H_ */
