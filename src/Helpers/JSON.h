/*
 * JSON.h
 *
 *  Created on: Jan 11, 2013
 *      Author: ASUS
 */

#ifndef JSON_H_
#define JSON_H_

#include <bb/cascades/QmlDocument>

using namespace bb::cascades;

struct JSONData;
class JSON
{
public:
    static JSON& instance();
    JSON();
    virtual ~JSON();

    QVariantMap parse(const QString& string) const;
        QString serialize(const QVariant& value) const;
    //QScriptValue CreateValue(const QVariant& value, QScriptEngine& engine);

private:
    JSONData* d;
};

#endif /* JSON_H_ */
