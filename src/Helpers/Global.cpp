/*
 * Global.cpp
 *
 *  Created on: Nov 8, 2014
 *      Author: yogi
 */

#include "Global.h"

#include <QString>

// ALL THE GLOBAL DEFINITIONS

qreal g_some_double = 0.5;

//QString USER_ID = "web_api_test";
QString USER_ID = "mobile_apps_elasitas";
QString LAST_NO_AKUN = "last_no_akun";
QString FIRST_OPEN_APPS = "first_open_apps";
QString SHOW_OTP = "show_otp_live";

//http://114.4.68.110:14567/mfsmw/webapi/
QString API_ROOT = "https://mapi.dompetku.com/webapi/";

//http://fb.elasitas.com/jason/api_dompetku/api_c/
QString API_BILLER_ROOT = "https://mapi.dompetku.com:8300/";

// API BILLER
QString API_MERCHANT = API_BILLER_ROOT + "api_c/list_merchant/";
QString API_INFORMASI_MERCHANT = API_BILLER_ROOT + "api_c/list_listmerchant/";
QString API_FIELD = API_BILLER_ROOT + "api_c/list_field/";
QString API_PROMO = API_BILLER_ROOT + "api_c/list_promo/";
QString API_OTP = API_BILLER_ROOT + "otp/";

// API DOMPETKU
QString API_LOGIN = API_ROOT + "login";
QString API_REGISTER = API_ROOT + "register";
//QString API_GET_BALANCE = API_ROOT + "balance_check";
QString API_GET_BALANCE = API_ROOT + "user_inquiry";
QString API_HISTORY_TRANSACTION = API_ROOT + "history_transaction";
QString API_GET_TOKEN = API_ROOT + "create_coupon";
QString API_CHANGE_PIN = API_ROOT + "change_pin";
QString API_MONEY_TRANSFER = API_ROOT + "money_transfer";
QString API_BANK_TRANSFER = API_ROOT + "bank_transfer";
QString API_TRANSFER_OP_LAIN = API_ROOT + "p2p_transfer";
QString API_TRANSFER_DOMPETKU = API_ROOT + "money_transfer";


//wording
QString WORDING_KONEKSI_GAGAL = "Koneksi gagal. Cobalah beberapa saat lagi";

