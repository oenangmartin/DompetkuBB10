/*
 * MerchantManager.cpp
 *
 *  Created on: Nov 8, 2014
 *      Author: yogi
 */

#include "MerchantManager.h"
#include "Global.h"
#include "AccountManager.h"

MerchantManager* MerchantManager::instance = NULL;

MerchantManager::MerchantManager() {
	_isDataLoaded = false;
	httpRequest = new HttpRequest(this);
		connect(httpRequest, SIGNAL(httpReply(QByteArray)), this,
				SLOT(httpReply(QByteArray)));

}

MerchantManager* MerchantManager::getInstance(){
	if(instance == NULL) instance = new MerchantManager;
	return instance;
}

void MerchantManager::getMerchantList(){
	httpRequest->get(API_MERCHANT + AccountManager::getInstance()->getUser()->getToken());
}

const QVariantList& MerchantManager::getBayarList() const {
	return bayarList;
}

const QVariantList& MerchantManager::getBeliList() const {
	return beliList;
}

bool MerchantManager::isDataLoaded(){
	return _isDataLoaded;
}

void MerchantManager::httpReply(QByteArray data) {
	if (!data.isNull()) {

		QString dataStr(data);
		qDebug() << ">>data: " << dataStr;

		JsonDataAccess jda;
		QVariantMap qmap = jda.loadFromBuffer(data).value<QVariantMap>();

		beliList = qmap.value("Beli").toList();
		bayarList = qmap.value("Bayar").toList();

		_isDataLoaded = true;
		emit dataLoaded(true);

	}
}

MerchantManager::~MerchantManager() {
	// TODO Auto-generated destructor stub
}

