/*
 * HistoryManager.h
 *
 *  Created on: Nov 30, 2014
 *      Author: yogi
 */

#ifndef HistoryManager_H_
#define HistoryManager_H_

#include <bb/cascades/QmlDocument>
#include "HttpRequest.h"

#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>

using namespace bb::data;

class HistoryManager: public QObject {
Q_OBJECT
public:
	HistoryManager();
	virtual ~HistoryManager();

	static HistoryManager* getInstance();
	void getHistory(QString count);

private:
	static HistoryManager *instance;
	HttpRequest *httpRequest;

private slots:
	void httpReply(QByteArray data);

signals:
	void historyLoaded(QVariantMap qmap);
	void failedLoadHistory();
};

#endif /* HistoryManager_H_ */
