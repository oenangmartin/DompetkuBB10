/*
 * ApiEngine.h
 *
 *  Created on: Nov 23, 2014
 *      Author: yogi
 */

#ifndef APIENGINE_H_
#define APIENGINE_H_

#include <bb/cascades/QmlDocument>
#include "HttpRequest.h"

#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>

#include "Global.h"

using namespace bb::data;

class ApiEngine : public QObject{
	Q_OBJECT
public:
	ApiEngine();
	virtual ~ApiEngine();

	void getListField(QString id, QObject *parent, const char* slot);
};

#endif /* APIENGINE_H_ */
