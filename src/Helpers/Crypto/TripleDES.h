/*
 * TripleDES.h
 *
 *  Created on: Nov 30, 2014
 *      Author: yogi
 */

#ifndef TRIPLEDES_H_
#define TRIPLEDES_H_

#include <QObject>
#include "GlobalContext.hpp"
#include "SBError.hpp"
#include "TripleDESKey.h"
#include "TripleDESParams.h"
#include "hudes.h"
#include <sbreturn.h>
#include <hurandom.h>


#include <bb/cascades/QmlDocument>
using namespace bb::cascades;

class TripleDES : public QObject
{
    Q_OBJECT
public:
	TripleDES();
	virtual ~TripleDES();
	QString encrypt(QString plainText);
private:
	bool fromHex(const QString in, QByteArray & out);
	QString toHex(const QByteArray & in);

	void pad(QByteArray & in);
	bool removePadding(QByteArray & out);

	bool crypt(bool isEncrypt, const QByteArray & in, QByteArray & out);

	char nibble(char);

	    GlobalContext globalContext;

	    void toast(const QString & message);
	    void toast(const char * message);
};

#endif /* TRIPLEDES_H_ */
