/*
 * TripleDESParams.cpp
 *
 *  Created on: Nov 28, 2014
 *      Author: yogi
 */

#include "TripleDESParams.h"


TripleDESParams::TripleDESParams(GlobalContext & g) :
		Crypto("tripleDesParams"), _globalContext(g), _tripleDesParams(NULL) {

	int rc = hu_DESParamsCreate(SB_DES_TDES, SB_DES_ECB, SB_DES_PARITY_OFF, SB_DES_WEAK_KEY_OFF, NULL, NULL, &_tripleDesParams, _globalContext.ctx());
	maybeLog("tripleDesParamsCreate", rc);
	//a140e5bd0f91e0dd36617982a7e37d1b6a837dd5b5e298392fdc1663b5cf22f3117d1a2b20b7ce9b99eb68c5c6a25e20
	//852a38c85bbb6c30c540b84c8509e32d3dc58429c6347892b8e76a3caae55c2a63254e9b3862d976cd8b496dcf865877
}

TripleDESParams::~TripleDESParams() {
	if (_tripleDesParams != NULL) {
		// cowardly ignoring return code.
		int rc = hu_DESParamsDestroy(&_tripleDesParams, _globalContext.ctx());
		maybeLog("tripleDesParamsDestroy", rc);
		_tripleDesParams = NULL;
	}
}

//SB_DES_DES, SB_DES_TDES SB_DES_DESX
