/*
 * TripleDESKey.h
 *
 *  Created on: Nov 28, 2014
 *      Author: yogi
 */

#ifndef TRIPLEDESKEY_H_
#define TRIPLEDESKEY_H_


#include <QByteArray>
#include "hudes.h"

#include "TripleDESParams.h"

class TripleDESKey : public Crypto {
public:
	TripleDESKey(TripleDESParams & params, const QByteArray & value);
	virtual ~TripleDESKey();

	sb_Key desKey() {
		return _desKey;
	}

	TripleDESParams & params() {
		return _params;
	}

	virtual bool isValid() {
		return _desKey!=NULL;
	}

private:
	TripleDESParams & _params;
	sb_Key _desKey;
};

#endif /* TRIPLEDESKEY_H_ */
