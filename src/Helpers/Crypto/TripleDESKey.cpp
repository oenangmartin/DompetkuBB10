/*
 * TripleDESKey.cpp
 *
 *  Created on: Nov 28, 2014
 *      Author: yogi
 */

#include "TripleDESKey.h"
#include <sbreturn.h>

typedef unsigned char uint8_t;

TripleDESKey::TripleDESKey(TripleDESParams & p, const QByteArray & content) :
		Crypto("desKey"), _params(p), _desKey(NULL) {

	uint8_t *key = NULL;


	//Th1s_0nLy_F0uR_t3sT1nG__

	/*unsigned char bytes[] = {
			0x54,0x68,0x31,0x73,0x5F,0x30,0x6E,0x4C
			,0x79,0x5F,0x46,0x30,0x75,0x52,0x5F,0x74
			,0x33,0x73,0x54,0x31,0x6E,0x47,0x5F,0x5F
	};*/

	//Th1s_0nLy_F0uR_t3sT1nG__
	//EL_@51t4s_D0mp3Tku__REqV
	unsigned char bytes[] = {
	        0x45,0x4C,0x5F,0x40,0x35,0x31,0x74,0x34,0x73,0x5F,0x44,0x30,0x6D,0x70,0x33,0x54,0x6B,0x75,0x5F,0x5F,0x52,0x45,0x71,0x56
	};

	//prepare key length
	size_t keyLength = 3 * SB_DES_KEY_SIZE;

	//prepare key space
	key = (uint8_t *)malloc(keyLength);
	memcpy(key, bytes, keyLength);


	//generate TDES key
	int rc = hu_DESKeySet(_params.tripleDesParams(),
		8, key,
		8, &key[8],
		8, &key[16],
		&_desKey, _params.globalContext().ctx());

	maybeLog("desKey", rc);

	/*int rc = hu_desKeySet(_params.tripleDesParams(), content.length() * 8,
			(unsigned char *) content.constData(), &_desKey,
			_params.globalContext().ctx());
	maybeLog("desKey", rc);*/


}

TripleDESKey::~TripleDESKey() {
	/*if (_desKey != NULL) {
		int rc = hu_desKeyDestroy(_params.tripleDesParams(), &_desKey,
				_params.globalContext().ctx());
		maybeLog("~desKey", rc);
		_desKey = NULL;
	}*/
}
