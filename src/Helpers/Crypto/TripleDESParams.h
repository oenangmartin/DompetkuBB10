/*
 * TripleDESParams.h
 *
 *  Created on: Nov 28, 2014
 *      Author: yogi
 */

#ifndef TRIPLEDESPARAMS_H_
#define TRIPLEDESPARAMS_H_

#include "hudes.h"

#include "GlobalContext.hpp"

class TripleDESParams : public Crypto {
public:
	TripleDESParams(GlobalContext & gc);
	virtual ~TripleDESParams();

	sb_Params tripleDesParams() {
		return _tripleDesParams;
	}

	GlobalContext & globalContext() {
		return _globalContext;
	}

	virtual bool isValid() {
		return _tripleDesParams!=NULL;
	}

private:
	GlobalContext & _globalContext;
	sb_Params _tripleDesParams;
};


#endif /* TRIPLEDESPARAMS_H_ */
