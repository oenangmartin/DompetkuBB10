/*
 * TripleDES.cpp
 *
 *  Created on: Nov 30, 2014
 *      Author: yogi
 */

#include "TripleDES.h"

TripleDES::TripleDES() {
	// TODO Auto-generated constructor stub

}

QString TripleDES::encrypt(QString plainText) {
	QByteArray in(plainText.toUtf8());
	pad(in);
	QByteArray out(in.length(), 0);


	if (crypt(true, in, out)) {
		qDebug() << ">>base64: " << QString(out.toBase64());
		return QString(out.toBase64());
	}
	return "";

}

void TripleDES::pad(QByteArray & in) {
	int padLength = 8 - (in.length() % 8);
	for (int i = 0; i < padLength; ++i) {
		in.append((char) padLength);
	}
}

bool TripleDES::crypt(bool isEncrypt, const QByteArray & in,
		QByteArray & out) {
	QByteArray key, iv;
	QString fail;

	/*if (!fromHex(_key, key)) {
		fail += "Key is not valid hex. ";
	}
	if (!fromHex(_iv, iv)) {
		fail += "IV is not valid hex. ";
	}*/

	if (!fail.isEmpty()) {
		toast(fail);
		return false;
	}

	TripleDESParams params(globalContext);

	if (!params.isValid()) {
		toast(
				QString("Could not create params. %1").arg(
						SBError::getErrorText(params.lastError())));
		return false;
	}

	//AESKey aesKey(params, key);
	TripleDESKey desKey(params, key);
	if (!desKey.isValid()) {
		toast(
				QString("Could not create a key. %1").arg(
						SBError::getErrorText(desKey.lastError())));
		return false;
	}

	int rc;
	if (isEncrypt) {
		rc = hu_DESEncryptMsg(params.tripleDesParams(), desKey.desKey(), 0, NULL,in.length(), (const unsigned char *) in.constData(), (unsigned char *) out.data(), globalContext.ctx());
	} else {
		rc = hu_DESDecryptMsg(params.tripleDesParams(), desKey.desKey(), SB_DES_IV_SIZE, NULL, in.length(), (const unsigned char *) in.constData(),
				(unsigned char *) out.data(), globalContext.ctx());
	}
	if (rc == SB_SUCCESS) {
		return true;
	}

	toast(QString("Crypto operation failed. %1").arg(SBError::getErrorText(rc)));
	return false;

}

QString TripleDES::toHex(const QByteArray & in) {
	static char hexChars[] = "0123456789abcdef";

	const char * c = in.constData();
	QString toReturn;
	for (int i = 0; i < in.length(); ++i) {
		toReturn += hexChars[(c[i] >> 4) & 0xf];
		toReturn += hexChars[(c[i]) & 0xf];
	}

	return toReturn;
}

bool TripleDES::fromHex(const QString in, QByteArray & toReturn) {
	QString temp(in);
	temp.replace(" ","");
	temp.replace(":","");
	temp.replace(".","");
	QByteArray content(temp.toLocal8Bit());

	const char * c(content.constData());

	if (content.length() == 0 || ((content.length() % 2) != 0)) {
		return false;
	}

	for (int i = 0; i < content.length(); i += 2) {
		char a = c[i];
		char b = c[i + 1];
		a = nibble(a);
		b = nibble(b);
		if (a < 0 || b < 0) {
			toReturn.clear();
			return false;
		}
		toReturn.append((a << 4) | b);
	}
	return true;
}

char TripleDES::nibble(char c) {
	if (c >= '0' && c <= '9') {
		return c - '0';
	} else if (c >= 'a' && c <= 'f') {
		return c - 'a' + 10;
	} else if (c >= 'A' && c <= 'F') {
		return c - 'A' + 10;
	}
	return -1;
}

void TripleDES::toast(const QString & message) {
	qDebug() << message;
	/*SystemToast * t(new SystemToast(this));
	t->setBody(message);
	t->show();*/
}

void TripleDES::toast(const char * message) {
	toast(QString(message));
}


TripleDES::~TripleDES() {
	// TODO Auto-generated destructor stub
}

