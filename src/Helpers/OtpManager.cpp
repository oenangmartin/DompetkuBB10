/*
 * OtpManager.cpp
 *
 *  Created on: Nov 30, 2014
 *      Author: yogi
 */

#include "OtpManager.h"
#include "Global.h"
#include "AccountManager.h"
#include "applicationui.hpp"
#include "UiUtils.h"

OtpManager* OtpManager::instance = NULL;

OtpManager::OtpManager() {
	httpRequest = new HttpRequest(this);
		connect(httpRequest, SIGNAL(httpReply(QByteArray)), this,
				SLOT(httpReply(QByteArray)));
		retry = 0;

}

OtpManager* OtpManager::getInstance(){
	if(instance == NULL) instance = new OtpManager;
	return instance;
}

void OtpManager::getOtp(QString mode, QString phoneNo, QString code){
	this->mode = mode;
	QString url = API_OTP + "?mode=" + mode + "&msisdn=" + phoneNo + "&code=" +code;
	qDebug() << ">>url: " << url;
	httpRequest->get(url);

	/*QUrl postData;
	postData.addQueryItem("userid", accManager->getUser()->getUserId());
	postData.addQueryItem("signature", accManager->getSignature());
	postData.addQueryItem("to", accManager->getUser()->getNoTlp());
	httpRequest->post(API_GET_BALANCE, postData);*/
}

const QString& OtpManager::getMode() const {
	return mode;
}

void OtpManager::httpReply(QByteArray data) {
	if (!data.isNull()) {

		QString dataStr(data);
		qDebug() << ">>otp: " << dataStr;

		JsonDataAccess jda;
		QVariantMap qmap = jda.loadFromBuffer(data).value<QVariantMap>();
		emit otpReplied(qmap.value("status").toInt(), qmap);

	}
	else{
		/*if(retry < 5){
			retry++;
			getBalance();
		}
		else*/
		QVariantMap qmap;
		qmap.insertMulti("msg", WORDING_KONEKSI_GAGAL);
		emit otpReplied(-999999, qmap);
	}
}

OtpManager::~OtpManager() {
	// TODO Auto-generated destructor stub
}

