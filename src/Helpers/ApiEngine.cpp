/*
 * ApiEngine.cpp
 *
 *  Created on: Nov 23, 2014
 *      Author: yogi
 */

#include "ApiEngine.h"

ApiEngine::ApiEngine() {
	// TODO Auto-generated constructor stub

}

void ApiEngine::getListField(QString id, QObject *parent, const char* slot){
	HttpRequest *httpRequest = new HttpRequest(this);
	connect(httpRequest, SIGNAL(httpReply(QByteArray)), parent,
			SLOT(slot));

	httpRequest->get(API_FIELD + id);
}

ApiEngine::~ApiEngine() {
	// TODO Auto-generated destructor stub
}

