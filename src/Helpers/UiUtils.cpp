/*
 * UiUtils.cpp
 *
 *  Created on: Sep 11, 2014
 *      Author: yogi
 */

#include "UiUtils.h"

int UiUtils::DisplayW;
int UiUtils::DisplayH;

UiUtils::UiUtils() {
	// TODO Auto-generated constructor stub

}

QmlDocument* UiUtils::LoadQml(QString qmlName, QObject *parent) {
	DisplayInfo display;
	DisplayW = display.pixelSize().width();
	DisplayH = display.pixelSize().height();

	QmlDocument *qml = QmlDocument::create("asset:///" + qmlName).parent(
			parent);

	QDeclarativePropertyMap* propertyMap = new QDeclarativePropertyMap;
	propertyMap->insert("ScreenWidth", QVariant(display.pixelSize().width()));
	propertyMap->insert("ScreenHeight", QVariant(display.pixelSize().height()));


	TextStyleDefinition *textStyle = new TextStyleDefinition();
    //textStyle->setColor(Color::Black);
    textStyle->setFontSize(FontSize::PointValue);
    textStyle->setFontSizeValue(8);

    QVariant style = textStyle->property("style");
	propertyMap->insert("DefaultFont", style);

	qml->setContextProperty("UiUtils", propertyMap);

	return qml;
}

QVariant UiUtils::getDefaultFontStyle() {
    TextStyleDefinition *textStyle = new TextStyleDefinition();
    textStyle->setFontSize(FontSize::PointValue);
   textStyle->setFontSizeValue(8);

    QVariant style = textStyle->property("style");
    return style;
}


QmlDocument* UiUtils::LoadQml(QString qmlName, QObject *parent, QVariantList variableList) {
	DisplayInfo display;

	QmlDocument *qml = QmlDocument::create("asset:///" + qmlName).parent(
			parent);

	QDeclarativePropertyMap* propertyMap = new QDeclarativePropertyMap;
	propertyMap->insert("ScreenWidth", QVariant(display.pixelSize().width()));
	propertyMap->insert("ScreenHeight", QVariant(display.pixelSize().height()));

	for(int i = 0; i < variableList.size(); i++){
		QVariantMap qmap = variableList.at(i).toMap();
		propertyMap->insert(qmap.value("key").toString(), qmap.value("value"));
		qDebug() << ">>key: " << qmap.value("key").toString();
		qDebug() << ">>value: " << qmap.value("value").toString();
	}

	qml->setContextProperty("UiUtils", propertyMap);

	return qml;
}

bool UiUtils::IsNeedToSetASMainScroll(){
	DisplayInfo display;
	if(display.pixelSize().width() == display.pixelSize().height()) return true;
	return false;
}

QString UiUtils::AddThousandSeparator(QString text) {
	int size = text.size();
	if (size > 3) {
		text.insert(size - 3, ".");
	}
	if (size > 6) {
		text.insert(size - 6, ".");
	}
	if (size > 9) {
		text.insert(size - 9, ".");
	}

	return text;
}

void UiUtils::toast(QString text, QObject *parent){
	SystemToast *toast = new SystemToast(parent);
	toast->setBody(text);
	toast->show();
}

void UiUtils::alert(const QString &message, QObject *parent) {
	qDebug() << "alert: " << message;
	SystemDialog *dialog; // SystemDialog uses the BB10 native dialog.
	dialog = new SystemDialog(tr("OK"), parent); // New dialog with on 'Ok' button, no 'Cancel' button
	dialog->setTitle(tr("Alert")); // set a title for the message
	dialog->setBody(message); // set the message itself
	dialog->setDismissAutomatically(true); // Hides the dialog when a button is pressed.

	// Setup slot to mark the dialog for deletion in the next event loop after the dialog has been accepted.
	bool ok = connect(dialog,
			SIGNAL(finished(bb::system::SystemUiResult::Type)), dialog,
			SLOT(deleteLater()));
	Q_ASSERT(ok);
	Q_UNUSED(ok);
	dialog->show();
}

UiUtils::~UiUtils() {
	// TODO Auto-generated destructor stub
}

