/*
 * FavoritController.h
 *
 *  Created on: Dec 25, 2014
 *      Author: yogi
 */

#ifndef FAVORITCONTROLLER_H_
#define FAVORITCONTROLLER_H_

#include <bb/cascades/QmlDocument>
#include <bb/system/SystemPrompt>
#include <bb/system/SystemUiResult>
#include "data/BaseDAO.hpp"
#include "FavoritData.h"

using namespace bb::cascades;
using namespace bb::system;

class FavoritController : public BaseDAO{
	Q_OBJECT
public:
	FavoritController();
	virtual ~FavoritController();

	void addToFavorit(FavoritData *favoritData, QString defaultLabel);

	bool createFavoritTable();
	int add(const FavoritData &data);
	bool remove(int pushSeqNum);
	bool removeAll();
	QVariantList getFavoritList();

private:
	SystemPrompt *prompt;
	FavoritData *tempFavoritData;
    FavoritData retrieveFavoritData(const QSqlQuery &sqlQuery);

private slots:
	void onPromptFinished(bb::system::SystemUiResult::Type type);
};

#endif /* FAVORITCONTROLLER_H_ */
