/*
 * ImageRetriever.cpp
 *
 *  Created on: Jan 30, 2013
 *      Author: ASUS
 */

#include "ImageRetriever.h"

ImageRetriever::ImageRetriever() {
	httpRequest = new HttpRequest(this);
	connect(httpRequest, SIGNAL(httpReply(QByteArray)), this,
			SLOT(httpReply(QByteArray)));
	retry = 0;
	cachePath = QDir::homePath() + "/cache/";
	if (!QDir(cachePath).exists()) {
		if (!QDir().mkdir(cachePath))
			qDebug() << "failed create cache folder";
	}
}

ImageRetriever::ImageRetriever(QString cachePath) {
	httpRequest = new HttpRequest(this);
	connect(httpRequest, SIGNAL(httpReply(QByteArray)), this,
			SLOT(httpReply(QByteArray)));
	retry = 0;
	this->cachePath = cachePath;

}

void ImageRetriever::setCachePath(QString cachePath){
	this->cachePath = cachePath;
}

void ImageRetriever::setImagePath(QString path) {
	if (path.length() > 0) {
		QString ext = ".JPG";
		//if (path.indexOf(".png", Qt::CaseInsensitive) > 0) {
		if (path.right(4).compare(".png") == 0 || path.right(4).compare(".PNG") == 0 ){
			//qDebug() << ">>consist png ";
			ext = ".PNG";
		}
		QString diskPath = cachePath
				+ QString::number(qHash(path)) + ext;
		QFile imageFile(diskPath);

		if (imageFile.exists()) {
			//qDebug() << ">>image from cache: " << diskPath;
			emit imageLoaded(diskPath);
		} else {
			//qDebug() << ">>load image from " << path;
			//if (_noImage->isNull())
			if (path.length() > 1) {
				if (mQueue.isEmpty()) {
					//qDebug() << ">>get img: " << path;
					httpRequest->get(path);
				}
				mQueue.append(path);
				//qDebug() << ">>append";
			}
		}
	}
	else emit imageLoaded(NULL);
}

void ImageRetriever::httpReply(QByteArray data) {
	//qDebug() << ">>httpReply";
	if (!data.isNull()) {
		//qDebug() << ">>prepare qimage";
		QImage *qImage = new QImage();

		qImage->loadFromData(data);
		//qDebug() << ">>qimage loaded";
		QString imgPath = mQueue.first();
		if (qImage != NULL) {

			QString ext = ".JPG";
			//if (imgPath.indexOf(".PNG", Qt::CaseInsensitive) > 0) {
			if (imgPath.right(4).compare(".png") == 0 || imgPath.right(4).compare(".PNG") == 0 ){
				//qDebug() << ">>consist png ";
				ext = ".PNG";
			}

			QString diskPath = cachePath
					+ QString::number(qHash(imgPath)) + ext;

			if (qImage->save(diskPath)) {
				//qDebug() << ">>image saved: " << diskPath;
				emit imageLoaded(diskPath);

				if (!mQueue.isEmpty()) {
					mQueue.removeFirst();

					if (!mQueue.isEmpty()) {
						QString url = mQueue.first();
						httpRequest->get(url);
					}
				}
			} else {
				qDebug() << ">>not saved: " << diskPath;
				emit imageLoaded(NULL);
			}
			//qDebug() << ">>image loaded";
		} else {
			emit imageLoaded(NULL);
			//qDebug() << ">>qimage null";
		}

	} else {
		if (retry < 3) {
			retry++;
			httpRequest->get(mQueue.first());
			qDebug() << ">>retry";
		} else
		{
			emit imageLoaded(NULL);
			//qDebug() << ">>image http null";
		}
	}

}

ImageRetriever::~ImageRetriever() {
	// TODO Auto-generated destructor stub
}

