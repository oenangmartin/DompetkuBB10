/*
 * Form.h
 *
 *  Created on: Nov 16, 2014
 *      Author: yogi
 */

#ifndef FORM_H_
#define FORM_H_

#include <bb/cascades/DockLayout>
#include <bb/cascades/ImageView>
#include <bb/cascades/Container>
#include <bb/cascades/Label>
#include <bb/cascades/TextField>
#include <bb/cascades/Container>

using namespace bb::cascades;

class Form : public QObject{
	Q_OBJECT
public:
	Form();
	virtual ~Form();

	static Container* createTextField(QString text, int type, TextField *textField);
};

#endif /* FORM_H_ */
