/*
 * ImageRetriever.h
 *
 *  Created on: Jan 30, 2013
 *      Author: ASUS
 */

#ifndef IMAGERETRIEVER_H_
#define IMAGERETRIEVER_H_

#include <bb/cascades/ImageView>
#include "HttpRequest.h"

using namespace bb::cascades;

class ImageRetriever : public QObject {
	Q_OBJECT
public:
	ImageRetriever();
	ImageRetriever(QString cachePath);
	void setImagePath(QString path);
	virtual ~ImageRetriever();

	void setCachePath(QString cachePath);

	signals:
	void imageLoaded(QString path);


private slots:
	void httpReply(QByteArray data);

private:
	HttpRequest *httpRequest;
	QList<QString> mQueue;
	int retry;
	QString cachePath;

};

#endif /* IMAGERETRIEVER_H_ */
