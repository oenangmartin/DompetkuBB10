/*
 * AccountManager.h
 *
 *  Created on: Nov 8, 2014
 *      Author: yogi
 */

#ifndef ACCOUNTMANAGER_H_
#define ACCOUNTMANAGER_H_

#include "User.h"
#include "TripleDES.h"

#define QSETTINGS_USER_GROUP "user"
#define USER_ID_KEY "useridKey"
#define PIN_KEY "pinKey"
#define TLP_KEY "tlpKey"

class AccountManager : public QObject{
	Q_OBJECT
public:
	AccountManager();
	virtual ~AccountManager();
	static AccountManager* getInstance();
	User* getUser();
	QString generateSignature(QString pin);
	QString getSignature();
	QString getEncodedSignature();

	void save();
	void remove();
	void load();
	bool hasExisting() const;

private:
	static AccountManager *instance;
	User *user;
	TripleDES *tdes;
	QString signature;
};

#endif /* ACCOUNTMANAGER_H_ */
