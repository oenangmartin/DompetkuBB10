/*
 * BalanceManager.h
 *
 *  Created on: Nov 30, 2014
 *      Author: yogi
 */

#ifndef BALANCEMANAGER_H_
#define BALANCEMANAGER_H_

#include <bb/cascades/QmlDocument>
#include "HttpRequest.h"

#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>

using namespace bb::data;

class BalanceManager: public QObject {
Q_OBJECT
public:
	BalanceManager();
	virtual ~BalanceManager();

	static BalanceManager* getInstance();
	void getBalance();

private:
	static BalanceManager *instance;
	HttpRequest *httpRequest;
	int retry;

private slots:
	void httpReply(QByteArray data);

signals:
	void balanceLoaded(QVariantMap qmap);
	void failedLoadBalance();
};

#endif /* BALANCEMANAGER_H_ */
