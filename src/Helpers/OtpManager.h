/*
 * OtpManager.h
 *
 *  Created on: Jan 24, 2015
 *      Author: yogi
 */
/*
 * OtpManager.h
 *
 *  Created on: Nov 30, 2014
 *      Author: yogi
 */

#ifndef OtpManager_H_
#define OtpManager_H_

#include <bb/cascades/QmlDocument>
#include "HttpRequest.h"

#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>

using namespace bb::data;

class OtpManager: public QObject {
Q_OBJECT
public:
	OtpManager();
	virtual ~OtpManager();

	static OtpManager* getInstance();
	void getOtp(QString mode, QString phoneNo, QString code);
	const QString& getMode() const;

private:
	static OtpManager *instance;
	HttpRequest *httpRequest;
	QString mode;
	int retry;

private slots:
	void httpReply(QByteArray data);

signals:
	void otpReplied(int status, QVariantMap qmap);
};

#endif /* OtpManager_H_ */
