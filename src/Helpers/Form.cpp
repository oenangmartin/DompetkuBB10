/*
 * Form.cpp
 *
 *  Created on: Nov 16, 2014
 *      Author: yogi
 */

#include "Form.h"


Form::Form() {
	// TODO Auto-generated constructor stub

}
/*
 *
  Label {
	topMargin: 40
	text: qsTr("PIN") + Retranslate.onLocaleOrLanguageChanged
}
 Container {
	layout: DockLayout {

	}
	ImageView {
		imageSource: "asset:///images/border/input.amd"
		verticalAlignment: VerticalAlignment.Fill
		horizontalAlignment: HorizontalAlignment.Fill
	}
	TextField {
		leftPadding: 40
		verticalAlignment: VerticalAlignment.Center
		backgroundVisible: false
		hintText: ""
	}
}
 */
Container* Form::createTextField(QString text, int type, TextField *textField){
	Container *base = Container::create();
	Label *label = Label::create().text(text);
	base->add(label);

	Container *wrapTextField = Container::create().layout(DockLayout::create());
	ImageView *border = ImageView::create();
	border->setImageSource(QUrl("asset:///images/border/input.amd"));
	border->setHorizontalAlignment(HorizontalAlignment::Fill);
	border->setVerticalAlignment(VerticalAlignment::Fill);
	wrapTextField->add(border);

	textField = new TextField;
	textField->setBackgroundVisible(false);
	textField->setVerticalAlignment(VerticalAlignment::Center);
	wrapTextField->add(textField);

	return base;
}
Form::~Form() {
	// TODO Auto-generated destructor stub
}

