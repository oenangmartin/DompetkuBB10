/*
 * InboxController.h
 *
 *  Created on: Jan 11, 2015
 *      Author: yogi
 */

/*
 * InboxController.h
 *
 *  Created on: Dec 25, 2014
 *      Author: yogi
 */

#ifndef InboxController_H_
#define InboxController_H_

#include <bb/cascades/QmlDocument>
#include <bb/system/SystemDialog>
#include <bb/system/SystemUiResult>
#include "data/BaseDAO.hpp"
#include "InboxData.h"

using namespace bb::cascades;
using namespace bb::system;

class InboxController : public BaseDAO{
	Q_OBJECT
public:
	InboxController();
	virtual ~InboxController();

	bool createTable();
	int add(const InboxData &data);
	bool remove(int pushSeqNum);
	bool removeAll();
	QVariantList getInboxList();
	int deleteId;

private:
	InboxData *tempInboxData;
    InboxData retrieveInboxData(const QSqlQuery &sqlQuery);
    SystemDialog *prompt;

private slots:
    void onPromptFinished(bb::system::SystemUiResult::Type type);

    signals:
    	 void inboxDeleted(int id);

};

#endif /* InboxController_H_ */

