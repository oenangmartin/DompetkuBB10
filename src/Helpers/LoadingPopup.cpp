/*
 * LoadingPopup.cpp
 *
 *  Created on: Jan 8, 2013
 *      Author: ASUS
 */

#include "LoadingPopup.h"
#include <bb/cascades/QmlDocument>
#include <bb/cascades/Dialog>
#include "UiUtils.h"
LoadingPopup::LoadingPopup(QObject *parent, QString text){
	QmlDocument *dialogQml = UiUtils::LoadQml("LoadingPopup.qml", parent);
	dialog = dialogQml->createRootObject<Dialog>();

	loadingText = dialog->findChild<Label*>("loadingText");
	if(loadingText) loadingText->setText(text);

	indicator = dialog->findChild<ActivityIndicator*>("indicator");
}

void LoadingPopup::showLoading(){
	if(indicator) indicator->start();
	dialog->open();
}

void LoadingPopup::hideLoading(){
	if(indicator) indicator->stop();
	if(dialog->isOpened())dialog->close();
}

void LoadingPopup::setLoadingText(QString text){
	loadingText->setText(text);
}

LoadingPopup::~LoadingPopup() {
	// TODO Auto-generated destructor stub
}

