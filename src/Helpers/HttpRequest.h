/*
 * HttpRequest.h
 *
 *  Created on: Feb 25, 2013
 *      Author: ASUS
 */

#ifndef HTTPREQUEST_H_
#define HTTPREQUEST_H_


#include <bb/cascades/Image>
#include <QObject>
using namespace bb::cascades;

class HttpRequest: public QObject
{
    Q_OBJECT
public:
	HttpRequest(QObject *parent);
	virtual ~HttpRequest();

	void get(QString url);

	/*!
		  @param url : url api
		  @param postData: QUrl postData;
			postData.addQueryItem("param1", "data1");
			postData.addQueryItem("param2", "data2");
		 */
	void post(QString url, QUrl postdata);

private:
	QNetworkAccessManager *mNetworkAccessManager;
	QNetworkRequest request;

protected slots:
    void requestFinished(QNetworkReply* reply);

signals:
	void httpReply(QByteArray data);

};
#endif /* HTTPREQUEST_H_ */
