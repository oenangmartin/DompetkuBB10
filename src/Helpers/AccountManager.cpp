/*
 * AccountManager.cpp
 *
 *  Created on: Nov 8, 2014
 *      Author: yogi
 */

#include "AccountManager.h"

AccountManager* AccountManager::instance = NULL;

AccountManager::AccountManager() {
	signature = "";
	user = new User;
	user->setToken("204a217e5873d4a00868f3224cebd607");
	tdes = new TripleDES;
}

AccountManager* AccountManager::getInstance() {
	//if(instance == NULL)
	instance = new AccountManager;
	return instance;

}

QString AccountManager::getEncodedSignature() {
	return QUrl::toPercentEncoding(getSignature());
}
QString AccountManager::getSignature() {
	if (signature.length() == 0) {
		QString reversePin = "";
		for (int i = user->getPin().length() - 1; i >= 0; i--)
			reversePin.append(user->getPin().at(i));

		QString signA = QString::number(QDateTime::currentMSecsSinceEpoch())
				+ user->getPin();
		QString signB = reversePin + "|" + user->getNoTlp();
		QString signC = signA + "|" + signB;
		signature = (tdes->encrypt(signC));
		//QString signature = QUrl::toPercentEncoding(tdes->encrypt(signC));
	}

	return signature;
}
QString AccountManager::generateSignature(QString pin) {
	QString newsignature;
		QString reversePin = "";
		for (int i = pin.length() - 1; i >= 0; i--)
			reversePin.append(pin.at(i));

		QString signA = QString::number(QDateTime::currentMSecsSinceEpoch())
				+pin;
		QString signB = reversePin + "|" + user->getNoTlp();
		QString signC = signA + "|" + signB;
		newsignature = (tdes->encrypt(signC));
		//QString signature = QUrl::toPercentEncoding(tdes->encrypt(signC));

	return newsignature;
}

User* AccountManager::getUser() {
	return user;
}

void AccountManager::save() {
	QSettings settings;
	settings.beginGroup(QSETTINGS_USER_GROUP);
	settings.setValue(USER_ID_KEY, user->getUserId());
	settings.setValue(PIN_KEY, user->getPin());
	settings.setValue(TLP_KEY, user->getNoTlp());
	settings.endGroup();
}

void AccountManager::remove() {
	QSettings settings;
	settings.remove(QSETTINGS_USER_GROUP);
}

void AccountManager::load() {
	QSettings settings;

	settings.beginGroup(QSETTINGS_USER_GROUP);
	user->setUserId(settings.value(USER_ID_KEY).toString());
	user->setPin(settings.value(PIN_KEY).toString());
	user->setNoTlp(settings.value(TLP_KEY).toString());
	settings.endGroup();

	// return m_user;
}

bool AccountManager::hasExisting() const {
	int keysCount = 0;

	QSettings settings;
	settings.beginGroup(QSETTINGS_USER_GROUP);
	keysCount = settings.allKeys().count();
	settings.endGroup();

	return keysCount > 0;
}

AccountManager::~AccountManager() {
	// TODO Auto-generated destructor stub
}

