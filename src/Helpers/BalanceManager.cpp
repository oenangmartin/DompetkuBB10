/*
 * BalanceManager.cpp
 *
 *  Created on: Nov 30, 2014
 *      Author: yogi
 */

#include "BalanceManager.h"
#include "Global.h"
#include "AccountManager.h"
#include "applicationui.hpp"
#include "UiUtils.h"

BalanceManager* BalanceManager::instance = NULL;

BalanceManager::BalanceManager() {
	httpRequest = new HttpRequest(this);
		connect(httpRequest, SIGNAL(httpReply(QByteArray)), this,
				SLOT(httpReply(QByteArray)));
		retry = 0;

}

BalanceManager* BalanceManager::getInstance(){
	if(instance == NULL) instance = new BalanceManager;
	return instance;
}

void BalanceManager::getBalance(){
	AccountManager *accManager = ApplicationUI::accManager;
	/*QString url = API_GET_BALANCE + "?signature=" +QUrl::toPercentEncoding(accManager->getSignature()) + "&userid=" + accManager->getUser()->getUserId() + "&to=" + accManager->getUser()->getNoTlp();
	qDebug() << ">>url: " << url;
	httpRequest->get(url);*/

	QUrl postData;
	postData.addQueryItem("userid", accManager->getUser()->getUserId());
	postData.addQueryItem("signature", accManager->getSignature());
	postData.addQueryItem("to", accManager->getUser()->getNoTlp());
	httpRequest->post(API_GET_BALANCE, postData);
}


void BalanceManager::httpReply(QByteArray data) {
	if (!data.isNull()) {

		QString dataStr(data);
		qDebug() << ">>data: " << dataStr;

		JsonDataAccess jda;
		QVariantMap qmap = jda.loadFromBuffer(data).value<QVariantMap>();
		emit balanceLoaded(qmap);

	}
	else{
		/*if(retry < 5){
			retry++;
			getBalance();
		}
		else*/
			emit failedLoadBalance();
			UiUtils::toast(WORDING_KONEKSI_GAGAL, this);
	}
}

BalanceManager::~BalanceManager() {
	// TODO Auto-generated destructor stub
}

