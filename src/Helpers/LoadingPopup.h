/*
 * LoadingPopup.h
 *
 *  Created on: Jan 8, 2013
 *      Author: ASUS
 */

#ifndef LOADINGPOPUP_H_
#define LOADINGPOPUP_H_

#include <bb/cascades/Dialog>
#include <bb/cascades/Label>
#include <bb/cascades/ActivityIndicator>

using namespace bb::cascades;

class LoadingPopup{

public:
	LoadingPopup(QObject *parent, QString text);
	virtual ~LoadingPopup();


public:
	void showLoading();
	void hideLoading();
	void setLoadingText(QString text);


private:
	Dialog *dialog;
	ActivityIndicator *indicator;
	Label *loadingText;
};

#endif /* LOADINGPOPUP_H_ */
