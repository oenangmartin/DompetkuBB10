/*
 * PromoManager.cpp
 *
 *  Created on: Nov 8, 2014
 *      Author: yogi
 */

#include "PromoManager.h"
#include "Global.h"
#include "AccountManager.h"

PromoManager* PromoManager::instance = NULL;

PromoManager::PromoManager() {
	httpRequest = new HttpRequest(this);
		connect(httpRequest, SIGNAL(httpReply(QByteArray)), this,
				SLOT(httpReply(QByteArray)));

}

PromoManager* PromoManager::getInstance(){
	if(instance == NULL) instance = new PromoManager;
	return instance;
}

void PromoManager::getPromo(){
	httpRequest->get(API_PROMO + AccountManager::getInstance()->getUser()->getToken());
}


void PromoManager::httpReply(QByteArray data) {
	if (!data.isNull()) {

		QString dataStr(data);
		qDebug() << ">>data: " << dataStr;

		JsonDataAccess jda;
		QVariantList qlist = jda.loadFromBuffer(data).value<QVariantList>();


		emit promoLoaded(true, qlist);

	}
}

PromoManager::~PromoManager() {
	// TODO Auto-generated destructor stub
}

