/*
 * FavoritItem.cpp
 *
 *  Created on: Feb 13, 2013
 *      Author: ASUS
 */

#include "FavoritItem.h"
#include "applicationui.hpp"

FavoritItem::FavoritItem() {
	QmlDocument *qml = QmlDocument::create("asset:///listItem/FavoritItemView.qml").parent(
			this);

	Container *baseContainer = qml->createRootObject<Container>();
	baseContainer->setParent(this);
	Container *contentContainer = baseContainer->findChild<Container*>(
			"contentContainer");
	border = baseContainer->findChild<ImageView*>("border");
	lblNama = baseContainer->findChild<Label*>("lblNama");
	lblNumber = baseContainer->findChild<Label*>("lblNumber");
	lblTypeLabel = baseContainer->findChild<Label*>("lblTypeLabel");
	deleteButton = baseContainer->findChild<ImageButton*>("deleteButton");
	connect(deleteButton, SIGNAL(clicked()), this,
				SLOT(onDeleteButtonClicked()));
	setRoot(baseContainer);

	connect(contentContainer, SIGNAL(touch(bb::cascades::TouchEvent *)), this,
			SLOT(onClicked(bb::cascades::TouchEvent *)));


	tag = 0;

}


void FavoritItem::onDeleteButtonClicked() {
	qDebug() << ">>delete: " << QString::number(tag);
	prompt = new SystemDialog("Yes","No");

	prompt->setTitle("Hapus");

	prompt->setBody("Hapus dari favorit?");


	// Connect the finished() signal
	// to the onPromptFinished() slot.

	bool success = QObject::connect(prompt,
	  SIGNAL(finished(bb::system::SystemUiResult::Type)),
	  this,
	  SLOT(onPromptFinished(bb::system::SystemUiResult::Type)));

	if (success) {

		prompt->show();

	} else {
		prompt->deleteLater();
	}
	//
}

void FavoritItem::onPromptFinished(
    bb::system::SystemUiResult::Type type)
{
    if (type == SystemUiResult::ConfirmButtonSelection)
    {
    	ApplicationUI::favoritController->remove(tag);
    	ApplicationUI::favoritViewController->loadFavorit();
    }
    else {
        qDebug() << "Prompt Rejected";
        // The user rejected the prompt.
    }
}
void FavoritItem::setData(QVariantMap qmap) {
	qDebug() << ">>data: " << qmap;
	lblNama->setText(qmap.value("nama").toString());
	lblNumber->setText(qmap.value("number").toString());
	lblTypeLabel->setText(qmap.value("typeLabel").toString());
	tag = qmap.value("id").toInt();
}



void FavoritItem::onClicked(bb::cascades::TouchEvent *touchEvent) {
	if (touchEvent->touchType() == TouchType::Up) {
		border->setImageSource(QUrl("asset:///images/border/arrow_border.amd"));
	} else if (touchEvent->touchType() == TouchType::Down) {
		border->setImageSource(QUrl("asset:///images/border/arrow_border_sel.amd"));
	} else if (touchEvent->touchType() == TouchType::Cancel) {
		border->setImageSource(QUrl("asset:///images/border/arrow_border.amd"));
	}
}

FavoritItem::~FavoritItem() {
	// TODO Auto-generated destructor stub
}

