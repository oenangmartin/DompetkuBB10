/*
 * MerchantItem.cpp
 *
 *  Created on: Feb 13, 2013
 *      Author: ASUS
 */

#include "MerchantItem.h"

MerchantItem::MerchantItem() {
	QmlDocument *qml = QmlDocument::create("asset:///listItem/MerchantItemView.qml").parent(
			this);

	Container *baseContainer = qml->createRootObject<Container>();
	baseContainer->setParent(this);
	Container *contentContainer = baseContainer->findChild<Container*>(
			"contentContainer");
	border = baseContainer->findChild<ImageView*>("border");
	lblName = baseContainer->findChild<Label*>("lblName");
	icon = baseContainer->findChild<WebImage*>("icon");
	icon->setImageWidth(85);

	setRoot(baseContainer);

	connect(contentContainer, SIGNAL(touch(bb::cascades::TouchEvent *)), this,
			SLOT(onClicked(bb::cascades::TouchEvent *)));


	tag = 0;

}


void MerchantItem::setData(QVariantMap qmap) {
	lblName->setText(qmap.value("mnama").toString());
	icon->setImagePath(qmap.value("mimage").toString());
}



void MerchantItem::onClicked(bb::cascades::TouchEvent *touchEvent) {
	if (touchEvent->touchType() == TouchType::Up) {
		border->setImageSource(QUrl("asset:///images/border/arrow_border.amd"));
	} else if (touchEvent->touchType() == TouchType::Down) {
		border->setImageSource(QUrl("asset:///images/border/arrow_border_sel.amd"));
	} else if (touchEvent->touchType() == TouchType::Cancel) {
		border->setImageSource(QUrl("asset:///images/border/arrow_border.amd"));
	}
}

MerchantItem::~MerchantItem() {
	// TODO Auto-generated destructor stub
}

