/*
 * InboxItem.h
 *
 *  Created on: Jan 18, 2015
 *      Author: yogi
 */

/*
 * InboxItem.h
 *
 *  Created on: Feb 13, 2013
 *      Author: ASUS
 */

#ifndef InboxItem_H_
#define InboxItem_H_

#include <bb/cascades/CustomControl>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/Container>
#include <bb/cascades/Button>
#include <bb/cascades/Label>
#include <bb/cascades/ImageButton>
#include <bb/system/SystemToast>
#include "WebImage.h"
#include <bb/system/SystemDialog>
#include <bb/system/SystemUiResult>

using namespace bb::system;
using namespace bb::cascades;

class InboxItem: public bb::cascades::CustomControl {
Q_OBJECT
Q_PROPERTY(QVariantMap data WRITE setData )
public:
	InboxItem();
	virtual ~InboxItem();

	void setData(QVariantMap jsonProfileData);

	int tag;

	private slots:
	void onDeleteButtonClicked();
	void onInboxDeleted(int id);
	void onClicked(bb::cascades::TouchEvent *pTouchEvent);

private:
	Label *lblNama;
	Label *lblTanggal;
	ImageView *border;
	ImageButton *deleteButton;

	signals:
		void clicked();
		void clicked(int tag);
};

#endif /* InboxItem_H_ */

