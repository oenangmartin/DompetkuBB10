/*
 * InboxItem.cpp
 *
 *  Created on: Feb 13, 2013
 *      Author: ASUS
 */

#include "InboxItem.h"
#include "applicationui.hpp"

InboxItem::InboxItem() {
	QmlDocument *qml = QmlDocument::create("asset:///listItem/InboxItemView.qml").parent(
			this);

	Container *baseContainer = qml->createRootObject<Container>();
	baseContainer->setParent(this);
	Container *contentContainer = baseContainer->findChild<Container*>(
			"contentContainer");
	border = baseContainer->findChild<ImageView*>("border");
	lblNama = baseContainer->findChild<Label*>("lblNama");
	lblTanggal = baseContainer->findChild<Label*>("lblTanggal");
	deleteButton = baseContainer->findChild<ImageButton*>("deleteButton");
	connect(deleteButton, SIGNAL(clicked()), this,
				SLOT(onDeleteButtonClicked()));
	setRoot(baseContainer);

	connect(contentContainer, SIGNAL(touch(bb::cascades::TouchEvent *)), this,
			SLOT(onClicked(bb::cascades::TouchEvent *)));


	tag = 0;

}


void InboxItem::onDeleteButtonClicked() {
	qDebug() << ">>delete: " << QString::number(tag);
	ApplicationUI::inboxController->remove(tag);
	connect(ApplicationUI::inboxController, SIGNAL(inboxDeleted(int)), this,
				SLOT(onInboxDeleted(int)));
}

void InboxItem::onInboxDeleted(int id) {
		ApplicationUI::inboxViewController->load();
}

void InboxItem::setData(QVariantMap qmap) {
	qDebug() << ">>data: " << qmap;
	InboxData *data = new InboxData(qmap);
	lblNama->setText(data->getAction());
	lblTanggal->setText(data->getTanggal());
	tag = data->getId();
}



void InboxItem::onClicked(bb::cascades::TouchEvent *touchEvent) {
	if (touchEvent->touchType() == TouchType::Up) {
		border->setImageSource(QUrl("asset:///images/border/arrow_border.amd"));
	} else if (touchEvent->touchType() == TouchType::Down) {
		border->setImageSource(QUrl("asset:///images/border/arrow_border_sel.amd"));
	} else if (touchEvent->touchType() == TouchType::Cancel) {
		border->setImageSource(QUrl("asset:///images/border/arrow_border.amd"));
	}
}

InboxItem::~InboxItem() {
	// TODO Auto-generated destructor stub
}

