/*
 * FavoritItem.h
 *
 *  Created on: Feb 13, 2013
 *      Author: ASUS
 */

#ifndef FavoritItem_H_
#define FavoritItem_H_

#include <bb/cascades/CustomControl>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/Container>
#include <bb/cascades/Button>
#include <bb/cascades/Label>
#include <bb/cascades/ImageButton>
#include <bb/system/SystemToast>
#include "WebImage.h"
#include <bb/system/SystemDialog>
#include <bb/system/SystemUiResult>

using namespace bb::system;
using namespace bb::cascades;

class FavoritItem: public bb::cascades::CustomControl {
Q_OBJECT
Q_PROPERTY(QVariantMap data WRITE setData )
public:
	FavoritItem();
	virtual ~FavoritItem();

	void setData(QVariantMap jsonProfileData);

	int tag;

	private slots:
		void onPromptFinished(bb::system::SystemUiResult::Type type);
	void onDeleteButtonClicked();
	void onClicked(bb::cascades::TouchEvent *pTouchEvent);

private:
	Label *lblNama;
	Label *lblNumber;
	Label *lblTypeLabel;
	ImageView *border;
	ImageButton *deleteButton;
	SystemDialog *prompt;

	signals:
		void clicked();
		void clicked(int tag);
};

#endif /* FavoritItem_H_ */
