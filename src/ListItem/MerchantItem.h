/*
 * MerchantItem.h
 *
 *  Created on: Feb 13, 2013
 *      Author: ASUS
 */

#ifndef MerchantItem_H_
#define MerchantItem_H_

#include <bb/cascades/CustomControl>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/Container>
#include <bb/cascades/Button>
#include <bb/cascades/Label>
#include <bb/cascades/ImageView>
#include <bb/system/SystemToast>
#include "WebImage.h"

using namespace bb::system;
using namespace bb::cascades;

class MerchantItem: public bb::cascades::CustomControl {
Q_OBJECT
Q_PROPERTY(QVariantMap data WRITE setData )
public:
	MerchantItem();
	virtual ~MerchantItem();

	void setData(QVariantMap jsonProfileData);

	int tag;

private slots:
	void onClicked(bb::cascades::TouchEvent *pTouchEvent);

private:
	Label *lblName;
	ImageView *border;
	WebImage *icon;

	signals:
		void clicked();
		void clicked(int tag);
};

#endif /* MerchantItem_H_ */
