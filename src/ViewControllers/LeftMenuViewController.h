/*
 * LeftMenuViewController.h
 *
 *  Created on: Dec 25, 2014
 *      Author: yogi
 */

#ifndef LEFTMENUVIEWCONTROLLER_H_
#define LEFTMENUVIEWCONTROLLER_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Label>
#include <bb/cascades/ListView>
#include <bb/cascades/Container>
#include <bb/cascades/GroupDataModel>

using namespace bb::cascades;

class LeftMenuViewController : public QObject {
	Q_OBJECT
public:
	LeftMenuViewController(Label *lblUser, ListView *menuList);
	virtual ~LeftMenuViewController();

	Q_INVOKABLE void logout();
	Q_INVOKABLE void inbox();
	Q_INVOKABLE void informasi();
	Q_INVOKABLE void favorit();
	Q_INVOKABLE void administrasi();
	Q_INVOKABLE void beranda();

	void setAccountName(QString name);

private:
	Label *lblUser;
	GroupDataModel *dataModel;

	private slots:
			void onListClicked(const QVariantList indexPath);
};

#endif /* LEFTMENUVIEWCONTROLLER_H_ */
