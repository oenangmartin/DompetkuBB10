/*
 * LupaPinViewController.h
 *
 *  Created on: Nov 30, 2014
 *      Author: yogi
 */

#ifndef LUPAPINVIEWCONTROLLER_H_
#define LUPAPINVIEWCONTROLLER_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>

using namespace bb::cascades;

class LupaPinViewController : public Page {
Q_OBJECT
public:
	LupaPinViewController();
	virtual ~LupaPinViewController();

	Q_INVOKABLE void back();
};

#endif /* LUPAPINVIEWCONTROLLER_H_ */
