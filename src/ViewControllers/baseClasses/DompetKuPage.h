/*
 * DompetKuPage.h
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#ifndef DOMPETKUPAGE_H_
#define DOMPETKUPAGE_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Container>
#include <bb/cascades/Label>
#include <bb/cascades/Page>
#include <bb/cascades/ImageButton>
#include <bb/cascades/ActivityIndicator>
#include "BottomMenuController.h"

using namespace bb::cascades;

class DompetKuPage : public Page{
	Q_OBJECT
public:
	DompetKuPage();
	virtual ~DompetKuPage();
	void setTitle(QString title);
	QString getTitle();
	void setBottomMenuVisible(bool visible);

protected:
	Container *contentContainer;
	Container *bottomContainer;
	Label *labelTitle;
	ImageButton *buttonBack;
	BottomMenuController *bottomMenuController;
	ActivityIndicator *indicator;
	bool backClicked;

private slots:
	void onButtonBackClicked();
};

#endif /* DOMPETKUPAGE_H_ */
