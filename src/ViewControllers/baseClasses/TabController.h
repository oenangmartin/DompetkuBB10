/*
 * TabController.h
 *
 *  Created on: Dec 11, 2014
 *      Author: yogi
 */

#ifndef TABCONTROLLER_H_
#define TABCONTROLLER_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/NavigationPane>
#include <bb/cascades/Label>
#include <bb/cascades/ScrollView>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include <bb/cascades/ListView>
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>
#include "UiUtils.h"
#include "TabField.h"
#include "InboxTab.h"
#include "TabInformasi.h"
#include "TabBeranda.h"

using namespace bb::data;
using namespace bb::cascades;

class TabController  : public Page{
Q_OBJECT
public:
	TabController();
	virtual ~TabController();

private:
	Container *mainContainer;
	Container *fadeContainer;
	Container *tabItemContainer;
	Container *tabContentContainer;
	TabBeranda *tabBeranda;
	InboxTab *tabInbox;
	TabField *tabFavorit;
	TabField *tabAdministrasi;
	TabInformasi *tabInformasi;
	TabField *tabLogout;
	Label *labelTitle;

	void addTab(TabField *tabField);
	QVector<TabField*> tabVector;


private slots:
	void onActiveTabChanged(TabField *activeTabfield);
};

#endif /* TABCONTROLLER_H_ */
