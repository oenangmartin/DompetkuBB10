/*
 * DompetKuPage.cpp
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#include "DompetKuPage.h"
#include "UiUtils.h"
#include "applicationui.hpp"
#include "BottomMenuController.h"

DompetKuPage::DompetKuPage() {
	QmlDocument *qml = UiUtils::LoadQml("DompetKuPage.qml", this);
	Container* root = qml->createRootObject<Container>();
	root->setImplicitLayoutAnimationsEnabled(false);
	setContent(root);

	contentContainer = root->findChild<Container*>("contentContainer");

	indicator = root->findChild<ActivityIndicator*>("indicator");

	labelTitle = root->findChild<Label*>("labelTitle");
	buttonBack = root->findChild<ImageButton*>("buttonBack");
	connect(buttonBack, SIGNAL(clicked()), this,SLOT(onButtonBackClicked()));

	Container *bottomButtonContainer = root->findChild<Container*>("bottomButtonContainer");
	bottomMenuController = new BottomMenuController(this);
	bottomButtonContainer->add(bottomMenuController);

	bottomContainer = root->findChild<Container*>("bottomContainer");
	backClicked = false;
}

void DompetKuPage::onButtonBackClicked(){
	//if(!backClicked)
	{
		backClicked = true;
		NavigationPane *nav = ApplicationUI::nav;
		nav->remove(nav->top());
	}
}

void DompetKuPage::setBottomMenuVisible(bool visible){
	bottomContainer->setVisible(visible);
}
void DompetKuPage::setTitle(QString title){
	labelTitle->setText(title);
}

QString DompetKuPage::getTitle(){
	return labelTitle->text();
}

DompetKuPage::~DompetKuPage() {
	// TODO Auto-generated destructor stub
}

