/*
 * RightMenuController.h
 *
 *  Created on: Nov 5, 2014
 *      Author: yogi
 */

#ifndef RIGHTMENUCONTROLLER_H_
#define RIGHTMENUCONTROLLER_H_


#include <bb/cascades/QmlDocument>
#include <bb/cascades/Label>
#include <bb/cascades/Container>
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>

using namespace bb::data;
using namespace bb::cascades;

class RightMenuController : public Container {
Q_OBJECT
public:
	RightMenuController();
	virtual ~RightMenuController();
	Container* getMenuContainer();

	Q_INVOKABLE void logout();
	Q_INVOKABLE void inbox();
	Q_INVOKABLE void informasi();

	void setAccoutName(QString name);
private:
	Container *menuContainer;

};

#endif /* RIGHTMENUCONTROLLER_H_ */
