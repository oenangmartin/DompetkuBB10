/*
 * BottomMenuController.cpp
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#include "BottomMenuController.h"
#include "applicationui.hpp"
#include "MenuBayar.h"
#include "MenuBeli.h"
#include "MenuToken.h"
#include "MenuTransfer.h"
#include <bb/cascades/StackLayout>

BottomMenuController::BottomMenuController(QObject *parent) {
	setParent(parent);
	disableSelectedState = false;
	nav = ApplicationUI::nav;

	StackLayout *pStackLayout = new StackLayout();
	pStackLayout->setOrientation(LayoutOrientation::LeftToRight);
	setLayout(pStackLayout);

	buttonBeli = new TabButton;
	buttonBeli->setText(tr("Beli"));
	buttonBeli->setIconUrl("asset:///images/tab/button-menu-beli.png");
	buttonBeli->setSelectedIconUrl(
			"asset:///images/tab/button-menu-beli-h.png");
	connect(buttonBeli, SIGNAL(clicked(QString)), this,
			SLOT(onMenuBeliClicked(QString)));
	add(buttonBeli);

	buttonBayar = new TabButton();
	buttonBayar->setText(tr("Bayar"));
	buttonBayar->setIconUrl("asset:///images/tab/button-menu-bayar.png");
	buttonBayar->setSelectedIconUrl(
			"asset:///images/tab/button-menu-bayar-h.png");
	connect(buttonBayar, SIGNAL(clicked(QString)), this,
			SLOT(onMenuBayarClicked(QString)));
	add(buttonBayar);

	buttonToken = new TabButton();
	buttonToken->setText(tr("Minta Token"));
	buttonToken->setIconUrl("asset:///images/tab/button-menu-token.png");
	buttonToken->setSelectedIconUrl(
			"asset:///images/tab/button-menu-token-h.png");
	connect(buttonToken, SIGNAL(clicked(QString)), this,
			SLOT(onMenuTokenClicked(QString)));
	add(buttonToken);

	buttonTransfer = new TabButton();
	buttonTransfer->setText(tr("Transfer"));
	buttonTransfer->setIconUrl("asset:///images/tab/button-menu-transfer.png");
	buttonTransfer->setSelectedIconUrl(
			"asset:///images/tab/button-menu-transfer-h.png");
	connect(buttonTransfer, SIGNAL(clicked(QString)), this,
			SLOT(onMenuTransferClicked(QString)));
	add(buttonTransfer);

}

void BottomMenuController::disableSelectionIcon(bool disable){
	disableSelectedState = disable;
}

void BottomMenuController::onMenuBeliClicked(QString title) {
	if (!buttonBeli->isSelected()) {
		if(disableSelectedState)buttonBeli->setSelected(false);
		else buttonBeli->setSelected(true);
		buttonBayar->setSelected(false);
		buttonToken->setSelected(false);
		buttonTransfer->setSelected(false);
		popAllPage();
		nav->insert(9999,new MenuBeli);
	}
}
void BottomMenuController::onMenuBayarClicked(QString title) {
	if (!buttonBayar->isSelected()) {
		buttonBeli->setSelected(false);
		if(disableSelectedState) buttonBayar->setSelected(false);
		else buttonBayar->setSelected(true);
		buttonToken->setSelected(false);
		buttonTransfer->setSelected(false);
		popAllPage();
		nav->insert(9999,new MenuBayar);
	}
}
void BottomMenuController::onMenuTokenClicked(QString title) {
	if (!buttonToken->isSelected()) {
		buttonBeli->setSelected(false);
		buttonBayar->setSelected(false);
		if(disableSelectedState)buttonToken->setSelected(false);
		else buttonToken->setSelected(true);
		buttonTransfer->setSelected(false);
		popAllPage();
		nav->insert(9999,new MenuToken);
	}
}

void BottomMenuController::onMenuTransferClicked(QString title) {
	if (!buttonTransfer->isSelected()) {
		buttonBeli->setSelected(false);
		buttonBayar->setSelected(false);
		buttonToken->setSelected(false);
		if(disableSelectedState)buttonTransfer->setSelected(false);
		else buttonTransfer->setSelected(true);
		popAllPage();
		nav->insert(9999,new MenuTransfer);
	}
}

TabButton* BottomMenuController::getButtonBayar() {
	return buttonBayar;
}

TabButton* BottomMenuController::getButtonBeli() {
	return buttonBeli;
}

TabButton* BottomMenuController::getButtonToken() {
	return buttonToken;
}

TabButton* BottomMenuController::getButtonTransfer() {
	return buttonTransfer;
}

void BottomMenuController::popAllPage() {
	qDebug() << ">>num: " << QString::number(nav->count());
	while(nav->count() >= 2)
	{
		qDebug(">>rem");
		nav->remove(nav->top());
	}
}

BottomMenuController::~BottomMenuController() {
	// TODO Auto-generated destructor stub
}

