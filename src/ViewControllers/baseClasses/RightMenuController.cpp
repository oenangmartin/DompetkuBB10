/*
 * RightMenuController.cpp
 *
 *  Created on: Nov 5, 2014
 *      Author: yogi
 */

#include "RightMenuController.h"
#include "LoginViewController.h"
#include "UiUtils.h"
#include "applicationui.hpp"
#include "InboxViewController.h"
#include "InformasiViewController.h"
#include <bb/cascades/ListView>

RightMenuController::RightMenuController() {
	QmlDocument *qml = UiUtils::LoadQml("pages/RightMenuView.qml", this);
	qml->setContextProperty("page", this);
	menuContainer = qml->createRootObject<Container>();
	add(menuContainer);

	/*ListView *listView = menuContainer->findChild<ListView*>("list");

	 if (listView) {
	 //connect(listView, SIGNAL(triggered(const QVariantList)), this,SLOT(onListClicked(const QVariantList)));
	 JsonDataAccess jda;
	 QVariantList list = jda.load(
	 QDir::currentPath()
	 + "/app/native/assets/models/LeftMenu.json").value<
	 QVariantList>();
	 GroupDataModel *dataModel = new GroupDataModel();
	 dataModel->setParent(this);
	 dataModel->insertList(list);
	 dataModel->setGrouping(ItemGrouping::None);
	 dataModel->setSortedAscending(false);
	 listView->setDataModel(dataModel);
	 }*/
}

void RightMenuController::setAccoutName(QString name) {
	Label *labelNama = menuContainer->findChild<Label*>("labelNama");
	labelNama->setText(name);
}

void RightMenuController::inbox() {
	qDebug() << ">>inbox page";
	ApplicationUI::nav->push(new InboxViewController);

}
void RightMenuController::informasi() {
	qDebug() << ">>informasi page";
	ApplicationUI::nav->push(new InformasiViewController);

}
void RightMenuController::logout() {
	while (ApplicationUI::nav->count() > 0)
		ApplicationUI::nav->pop();
	LoginViewController *loginView = new LoginViewController;
	ApplicationUI::nav->push(loginView);
}

Container* RightMenuController::getMenuContainer() {
	return menuContainer;
}
RightMenuController::~RightMenuController() {
	// TODO Auto-generated destructor stub
}

