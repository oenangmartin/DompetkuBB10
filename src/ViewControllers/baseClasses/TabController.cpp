/*
 * TabController.cpp
 *
 *  Created on: Dec 11, 2014
 *      Author: yogi
 */

#include "TabController.h"

/*
 * TabbedItem {
 id: tab1
 tabTitle: "Item 1"
 tabImage: "asset:///images/selected_tab.png"
 tabIcon: "asset:///images/1tab.png"
 gestureHandlers: TapHandler {
 onTapped: {
 tab1.tabImage = "asset:///images/selected_tab.png"
 tab2.tabImage = "asset:///images/unselected_tab.png"
 tab3.tabImage = "asset:///images/unselected_tab.png"
 tab4.tabImage = "asset:///images/unselected_tab.png"
 ab_selected_img.imageSource = "asset:///images/1tab.png"
 ab_selected_img.visible = true;
 ab_bg_img.imageSource = "asset:///images/action_bg.png"
 tabDelegate.source = "TabOneContent.qml"
 contentCont.translationX = 0;
 startPosition = 0;
 finalPosition = 0;
 fadeContainer.opacity = (contentCont.translationX/1000)*0.5;
 }
 }
 }
 */
TabController::TabController() {
	QmlDocument *qml = UiUtils::LoadQml("TabView.qml", this);
	qml->setContextProperty("page", this);
	Container *root = qml->createRootObject<Container>();
	setContent(root);

	labelTitle = root->findChild<Label*>("labelTitle");
	mainContainer = root->findChild<Container*>("mainContainer");
	fadeContainer = root->findChild<Container*>("fadeContainer");
	tabItemContainer = root->findChild<Container*>("tabItemContainer");
	tabContentContainer = root->findChild<Container*>("tabContentContainer");


	tabBeranda = new TabBeranda("Beranda");
			addTab(tabBeranda);

	tabInbox = new InboxTab("Inbox");
	tabInbox->setVisible(false);
	addTab(tabInbox);

	tabInformasi = new TabInformasi("Informasi");
	tabInformasi->setVisible(false);
	addTab(tabInformasi);



	/*tabInbox = new TabField("Inbox");
	 tabItemContainer->add(tabInbox->getTabItem());
	 addTab(tabInbox);

	 tabFavorit = new TabField("Favorit");
	 tabItemContainer->add(tabFavorit->getTabItem());
	 addTab(tabFavorit);

	 tabAdministrasi = new TabField("Administrasi");
	 tabItemContainer->add(tabAdministrasi->getTabItem());
	 addTab(tabAdministrasi);

	 tabInformasi = new TabField("Informasi");
	 tabItemContainer->add(tabInformasi->getTabItem());
	 addTab(tabInformasi);

	 tabLogout = new TabField("Logout");
	 tabItemContainer->add(tabLogout->getTabItem());
	 addTab(tabLogout);*/

}

void TabController::addTab(TabField *tabField) {
	tabVector.append(tabField);
	tabItemContainer->add(tabField->getTabItem());
	tabContentContainer->add(tabField);
	connect(tabField, SIGNAL(activeTabChanged(TabField *)), this,
			SLOT(onActiveTabChanged(TabField *)));

}

/*
 tab1.tabImage = "asset:///images/selected_tab.png"
 tab2.tabImage = "asset:///images/unselected_tab.png"
 tab3.tabImage = "asset:///images/unselected_tab.png"
 tab4.tabImage = "asset:///images/unselected_tab.png"
 ab_selected_img.imageSource = "asset:///images/1tab.png"
 ab_selected_img.visible = true;
 ab_bg_img.imageSource = "asset:///images/action_bg.png"
 tabDelegate.source = "TabOneContent.qml"
 contentCont.translationX = 0;
 startPosition = 0;
 finalPosition = 0;
 fadeContainer.opacity = (contentCont.translationX/1000)*0.5;
 */

void TabController::onActiveTabChanged(TabField *activeTabfield) {
	qDebug() << ">>tab selected: " << activeTabfield->getTabItem()->title();
	mainContainer->setTranslationX(0);
	fadeContainer->setOpacity(0);
	labelTitle->setText(activeTabfield->getTabItem()->title());

	for (int i = 0; i < tabVector.size(); i++) {
		TabField *tabfield = tabVector.at(i);
		tabfield->setVisible(false);
	}
	activeTabfield->setVisible(true);

}

TabController::~TabController() {
	// TODO Auto-generated destructor stub
}

