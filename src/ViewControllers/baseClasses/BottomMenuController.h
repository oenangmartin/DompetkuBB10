/*
 * BottomMenuController.h
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#ifndef BOTTOMMENUCONTROLLER_H_
#define BOTTOMMENUCONTROLLER_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Container>
#include <bb/cascades/NavigationPane>
#include "TabButton.h"

using namespace bb::cascades;

class BottomMenuController : public Container{
Q_OBJECT
public:
	BottomMenuController(QObject *parent);
	virtual ~BottomMenuController();
	 TabButton* getButtonBayar() ;
	 TabButton* getButtonBeli() ;
	 TabButton* getButtonToken() ;
	 TabButton* getButtonTransfer() ;

	 void disableSelectionIcon(bool disable);

private:
	TabButton *buttonBeli;
	TabButton *buttonBayar;
	TabButton *buttonToken;
	TabButton *buttonTransfer;
	NavigationPane *nav;
	bool disableSelectedState;

	void pushPage(Page *page);
	void popAllPage();


private slots:
	void onMenuBeliClicked(QString title);
	void onMenuBayarClicked(QString title);
	void onMenuTokenClicked(QString title);
	void onMenuTransferClicked(QString title);
};

#endif /* BOTTOMMENUCONTROLLER_H_ */
