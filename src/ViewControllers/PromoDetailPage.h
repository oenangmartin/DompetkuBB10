/*
 * PromoDetailPage.h
 *
 *  Created on: Dec 21, 2014
 *      Author: yogi
 */


#ifndef PromoDetailPage_H_
#define PromoDetailPage_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Container>
#include <bb/cascades/Page>
#include <bb/cascades/StackLayout>
#include <bb/cascades/LayoutOrientation>
#include <bb/cascades/WebView>
#include <bb/cascades/ActivityIndicator>

#include "DompetKuPage.h"

using namespace bb::cascades;

class PromoDetailPage : public DompetKuPage {
	Q_OBJECT
public:
	PromoDetailPage(QObject *parent, QString url);
	virtual ~PromoDetailPage();
	WebView* getWebView();
private:
	ActivityIndicator *indicator;
	WebView *webview;
private slots:
	void webviewLoadProgress(int);
	//void onUrlChanged(const QUrl &url);


};

#endif /* FRAMELISTPAGE_H_ */

