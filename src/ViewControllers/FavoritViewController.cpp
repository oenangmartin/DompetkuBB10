/*
 * FavoritViewController.cpp
 *
 *  Created on: Dec 8, 2014
 *      Author: yogi
 */

#include "FavoritViewController.h"

#include "applicationui.hpp"
#include "UiUtils.h"
#include "FormBeliBayar.h"
#include "FavoritController.h"
#include <bb/cascades/DeleteActionItem>

FavoritViewController::FavoritViewController() {
	ApplicationUI::favoritViewController = this;

	QmlDocument *qml = UiUtils::LoadQml("FavoritPage.qml", this);
	qml->setContextProperty("page", this);
	Container *root = qml->createRootObject<Container>();
	setContent(root);

	listView = root->findChild<ListView*>("list");
	if (listView) {
		qDebug() << ">>fav";
		connect(listView, SIGNAL(triggered(const QVariantList)), this,SLOT(onListClicked(const QVariantList)));
		JsonDataAccess jda;
		dataModel = new GroupDataModel();
		dataModel->setParent(this);
		dataModel->insertList(ApplicationUI::favoritController->getFavoritList());
		dataModel->setGrouping(ItemGrouping::None);
		dataModel->setSortedAscending(false);
		listView->setDataModel(dataModel);

		 DeleteActionItem* daction = root->findChild<DeleteActionItem*>("deleteAction");
		    if (daction)
		    	connect(daction, SIGNAL(triggered()), this, SLOT(onListActionDelete()));

	}

}

void FavoritViewController::loadFavorit() {
	dataModel->clear();
	dataModel->insertList(ApplicationUI::favoritController->getFavoritList());
	dataModel->setGrouping(ItemGrouping::None);
	dataModel->setSortedAscending(false);
}
void FavoritViewController::onListClicked(QVariantList indexPath) {
	QVariantMap map = dataModel->data(indexPath).toMap();
	qDebug() << ">>mapp: " << map;
	JsonDataAccess jda;
	QVariantMap qmapMerchant = jda.loadFromBuffer(map.value("jsonData").toString().toUtf8()).value<QVariantMap>();
	FormBeliBayar *form = new FormBeliBayar(qmapMerchant, map.value("type").toInt(), map.value("namaItem").toString());
	form->setFavoritNumber(map.value("number").toString());
	ApplicationUI::nav->push(form);
}

void FavoritViewController::onListActionDelete(){
	qDebug() << "Here I am: Delete!";
	qDebug() << sender();

	DeleteActionItem* action = dynamic_cast<DeleteActionItem*>(sender());
	if (action == 0)
	{
		qDebug() << "Not expected type";
		return;
	}
	qDebug() << "Yes, found!";

	QObject* parent = action->parent(); // get ActionSet
	if (parent != 0) parent = parent->parent();
	if (parent == 0)
	{
		qDebug() << "No parent!";
		return;
	}
	const QMetaObject* m = parent->metaObject();
	if (m == 0)
	{
		qDebug() << "No metaobject!";
	}
	qDebug() << m->className();
}

void FavoritViewController::back(){
	ApplicationUI::nav->pop();
}

FavoritViewController::~FavoritViewController() {
	// TODO Auto-generated destructor stub
}


