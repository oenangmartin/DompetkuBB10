/*
 * DaftarMerchantViewController.cpp
 *
 *  Created on: Jan 11, 2015
 *      Author: yogi
 */

/*
 * DaftarMerchantViewController.cpp
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#include "DaftarMerchantViewController.h"
#include "Global.h"
#include "UiUtils.h"
#include "MerchanDataModel.h"
#include "applicationui.hpp"
#include <bb/system/InvokeQueryTargetsReply>
#include <bb/system/InvokeQueryTargetsRequest>
#include <bb/system/InvokeReply>
#include <bb/system/InvokeRequest>
#include <bps/navigator.h>

DaftarMerchantViewController::DaftarMerchantViewController() {
	httpRequest = NULL;
	loadingPopup = NULL;

	setTitle("Daftar Merchant");
	setBottomMenuVisible(false);
	QmlDocument *qml = UiUtils::LoadQml("informasi/DaftarMerchant.qml",this);
	qml->setContextProperty("page", this);

	Container* pageContent = qml->createRootObject<Container>();
	pageContent->setImplicitLayoutAnimationsEnabled(false);

	contentContainer->add(pageContent);

	httpRequest = new HttpRequest(this);
			connect(httpRequest, SIGNAL(httpReply(QByteArray)), this, SLOT(onHttpReply(QByteArray)));


			loadingPopup = new LoadingPopup(this, "Loading");
				loadingPopup->showLoading();
				qml->setContextProperty("page", this);

	dataModel = new MerchanDataModel("a");
	connect(dataModel, SIGNAL(dataLoaded(int)), this, SLOT(onDataLoaded(int)));
	dataModel->removeAllData();
	dataModel->loadData(true);


	qml->setContextProperty("dataModel", dataModel);


	listView = pageContent->findChild<ListView*>("list");
	listView->setDataModel(dataModel);
	connect(listView, SIGNAL(triggered(const QVariantList)), this,SLOT(onListClicked(const QVariantList)));

	page = 1;
	//getMerchant();
}

void DaftarMerchantViewController::loadMore(){
	loadingPopup->showLoading();
}
void DaftarMerchantViewController::onDataLoaded(int status){
	loadingPopup->hideLoading();
}
void DaftarMerchantViewController::getMerchant(){
	if (loadingPopup == NULL) {
		loadingPopup = new LoadingPopup(this, "Loading");
	}
	loadingPopup->showLoading();

	QString url = API_INFORMASI_MERCHANT + AccountManager::getInstance()->getUser()->getToken() + "/10/" + QString::number(page);
			qDebug() << ">>url: " << url;
			httpRequest->get(url);
}

void DaftarMerchantViewController::onHttpReply(QByteArray data) {
	/*loadingPopup->hideLoading();
	if (!data.isNull()) {
		QString dataStr(data);
		qDebug() << ">>onHttpReply: " << dataStr;
		JsonDataAccess jda;
		QVariantMap qmap = jda.loadFromBuffer(data).value<QVariantMap>();
		maxPage = qmap.value("total_page").toInt();
		QVariantList list = qmap.value("detail").toList();
		if(dataModel == NULL){
			dataModel = new GroupDataModel();
			dataModel->setParent(this);
			dataModel->setGrouping(ItemGrouping::None);
			listView->setDataModel(dataModel);
			modelList.append(list);
			dataModel->setSortedAscending(false);
		}
		modelList.insert(modelList.size(), list);
					dataModel->insertList(modelList);


	}
  else {
	  if(page > 1) page--;
		UiUtils::alert(WORDING_KONEKSI_GAGAL, this);
	}*/
}
void DaftarMerchantViewController::onListClicked(QVariantList indexPath) {
	QVariantMap map = dataModel->data(indexPath).toMap();
	QString url = map.value("list_merchant_url").toString();
	navigator_invoke(url.toStdString().c_str(), 0);
}


DaftarMerchantViewController::~DaftarMerchantViewController() {
	// TODO Auto-generated destructor stub
}



