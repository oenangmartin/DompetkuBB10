/*
 * TutorialViewController.h
 *
 *  Created on: Jan 18, 2015
 *      Author: yogi
 */

/*
 * TutorialViewController.h
 *
 *  Created on: Jan 8, 2013
 *      Author: ASUS
 */

#ifndef TutorialViewController_H_
#define TutorialViewController_H_

#include <bb/cascades/Dialog>
#include <bb/cascades/Label>
#include <bb/cascades/ActivityIndicator>

using namespace bb::cascades;

class TutorialViewController{

public:
	TutorialViewController(QObject *parent);
	virtual ~TutorialViewController();


public:
	void show();
	void close();


private:
	Dialog *dialog;
};

#endif /* TutorialViewController_H_ */

