/*
 * VerifikasiNoViewController.h
 *
 *  Created on: Jan 24, 2015
 *      Author: yogi
 */

/*
 * VerifikasiNoViewController.h
 *
 *  Created on: Nov 2, 2014
 *      Author: yogi
 */

#ifndef VerifikasiNoViewController_H_
#define VerifikasiNoViewController_H_
#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/TextField>
#include <bb/cascades/TextArea>
#include <bb/cascades/ImageToggleButton>
#include <bb/cascades/Container>
#include <bb/device/DisplayInfo>
#include <bb/system/SystemListDialog>
#include <bb/system/SystemDialog>
#include <bb/cascades/DateTimePicker>
#include "LoadingPopup.h"
#include "OtpManager.h"
#include "HttpRequest.h"
#include <bb/data/JsonDataAccess>

using namespace bb::device;
using namespace bb::cascades;
using namespace bb::system;

class VerifikasiNoViewController  : public Page {
Q_OBJECT
public:
	VerifikasiNoViewController();
	Q_INVOKABLE void back();
	Q_INVOKABLE void kirimSms();
	virtual ~VerifikasiNoViewController();

private:
	TextField *noHpField;
	OtpManager *otpManager;
	LoadingPopup *loadingPopup;

private slots:
	void onOtpReplied(int status, QVariantMap qmap);

};

#endif /* VerifikasiNoViewController_H_ */

