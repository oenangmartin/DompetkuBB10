/*
 * SubMenu.cpp
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#include "SubMenu.h"
#include "applicationui.hpp"
#include "MerchantManager.h"
#include "FormBeliBayar.h"
#include "Global.h"

SubMenu::SubMenu(QString title, QVariantList list, int type) {
	this->title = title;
	this->type = type;
	setTitle(title);

	if(type == TYPE_BELI)
		bottomMenuController->getButtonBeli()->setSelected(true);
	else
		bottomMenuController->getButtonBayar()->setSelected(true);
	QmlDocument *qml =
			QmlDocument::create("asset:///bottomMenu/SubMenu.qml").parent(
					this);
	Container* pageContent = qml->createRootObject<Container>();
	contentContainer->add(pageContent);

	listView = pageContent->findChild<ListView*>("list");

	if (listView) {
		connect(listView, SIGNAL(triggered(const QVariantList)), this,SLOT(onListClicked(const QVariantList)));
		JsonDataAccess jda;
		dataModel = new GroupDataModel();
		dataModel->setParent(this);
		dataModel->insertList(list);
		dataModel->setGrouping(ItemGrouping::None);
		dataModel->setSortedAscending(false);
		listView->setDataModel(dataModel);
	}
}

void SubMenu::onListClicked(QVariantList indexPath) {
	QVariantMap map = dataModel->data(indexPath).toMap();
	qDebug() << ">>map: " << map;

	FormBeliBayar *form = new FormBeliBayar(map, type, title);
	ApplicationUI::nav->push(form);
}

SubMenu::~SubMenu() {
	// TODO Auto-generated destructor stub
}

