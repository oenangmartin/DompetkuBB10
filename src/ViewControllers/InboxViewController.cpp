#include "InboxViewController.h"

#include "applicationui.hpp"
#include "UiUtils.h"
#include "InboxData.h"
#include "InboxDetail.h"
#include "FormBeliBayar.h"
#include "FavoritController.h"
#include <bb/cascades/DeleteActionItem>

InboxViewController::InboxViewController() {
	ApplicationUI::inboxViewController = this;
	QmlDocument *qml = UiUtils::LoadQml("InboxPage.qml", this);
	qml->setContextProperty("page", this);
	Container *root = qml->createRootObject<Container>();
	setContent(root);

	listView = root->findChild<ListView*>("list");
	if (listView) {
		qDebug() << ">>fav";
		connect(listView, SIGNAL(triggered(const QVariantList)), this,
				SLOT(onListClicked(const QVariantList)));
		JsonDataAccess jda;
		dataModel = new GroupDataModel();
		dataModel->setParent(this);
		QVariantList list = ApplicationUI::inboxController->getInboxList();
		if (list.size() == 0) {
			UiUtils::toast("Tidak ada data", this);
			return;
		}
		dataModel->insertList(list);
		dataModel->setGrouping(ItemGrouping::None);
		dataModel->setSortedAscending(false);
		listView->setDataModel(dataModel);
	}

}

void InboxViewController::load() {
	dataModel->clear();
	dataModel->insertList(ApplicationUI::inboxController->getInboxList());
	dataModel->setGrouping(ItemGrouping::None);
	dataModel->setSortedAscending(false);
}
void InboxViewController::onListClicked(QVariantList indexPath) {
	QVariantMap map = dataModel->data(indexPath).toMap();
	qDebug() << ">>mapp: " << map;
	InboxData *data = new InboxData(map);
	InboxDetail *inboxDetail = new InboxDetail(data);

	ApplicationUI::nav->push(inboxDetail);
}

void InboxViewController::onListActionDelete() {
	qDebug() << "Here I am: Delete!";
	qDebug() << sender();

	DeleteActionItem* action = dynamic_cast<DeleteActionItem*>(sender());
	if (action == 0) {
		qDebug() << "Not expected type";
		return;
	}
	qDebug() << "Yes, found!";

	QObject* parent = action->parent(); // get ActionSet
	if (parent != 0)
		parent = parent->parent();
	if (parent == 0) {
		qDebug() << "No parent!";
		return;
	}
	const QMetaObject* m = parent->metaObject();
	if (m == 0) {
		qDebug() << "No metaobject!";
	}
	qDebug() << m->className();
}

void InboxViewController::back() {
	ApplicationUI::nav->pop();
}

InboxViewController::~InboxViewController() {
	// TODO Auto-generated destructor stub
}

