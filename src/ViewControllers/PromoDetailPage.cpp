/*
 * PromoDetailPage.cpp
 *
 *  Created on: Jan 18, 2013
 *      Author: ASUS
 */

#include "PromoDetailPage.h"
#include <bb/cascades/InvokeActionItem>
#include <bb/cascades/InvokeQuery>
PromoDetailPage::PromoDetailPage(QObject *parent, QString url) {
	qDebug() << ">>url: " <<  url;
	setTitle("Promo");
	QmlDocument *qml =
			QmlDocument::create("asset:///pages/PromoDetailPage.qml").parent(this);

	Container* pageContent = qml->createRootObject<Container>();
		contentContainer->add(pageContent);

	Container *webViewContainer = pageContent->findChild<Container*>("webViewContainer");
	indicator = pageContent->findChild<ActivityIndicator*>("indicator");
		indicator->start();

	webview = new WebView(webViewContainer);
	if(url.length() > 0){
		webview->setUrl(url);
	}
	connect(webview, SIGNAL(loadProgressChanged(int)), this,
					SLOT(webviewLoadProgress(int)));

	//

}


WebView* PromoDetailPage::getWebView(){
	return webview;
}

void PromoDetailPage::webviewLoadProgress(int val){
	qDebug() << ">>progres: " <<  val;
	if(val > 50) indicator->stop();
}

PromoDetailPage::~PromoDetailPage() {
	// TODO Auto-generated destructor stub
}

