/*
 * HomeViewController.h
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#ifndef HOMEVIEWCONTROLLER_H_
#define HOMEVIEWCONTROLLER_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/NavigationPane>
#include <bb/cascades/Label>
#include <bb/cascades/ScrollView>
#include <bb/cascades/ImageView>
#include <bb/cascades/Page>
#include <bb/cascades/ListView>
#include <bb/cascades/ActivityIndicator>
#include "RightMenuController.h"
#include "BalanceManager.h"
#include "HistoryManager.h"
#include "PromoManager.h"
#include "Timer.h"
#include "LeftMenuViewController.h"
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/pickers/ContactPicker>

using namespace bb::data;
using namespace bb::cascades;
using namespace bb::cascades::pickers;

class HomeViewController : public Page{
Q_OBJECT
public:
	HomeViewController();
	virtual ~HomeViewController();
	Q_INVOKABLE void rightMenuTapped();
	Q_INVOKABLE void closeLeftMenu();
	Q_INVOKABLE void refresh();
	Q_INVOKABLE void showMoreHistory();

	void loadHistoryTransaction(QString count);

private:
	Timer *timer;
	LeftMenuViewController *leftMenuController;
	//RightMenuController *rightMenuController;
	Container *contentContainer;
	Container *menuContainer;
	Container *transaksiContainer;
	BalanceManager *balanceManager;
	HistoryManager *historyManager;
	PromoManager *promoManager;
	GroupDataModel *promoDataModel ;
	ImageView *refreshButton;
	ActivityIndicator *indicator;
	Container* root;
	int leftMenuWidth;
	int bottomMenuHeight;
	int headerHeight;

	int saldoHeight;
	int profileHeight;
	int transaksiHeight;
	int promoHeight;
	int currPromoIdx;

	int loadCount;
	ScrollView *scroll;

	void setLoading(bool isLoad);

private slots:
	void headerUpdateHandler(QRectF rect);
	void bottomMenuUpdateHandler(QRectF rect);
	void profileHandler(QRectF rect);
	void saldoHandler(QRectF rect);
	void transaksiUpdateHandler(QRectF rect);
	void promoUpdateHandler(QRectF rect);
	void onBalanceLoaded(QVariantMap qmap);
	void onFailedLoadBalance();
	void onFailedLoadHistory();
	void onHistoryLoaded(QVariantMap qmap);
	void onPromoLoaded(bool status, QVariantList qmap);
	void onPromoClicked(const QVariantList indexPath);
	void onTimerPromoTimeout();
};

#endif /* HOMEVIEWCONTROLLER_H_ */
