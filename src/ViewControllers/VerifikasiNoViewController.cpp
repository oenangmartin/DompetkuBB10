/*
 * VerifikasiNoViewController.cpp
 *
 *  Created on: Nov 2, 2014
 *      Author: yogi
 */

#include "VerifikasiNoViewController.h"
#include "OneTimePassViewController.h"
#include "UiUtils.h"
#include "Terms.h"
#include "applicationui.hpp"
#include "Global.h"
#include "AccountManager.h"
#include "RegisterViewController.h"
#include "LoginViewController.h"

VerifikasiNoViewController::VerifikasiNoViewController() {
	QmlDocument *qml = UiUtils::LoadQml("login/VerifikasiNo.qml", this);
	qml->setContextProperty("page", this);
	Container *root = qml->createRootObject<Container>();
	setContent(root);

	otpManager = new OtpManager();
						connect(otpManager, SIGNAL(otpReplied(int, QVariantMap)), this,
								SLOT(onOtpReplied(int, QVariantMap)));


	noHpField = root->findChild<TextField*>("noHpField");

	loadingPopup = new LoadingPopup(this, "Loading");

}

void VerifikasiNoViewController::onOtpReplied(int status, QVariantMap qmap) {
	loadingPopup->hideLoading();
	if(status == 0)
		ApplicationUI::nav->push(new OneTimePassViewController(noHpField->text()));
	else if(status == -98)
			UiUtils::alert("Koneksi ke server gagal. Silakan coba kembali.", this);
	else
		UiUtils::alert(qmap.value("msg").toString(), this);
}

void VerifikasiNoViewController::kirimSms() {
	if(noHpField->text().trimmed().length() == 0){
		UiUtils::toast("Mohon isi no handphone", this);
		return;
	}
	otpManager->getOtp("request", noHpField->text().trimmed(), "");
	loadingPopup->showLoading();


}
void VerifikasiNoViewController::back() {
	ApplicationUI::nav->pop();
}

VerifikasiNoViewController::~VerifikasiNoViewController() {
	// TODO Auto-generated destructor stub
}

