/*
 * LupaPinViewController.cpp
 *
 *  Created on: Nov 30, 2014
 *      Author: yogi
 */

#include "LupaPinViewController.h"
#include "UiUtils.h"
#include "applicationui.hpp"

LupaPinViewController::LupaPinViewController() {
	QmlDocument *qml = UiUtils::LoadQml("login/LupaPinView.qml", this);
		qml->setContextProperty("page", this);
		Container *container = qml->createRootObject<Container>();
		setContent(container);
}


void LupaPinViewController::back(){
	ApplicationUI::nav->pop();
}


LupaPinViewController::~LupaPinViewController() {
	// TODO Auto-generated destructor stub
}

