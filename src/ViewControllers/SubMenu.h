/*
 * SubMenu.h
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#ifndef SubMenu_H_
#define SubMenu_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include <bb/cascades/ListView>
#include "DompetKuPage.h"

#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>

using namespace bb::data;
using namespace bb::cascades;

class SubMenu : public DompetKuPage{
	Q_OBJECT
public:
	SubMenu(QString title, QVariantList list, int type);
	virtual ~SubMenu();

private:
	QString title;
	ListView *listView;
	GroupDataModel *dataModel;
	int type;
private slots:
	void onListClicked(const QVariantList indexPath);
};

#endif /* SubMenu_H_ */
