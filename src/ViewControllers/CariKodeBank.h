/*
 * CariKodeBank.h
 *
 *  Created on: Dec 21, 2014
 *      Author: yogi
 */

/*
 * CariKodeBank.h
 *
 *  Created on: Nov 30, 2014
 *      Author: yogi
 */

#ifndef CariKodeBank_H_
#define CariKodeBank_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/ListView>
#include <bb/cascades/Container>
#include <bb/cascades/TextField>
#include "DompetKuPage.h"
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>

using namespace bb::data;
using namespace bb::cascades;


using namespace bb::cascades;

class CariKodeBank : public Page{
	Q_OBJECT
public:
	CariKodeBank();
	virtual ~CariKodeBank();
	Q_INVOKABLE void back();

private:
	ListView *listView;
	GroupDataModel *dataModel;
	TextField *searchText;
	QVariantList bankList, resultList;

private slots:
	void onListClicked(const QVariantList indexPath);
	void onSearchTextChanged(QString text);

signals:
	void bankSelected(QString kode, QString nama);

};

#endif /* CariKodeBank_H_ */

