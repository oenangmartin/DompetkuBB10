/*
 * OneTimePassViewController.cpp
 *
 *  Created on: Jan 24, 2015
 *      Author: yogi
 */

/*
 * OneTimePassViewController.cpp
 *
 *  Created on: Nov 2, 2014
 *      Author: yogi
 */

#include "OneTimePassViewController.h"
#include "UiUtils.h"
#include "Terms.h"
#include "applicationui.hpp"
#include "Global.h"
#include "AccountManager.h"
#include "RegisterViewController.h"
#include "LoginViewController.h"

#include <bb/pim/account/Service>
#include <bb/pim/account/Account>
#include <bb/pim/message/MessageService>
#include <bb/pim/message/ConversationBuilder>
#include <bb/pim/message/MessageBuilder>
#include <bb/pim/message/MessageContact>
#include <bb/pim/account/AccountService>

#include <bb/pim/message/Keys>
#include <bb/pim/message/StatusReport>
#include <bb/pim/message/MessageUpdateType>
#include <bb/pim/message/MimeTypes>
#include <bb/pim/message/Message>

using namespace bb::pim::account;
using namespace bb::pim::message;

OneTimePassViewController::OneTimePassViewController(QString phoneNo) {
	this->phoneNo = phoneNo;
	QmlDocument *qml = UiUtils::LoadQml("login/OneTimePass.qml", this);
	qml->setContextProperty("page", this);
	Container *root = qml->createRootObject<Container>();
	setContent(root);

	timeout = 300;

	doneContainer = root->findChild<Container*>("doneContainer");
	waitContainer = root->findChild<Container*>("waitContainer");
	codeField = root->findChild<TextField*>("noHpField");
	lblTimer = root->findChild<Label*>("lblTimer");
	indicator = root->findChild<ActivityIndicator*>("indicator");
	indicator->start();

	otpManager = new OtpManager();
					connect(otpManager, SIGNAL(otpReplied(int, QVariantMap)), this,
							SLOT(onOtpReplied(int, QVariantMap)));
	loadingPopup = new LoadingPopup(this, "Loading");

	timePassed = timeout;

	timer = new Timer(this);
	timer->setInterval(1000);
	connect(timer, SIGNAL(timeout()), this, SLOT(onTimerTimeout()));
	timer->start();

	_account_service = new AccountService();
		qDebug() << "XXXX getting SMS-MMS account";
			QList<Account> account_list = _account_service->accounts(Service::Messages, "sms-mms");
			_sms_account_id = account_list.first().id();
			qDebug() << "XXXX got SMS-MMS account. id=" << _sms_account_id;

		_message_service = new bb::pim::message::MessageService(this);
		bool res = connect(_message_service, SIGNAL(
					messageAdded(bb::pim::account::AccountKey, bb::pim::message::ConversationKey, bb::pim::message::MessageKey)), SLOT(
					messageReceived(bb::pim::account::AccountKey, bb::pim::message::ConversationKey, bb::pim::message::MessageKey)));
}


void OneTimePassViewController::onOtpReplied(int status, QVariantMap qmap) {
	loadingPopup->hideLoading();

	if(status == 0)
	{
		if(otpManager->getMode().compare("request") == 0)
			timer->start();
		else{
			QSettings settings;
			settings.setValue(SHOW_OTP, QVariant("1"));

			settings.setValue(LAST_NO_AKUN, QVariant(phoneNo));


			LoginViewController *loginView = new LoginViewController;
			loginView->setNoHp(phoneNo);
			ApplicationUI::popAllPage();
			ApplicationUI::nav->push(loginView);


		}
	}
	else if(status == -11)
	{
		QSettings settings;
		settings.setValue(LAST_NO_AKUN, QVariant(phoneNo));
		RegisterViewController *registerView = new RegisterViewController;
		registerView->setNoHp(phoneNo);
		ApplicationUI::popAllPage();
		ApplicationUI::nav->push(registerView);
	}

	else if(status == -98)
		UiUtils::alert("Koneksi ke server gagal. Silakan coba kembali.", this);
	else
		UiUtils::alert(qmap.value("msg").toString(), this);

}
void OneTimePassViewController::onTimerTimeout() {
	timePassed--;
	if(timePassed < 0)
	{
		waitContainer->setVisible(false);
		doneContainer->setVisible(true);
		indicator->stop();
		timer->stop();
		timePassed = timeout;
		lblTimer->setVisible(false);
	}
	else
	{
		if(!indicator->isRunning()) indicator->start();
		int minute = timePassed / 60;
		QString second = QString::number(timePassed % 60);
		if(second.length() == 1) second = "0" + second;
		lblTimer->setVisible(true);
		lblTimer->setText(QString::number(minute) + ":" + second);
	}
}

void OneTimePassViewController::confirmOtp() {
	if(codeField->text().trimmed().length() == 0){
		UiUtils::toast("Mohon isi kode Verifikasi", this);
		codeField->requestFocus();
		return;
	}
	otpManager->getOtp("confirm", phoneNo, codeField->text().trimmed());
	loadingPopup->showLoading();
}
void OneTimePassViewController::requestOtp() {
	waitContainer->setVisible(true);
	doneContainer->setVisible(false);
	otpManager->getOtp("request", phoneNo, "");
	loadingPopup->showLoading();

}
void OneTimePassViewController::back() {
	ApplicationUI::nav->pop();
}

void OneTimePassViewController::messageReceived(bb::pim::account::AccountKey account_key, bb::pim::message::ConversationKey conv, bb::pim::message::MessageKey message_key) {

	qDebug() << "XXXX messageReceived";
	Message message = _message_service->message(_sms_account_id, message_key);
	qDebug() << "XXXX Message mime type:" << message.mimeType();
	if (message.mimeType() == MimeTypes::Sms)
	{
		qDebug() << "XXXX messageReceived: It's an SMS!!!";
		if (message.isInbound())
		{
			qDebug() << "XXXX sender:" << message.sender().displayableName();
			if (message.attachmentCount() > 0) {
				bb::pim::message::Attachment attachment = message.attachmentAt(0);
				QString from = message.sender().displayableName();
				QString message = attachment.data();
				if(from.compare("789") == 0 || from.compare("788") == 0)
				{
					/*
					SMS OTP akan di kirim melalui 789 & 788.

					SMS OTP formatnya as follow:
					"Kode verifikasi anda: XXXXXX Silakan masukkan kode verifikasi ini pada aplikasi Dompetku"

					Format OTP Code: selalu 6 digit.
					Masa berlaku: 5 menit, lebih dari itu harus request lagi
					 */
					if(message.indexOf("Dompetku") > -1 && message.indexOf(":") > -1)
					{
						int start = message.indexOf(":") + 2;
						int n = 6;
						QString verificationCode = message.mid(start, n);
						qDebug() << ">>verificationCode: " << verificationCode;
						codeField->setText(verificationCode);
						confirmOtp();
					}
					else{
					}
				}
				//qDebug() << QString("RCVD: SMS\n").append("RCVD: from " + message.sender().displayableName()).append("\nRCVD: ").append(QString(attachment.data()));
			} else {
				qDebug() << "XXXX messageReceived: no attachments";
			}
		}
		else {
			qDebug() << "XXXX messageReceived: not inbound";
		}
	}  else {
		qDebug() << "XXXX messageReceived: not an SMS";
	}
}

OneTimePassViewController::~OneTimePassViewController() {
	// TODO Auto-generated destructor stub
}

/*
 *
 * _account_service = new AccountService();
	qDebug() << "XXXX getting SMS-MMS account";
		QList<Account> account_list = _account_service->accounts(Service::Messages, "sms-mms");
		_sms_account_id = account_list.first().id();
		qDebug() << "XXXX got SMS-MMS account. id=" << _sms_account_id;

	_message_service = new bb::pim::message::MessageService(this);
	bool res = connect(_message_service, SIGNAL(
				messageAdded(bb::pim::account::AccountKey, bb::pim::message::ConversationKey, bb::pim::message::MessageKey)), SLOT(
				messageReceived(bb::pim::account::AccountKey, bb::pim::message::ConversationKey, bb::pim::message::MessageKey)));
 *
 *

 */



