/*
 * LoginViewController.h
 *
 *  Created on: Nov 2, 2014
 *      Author: yogi
 */

#ifndef LOGINVIEWCONTROLLER_H_
#define LOGINVIEWCONTROLLER_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include <bb/cascades/TextField>
#include <bb/system/SystemToast>
#include <bb/device/DisplayInfo>
#include "HttpRequest.h"
#include "LoadingPopup.h"

using namespace bb::device;
using namespace bb::system;
using namespace bb::cascades;

class LoginViewController  : public Page {
Q_OBJECT
public:
	LoginViewController();
	Q_INVOKABLE void login();
	Q_INVOKABLE void showRegisterView();
	Q_INVOKABLE void showTerms();
	Q_INVOKABLE void showLupaPinView();
	virtual ~LoginViewController();
	void setNoHp(QString text);

private:
	HttpRequest *httpRequest;
	TextField *nomorAkunTextField;
	TextField *pinTextField;
	LoadingPopup *loadingPopup;

private slots:
	void httpReply(QByteArray data);
	void onTextPinChanging(QString text);
};

#endif /* LOGINVIEWCONTROLLER_H_ */
