/*
 * RegisterViewController.h
 *
 *  Created on: Nov 2, 2014
 *      Author: yogi
 */

#ifndef REGISTERVIEWCONTROLLER_H_
#define REGISTERVIEWCONTROLLER_H_
#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/TextField>
#include <bb/cascades/TextArea>
#include <bb/cascades/ImageToggleButton>
#include <bb/cascades/Container>
#include <bb/device/DisplayInfo>
#include <bb/system/SystemListDialog>
#include <bb/system/SystemDialog>
#include <bb/cascades/DateTimePicker>
#include "LoadingPopup.h"
#include "HttpRequest.h"
#include <bb/data/JsonDataAccess>

using namespace bb::device;
using namespace bb::cascades;
using namespace bb::system;

class RegisterViewController  : public Page {
Q_OBJECT
public:
	RegisterViewController();
	Q_INVOKABLE void back();
	Q_INVOKABLE void daftar();
	Q_INVOKABLE void showTermsView();
	Q_INVOKABLE void openTipeIdentitas();
	Q_INVOKABLE void openDatePicker();
	virtual ~RegisterViewController();

	void setNoHp(QString text);

private:
	SystemListDialog *listTipeIdentitasDialog;
	TextField *emailField;
	TextField *noHpField;
	TextField *namaDepanField;
	TextField *namaBelakangField;
	TextArea *alamatField;
	TextField *identitasField;
	TextField *noIdentitasField;
	TextField *namaIbuField;
	TextField *pinField;
	TextField *pin2Field;

	ImageToggleButton *priaField;
	ImageToggleButton *wanitaField;
	ImageToggleButton *agreeField;

	DateTimePicker *datePicker;

	LoadingPopup *loadingPopup;
		HttpRequest *httpRequest;

	private slots:
		void onTextChanging(QString text);
		void onTextPin2Changing(QString text);
		void onAlertDismiss();
		void httpReply(QByteArray data);
		void onTipeIdentitasSelected(int val);
};

#endif /* REGISTERVIEWCONTROLLER_H_ */
