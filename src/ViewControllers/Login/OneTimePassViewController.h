/*
 * OneTimePassViewController.h
 *
 *  Created on: Jan 24, 2015
 *      Author: yogi
 */

/*
 * OneTimePassViewController.h
 *
 *  Created on: Nov 2, 2014
 *      Author: yogi
 */

#ifndef OneTimePassViewController_H_
#define OneTimePassViewController_H_
#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/TextField>
#include <bb/cascades/TextArea>
#include <bb/cascades/Label>
#include <bb/cascades/ActivityIndicator>
#include <bb/cascades/ImageToggleButton>
#include <bb/cascades/Container>
#include <bb/device/DisplayInfo>
#include <bb/system/SystemListDialog>
#include <bb/system/SystemDialog>
#include <bb/cascades/DateTimePicker>
#include "LoadingPopup.h"
#include "HttpRequest.h"
#include "Timer.h"
#include "OtpManager.h"
#include <bb/data/JsonDataAccess>

#include <bb/pim/message/MessageService>
#include <bb/pim/account/AccountService>
#include <bb/pim/account/Account>

using namespace bb::device;
using namespace bb::cascades;
using namespace bb::system;
using namespace bb::pim;

class OneTimePassViewController  : public Page {
Q_OBJECT
public:
	OneTimePassViewController(QString phoneNo);
	Q_INVOKABLE void back();
	Q_INVOKABLE void requestOtp();
	Q_INVOKABLE void confirmOtp();
	virtual ~OneTimePassViewController();

private:
	int _sms_account_id;
	bb::pim::account::AccountService* _account_service;
	bb::pim::message::MessageService* _message_service;

	QString phoneNo;
	Timer *timer;
	TextField *codeField;
	Label *lblTimer;
	ActivityIndicator *indicator;
	Container *doneContainer;
	Container *waitContainer;
	int timePassed;
	OtpManager *otpManager;
	LoadingPopup *loadingPopup;

	int timeout;


private slots:
	void onTimerTimeout();
	void onOtpReplied(int status, QVariantMap qmap);
	void messageReceived(bb::pim::account::AccountKey accKey, bb::pim::message::ConversationKey convKey, bb::pim::message::MessageKey msgKey);

};

#endif /* OneTimePassViewController_H_ */

