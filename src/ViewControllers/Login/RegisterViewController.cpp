/*
 * RegisterViewController.cpp
 *
 *  Created on: Nov 2, 2014
 *      Author: yogi
 */

#include "RegisterViewController.h"
#include "LoginViewController.h"
#include "UiUtils.h"
#include "Terms.h"
#include "applicationui.hpp"
#include "Global.h"
#include "AccountManager.h"

RegisterViewController::RegisterViewController() {
	httpRequest = NULL;
	loadingPopup = NULL;

	QmlDocument *qml = UiUtils::LoadQml("login/RegistrasiView.qml", this);
	qml->setContextProperty("page", this);
	Container *root = qml->createRootObject<Container>();
	setContent(root);

	identitasField = root->findChild<TextField*>("identitasField");

	emailField = root->findChild<TextField*>("emailField");
	noHpField = root->findChild<TextField*>("noHpField");
	noHpField->setEnabled(false);
	namaDepanField = root->findChild<TextField*>("namaDepanField");
	namaBelakangField = root->findChild<TextField*>("namaBelakangField");
	alamatField = root->findChild<TextArea*>("alamatField");
	identitasField = root->findChild<TextField*>("identitasField");
	noIdentitasField = root->findChild<TextField*>("noIdentitasField");
	namaIbuField = root->findChild<TextField*>("namaIbuField");
	pinField = root->findChild<TextField*>("pinField");
	connect(pinField, SIGNAL(textChanging(QString) ), this,
				SLOT(onTextChanging(QString)));


	pin2Field = root->findChild<TextField*>("pin2Field");
	connect(pin2Field, SIGNAL(textChanging(QString) ), this,
					SLOT(onTextPin2Changing(QString)));

	priaField = root->findChild<ImageToggleButton*>("priaField");
	wanitaField = root->findChild<ImageToggleButton*>("wanitaField");
	agreeField = root->findChild<ImageToggleButton*>("agreeField");

	datePicker = root->findChild<DateTimePicker*>("datePicker");

	/*emailField->setText("yoginih@gmail.com");
	noHpField->setText("08123234133");
	namaDepanField->setText("aaa");
	namaBelakangField->setText("aaa");
	alamatField->setText("aaa");
	identitasField->setText("KTP");
	noIdentitasField->setText("aaa");
	namaIbuField->setText("aaa");
	pinField->setText("123456");
	pin2Field->setText("123456");
	priaField->setChecked(true);
	agreeField->setChecked(true);*/

}

void RegisterViewController::onTextChanging(QString text){
	if(text.length() > 6){
		QString newtext = text.left(6);
		pinField->setText(newtext);
	}
}
void RegisterViewController::onTextPin2Changing(QString text){
	if(text.length() > 6){
		pin2Field->setText(text.left(6));
	}
}

void RegisterViewController::setNoHp(QString text){
	noHpField->setText(text);
}

void RegisterViewController::openTipeIdentitas() {
	listTipeIdentitasDialog = new SystemListDialog("ok", this);
	listTipeIdentitasDialog->setTitle("Tipe Identitas");
	connect(listTipeIdentitasDialog, SIGNAL(finished(int) ), this,
			SLOT(onTipeIdentitasSelected(int)));

	listTipeIdentitasDialog->appendItem("KTP");
	listTipeIdentitasDialog->appendItem("SIM");
	listTipeIdentitasDialog->appendItem("KITAS");
	listTipeIdentitasDialog->show();

}
void RegisterViewController::onTipeIdentitasSelected(int val) {
	if (listTipeIdentitasDialog->selectedIndices().size() == 0)
		return;

	qDebug() << ">>val: ";
	qDebug() << listTipeIdentitasDialog->selectedIndices()[0];
	switch (listTipeIdentitasDialog->selectedIndices()[0]) {
	case 0: {
		identitasField->setText("KTP");
		break;
	}
	case 1: {
		identitasField->setText("SIM");
		break;
	}
	case 2:
		identitasField->setText("KITAS");
		break;

	default:
		break;
	}

}
void RegisterViewController::openDatePicker() {

}
void RegisterViewController::daftar() {
	/*if(emailField->text().trimmed().length() == 0){
		UiUtils::toast("Mohon isi Email", this);
		return;
	}*/
	if(noHpField->text().trimmed().length() == 0){
		UiUtils::toast("Mohon isi no handphone", this);
		return;
	}
	if(namaDepanField->text().trimmed().length() == 0){
		UiUtils::toast("Mohon isi Nama Depan", this);
		return;
	}
	/*if(namaBelakangField->text().trimmed().length() == 0){
		UiUtils::toast("Mohon isi Nama Belakang", this);
		return;
	}*/
	if(priaField->isChecked() == false && wanitaField->isChecked() == false){
		UiUtils::toast("Mohon isi Jenis Kelamin", this);
				return;
	}
	/*if(alamatField->text().trimmed().length() == 0){
		UiUtils::toast("Mohon isi Alamat", this);
		return;
	}
	if(identitasField->text().trimmed().length() == 0){
		UiUtils::toast("Mohon isi Tipe Identitas", this);
		return;
	}
	if(noIdentitasField->text().trimmed().length() == 0){
		UiUtils::toast("Mohon isi No Identitas", this);
		return;
	}*/
	if(namaIbuField->text().trimmed().length() == 0){
		UiUtils::toast("Mohon isi Nama Ibu", this);
		return;
	}
	if(pinField->text().trimmed().length() == 0){
		UiUtils::toast("Mohon isi PIN", this);
		return;
	}
	if(pin2Field->text().trimmed().compare(pinField->text().trimmed()) != 0){
		UiUtils::toast("Mohon isi PIN dan Konfirmasi PIN harus sama", this);
		return;
	}
	if(agreeField->isChecked() == false){
		UiUtils::toast("Centang tanda Setuju dengan Syarat & Ketentuan ", this);
		return;
	}

	if(httpRequest == NULL){
			httpRequest = new HttpRequest(this);
				connect(httpRequest, SIGNAL(httpReply(QByteArray)), this,
						SLOT(httpReply(QByteArray)));
	}
	if (loadingPopup == NULL) {
		loadingPopup = new LoadingPopup(this, "Processing");
	}
	loadingPopup->showLoading();

	QDateTime selectedDate = datePicker->value();
	AccountManager *accManager = new AccountManager;
		accManager->getUser()->setPin(pinField->text());
		accManager->getUser()->setNoTlp(noHpField->text());
		accManager->getUser()->setUserId(USER_ID);
	QString tipeid;
	if(identitasField->text().compare("KTP") == 0) tipeid = "1";
	else if(identitasField->text().compare("SIM") == 0) tipeid = "2";
	else tipeid = "3";

	/*QString url = API_REGISTER + "?userid=" + USER_ID
			+"&firstname=" + namaDepanField->text()
			+"&lastname=" + namaBelakangField->text()
			+"&gender=" + (priaField->isChecked()?"1":"2")
			+"&msisdn=" + noHpField->text()
			+"&idtype=" + tipeid
			+"&idnumber=" + noIdentitasField->text()
			+"&address=" + alamatField->text()
			+"&mothername=" + namaIbuField->text()
			+"&email=" + emailField->text()
			+"&dob=" + selectedDate.toString("ddmmyyyy")
			+"&signature=" + accManager->getEncodedSignature()
			;
	httpRequest->get(url);*/

	QUrl postData;
		postData.addQueryItem("userid", USER_ID);
		postData.addQueryItem("firstname", namaDepanField->text());
		postData.addQueryItem("lastname", namaBelakangField->text());
		postData.addQueryItem("gender",  (priaField->isChecked()?"1":"2"));
		postData.addQueryItem("msisdn", noHpField->text());
		postData.addQueryItem("idtype", tipeid);
		postData.addQueryItem("idnumber", noIdentitasField->text());
		postData.addQueryItem("address", alamatField->text());
		postData.addQueryItem("mothername", namaIbuField->text());
		postData.addQueryItem("email", emailField->text());
		postData.addQueryItem("dob", selectedDate.toString("ddMMyyyy"));
		postData.addQueryItem("signature", accManager->getSignature());
		httpRequest->post(API_REGISTER, postData);

		qDebug() << ">>register: " << API_REGISTER;
		qDebug() << ">>postData: " << postData;

}
void RegisterViewController::httpReply(QByteArray data) {
	loadingPopup->hideLoading();
	if (!data.isNull()) {
		QString dataStr(data);
		qDebug() << ">>regis: " << dataStr;
		JsonDataAccess jda;
		QVariantMap qmap = jda.loadFromBuffer(data).value<QVariantMap>();
		if(qmap.value("status").toInt() == 0)
		{
			QSettings settings;
			settings.setValue(SHOW_OTP, QVariant("1"));

			/*LoginViewController *loginView = new LoginViewController;
			loginView->setNoHp(noHpField->text());
				ApplicationUI::nav->push(loginView);*/
			SystemDialog *dialog; // SystemDialog uses the BB10 native dialog.
			dialog = new SystemDialog(tr("OK"), this); // New dialog with on 'Ok' button, no 'Cancel' button
			dialog->setTitle(tr("Register Berhasil")); // set a title for the message
			dialog->setBody("Registrasi Berhasil, Silahkan login kembali dengan Nomor HP dan pin yang telah anda daftarkan"); // set the message itself
			dialog->setDismissAutomatically(true); // Hides the dialog when a button is pressed.

			// Setup slot to mark the dialog for deletion in the next event loop after the dialog has been accepted.
			bool ok = connect(dialog,
					SIGNAL(finished(bb::system::SystemUiResult::Type)), this,
					SLOT(onAlertDismiss()));
			Q_ASSERT(ok);
			Q_UNUSED(ok);
			dialog->show();
		}
		else
		{
			UiUtils::alert(qmap.value("msg").toString(), this);
		}

	}
  else {
		UiUtils::alert(WORDING_KONEKSI_GAGAL, this);
	}
}
void RegisterViewController::onAlertDismiss() {
	ApplicationUI::popAllPage();
	LoginViewController *loginView = new LoginViewController;
	loginView->setNoHp(noHpField->text());
	ApplicationUI::nav->push(loginView);
}
void RegisterViewController::back() {
	ApplicationUI::nav->pop();
}
void RegisterViewController::showTermsView() {
	ApplicationUI::nav->push(new Terms);
}

RegisterViewController::~RegisterViewController() {
	// TODO Auto-generated destructor stub
}

