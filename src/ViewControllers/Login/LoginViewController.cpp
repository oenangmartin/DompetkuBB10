/*
 * LoginViewController.cpp
 *
 *  Created on: Nov 2, 2014
 *      Author: yogi
 */

#include "LoginViewController.h"
#include "UiUtils.h"
#include "RegisterViewController.h"
#include "HomeViewController.h"
#include "applicationui.hpp"
#include "TripleDES.h"
#include "Global.h"
#include "Terms.h"
#include "UiUtils.h"
#include "LupaPinViewController.h"
#include "AccountManager.h"
#include "VerifikasiNoViewController.h"
/*
 signA = timestamp + pin
 signB = pin reversed + msisdn/email
 signC = signA|signB
 Signature = tripleDES(signC)

 Contoh:
 signA = 1417315314440 + 123456
 signA = 1416552660486 + 123456
 signB = 654321 + 0816775009

 signC = 1416552660486123456|6543210816775009

 123456|6543210816775009


 Hasil encrypt:
 JIM82meO5l87e4JxdaDTs8GUmRmumVQeMzCSGsxSeobVOnlkOL5ExQ==
 JIM82meO5l87e4JxdaDTs8GUmRmumVQeMzCSGsxSeobVOnlkOL5ExQ==
 */

LoginViewController::LoginViewController() {
	httpRequest = NULL;
	loadingPopup = NULL;

	QmlDocument *qml = UiUtils::LoadQml("login/LoginView.qml", this);
	qml->setContextProperty("page", this);
	Container *container = qml->createRootObject<Container>();
	setContent(container);

	nomorAkunTextField = container->findChild<TextField*>("nomorAkun");
	nomorAkunTextField->setEnabled(false);
	pinTextField = container->findChild<TextField*>("pin");
	connect(pinTextField, SIGNAL(textChanging(QString) ), this,
						SLOT(onTextPinChanging(QString)));

	QSettings settings;

	if (!settings.value(LAST_NO_AKUN).isNull()) {
		nomorAkunTextField->setText(settings.value(LAST_NO_AKUN).toString());
	}



	/*QString urlAction1 = "http://114.4.68.110:14567/mfsmw/webapi/airtime_inquiry?userid=web_api_test&extRef=1234567890&operatorName=Telkomsel";
	QString url = urlAction1.left(urlAction1.indexOf('?'));
	qDebug() << ">>url: " << url;
	QString param = str.right(str.length() - str.indexOf('?', 0, Qt::CaseInsensitive)-1);
	qDebug() << ">>param: " << param;
	QRegExp rx("[&]");
	QStringList list = param.split(rx, QString::SkipEmptyParts);
	qDebug() << ">>list: " << list;
	for (int i = 0; i < list.length(); i++)
	{
		QString parameter = list.at(i);
		QString paramName = parameter.left(parameter.indexOf('=', 0, Qt::CaseInsensitive));
		QString paramValue = parameter.right(parameter.length() - parameter.indexOf('=', 0, Qt::CaseInsensitive)-1);
		qDebug() << ">>paramName: " << paramName;
		qDebug() << ">>paramValue: " << paramValue;

	}*/

}

void LoginViewController::onTextPinChanging(QString text){
	if(text.length() > 6){
		pinTextField->setText(text.left(6));
	}
}

void LoginViewController::setNoHp(QString text){
	nomorAkunTextField->setText(text);
}
void LoginViewController::login() {
	qDebug() << ">>login";

	if(nomorAkunTextField->text().trimmed().length() == 0){
		UiUtils::toast("Mohon isi No HP", this);
		nomorAkunTextField->requestFocus();
		return;
	}
	if(pinTextField->text().trimmed().length() == 0){
		UiUtils::toast("Mohon isi PIN", this);
		pinTextField->requestFocus();
		return;
	}

	if (httpRequest == NULL) {
		httpRequest = new HttpRequest(this);
		connect(httpRequest, SIGNAL(httpReply(QByteArray)), this,
				SLOT(httpReply(QByteArray)));
	}

	if (loadingPopup == NULL)
		loadingPopup = new LoadingPopup(this, "Loading");

	loadingPopup->showLoading();

	//TripleDES *tdes = new TripleDES;

	ApplicationUI::accManager = AccountManager::getInstance();
	AccountManager *accManager = ApplicationUI::accManager;
	accManager->getUser()->setPin(pinTextField->text());
	accManager->getUser()->setNoTlp(nomorAkunTextField->text());
	accManager->getUser()->setUserId(USER_ID);

	/*QString pin = pinTextField->text();
	 QString hp = nomorAkunTextField->text();
	 QString reversePin = "";
	 for (int i = pin.length() - 1; i >= 0; i--)
	 reversePin.append(pin.at(i));

	 QString signA = QString::number(QDateTime::currentMSecsSinceEpoch()) + pin;
	 QString signB = reversePin + hp;
	 QString signC = signA + "|" + signB;
	 QString signature = tdes->encrypt(signC);*/

	/*QString url = API_LOGIN + "?signature="
			+ (accManager->getEncodedSignature()) + "&userid="
			+ accManager->getUser()->getUserId();
	//httpRequest->get("http://114.4.68.110:14567/mfsmw/webapi/login?signature=dCikmtZ90C6jBN88aTxTsIb9VrKmHU2BP2ReXf1VRQ+p5b6eVq4SbA==&userid=web_api_test");
	//httpRequest->get(url);*/

	QUrl postData;
	postData.addQueryItem("userid", accManager->getUser()->getUserId());
	postData.addQueryItem("signature", accManager->getSignature());
	httpRequest->post(API_LOGIN, postData);


	/*qDebug() << ">>signA: " << signA;
	 qDebug() << ">>signB: " << signB;
	 qDebug() << ">>signC: " << signC;*/
	//qDebug() << ">>tdes: " << tdes->encrypt(signC);
	//qDebug() << ">>url: " << url;

}

void LoginViewController::httpReply(QByteArray data) {
	loadingPopup->hideLoading();
	SystemToast *toast = new SystemToast(this);

	if (!data.isNull()) {

		QString dataStr(data);
		qDebug() << ">>data: " << dataStr;

		JsonDataAccess jda;
		QVariantMap qmap = jda.loadFromBuffer(data).value<QVariantMap>();

		if (qmap.value("status").toInt() == 0) {
			QSettings settings;
			settings.setValue(LAST_NO_AKUN, QVariant(nomorAkunTextField->text().trimmed()));
			//ApplicationUI::accManager->getUser()->setNoTlp(qmap.value("accountId").toString());
			ApplicationUI::accManager->save();
			ApplicationUI::homeViewController = new HomeViewController;
			ApplicationUI::popAllPage();
			ApplicationUI::nav->push(ApplicationUI::homeViewController);
		}
		else if (qmap.value("status").toInt() == -14) {
			UiUtils::alert("PIN Anda telah terblokir. SIlahkan menghubungi Customer service Indosat di 100/111", this);
		}
		else if (qmap.value("status").toInt() == 1014) {
			UiUtils::alert("No HP atau PIN salah. Periksa kembali No HP dan PIN Anda. ", this);
		}
		else {
			toast->setBody(qmap.value("msg").toString());
			toast->show();
		}
	} else {
		qDebug() << ">>data null";
		toast->setBody("Koneksi gagal. Cobalah beberapa saat lagi.");
		toast->show();
	}
}

void LoginViewController::showRegisterView() {
	ApplicationUI::nav->push(new RegisterViewController);
	//VerifikasiNoViewController *verifikasiNo = new VerifikasiNoViewController;
	//ApplicationUI::nav->push(verifikasiNo);
}

void LoginViewController::showTerms() {
	ApplicationUI::nav->push(new Terms);
}

void LoginViewController::showLupaPinView() {
	ApplicationUI::nav->push(new LupaPinViewController);
}

LoginViewController::~LoginViewController() {
	// TODO Auto-generated destructor stub
}

