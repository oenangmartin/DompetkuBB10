/*
 * CariKodeBank.cpp
 *
 *  Created on: Nov 30, 2014
 *      Author: yogi
 */

#include "CariKodeBank.h"
#include "applicationui.hpp"
#include "UiUtils.h"

CariKodeBank::CariKodeBank() {

	QmlDocument *qml = UiUtils::LoadQml("CariKodeBank.qml", this);
	qml->setContextProperty("page", this);
	Container *root = qml->createRootObject<Container>();
	setContent(root);

	searchText = root->findChild<TextField*>("searchText");
	connect(searchText, SIGNAL(textChanging(QString)), this,
				SLOT(onSearchTextChanged(QString)));

	listView = root->findChild<ListView*>("list");
	if (listView) {
		connect(listView, SIGNAL(triggered(const QVariantList)), this, SLOT(onListClicked(const QVariantList)));
		JsonDataAccess jda;
		bankList =
				jda.load(
						QDir::currentPath()
								+ "/app/native/assets/models/KodeBank.json").value<
						QVariantList>();
		dataModel = new GroupDataModel();
		dataModel->setParent(this);
		dataModel->insertList(bankList);
		dataModel->setGrouping(ItemGrouping::None);
		dataModel->setSortingKeys( QStringList() << "kode");
		dataModel->setSortedAscending(true);
		listView->setDataModel(dataModel);
	}

}

void CariKodeBank::onSearchTextChanged(QString text){
	qDebug() << ">>text: " << text;
	resultList.clear();
	for(int i = 0; i < bankList.size(); i++)
	{
		QVariantMap qmap = bankList.at(i).toMap();
		QString bankName = qmap.value("nama").toString();
		if(bankName.contains(text, Qt::CaseInsensitive))
			resultList.append(qmap);
	}
	dataModel->clear();
	dataModel->insertList(resultList);

}
void CariKodeBank::onListClicked(QVariantList indexPath) {
	QVariantMap map = dataModel->data(indexPath).toMap();
	emit bankSelected(map.value("kode").toString(), map.value("nama").toString());
	ApplicationUI::nav->pop();
}

void CariKodeBank::back() {
	ApplicationUI::nav->pop();
}

CariKodeBank::~CariKodeBank() {
	// TODO Auto-generated destructor stub
}

