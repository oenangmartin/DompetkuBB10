/*
 * GantiPinViewController.h
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#ifndef GantiPinViewController_H_
#define GantiPinViewController_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include <bb/cascades/ListView>
#include <bb/cascades/TextField>
#include "DompetKuPage.h"
#include "LoadingPopup.h"
#include "HttpRequest.h"
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>

using namespace bb::data;
using namespace bb::cascades;

class GantiPinViewController : public DompetKuPage{
	Q_OBJECT
public:
	GantiPinViewController();
	virtual ~GantiPinViewController();

	Q_INVOKABLE void submit();

private:
	LoadingPopup *loadingPopup;
	TextField *pinLamaField;
	TextField *pinBaruField;
	TextField *pinBaruUlangField;
	HttpRequest *httpRequest;

private slots:
	void onHttpReply(QByteArray data);
	void onTextPinChanging(QString text);
	void onTextPinBaruChanging(QString text);
	void onTextPinBaru2Changing(QString text);
};

#endif /* GantiPinViewController_H_ */
