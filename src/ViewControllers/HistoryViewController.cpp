/*
 * HistoryViewController.cpp
 *
 *  Created on: Jan 11, 2015
 *      Author: yogi
 */

/*
 * HistoryViewController.cpp
 *
 *  Created on: Jan 11, 2015
 *      Author: yogi
 */

/*
 * HistoryViewController.cpp
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#include "HistoryViewController.h"
#include "Global.h"
#include "UiUtils.h"
#include "applicationui.hpp"
#include <bb/system/InvokeQueryTargetsReply>
#include <bb/system/InvokeQueryTargetsRequest>
#include <bb/system/InvokeReply>
#include <bb/system/InvokeRequest>
#include <bps/navigator.h>

HistoryViewController::HistoryViewController() {
	httpRequest = NULL;

	setTitle("Histori Transaksi");
	//bottomMenuController->getButtonToken()->setSelected(true);
	setBottomMenuVisible(false);
	QmlDocument *qml = UiUtils::LoadQml("informasi/HistoriTransaksi.qml", this);
	qml->setContextProperty("page", this);

	Container* pageContent = qml->createRootObject<Container>();
	pageContent->setImplicitLayoutAnimationsEnabled(false);

	contentContainer->add(pageContent);

	httpRequest = new HttpRequest(this);
	connect(httpRequest, SIGNAL(httpReply(QByteArray)), this,
			SLOT(onHttpReply(QByteArray)));

	listView = pageContent->findChild<ListView*>("list");
	connect(listView, SIGNAL(triggered(const QVariantList)), this,
			SLOT(onListClicked(const QVariantList)));

	getHistory();

}
//0816775009
void HistoryViewController::getHistory() {
	indicator->start();
	AccountManager *accManager = ApplicationUI::accManager;
	/*QString url = API_HISTORY_TRANSACTION + "?signature="
			+ accManager->getEncodedSignature() + "&userid="
			+ accManager->getUser()->getUserId() + "&to="
			+ accManager->getUser()->getNoTlp() + "&count=50";
	qDebug() << ">>url: " << url;
	httpRequest->get(url);*/

	QUrl postData;
	postData.addQueryItem("userid", accManager->getUser()->getUserId());
	postData.addQueryItem("signature", accManager->getSignature());
	postData.addQueryItem("to", accManager->getUser()->getNoTlp());
	postData.addQueryItem("count", "50");
	httpRequest->post(API_HISTORY_TRANSACTION, postData);
}

void HistoryViewController::onHttpReply(QByteArray data) {
	indicator->stop();
	if (!data.isNull()) {
		QString dataStr(data);
		qDebug() << ">>onHttpReply: " << dataStr;
		JsonDataAccess jda;
		QVariantMap qmap = jda.loadFromBuffer(data).value<QVariantMap>();
		QVariantList list = qmap.value("trxList").toList();
		if (list.size() == 0) {
			UiUtils::toast("Tidak ada transaksi", this);
			return;
		}
		dataModel = new GroupDataModel();
		dataModel->setParent(this);
		dataModel->insertList(list);
		dataModel->setGrouping(ItemGrouping::None);
		dataModel->setSortedAscending(false);
		listView->setDataModel(dataModel);

	} else {
		UiUtils::alert(WORDING_KONEKSI_GAGAL, this);
	}
}
void HistoryViewController::onListClicked(QVariantList indexPath) {
	QVariantMap map = dataModel->data(indexPath).toMap();
	QString url = map.value("list_merchant_url").toString();
	navigator_invoke(url.toStdString().c_str(), 0);
}

HistoryViewController::~HistoryViewController() {
	// TODO Auto-generated destructor stub
}

