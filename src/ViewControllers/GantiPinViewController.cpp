/*
 * GantiPinViewController.cpp
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#include "GantiPinViewController.h"
#include "Global.h"
#include "UiUtils.h"
#include "LoginViewController.h"
#include "applicationui.hpp"

GantiPinViewController::GantiPinViewController() {
	httpRequest = NULL;
	loadingPopup = NULL;

	setTitle("Ganti PIN");
	setBottomMenuVisible(false);
	QmlDocument *qml = QmlDocument::create("asset:///GantiPinPage.qml").parent(this);
	qml->setContextProperty("page", this);

	Container* pageContent = qml->createRootObject<Container>();
	pageContent->setImplicitLayoutAnimationsEnabled(false);

	contentContainer->add(pageContent);

	pinLamaField = pageContent->findChild<TextField*>("pinLamaField");
	connect(pinLamaField, SIGNAL(textChanging(QString) ), this,
												SLOT(onTextPinChanging(QString)));
	pinBaruField = pageContent->findChild<TextField*>("pinBaruField");
	connect(pinBaruField, SIGNAL(textChanging(QString) ), this,
												SLOT(onTextPinBaruChanging(QString)));
	pinBaruUlangField = pageContent->findChild<TextField*>("pinBaruUlangField");
	connect(pinBaruUlangField, SIGNAL(textChanging(QString) ), this,
												SLOT(onTextPinBaru2Changing(QString)));

}

void GantiPinViewController::onTextPinChanging(QString text){
	if(text.length() > 6){
		pinLamaField->setText(text.left(6));
	}
}
void GantiPinViewController::onTextPinBaruChanging(QString text){
	if(text.length() > 6){
		pinBaruField->setText(text.left(6));
	}
}
void GantiPinViewController::onTextPinBaru2Changing(QString text){
	if(text.length() > 6){
		pinBaruUlangField->setText(text.left(6));
	}
}

void GantiPinViewController::submit(){
	if(pinLamaField->text().trimmed().length() == 0){
		UiUtils::toast("Mohon isi PIN Lama", this);
		pinLamaField->requestFocus();
		return;
	}
	if(pinBaruField->text().trimmed().length() == 0){
		UiUtils::toast("Mohon isi PIN Baru", this);
		pinBaruField->requestFocus();
		return;
	}
	if(pinBaruField->text().trimmed().compare(pinLamaField->text().trimmed()) == 0){
		UiUtils::toast("PIN lama & PIN Baru tidak boleh sama", this);
		pinBaruField->requestFocus();
		return;
	}
	if(pinBaruUlangField->text().trimmed().length() == 0){
		UiUtils::toast("Mohon isi PIN Baru", this);
		pinBaruUlangField->requestFocus();
		return;
	}
	if(pinBaruUlangField->text().trimmed().compare(pinBaruField->text().trimmed()) != 0){
		UiUtils::toast("Mohon isi PIN dan Konfirmasi PIN harus sama", this);
		pinBaruUlangField->requestFocus();
		return;
	}
	if(httpRequest == NULL){
		httpRequest = new HttpRequest(this);
			connect(httpRequest, SIGNAL(httpReply(QByteArray)), this,
					SLOT(onHttpReply(QByteArray)));
	}
	if (loadingPopup == NULL) {
		loadingPopup = new LoadingPopup(this, "Processing");
	}
	loadingPopup->showLoading();

	/*QString url = API_CHANGE_PIN + "?signature="
					+ ApplicationUI::accManager->getEncodedSignature() + "&userid=" + USER_ID + "&pin=" + pinBaruField->text().trimmed();
			qDebug() << ">>url: " << url;
			httpRequest->get(url);*/

			QUrl postData;
					postData.addQueryItem("userid", USER_ID);
					postData.addQueryItem("signature", ApplicationUI::accManager->generateSignature(pinLamaField->text().trimmed()));
					postData.addQueryItem("pin", pinBaruField->text().trimmed());
					httpRequest->post(API_CHANGE_PIN, postData);
}
void GantiPinViewController::onHttpReply(QByteArray data) {
	loadingPopup->hideLoading();
	if (!data.isNull()) {
		QString dataStr(data);
		qDebug() << ">>onHttpReply: " << dataStr;
		JsonDataAccess jda;
		QVariantMap qmap = jda.loadFromBuffer(data).value<QVariantMap>();
		int status = qmap.value("status").toInt();
		if (status == 0)
		{
			//ApplicationUI::showBerandaPage("PIN berhasil diganti");
		    UiUtils::toast("PIN berhasil diganti", this);
			while (ApplicationUI::nav->count() > 0)
			        ApplicationUI::nav->pop();
            LoginViewController *loginView = new LoginViewController;
            ApplicationUI::nav->push(loginView);
		}
		else if(status == 1011){
			UiUtils::alert("PIN atau agen tidak ditemukan atau salah", this);
		}
		else if(status == 1012){
			UiUtils::alert("PIN error, percobaan melebihi batas", this);
		}
		else if(status == 1013){
			UiUtils::alert("PIN sudah kadaluarsa", this);
		}
		else if(status == 1014){
			UiUtils::alert("PIN salah", this);
		}
		else if(status == 1015){
			UiUtils::alert("Penggantian PIN – PIN baru sama dengan PIN lama", this);
		}
		else if(status == 1016){
			UiUtils::alert("Penggantian PIN – PIN baru sama dengan PIN lama", this);
		}
		else if(status == 1016){
			UiUtils::alert("Penggantian PIN – PIN sudah dipakai sebelumnya", this);
		}
		else
		{
			UiUtils::alert(qmap.value("msg").toString(), this);
		}

	}
  else {
		UiUtils::alert(WORDING_KONEKSI_GAGAL, this);
	}
}

GantiPinViewController::~GantiPinViewController() {
	// TODO Auto-generated destructor stub
}

