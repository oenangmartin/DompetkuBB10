/*
 * FormBeliBayar.cpp
 *
 *  Created on: Nov 16, 2014
 *      Author: yogi
 */

#include "FormBeliBayar.h"
#include "Form.h"
#include "ApiEngine.h"
#include "AccountManager.h"
#include "applicationui.hpp"
#include "UiUtils.h"
#include "Global.h"
#include "JSON.h"
#include "InboxData.h"
#include <bb/cascades/StackLayout>
#include <bb/cascades/DockLayout>
#include <bb/cascades/LayoutOrientation>


#include <bb/cascades/DropDown>
FormBeliBayar::FormBeliBayar(QVariantMap qmap, int formType, QString namaItem) {
	this->formType = formType;
	this->namaItem = namaItem;
	jsonData = JSON::instance().serialize(qmap);
	submit1HttpRequest = NULL;
	submit2HttpRequest = NULL;
	loadingPopup = NULL;
	prompt = NULL;
	contactPicker = NULL;
	favoritController = NULL;
	favoritNumber = "";
	nominal = "";
	onlyOneUrl = false;
	mTo = "";
	mAmount = "";
	mDenom = "";
	price = "";
	voucherCode = "";
	fieldToExist = false;

	qDebug() << ">>mapp: " << qmap;
	setTitle(qmap.value("sub_merchant_nama").toString());
	QmlDocument *qml = QmlDocument::create(
			"asset:///bottomMenu/FormBeliBayar.qml").parent(this);
	qml->setContextProperty("page", this);
	Container* pageContent = qml->createRootObject<Container>();
	contentContainer->add(pageContent);

	formContainer = pageContent->findChild<Container*>("formContainer");

	if(formType == TYPE_BAYAR)
	{
		ImageButton *submitButton = pageContent->findChild<ImageButton*>("submitButton");
		if(submitButton) submitButton->setDefaultImageSource(QUrl("asset:///images/button/button-bayar.png"));
	}


	QString sub_merchant_id = qmap.value("sub_merchant_id").toString();

	/*char *slot = "onListFieldReturned(QByteArray)";
	 ApiEngine *apiEngine = new ApiEngine;
	 apiEngine->getListField(sub_merchant_id, this, slot);*/

	HttpRequest *httpRequest = new HttpRequest(this);
	connect(httpRequest, SIGNAL(httpReply(QByteArray)), this,
			SLOT(onListFieldReturned(QByteArray)));

	httpRequest->get(
			API_FIELD + AccountManager::getInstance()->getUser()->getToken()
					+ "/" + sub_merchant_id);
	indicator->start();
	qDebug() << ">>data: "
			<< API_FIELD + AccountManager::getInstance()->getUser()->getToken()
					+ "/" + sub_merchant_id;

}


void FormBeliBayar::hitUrl1(){
	QUrl postData;
	for (int i = 0; i < fieldVector.size(); i++)
	{
		TextField *textField = qobject_cast<TextField *>(fieldVector.at(i));
		if (textField != NULL) {
				if(listField.at(i).toMap().value("param_name").toString().compare("pin", Qt::CaseInsensitive) == 0)
				{
					continue;
				}
				postData.addQueryItem(listField.at(i).toMap().value("param_name").toString(), textField->text().trimmed());

				if(listField.at(i).toMap().value("param_name").toString().compare("amount") == 0)
				{
					postData.addQueryItem("denom", textField->text().trimmed());
				}
				if(listField.at(i).toMap().value("param_name").toString().compare("denom") == 0)
				{
					postData.addQueryItem("amount", textField->text().trimmed());
				}
		} else {
			DateTimePicker *dateField = qobject_cast<DateTimePicker *>(fieldVector.at(i));
			if (dateField != NULL) {
				QDateTime selectedDate = dateField->value();
				postData.addQueryItem(listField.at(i).toMap().value("param_name").toString(), selectedDate.toString("ddMMyyyy"));
			}
			else{
				DropDown *dropDown = qobject_cast<DropDown *>(fieldVector.at(i));
				if (dropDown->selectedIndex() >= 0)
				{
					qDebug() << ">>text drop: "
							<< dropDown->selectedOption()->text();
					QString str =
							listField.at(i).toMap().value("id_isi_dropdown").toString();
					QRegExp rx("[;]");
					QStringList list = str.split(rx, QString::SkipEmptyParts);
					postData.addQueryItem(listField.at(i).toMap().value("param_name").toString(), list.at(dropDown->selectedIndex()));
					if(listField.at(i).toMap().value("param_name").toString().compare("amount") == 0)
					{
						postData.addQueryItem("denom", list.at(dropDown->selectedIndex()));
					}
					if(listField.at(i).toMap().value("param_name").toString().compare("denom") == 0)
					{
						postData.addQueryItem("amount", list.at(dropDown->selectedIndex()));
					}
				}
			}
		}
	}
	if (loadingPopup == NULL) {
			loadingPopup = new LoadingPopup(this, "Processing");
		}
		loadingPopup->showLoading();
		if (submit1HttpRequest == NULL) {
			submit1HttpRequest = new HttpRequest(this);
			connect(submit1HttpRequest, SIGNAL(httpReply(QByteArray)), this,
					SLOT(onUrlAction1Returned(QByteArray)));
		}

		/*QString url = urlAction1 + params + "&signature="+ ApplicationUI::accManager->getEncodedSignature();
		qDebug() << ">>url1: " << url;
		submit1HttpRequest->get(url);*/

		//postData.addQueryItem("signature", ApplicationUI::accManager->getSignature());

		QString url = urlAction1;
		if(urlAction1.indexOf('?') > 0)
		{
			url = urlAction1.left(urlAction1.indexOf('?'));
			QString param = urlAction1.right(urlAction1.length() - urlAction1.indexOf('?', 0, Qt::CaseInsensitive)-1);
			QRegExp rx("[&]");
			QStringList list = param.split(rx, QString::SkipEmptyParts);
			qDebug() << ">>list: " << list;
			for (int i = 0; i < list.length(); i++)
			{
				QString parameter = list.at(i);
				QString paramName = parameter.left(parameter.indexOf('=', 0, Qt::CaseInsensitive));
				QString paramValue = parameter.right(parameter.length() - parameter.indexOf('=', 0, Qt::CaseInsensitive)-1);
				//qDebug() << ">>paramName: " << paramName;
				//qDebug() << ">>paramValue: " << paramValue;
				postData.addQueryItem(paramName, paramValue);
			}
		}
		postData.addQueryItem("signature", ApplicationUI::accManager->generateSignature(pin));
		submit1HttpRequest->post(url, postData);
		qDebug() << ">>d: " << postData;
}
void FormBeliBayar::hitUrl2(){
	if (submit2HttpRequest == NULL) {
		submit2HttpRequest = new HttpRequest(this);
		connect(submit2HttpRequest, SIGNAL(httpReply(QByteArray)), this,
				SLOT(onUrlAction2Returned(QByteArray)));
	}

	/*QString url = urlAction2 + "&transid=" + trxid + "&signature="
			+ ApplicationUI::accManager->getEncodedSignature();
	qDebug() << ">>url1: " << url;*/
	loadingPopup->showLoading();
	//submit2HttpRequest->get(url);

	QUrl postData;
	//postData.addQueryItem("userid", ApplicationUI::accManager->getUser()->getUserId());
	postData.addQueryItem("signature", ApplicationUI::accManager->generateSignature(pin));
	postData.addQueryItem("transid", trxid);

	if(mTo.length() == 0)
	{
		qDebug() << ">>addto " << msisdn;
		postData.addQueryItem("to", msisdn);
	}
	if(formType == TYPE_BAYAR)
	{
		postData.addQueryItem("denom", price);
		postData.addQueryItem("price", price);
		postData.addQueryItem("amount", price);
	}

	QString url = urlAction1;
	if(urlAction2.indexOf('?') > 0)
	{
		url = urlAction2.left(urlAction2.indexOf('?'));

		QString param = urlAction2.right(urlAction2.length() - urlAction2.indexOf('?', 0, Qt::CaseInsensitive)-1);
		QRegExp rx("[&]");
		QStringList list = param.split(rx, QString::SkipEmptyParts);
		qDebug() << ">>list: " << list;
		for (int i = 0; i < list.length(); i++)
		{
			QString parameter = list.at(i);
			QString paramName = parameter.left(parameter.indexOf('=', 0, Qt::CaseInsensitive));
			QString paramValue = parameter.right(parameter.length() - parameter.indexOf('=', 0, Qt::CaseInsensitive)-1);
			//qDebug() << ">>paramName: " << paramName;
			//qDebug() << ">>paramValue: " << paramValue;
			postData.addQueryItem(paramName, paramValue);
		}
	}

	//add param from form
	for (int i = 0; i < fieldVector.size(); i++) {
			TextField *textField = qobject_cast<TextField *>(fieldVector.at(i));
			if (textField != NULL) {
				qDebug() << ">>text: " << textField->text();
				{
					if(listField.at(i).toMap().value("param_name").toString().compare("pin", Qt::CaseInsensitive) == 0)
					{

						continue;
					}
					postData.addQueryItem(listField.at(i).toMap().value("param_name").toString(), textField->text().trimmed());
					if(listField.at(i).toMap().value("param_name").toString().compare("amount") == 0)
					{
						postData.addQueryItem("denom", textField->text().trimmed());
					}
					if(listField.at(i).toMap().value("param_name").toString().compare("denom") == 0)
					{
							postData.addQueryItem("amount", price);
					}
				}
			} else {
				DropDown *dropDown = qobject_cast<DropDown *>(fieldVector.at(i));
				if (dropDown->selectedIndex() >= 0) {
					qDebug() << ">>text drop: "
							<< dropDown->selectedOption()->text();
					QString str =
							listField.at(i).toMap().value("id_isi_dropdown").toString();
					QRegExp rx("[;]");
					QStringList list = str.split(rx, QString::SkipEmptyParts);
					postData.addQueryItem(listField.at(i).toMap().value("param_name").toString(), list.at(dropDown->selectedIndex()));
					if(listField.at(i).toMap().value("param_name").toString().compare("amount") == 0)
					{
						postData.addQueryItem("denom", list.at(dropDown->selectedIndex()));
					}
					if(listField.at(i).toMap().value("param_name").toString().compare("denom") == 0)
					{
						postData.addQueryItem("amount", price);

					}
				}
			}
		}

	submit2HttpRequest->post(url, postData);
	qDebug() << ">>url: " << url;
	qDebug() << ">>postData: " << postData;
}

void FormBeliBayar::setFavoritNumber(QString number){
	favoritNumber = number;
}
//<?xml version="1.0" encoding="UTF-8" standalone="yes"?><result><extRef>1234567890,1234567890</extRef><msg>invalid userid</msg><status>-5</status></result>

void FormBeliBayar::submitForm() {
	bool errorExist = false;
	QString params = "";
	QUrl postData;
	promptOneUrlMessage = "";
	if(formType == TYPE_BELI)
		promptOneUrlMessage = "Anda akan membeli ";
	else
		promptOneUrlMessage = "Anda akan membayar ";

	promptOneUrlMessage += namaItem + " " +  getTitle() + " dengan detail sebagai berikut:\n";

	//validasi
	for (int i = 0; i < fieldVector.size(); i++)
	{
		TextField *textField = qobject_cast<TextField *>(fieldVector.at(i));
		if (textField != NULL) {
			qDebug() << ">>text: " << textField->text();
			if (textField->text().trimmed().length() == 0) {
				UiUtils::toast(
						"Mohon isi "
								+ listField.at(i).toMap().value("label").toString(),
						this);
				errorExist = true;
				break;
			} else {
				if(listField.at(i).toMap().value("param_name").toString().compare("pin", Qt::CaseInsensitive) == 0)
				{
					pin = textField->text().trimmed();
					continue;
				}
				params += "&"
						+ listField.at(i).toMap().value("param_name").toString()
						+ "=" + textField->text().trimmed();
				postData.addQueryItem(listField.at(i).toMap().value("param_name").toString(), textField->text().trimmed());
				promptOneUrlMessage += listField.at(i).toMap().value("label").toString() + ": " + textField->text().trimmed() + "\n";
				if(listField.at(i).toMap().value("param_name").toString().compare("amount") == 0)
				{
					mAmount = textField->text().trimmed();
					price = textField->text().trimmed();
					postData.addQueryItem("denom", textField->text().trimmed());
				}
				if(listField.at(i).toMap().value("param_name").toString().compare("denom") == 0)
				{
					mDenom = textField->text().trimmed();
					price = textField->text().trimmed();
					postData.addQueryItem("amount", textField->text().trimmed());
				}
				if(listField.at(i).toMap().value("param_name").toString().compare("to") == 0)
				{
					fieldToExist = true;
					mTo = textField->text().trimmed();
				}
			}
 		}
		else {
			DateTimePicker *dateField = qobject_cast<DateTimePicker *>(fieldVector.at(i));
			if (dateField != NULL) {
				QDateTime selectedDate = dateField->value();
				promptOneUrlMessage += listField.at(i).toMap().value("label").toString() + ": " + selectedDate.toString("dd MMMM yyyy") + "\n";
				postData.addQueryItem(listField.at(i).toMap().value("param_name").toString(), selectedDate.toString("ddMMyyyy"));
			}
			else{
				DropDown *dropDown = qobject_cast<DropDown *>(fieldVector.at(i));
				if (dropDown->selectedIndex() >= 0) {
					qDebug() << ">>text drop: "
							<< dropDown->selectedOption()->text();
					QString str = listField.at(i).toMap().value("id_isi_dropdown").toString();
					QRegExp rx("[;]");
					QStringList list = str.split(rx, QString::SkipEmptyParts);

					str = listField.at(i).toMap().value("isi_dropdown").toString();
					QStringList listIsi = str.split(rx, QString::SkipEmptyParts);
					params += "&"
							+ listField.at(i).toMap().value("param_name").toString()
							+ "=" + list.at(dropDown->selectedIndex());
					postData.addQueryItem(listField.at(i).toMap().value("param_name").toString(), list.at(dropDown->selectedIndex()));
					promptOneUrlMessage += listField.at(i).toMap().value("label").toString() + ": " + listIsi.at(dropDown->selectedIndex()) + "\n";
					if(listField.at(i).toMap().value("param_name").toString().compare("amount") == 0)
					{
						mAmount = listIsi.at(dropDown->selectedIndex());
						price = listIsi.at(dropDown->selectedIndex());
						postData.addQueryItem("denom", list.at(dropDown->selectedIndex()));
					}
					if(listField.at(i).toMap().value("param_name").toString().compare("denom") == 0)
					{
						nominal = "\nNominal: " + list.at(dropDown->selectedIndex());
						mDenom = listIsi.at(dropDown->selectedIndex());
						price = listIsi.at(dropDown->selectedIndex());
						postData.addQueryItem("amount", list.at(dropDown->selectedIndex()));
					}
				}
			}
		}
	}

	if (!errorExist)
	{
		if(onlyOneUrl)
		{
			//prompt
			showPrompt(promptOneUrlMessage);

		}
		else
			hitUrl1();

	}
}

void FormBeliBayar::onUrlAction1Returned(QByteArray data) {
	loadingPopup->hideLoading();
	if (!data.isNull()) {
		QString dataStr(data);
		qDebug() << ">>onUrlAction1Returned: " << dataStr;
		JsonDataAccess jda;
		QVariantMap qmap = jda.loadFromBuffer(data).value<QVariantMap>();
		int status = qmap.value("status").toInt();
		if (status == 0)
		{
			trxid = qmap.value("trxid").toString();
			if(trxid.length() == 0)
			{
				UiUtils::alert(dataStr, this);
			}
			else{
				if(onlyOneUrl){
					showSuccessMessage();
				}
				else{
					QString message = "";
					if(qmap.value("price").toString().length() > 0)
						price = qmap.value("price").toString();
					if(formType == TYPE_BELI)
						message = "Anda akan membeli ";
					else
						message = "Anda akan membayar ";

					message += namaItem + " " + getTitle() + " dengan detail sebagai berikut: ";

					/*if(mTo != null){
						if(menuType.equalsIgnoreCase("Beli"))
							detail = detail + "\nNo. Tujuan: "+mTo;
						else
							detail = detail + "\nNo. Pelanggan: "+mTo;
						}
					else{
						if(objs.getMsisdn()!=null) mTo = objs.getMsisdn();
						else if(objs.getName()!=null) mTo = objs.getName();
						else mTo = "-";
					}*/
					if(mTo.length() > 0)
					{
						if(formType == TYPE_BELI)
							message +="\nNo. Tujuan: " + mTo;
						else
							message +="\nNo. Pelanggan: " + mTo;
					}
					else
					{
						if(qmap.value("msisdn").toString().length() > 0)
						{
							mTo = qmap.value("msisdn").toString();
							msisdn = qmap.value("msisdn").toString();
						}
						else if(qmap.value("name").toString().length() > 0){
							mTo = qmap.value("name").toString();
						}
						else
							mTo = "-";

					}

					if(qmap.value("name").toString().length() > 0)
					{
						message +="\nNama: " + qmap.value("name").toString();
					}

					if(mDenom.length() > 0)
					{
						message +="\nNominal: " + mDenom;
					}

					if(qmap.value("price").toString().length() > 0)
					{
						QString harga = qmap.value("price").toString();
						if(qmap.value("fee").toString().length() > 0)
						{
							int hargaInt = harga.toInt();
							int feeInt = qmap.value("fee").toInt();
							hargaInt = hargaInt - feeInt;
							harga = QString::number(hargaInt);
						}

						if(formType == TYPE_BELI)
							message +="\nHarga: " + harga;
						else
							message +="\nJumlah: " + harga;

						if(mAmount.length() == 0) mAmount = qmap.value("price").toInt();

					}

					if(qmap.value("fee").toString().length() > 0)
					{
						message +="\nFee: " + qmap.value("fee").toString();
						message += "\nTotal: " + qmap.value("price").toString();
					}
					showPrompt(message);
				}
			}
		}
		else if(status == 1099){
			UiUtils::alert("Transaksi gagal.", this);
		}
		else if(status == 1001){
			UiUtils::alert("Saldo Anda tidak mencukupi untuk melakukan transaksi ini", this);
		}
		else if(status == 1012){
			UiUtils::alert("Pin anda telah terblokir. Silahkan menghubungi Customer Service Indosat di 100/111.", this);
		}
		else if(status == 1014){
			UiUtils::alert("Pin Salah, Silahkan Periksa kembali Pin anda", this);
		}
		else {
			UiUtils::alert("Transaksi gagal", this);
			//UiUtils::alert(qmap.value("msg").toString(), this);
		}
	} else {
		UiUtils::alert(WORDING_KONEKSI_GAGAL, this);
	}
}

void FormBeliBayar::showPrompt(QString message){
	if (prompt == NULL)
	{

		if (formType == TYPE_BELI) {
			prompt = new SystemDialog("Beli", "Batal");
			prompt->setTitle("Beli");
		} else {
			prompt = new SystemDialog("Bayar", "Batal");
			prompt->setTitle("Bayar");
		}

		connect(prompt,
				SIGNAL(finished(bb::system::SystemUiResult::Type)),
				this,
				SLOT(onPromptAnswered(bb::system::SystemUiResult::Type)));
		prompt->setParent(this);

	}
	prompt->setBody(message);
	prompt->show();
}

void FormBeliBayar::onUrlAction2Returned(QByteArray data) {
	loadingPopup->hideLoading();
	if (!data.isNull()) {
		QString dataStr(data);
		qDebug() << ">>onUrlAction2Returned: " << dataStr;
		JsonDataAccess jda;
		QVariantMap qmap = jda.loadFromBuffer(data).value<QVariantMap>();
		int status = qmap.value("status").toInt();
		if (status == 0) {
			voucherCode = qmap.value("voucherCode").toString();
			showSuccessMessage();
		}
		else if(status == 1001){
			UiUtils::alert("Saldo Anda tidak mencukupi untuk melakukan transaksi ini", this);
		}
		else if(status == 1014){
			UiUtils::alert("PIN salah. Mohon periksa kembali PIN Anda", this);
		}
		else {
			UiUtils::alert(qmap.value("msg").toString(), this);
		}

	} else {
		UiUtils::alert(WORDING_KONEKSI_GAGAL, this);
	}
}

void FormBeliBayar::showSuccessMessage(){
	/*
	 Anda telah melakukan
	 <if:Pembelian/Pembayaran>
		 <Kategori:pulsa/voucher game>
		 <biller> pada <tanggal:Current>
		 ke <to/name>
		 sebesar <amount/price>.
		 Opt: Kode Voucher: <Code Voucher>
			.RefID:<RefID/TransactionID>
	 */

	QString message = "Anda telah melakukan";
	if (formType == TYPE_BELI) message += " pembelian ";
	else message += " pembayaran ";
	message += namaItem + " ";
	message += getTitle();
	message += " pada tanggal ";
	QDateTime now = QDateTime::currentDateTime();
	message += now.toString("dd/MM/yyyy");

	InboxData data;
	data.setRefId(trxid);
	data.setToken("");
	data.setExpired("");
	data.setTanggal(now.toString("dd/MM/yyyy"));
	data.setAction((formType == TYPE_BELI?"Beli ":"Bayar ") + namaItem);
	data.setMerchant(getTitle());
	data.setTujuan("-");
	data.setJumlah(price);
	ApplicationUI::inboxController->add(data);
	ApplicationUI::homeViewController->refresh();

	//{"status":0,"trxid":"21695842","msg":"Success","price":"11000","billpayQueryState":false,"billpayState":false}

	//message += " ke ";


	if(fieldToExist && mTo.length() > 0)
		message += " ke " + mTo;

	if(price.length() > 0)
		message += " Sebesar " + price;

	if(voucherCode.length() > 0)
		message += "\nVoucher: " + voucherCode;



	if(trxid.length() > 0)
		message += "\nRefID: " + trxid;

	alert(message);
}

void FormBeliBayar::onPromptAnswered(bb::system::SystemUiResult::Type type) {
	if (type == SystemUiResult::ConfirmButtonSelection) {
		if(onlyOneUrl)
			hitUrl1();
		else
			hitUrl2();
	}
}

/*
 >>url:  "http://114.4.68.110:14567/mfsmw/webapi/airtime_commit"
>>postData:   QUrl( "?userid=web_api_test&signature=dF8pC/3ab5FDm+vvPfPzGsGUmRmumVQehVStsmH4vzJwrReyfidDtg==&transid=25070103&denom=6000&price=6000&userid=web_api_test&extRef=1234567890&operatorName=Indosat" )

 >>postData:   QUrl( "?signature=R10AHQ5U65jr22PTRzfoVsGUmRmumVQehVStsmH4vzJwrReyfidDtg==&transid=25070809&denom=6000&price=6000&userid=web_api_test&extRef=1234567890&operatorName=Indosat" )
>>onUrlAction2Returned:  "{"status":-98,"msg":"System Error : End Point Failed","billpayQueryState":false,"billpayState":false}"
alert:  "System Error : End Point Failed"
 */

void FormBeliBayar::onListFieldReturned(QByteArray data) {
	indicator->stop();
	if (!data.isNull()) {
		ImageButton *submitButton = contentContainer->findChild<ImageButton*>(
				"submitButton");
		submitButton->setVisible(true);

		QString dataStr(data);
		qDebug() << ">>data: " << dataStr;
		JsonDataAccess jda;

		//QVariantMap qmap = jda.loadFromBuffer(data).value<QVariantMap>();

		QVariantList qlist = jda.loadFromBuffer(data).value<QVariantList>();
		QVariantMap qmap = qlist.at(0).toMap();
		listField = qmap.value("detail_field").toList();
		urlAction1 = qmap.value("url_action1").toString();
		urlAction2 = qmap.value("url_action2").toString();
		if(urlAction2.length() == 0) onlyOneUrl = true;

		for (int i = 0; i < listField.size(); i++) {

			QVariantMap qmapField = listField.at(i).toMap();
			qDebug() << ">>label: " << qmapField.value("label").toString();
			Label *label = Label::create().text(
					qmapField.value("label").toString());
			label->setMultiline(true);
			label->setTopMargin(40);
			label->textStyle()->setFontSize(FontSize::PointValue);
			label->textStyle()->setFontSizeValue(8);

			formContainer->add(label);


			//textfield
			if (qmapField.value("field_tipe").toInt() == 0 && qmapField.value("textview_type").toInt() == 2) {
				DateTimePicker *dateTimePicker = DateTimePicker::create()
				    .title("")
				    .mode(DateTimePickerMode::Date);
				fieldVector.append(dateTimePicker);
				formContainer->add(dateTimePicker);

			}
			else if (qmapField.value("field_tipe").toInt() == 0) {
				/*
				 textview_type
				 0 untuk textivew biasa,
				 1 untuk textview contact,
				 2 untuk textview calender,
				 3 untuk textview PIN,
				 4 untuk no telepon,
				 5 untuk kode bank,
				 6 untuk nama stasiun.
				 */
				TextField *textField = TextField::create();
				textField->setHintText("");
				switch (qmapField.value("textview_type").toInt()) {
				case 1:
					textField->setInputMode(TextFieldInputMode::PhoneNumber);
					break;
				case 2:

					break;
				case 3:
					pinTextField = textField;
					connect(pinTextField, SIGNAL(textChanging(QString) ), this,
											SLOT(onTextPinChanging(QString)));
					textField->setInputMode(TextFieldInputMode::NumericPassword);
					break;
				case 4:
					textField->setInputMode(TextFieldInputMode::PhoneNumber);
					break;
				default:
					textField->setInputMode(TextFieldInputMode::Default);
					break;
				}

				fieldVector.append(textField);

				if(qmapField.value("is_favorite").toInt() == 1 || qmapField.value("textview_type").toInt() == 1)
				{
					favoritLabel = label->text();
					phoneNumberField = textField;
					phoneNumberField->setHorizontalAlignment(HorizontalAlignment::Left);

					phoneNumberField->setText(favoritNumber);
					Container *fieldContainer = Container::create().layout(DockLayout::create());
					fieldContainer->setPreferredWidth(UiUtils::DisplayW);

					Container *dockContainer = Container::create().layout(DockLayout::create());

					                    ImageView *border = ImageView::create();
					                    border->setImageSource(QUrl("asset:///images/border/input.amd"));
					                    border->setHorizontalAlignment(HorizontalAlignment::Fill);
					                    border->setVerticalAlignment(VerticalAlignment::Fill);
					                    dockContainer->add(border);
					                    textField->setBackgroundVisible(false);
					                    textField->setLeftPadding(40);

					                    dockContainer->add(textField);
					fieldContainer->add(dockContainer);

					Container *buttonContainer = Container::create().layout(StackLayout::create().orientation(LayoutOrientation::LeftToRight));
					buttonContainer->setHorizontalAlignment(HorizontalAlignment::Right);
					buttonContainer->setVerticalAlignment(VerticalAlignment::Center);

					int totalButton = 0;
					if(qmapField.value("textview_type").toInt() == 1)
					{
					    totalButton++;
						ImageButton *btnSearch = ImageButton::create().defaultImage(QUrl("asset:///images/icon-search.png")).pressedImage(QUrl("asset:///images/icon-search-h.png"));
						btnSearch->setVerticalAlignment(VerticalAlignment::Center);
						btnSearch->setRightMargin(0);
						connect(btnSearch, SIGNAL(clicked()), this,
									SLOT(onBtnSearchClicked()));
						buttonContainer->add(btnSearch);
					}

					if(qmapField.value("is_favorite").toInt() == 1)
					{
					    totalButton++;
						favoriteButton = ImageButton::create().pressedImage(QUrl("asset:///images/icon-bookmark-h.png")).defaultImage(QUrl("asset:///images/icon-bookmark.png"));
						favoriteButton->setLeftMargin(5);
						connect(favoriteButton, SIGNAL(clicked()), this,
														SLOT(onFavoriteClicked()));
						favoriteButton->setVerticalAlignment(VerticalAlignment::Center);
						buttonContainer->add(favoriteButton);
					}

					int textfieldW = UiUtils::DisplayW - 160 - 75*totalButton;
					phoneNumberField->setPreferredWidth(textfieldW);
					dockContainer->setPreferredWidth(textfieldW);
					fieldContainer->add(buttonContainer);
					formContainer->add(fieldContainer);

				}
				else {
				    Container *dockContainer = Container::create().layout(DockLayout::create());
				    ImageView *border = ImageView::create();
				    border->setImageSource(QUrl("asset:///images/border/input.amd"));
				    border->setHorizontalAlignment(HorizontalAlignment::Fill);
				    border->setVerticalAlignment(VerticalAlignment::Fill);
				    dockContainer->add(border);
				    textField->setBackgroundVisible(false);
				                                            textField->setLeftPadding(40);
				    dockContainer->add(textField);
					formContainer->add(dockContainer);
				}
			}
			//dropdown
			else if (qmapField.value("field_tipe").toInt() == 1) {
				DropDown *dropDown1 = DropDown::create().title("");
				formContainer->add(dropDown1);
				//isi_dropdown: "10000;20000;25000;50000;100000",
				QString str = qmapField.value("isi_dropdown").toString();
				QRegExp rx("[;]");
				QStringList list = str.split(rx, QString::SkipEmptyParts);

				qDebug() << list;
				for (int j = 0; j < list.size(); j++)
					dropDown1->add(Option::create().text(list.at(j)));
				dropDown1->setSelectedIndex(0);
				fieldVector.append(dropDown1);
				/*dropDown1->add(Option::create().text("Option 1"));
				 dropDown1->add(Option::create().text("Option 2"));
				 dropDown1->add(Option::create().text("Option 3"));*/
			}
		}
	} else {
		qDebug() << ">>data null ";
	}
}

void FormBeliBayar::onTextPinChanging(QString text){
	if(text.length() > 6){
		pinTextField->setText(text.left(6));
	}
}

void FormBeliBayar::onFavoriteClicked() {
	if(phoneNumberField->text().trimmed().length() == 0){
		UiUtils::toast("Mohon isi " + favoritLabel, this);
		phoneNumberField->requestFocus();
		return;
	}
	if(favoritController == NULL)
	{
		favoritController = new FavoritController;
	}
	FavoritData *data = new FavoritData;
	data->setValue(phoneNumberField->text());

	QString typeLabel = "";
	if(formType == TYPE_BAYAR) typeLabel = "Bayar - ";
	else typeLabel = "Beli - ";
	typeLabel += namaItem + " - " + getTitle();
	qDebug() << ">>typelabel: " << typeLabel;
	data->setTypeLabel(typeLabel);
	data->setNamaItem(namaItem);
	data->setJsonData(jsonData);
	data->setType(formType);
	favoritController->addToFavorit(data, namaItem + "_" + getTitle() + "_" + phoneNumberField->text().trimmed());
}
void FormBeliBayar::onBtnSearchClicked() {

	if(contactPicker == NULL)
	{
		contactPicker = new ContactPicker();
		contactPicker->setMode(ContactSelectionMode::Single);
		contactPicker->setKindFilters(QSet<bb::pim::contacts::AttributeKind::Type>()
			 << bb::pim::contacts::AttributeKind::Phone);

		QObject::connect(contactPicker,
			 SIGNAL(contactSelected(int)),
			 this,
			 SLOT(onContactsSelected(int)));

		/*QObject::connect(contactPicker,
			 SIGNAL(canceled()),
			 this,
			 SLOT(onCanceled()));**/
	}
	contactPicker->open();
}

void FormBeliBayar::onContactsSelected(int contactId){
	Contact contact = ContactService().contactDetails(contactId);
	QList<ContactAttribute> list = contact.phoneNumbers();
	ContactAttribute selectedContact = list.at(0);
	if(selectedContact.kind() == AttributeKind::Phone){
		phoneNumberField->setText(selectedContact.value());
	}
	else{
		UiUtils::toast("Kontak yang dipilih tidak ada nomor telepon", this);
	}

}


void FormBeliBayar::alert( QString message) {
	qDebug() << "alert: " << message;
	SystemDialog *dialog; // SystemDialog uses the BB10 native dialog.
	dialog = new SystemDialog(tr("OK"), this); // New dialog with on 'Ok' button, no 'Cancel' button
	dialog->setTitle(tr("Alert")); // set a title for the message
	dialog->setBody(message); // set the message itself
	dialog->setDismissAutomatically(true); // Hides the dialog when a button is pressed.

	// Setup slot to mark the dialog for deletion in the next event loop after the dialog has been accepted.
	bool ok = connect(dialog,
			SIGNAL(finished(bb::system::SystemUiResult::Type)), this,
			SLOT(onAlertDismiss()));
	Q_ASSERT(ok);
	Q_UNUSED(ok);
	dialog->show();
}


void FormBeliBayar::onAlertDismiss() {
	ApplicationUI::showBerandaPage();

}
FormBeliBayar::~FormBeliBayar() {
	// TODO Auto-generated destructor stub
}

