/*
 * DaftarMerchantViewController.h
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#ifndef DaftarMerchantViewController_H_
#define DaftarMerchantViewController_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include <bb/cascades/ListView>
#include <bb/cascades/TextField>
#include "MerchanDataModel.h"
#include "DompetKuPage.h"
#include "LoadingPopup.h"
#include "HttpRequest.h"
#include <bb/data/JsonDataAccess>

using namespace bb::data;
using namespace bb::cascades;

class DaftarMerchantViewController : public DompetKuPage{
	Q_OBJECT
public:
	DaftarMerchantViewController();
	virtual ~DaftarMerchantViewController();

	Q_INVOKABLE void getMerchant();
	Q_INVOKABLE void loadMore();

private:
	int page;
	int maxPage;
	QVariantList modelList;
	LoadingPopup *loadingPopup;
	MerchanDataModel *dataModel;
	HttpRequest *httpRequest;
	ListView *listView;

private slots:
	void onHttpReply(QByteArray data);
	void onListClicked(const QVariantList indexPath);
	void onDataLoaded(int status);
};

#endif /* DaftarMerchantViewController_H_ */
