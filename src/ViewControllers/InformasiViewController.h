/*
 * InformasiViewController.h
 *
 *  Created on: Dec 21, 2014
 *      Author: yogi
 */

/*
 * InformasiViewController.h
 *
 *  Created on: Dec 8, 2014
 *      Author: yogi
 */

#ifndef InformasiViewController_H_
#define InformasiViewController_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include "DompetKuPage.h"
#include <bb/cascades/ListView>
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>

using namespace bb::data;
using namespace bb::cascades;

class InformasiViewController : public Page{
	Q_OBJECT

public:
	InformasiViewController();
	virtual ~InformasiViewController();
	Q_INVOKABLE void back();

private:
	ListView *listView;
	void setData(QVariantList list);

	GroupDataModel *dataModel;

private slots:
	void onListClicked(const QVariantList indexPath);
};

#endif /* InformasiViewController_H_ */

