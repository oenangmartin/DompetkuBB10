/*
 * Terms.h
 *
 *  Created on: Nov 30, 2014
 *      Author: yogi
 */

#ifndef TERMS_H_
#define TERMS_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include "DompetKuPage.h"


using namespace bb::cascades;

class Terms : public Page{
	Q_OBJECT
public:
	Terms();
	virtual ~Terms();
	Q_INVOKABLE void back();
};

#endif /* TERMS_H_ */
