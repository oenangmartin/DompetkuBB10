
/*
 * FavoritViewController.h
 *
 *  Created on: Dec 8, 2014
 *      Author: yogi
 */

#ifndef FavoritViewController_H_
#define FavoritViewController_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include "DompetKuPage.h"
#include <bb/cascades/ListView>
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>

using namespace bb::data;
using namespace bb::cascades;

class FavoritViewController : public Page{
	Q_OBJECT

public:
	FavoritViewController();
	virtual ~FavoritViewController();
	Q_INVOKABLE void back();
	void loadFavorit();

private:
	ListView *listView;


	GroupDataModel *dataModel;

private slots:
	void onListClicked(const QVariantList indexPath);
	void onListActionDelete();
};

#endif /* FavoritViewController_H_ */

