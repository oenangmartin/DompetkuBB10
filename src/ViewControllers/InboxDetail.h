/*
 * InboxDetail.h
 *
 *  Created on: Jan 18, 2015
 *      Author: yogi
 */


/*
 * InboxDetail.h
 *
 *  Created on: Dec 8, 2014
 *      Author: yogi
 */

#ifndef InboxDetail_H_
#define InboxDetail_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include "InboxData.h"
#include <bb/cascades/ListView>

using namespace bb::cascades;

class InboxDetail : public Page{
	Q_OBJECT

public:
	InboxDetail(InboxData *inboxData);
	virtual ~InboxDetail();
	Q_INVOKABLE void back();
	Q_INVOKABLE void deleteInbox();

private:
	InboxData *inboxData;

	private slots:
	void onInboxDeleted(int id);
};

#endif /* InboxDetail_H_ */


