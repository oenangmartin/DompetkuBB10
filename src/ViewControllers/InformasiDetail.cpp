/*
 * InformasiDetail.cpp
 *
 *  Created on: Jan 11, 2015
 *      Author: yogi
 */

/*
 * InformasiDetail.cpp
 *
 *  Created on: Nov 30, 2014
 *      Author: yogi
 */

#include "InformasiDetail.h"
#include "applicationui.hpp"
#include "UiUtils.h"

InformasiDetail::InformasiDetail(QString qmlSource) {

	QmlDocument *qml = UiUtils::LoadQml("informasi/"+qmlSource, this);
		qml->setContextProperty("page", this);
		Container *container = qml->createRootObject<Container>();
		setContent(container);

}

void InformasiDetail::back(){
	ApplicationUI::nav->pop();
}

InformasiDetail::~InformasiDetail() {
	// TODO Auto-generated destructor stub
}


