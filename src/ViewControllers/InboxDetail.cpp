#include "InboxDetail.h"

#include "applicationui.hpp"
#include "UiUtils.h"
#include "FormBeliBayar.h"
#include "FavoritController.h"
#include <bb/cascades/DeleteActionItem>

InboxDetail::InboxDetail(InboxData *inboxData) {
	this->inboxData = inboxData;
	QmlDocument *qml = UiUtils::LoadQml("InboxDetailPage.qml", this);
	qml->setContextProperty("page", this);
	Container *root = qml->createRootObject<Container>();
	setContent(root);

	Label *lblRefId = root->findChild<Label*>("lblRefId");
	lblRefId->setText("Ref ID: " + inboxData->getRefId());

	Label *lblTanggal = root->findChild<Label*>("lblTanggal");
	lblTanggal->setText("Tanggal: " + inboxData->getTanggal());

	Label *lblAction = root->findChild<Label*>("lblAction");
	lblAction->setText("Action: " + inboxData->getAction());

	Label *lblMerchant = root->findChild<Label*>("lblMerchant");
	if (inboxData->getMerchant().length() > 1)
		lblMerchant->setText("Merchant: " + inboxData->getMerchant());
	else
		lblMerchant->setVisible(false);

	Label *lblTujuan = root->findChild<Label*>("lblTujuan");
	if (inboxData->getTujuan().length() > 1)
		lblTujuan->setText("Ke: " + inboxData->getTujuan());
	else
		lblTujuan->setVisible(false);

	Label *lblJumlah = root->findChild<Label*>("lblJumlah");
	if (inboxData->getJumlah().length() > 1)
		lblJumlah->setText("Jumlah: " + inboxData->getJumlah());
	else
		lblJumlah->setVisible(false);

	if (inboxData->getToken().length() > 1) {
		Label *lblToken = root->findChild<Label*>("lblToken");
		lblToken->setText(
				"Token: " + inboxData->getToken() + " (berlaku s/d "
						+ inboxData->getExpired() + ")");

	}
}

void InboxDetail::deleteInbox() {
	ApplicationUI::inboxController->remove(inboxData->getId());
	connect(ApplicationUI::inboxController, SIGNAL(inboxDeleted(int)), this,
			SLOT(onInboxDeleted(int)));

}

void InboxDetail::onInboxDeleted(int id) {
	if (id == inboxData->getId()){
		ApplicationUI::inboxViewController->load();
		ApplicationUI::nav->pop();
	}
}
void InboxDetail::back() {
	ApplicationUI::nav->pop();
}

InboxDetail::~InboxDetail() {
	// TODO Auto-generated destructor stub
}

