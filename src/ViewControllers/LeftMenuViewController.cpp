/*
 * LeftMenuViewController.cpp
 *
 *  Created on: Dec 25, 2014
 *      Author: yogi
 */

#include "LeftMenuViewController.h"
#include "LoginViewController.h"
#include "UiUtils.h"
#include "applicationui.hpp"
#include "InboxViewController.h"
#include "InformasiViewController.h"
#include "FavoritViewController.h"
#include "AdministrasiViewController.h"
#include <bb/cascades/ListView>

LeftMenuViewController::LeftMenuViewController(Label *lblUser, ListView *menuList) {
	this->lblUser = lblUser;


		connect(menuList, SIGNAL(triggered(const QVariantList)), this,SLOT(onListClicked(const QVariantList)));
		JsonDataAccess jda;
		QVariantList list = jda.load(
				QDir::currentPath()
						+ "/app/native/assets/models/LeftMenu.json").value<
				QVariantList>();
		dataModel = new GroupDataModel();
		dataModel->setParent(this);
		dataModel->insertList(list);
		dataModel->setGrouping(ItemGrouping::None);
		dataModel->setSortedAscending(false);
		menuList->setDataModel(dataModel);
}

void LeftMenuViewController::onListClicked(QVariantList indexPath) {
	QVariantMap map = dataModel->data(indexPath).toMap();
	QString title = map.value("title").toString();
	if(title.compare("Inbox") == 0)
	{
		inbox();
	}
	else if(title.compare("Favorit") == 0)
	{
		favorit();
	}
	else if(title.compare("Administrasi") == 0)
	{
		administrasi();
	}
	else if(title.compare("Informasi") == 0)
	{
		informasi();
	}
	else if(title.compare("Logout") == 0)
	{
		logout();
	}

}

void LeftMenuViewController::setAccountName(QString name) {
	lblUser->setText(name);
}

void LeftMenuViewController::beranda() {
	qDebug() << ">>beranda";
	ApplicationUI::homeViewController->closeLeftMenu();

}
void LeftMenuViewController::inbox() {
	qDebug() << ">>inbox page";

	ApplicationUI::nav->push(new InboxViewController);
	ApplicationUI::homeViewController->closeLeftMenu();
}
void LeftMenuViewController::informasi() {
	qDebug() << ">>informasi page";
	ApplicationUI::nav->push(new InformasiViewController);
	ApplicationUI::homeViewController->closeLeftMenu();
}
void LeftMenuViewController::favorit() {
	qDebug() << ">>favorit page";
	ApplicationUI::nav->push(new FavoritViewController);
	ApplicationUI::homeViewController->closeLeftMenu();
}
void LeftMenuViewController::administrasi() {
	qDebug() << ">>administrasi page";
	ApplicationUI::nav->push(new AdministrasiViewController);
	ApplicationUI::homeViewController->closeLeftMenu();
}
void LeftMenuViewController::logout() {
	while (ApplicationUI::nav->count() > 0)
		ApplicationUI::nav->pop();
	LoginViewController *loginView = new LoginViewController;
	ApplicationUI::nav->push(loginView);
}

LeftMenuViewController::~LeftMenuViewController() {
	// TODO Auto-generated destructor stub
}

