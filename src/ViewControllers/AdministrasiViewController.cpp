/*
 * AdministrasiViewController.cpp
 *
 *  Created on: Dec 8, 2014
 *      Author: yogi
 */

#include "AdministrasiViewController.h"

#include "applicationui.hpp"
#include "UiUtils.h"
#include "FormBeliBayar.h"
#include "FavoritController.h"
#include "GantiPinViewController.h"
#include <bb/cascades/DeleteActionItem>

AdministrasiViewController::AdministrasiViewController() {

	QmlDocument *qml = UiUtils::LoadQml("AdministrasiPage.qml", this);
	qml->setContextProperty("page", this);
	Container *root = qml->createRootObject<Container>();
	setContent(root);

	listView = root->findChild<ListView*>("list");
	if (listView) {
		qDebug() << ">>fav";
		connect(listView, SIGNAL(triggered(const QVariantList)), this, SLOT(onListClicked(const QVariantList)));
		JsonDataAccess jda;
		QVariantList list = jda.load(
				QDir::currentPath()
						+ "/app/native/assets/models/AdministrasiMenu.json").value<
				QVariantList>();
		dataModel = new GroupDataModel();
		dataModel->setParent(this);
		dataModel->insertList(list);
		dataModel->setGrouping(ItemGrouping::None);
		dataModel->setSortedAscending(false);
		listView->setDataModel(dataModel);
	}

}

void AdministrasiViewController::onListClicked(QVariantList indexPath) {
	ApplicationUI::nav->push(new GantiPinViewController);
}

void AdministrasiViewController::onListActionDelete(){
	qDebug() << "Here I am: Delete!";
	qDebug() << sender();

	DeleteActionItem* action = dynamic_cast<DeleteActionItem*>(sender());
	if (action == 0)
	{
		qDebug() << "Not expected type";
		return;
	}
	qDebug() << "Yes, found!";

	QObject* parent = action->parent(); // get ActionSet
	if (parent != 0) parent = parent->parent();
	if (parent == 0)
	{
		qDebug() << "No parent!";
		return;
	}
	const QMetaObject* m = parent->metaObject();
	if (m == 0)
	{
		qDebug() << "No metaobject!";
	}
	qDebug() << m->className();
}

void AdministrasiViewController::back(){
	ApplicationUI::nav->pop();
}

AdministrasiViewController::~AdministrasiViewController() {
	// TODO Auto-generated destructor stub
}


