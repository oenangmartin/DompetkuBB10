/*
 * MenuTransferDompetku.cpp
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

/*
 * MenuTransferDompetku.cpp
 *
 *  Created on: Feb 1, 2015
 *      Author: yogi
 */

/*
 * MenuTransferDompetku.cpp
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#include "MenuTransferDompetku.h"
#include "Global.h"
#include "UiUtils.h"
#include "applicationui.hpp"

MenuTransferDompetku::MenuTransferDompetku() {
	httpRequest = NULL;
	httpRequestConfirm = NULL;
	loadingPopup = NULL;
	contactPicker = NULL;
	prompt = NULL;

	setTitle("Dompetku");
	bottomMenuController->getButtonTransfer()->setSelected(true);
	QmlDocument *qml = UiUtils::LoadQml("bottomMenu/MenuTransferDompetku.qml",this);
	qml->setContextProperty("page", this);

	Container* pageContent = qml->createRootObject<Container>();
	pageContent->setImplicitLayoutAnimationsEnabled(false);

	contentContainer->add(pageContent);

	pinField = pageContent->findChild<TextField*>("pinField");
	connect(pinField, SIGNAL(textChanging(QString) ), this,
							SLOT(onTextPinChanging(QString)));

	jumlahField = pageContent->findChild<TextField*>("jumlahField");
	tujuanField = pageContent->findChild<TextField*>("tujuanField");
	connect(tujuanField, SIGNAL(textChanged(QString) ), this,
								SLOT(onTextTujuanChanging(QString)));

}

void MenuTransferDompetku::onTextPinChanging(QString text){
	if(text.length() > 6){
		pinField->setText(text.left(6));
	}
}
void MenuTransferDompetku::onTextTujuanChanging(QString text){
	//remove 62 or +62
	if(text.length() > 2 && text.left(2).compare("62") == 0)
	{
		text = "0" + text.right(text.length() - 2);
	}
	else if(text.length() > 3 && text.left(3).compare("+62") == 0)
	{
		text = "0" + text.right(text.length() - 3);
	}

	text.replace('-', "", Qt::CaseInsensitive);
	text.replace(' ', "", Qt::CaseInsensitive);
	tujuanField->setText(text);

	if(text.length() > 4 && !validateTujuan(text)){
		UiUtils::toast(
						"Nomor Tujuan harus operator Indosat",this);
		tujuanField->requestFocus();
	}
}

bool MenuTransferDompetku::validateTujuan(QString text){
	//tsel / xl saja
	QRegExp rx("[;]");
	QString allowedNumber = "0814;0815;0816;0855;0856;0857;0858";
	QStringList list = allowedNumber.split(rx, QString::SkipEmptyParts);
	qDebug() << ">>list: " << list;
	for (int i = 0; i < list.length(); i++)
	{
		if(text.left(4).compare(list.at(i)) == 0) return true;
	}

	return false;
}

void MenuTransferDompetku::searchTujuan(){
	if(contactPicker == NULL)
		{
			contactPicker = new ContactPicker();
			contactPicker->setMode(ContactSelectionMode::Single);
			contactPicker->setKindFilters(QSet<bb::pim::contacts::AttributeKind::Type>()
				 << bb::pim::contacts::AttributeKind::Phone);

			QObject::connect(contactPicker,
				 SIGNAL(contactSelected(int)),
				 this,
				 SLOT(onContactsSelected(int)));

			/*QObject::connect(contactPicker,
				 SIGNAL(canceled()),
				 this,
				 SLOT(onCanceled()));**/
		}
		contactPicker->open();
	}

	void MenuTransferDompetku::onContactsSelected(int contactId){
		Contact contact = ContactService().contactDetails(contactId);
		QList<ContactAttribute> list = contact.phoneNumbers();
		ContactAttribute selectedContact = list.at(0);
		if(selectedContact.kind() == AttributeKind::Phone){
			tujuanField->setText(selectedContact.value());
		}
		else{
			UiUtils::toast("Kontak yang dipilih tidak ada nomor telepon", this);
		}

	}
void MenuTransferDompetku::kirimUang(){
	if (tujuanField->text().trimmed().length() == 0) {
		UiUtils::toast(
				"Mohon isi Nomor Tujuan",this);
		tujuanField->requestFocus();
		return;
	}
	if(!validateTujuan(tujuanField->text().trimmed())){
	        UiUtils::toast(
	                        "Nomor Tujuan harus operator Indosat",this);
	        tujuanField->requestFocus();
	        return;
	    }
	if (jumlahField->text().trimmed().length() == 0) {
		UiUtils::toast(
				"Mohon isi Jumlah",this);
		jumlahField->requestFocus();
		return;
	}
	if (pinField->text().trimmed().length() == 0) {
		UiUtils::toast(
				"Mohon isi PIN",this);
		pinField->requestFocus();
		return;
	}

	if(httpRequest == NULL){
		httpRequest = new HttpRequest(this);
			connect(httpRequest, SIGNAL(httpReply(QByteArray)), this,
					SLOT(onHttpReply(QByteArray)));
	}
	if (loadingPopup == NULL) {
		loadingPopup = new LoadingPopup(this, "Loading");
	}
	loadingPopup->showLoading();

	//signature=dCikmtZ90C6jBN88aTxTsIb9VrKmHU2BP2ReXf1VRQ+p5b6eVq4SbA==&userid=web_api_test&msisdn=085814047024&amount=3000

	/*QString url = API_MONEY_TRANSFER + "?signature="
					+ ApplicationUI::accManager->getEncodedSignature() + "&userid=" + USER_ID;
			qDebug() << ">>url: " << url;
			httpRequest->get(url);*/

	QUrl postData;
		postData.addQueryItem("userid", ApplicationUI::accManager->getUser()->getUserId());
		postData.addQueryItem("signature", ApplicationUI::accManager->generateSignature(pinField->text().trimmed()));
		postData.addQueryItem("to", tujuanField->text().trimmed());
		//postData.addQueryItem("amount", jumlahField->text().trimmed());
		httpRequest->post(API_GET_BALANCE, postData);

}

void MenuTransferDompetku::onHttpReplyConfirm(QByteArray data) {
	loadingPopup->hideLoading();
	if (!data.isNull()) {
		QString dataStr(data);
		qDebug() << ">>onHttpReplyConfirm: " << dataStr;
		JsonDataAccess jda;
		QVariantMap qmap = jda.loadFromBuffer(data).value<QVariantMap>();
		int status = qmap.value("status").toInt();
		if(status == 0 && qmap.value("status").toString().length() > 0)
		{

			QDateTime now = QDateTime::currentDateTime();
			QString trxid = qmap.value("trxid").toString();
			//save to inbox
			InboxData data;
			data.setRefId(trxid);
			data.setToken("");
			data.setExpired("");
			data.setTanggal(now.toString("dd/MM/yyyy"));
			data.setAction("Transfer E-Money");
			data.setMerchant("E-Money");
			data.setTujuan(tujuanField->text());
			data.setJumlah(jumlahField->text());
			ApplicationUI::inboxController->add(data);
			ApplicationUI::homeViewController->refresh();

			QString message = "Anda telah melakukan Kirim Uang pada tanggal " + now.toString("dd/MM/yyyy") + " ke " + tujuanField->text() + " sebesar " + jumlahField->text() + "\nRefID: " + trxid;

			SystemDialog *dialog; // SystemDialog uses the BB10 native dialog.
			dialog = new SystemDialog(tr("OK"), this); // New dialog with on 'Ok' button, no 'Cancel' button
			dialog->setTitle(tr("Berhasil")); // set a title for the message
			dialog->setBody(message); // set the message itself
			dialog->setDismissAutomatically(true); // Hides the dialog when a button is pressed.

			// Setup slot to mark the dialog for deletion in the next event loop after the dialog has been accepted.
			bool ok = connect(dialog,
					SIGNAL(finished(bb::system::SystemUiResult::Type)), this,
					SLOT(onAlertDismiss()));
			Q_ASSERT(ok);
			Q_UNUSED(ok);
			dialog->show();
		}
		else if(status == 1012){
			UiUtils::alert("Pin anda telah terblokir. Silahkan menghubungi Customer Service Indosat di 100/111.", this);
		}
		else if(status == 1014){
			UiUtils::alert("Pin Salah, Silahkan Periksa kembali Pin anda", this);
		}
		else {
			UiUtils::alert("Transaksi gagal", this);
			//UiUtils::alert(qmap.value("msg").toString(), this);
		}

	}
  else {
		UiUtils::alert(WORDING_KONEKSI_GAGAL, this);
	}

}



void MenuTransferDompetku::onAlertDismiss() {
	ApplicationUI::showBerandaPage();

}
void MenuTransferDompetku::onHttpReply(QByteArray data) {
	loadingPopup->hideLoading();
	if (!data.isNull()) {
		QString dataStr(data);
		qDebug() << ">>onHttpReply: " << dataStr;
		JsonDataAccess jda;
		QVariantMap qmap = jda.loadFromBuffer(data).value<QVariantMap>();
		if(qmap.value("status").toInt() == 0 && qmap.value("status").toString().length() > 0)
		{
			if (prompt == NULL)
			{

				prompt = new SystemDialog("Setuju", "Batal");
				prompt->setTitle("Konfirmasi");

				connect(prompt,
						SIGNAL(finished(bb::system::SystemUiResult::Type)),
						this,
						SLOT(onPromptAnswered(bb::system::SystemUiResult::Type)));
				prompt->setParent(this);

			}
			int fee = 500;
			QString message = "Anda akan melakukan kirim uang ke\nNomor Pelanggan: " + tujuanField->text().trimmed() + ""
								"\nNama: "+ qmap.value("name").toString() +
								"\nAmount: " + jumlahField->text().trimmed() +
								"\nFee: "+ QString::number(fee) +
								"\nTotal: "+ QString::number(jumlahField->text().toInt() + fee) ;
			prompt->setBody(message);
			prompt->show();

		}
		else
		{
			UiUtils::alert(qmap.value("msg").toString(), this);
		}

	}
  else {
		UiUtils::alert(WORDING_KONEKSI_GAGAL, this);
	}
}

void MenuTransferDompetku::onPromptAnswered(bb::system::SystemUiResult::Type type) {
	if (type == SystemUiResult::ConfirmButtonSelection) {
		if(httpRequestConfirm == NULL){
			httpRequestConfirm = new HttpRequest(this);
				connect(httpRequestConfirm, SIGNAL(httpReply(QByteArray)), this,
						SLOT(onHttpReplyConfirm(QByteArray)));
		}
		loadingPopup->showLoading();
		QUrl postData;
		postData.addQueryItem("userid", ApplicationUI::accManager->getUser()->getUserId());
		postData.addQueryItem("signature", ApplicationUI::accManager->generateSignature(pinField->text().trimmed()));
		postData.addQueryItem("to", tujuanField->text().trimmed());
		postData.addQueryItem("amount", jumlahField->text().trimmed());
		httpRequestConfirm->post(API_MONEY_TRANSFER, postData);
	}
}

MenuTransferDompetku::~MenuTransferDompetku() {
	// TODO Auto-generated destructor stub
}





