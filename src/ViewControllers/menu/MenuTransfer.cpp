/*
 * MenuTransfer.cpp
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#include "MenuTransfer.h"
#include "MenuUangElektronik.h"
#include "MenuTransferOperatorLain.h"
#include "MenuTransferDompetku.h"
#include "MenuRekeningBank.h"
#include "applicationui.hpp"

MenuTransfer::MenuTransfer() {
	setTitle("Transfer");
	bottomMenuController->getButtonTransfer()->setSelected(true);
	QmlDocument *qml =
			QmlDocument::create("asset:///bottomMenu/MenuTransfer.qml").parent(
					this);
	Container* pageContent = qml->createRootObject<Container>();
	contentContainer->add(pageContent);

	ListView *listView = pageContent->findChild<ListView*>("list");

	if (listView) {
		connect(listView, SIGNAL(triggered(const QVariantList)), this,SLOT(onListClicked(const QVariantList)));
		JsonDataAccess jda;
		QVariantList list = jda.load(
				QDir::currentPath()
						+ "/app/native/assets/models/TransferMenu.json").value<
				QVariantList>();
		dataModel = new GroupDataModel();
		dataModel->setParent(this);
		dataModel->insertList(list);
		dataModel->setGrouping(ItemGrouping::None);
		dataModel->setSortedAscending(false);
		listView->setDataModel(dataModel);
	}

}

void MenuTransfer::onListClicked(QVariantList indexPath) {
	QVariantMap map = dataModel->data(indexPath).toMap();
	QString title = map.value("title").toString();
	if(title.compare("Rekening Bank") == 0)
	{
		MenuRekeningBank *page = new MenuRekeningBank;
		ApplicationUI::nav->push(page);
	}
	else if(title.compare("Dompetku") == 0)
	{
		MenuTransferDompetku *page = new MenuTransferDompetku;
				ApplicationUI::nav->push(page);
	}
	else if(title.compare("Operator Lain") == 0)
	{
		MenuTransferOperatorLain *page = new MenuTransferOperatorLain;
		ApplicationUI::nav->push(page);
	}


}
MenuTransfer::~MenuTransfer() {
	// TODO Auto-generated destructor stub
}

