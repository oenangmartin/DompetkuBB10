/*
 * MenuRekeningBank.cpp
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#include "MenuRekeningBank.h"
#include "Global.h"
#include "UiUtils.h"
#include "applicationui.hpp"

MenuRekeningBank::MenuRekeningBank() {
	httpRequest = NULL;
	loadingPopup = NULL;

	setTitle("Rekening Bank");
	bottomMenuController->getButtonTransfer()->setSelected(true);
	QmlDocument *qml = QmlDocument::create("asset:///bottomMenu/MenuRekeningBank.qml").parent(this);
	qml->setContextProperty("page", this);

	Container* pageContent = qml->createRootObject<Container>();
	pageContent->setImplicitLayoutAnimationsEnabled(false);

	contentContainer->add(pageContent);

	kodeField = pageContent->findChild<TextField*>("kodeField");
	pinField = pageContent->findChild<TextField*>("pinField");
	connect(pinField, SIGNAL(textChanging(QString) ), this,
								SLOT(onTextPinChanging(QString)));
	jumlahField = pageContent->findChild<TextField*>("jumlahField");
	tujuanField = pageContent->findChild<TextField*>("tujuanField");

	cariKodeBank = new CariKodeBank;
	connect(cariKodeBank, SIGNAL(bankSelected(QString, QString)), this,
						SLOT(onBankSelected(QString, QString)));

	kodeBankSelected = "";

}

void MenuRekeningBank::onTextPinChanging(QString text){
	if(text.length() > 6){
		pinField->setText(text.left(6));
	}
}

void MenuRekeningBank::onBankSelected(QString kode, QString nama){
    kodeBankSelected = kode;
	kodeField->setText(kode + " - " + nama);
}

void MenuRekeningBank::searchBank(){
	ApplicationUI::nav->push(cariKodeBank);

}
void MenuRekeningBank::kirimUang(){
	if (kodeField->text().trimmed().length() == 0) {
		UiUtils::toast(
				"Mohon isi Kode Bank",this);
		kodeField->requestFocus();
		return;
	}
	if (tujuanField->text().trimmed().length() == 0) {
		UiUtils::toast(
				"Mohon isi No Rekening Tujuan",this);
		tujuanField->requestFocus();
		return;
	}
	if (jumlahField->text().trimmed().length() == 0) {
		UiUtils::toast(
				"Mohon isi Jumlah",this);
		jumlahField->requestFocus();
		return;
	}
	if (pinField->text().trimmed().length() == 0) {
		UiUtils::toast(
				"Mohon isi PIN",this);
		pinField->requestFocus();
		return;
	}

	if(httpRequest == NULL){
		httpRequest = new HttpRequest(this);
			connect(httpRequest, SIGNAL(httpReply(QByteArray)), this,
					SLOT(onHttpReply(QByteArray)));
	}
	if (loadingPopup == NULL) {
		loadingPopup = new LoadingPopup(this, "Loading");
	}
	loadingPopup->showLoading();

	//?signature=dCikmtZ90C6jBN88aTxTsIb9VrKmHU2BP2ReXf1VRQ+p5b6eVq4SbA==&userid=web_api_test&bankCode=008&bankAccNo=532113322&amount=50000
	QUrl postData;
	postData.addQueryItem("userid", ApplicationUI::accManager->getUser()->getUserId());
	postData.addQueryItem("signature", ApplicationUI::accManager->generateSignature(pinField->text().trimmed()));
	postData.addQueryItem("to", tujuanField->text().trimmed());
	postData.addQueryItem("bankAccNo", tujuanField->text().trimmed());
	postData.addQueryItem("amount", jumlahField->text().trimmed());
	postData.addQueryItem("bankCode", kodeBankSelected);
	httpRequest->post(API_BANK_TRANSFER, postData);

	qDebug() << ">>url: " << postData;
	//signature, userid, amount, to, bankAccNo, bankCode
}

void MenuRekeningBank::onHttpReply(QByteArray data) {
	loadingPopup->hideLoading();
	if (!data.isNull()) {
		QString dataStr(data);
		qDebug() << ">>onHttpReply: " << dataStr;
		JsonDataAccess jda;
		QVariantMap qmap = jda.loadFromBuffer(data).value<QVariantMap>();
		int status = qmap.value("status").toInt();
		if (status == 0)
		{
			//UiUtils::alert("Transaksi berhasil! \nToken Anda: " + qmap.value("couponId").toString() , this);
			ApplicationUI::showBerandaPage("Anda akan menerima SMS berisi cara konfirmasi transfer ke rekening bank dari 789");
		}
		else if(status == 1012){
			UiUtils::alert("Pin anda telah terblokir. Silahkan menghubungi Customer Service Indosat di 100/111.", this);
		}
		else if(status == 1014){
			UiUtils::alert("Pin Salah, Silahkan Periksa kembali Pin anda", this);
		}
		else {
			UiUtils::alert("Transaksi gagal", this);
			//UiUtils::alert(qmap.value("msg").toString(), this);
		}

	}
  else {
		UiUtils::alert(WORDING_KONEKSI_GAGAL, this);
	}
}

MenuRekeningBank::~MenuRekeningBank() {
	// TODO Auto-generated destructor stub
}

