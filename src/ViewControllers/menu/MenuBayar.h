/*
 * MenuBayar.h
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#ifndef MENUBAYAR_H_
#define MENUBAYAR_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include <bb/cascades/ListView>
#include "DompetKuPage.h"

#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>

using namespace bb::data;
using namespace bb::cascades;

class MenuBayar : public DompetKuPage{
	Q_OBJECT
public:
	MenuBayar();
	virtual ~MenuBayar();

private:
	ListView *listView;
	void setData(QVariantList list);

	GroupDataModel *dataModel;

private slots:
	void onListClicked(const QVariantList indexPath);

	void onBayarListLoaded(bool success);
};

#endif /* MENUBAYAR_H_ */
