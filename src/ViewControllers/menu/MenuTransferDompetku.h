/*
 * MenuTransferDompetku.h
 *
 *  Created on: Feb 1, 2015
 *      Author: yogi
 */

/*
 * MenuTransferDompetku.h
 *
 *  Created on: Dec 14, 2014
 *      Author: yogi
 */

/*
 * MenuTransferDompetku.h
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#ifndef MenuTransferDompetku_H_
#define MenuTransferDompetku_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include <bb/cascades/ListView>
#include <bb/cascades/TextField>
#include "DompetKuPage.h"
#include "LoadingPopup.h"
#include "HttpRequest.h"
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/pickers/ContactPicker>
#include <bb/pim/contacts/ContactService>
#include <bb/pim/contacts/Contact>
#include <bb/pim/contacts/ContactAttribute>
#include <bb/system/SystemDialog>

using namespace bb::system;
using namespace bb::data;
using namespace bb::cascades;
using namespace bb::pim::contacts;
using namespace bb::cascades::pickers;

class MenuTransferDompetku : public DompetKuPage{
	Q_OBJECT
public:
	MenuTransferDompetku();
	virtual ~MenuTransferDompetku();

	Q_INVOKABLE void kirimUang();
	Q_INVOKABLE void searchTujuan();

private:
	LoadingPopup *loadingPopup;
	TextField *pinField;
	TextField *jumlahField;
	TextField *tujuanField;
	HttpRequest *httpRequest;
	HttpRequest *httpRequestConfirm;
	SystemDialog *prompt;
	ContactPicker *contactPicker;

	bool validateTujuan(QString text);

private slots:
	void onHttpReply(QByteArray data);
	void onHttpReplyConfirm(QByteArray data);
	void onTextPinChanging(QString text);
	void onTextTujuanChanging(QString text);
	void onPromptAnswered(bb::system::SystemUiResult::Type);
	void onAlertDismiss();
	void onContactsSelected(int);
};

#endif /* MenuTransferDompetku_H_ */


