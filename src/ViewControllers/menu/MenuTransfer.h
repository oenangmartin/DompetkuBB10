/*
 * MenuTransfer.h
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#ifndef MENUTRANSFER_H_
#define MENUTRANSFER_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include <bb/cascades/ListView>
#include "DompetKuPage.h"

#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>

using namespace bb::data;
using namespace bb::cascades;

class MenuTransfer : public DompetKuPage{
	Q_OBJECT
public:
	MenuTransfer();
	virtual ~MenuTransfer();

private:
	GroupDataModel *dataModel;

	private slots:
		void onListClicked(const QVariantList indexPath);
};

#endif /* MENUTRANSFER_H_ */
