/*
 * MenuUangElektronik.cpp
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#include "MenuUangElektronik.h"
#include "Global.h"
#include "UiUtils.h"
#include "applicationui.hpp"

MenuUangElektronik::MenuUangElektronik() {
	httpRequest = NULL;
	loadingPopup = NULL;

	setTitle("Uang Elektronik");
	bottomMenuController->getButtonTransfer()->setSelected(true);
	QmlDocument *qml = QmlDocument::create("asset:///bottomMenu/MenuUangElektronik.qml").parent(this);
	qml->setContextProperty("page", this);

	Container* pageContent = qml->createRootObject<Container>();
	pageContent->setImplicitLayoutAnimationsEnabled(false);

	contentContainer->add(pageContent);

	pinField = pageContent->findChild<TextField*>("pinField");
	jumlahField = pageContent->findChild<TextField*>("jumlahField");
	tujuanField = pageContent->findChild<TextField*>("tujuanField");

}

void MenuUangElektronik::kirimUang(){
	if (tujuanField->text().trimmed().length() == 0) {
		UiUtils::toast(
				"Mohon isi ID Tujuan",this);
		tujuanField->requestFocus();
		return;
	}
	if (jumlahField->text().trimmed().length() == 0) {
		UiUtils::toast(
				"Mohon isi Jumlah",this);
		jumlahField->requestFocus();
		return;
	}
	if (pinField->text().trimmed().length() == 0) {
		UiUtils::toast(
				"Mohon isi PIN",this);
		pinField->requestFocus();
		return;
	}

	if(httpRequest == NULL){
		httpRequest = new HttpRequest(this);
			connect(httpRequest, SIGNAL(httpReply(QByteArray)), this,
					SLOT(onHttpReply(QByteArray)));
	}
	if (loadingPopup == NULL) {
		loadingPopup = new LoadingPopup(this, "Loading");
	}
	loadingPopup->showLoading();

	//signature=dCikmtZ90C6jBN88aTxTsIb9VrKmHU2BP2ReXf1VRQ+p5b6eVq4SbA==&userid=web_api_test&msisdn=085814047024&amount=3000

	/*QString url = API_MONEY_TRANSFER + "?signature="
					+ ApplicationUI::accManager->getEncodedSignature() + "&userid=" + USER_ID;
			qDebug() << ">>url: " << url;
			httpRequest->get(url);*/

	QUrl postData;
		postData.addQueryItem("userid", ApplicationUI::accManager->getUser()->getUserId());
		postData.addQueryItem("signature", ApplicationUI::accManager->getSignature());
		postData.addQueryItem("msisdn", tujuanField->text().trimmed());
		postData.addQueryItem("amount", jumlahField->text().trimmed());
		httpRequest->post(API_MONEY_TRANSFER, postData);
		qDebug() << ">>url: " << postData;
}

void MenuUangElektronik::onHttpReply(QByteArray data) {
	loadingPopup->hideLoading();
	if (!data.isNull()) {
		QString dataStr(data);
		qDebug() << ">>onHttpReply: " << dataStr;
		JsonDataAccess jda;
		QVariantMap qmap = jda.loadFromBuffer(data).value<QVariantMap>();
		if(qmap.value("status").toInt() == 0)
		{
			UiUtils::alert("Transaksi berhasil! \nToken Anda: " + qmap.value("couponId").toString() , this);
		}
		else
		{
			UiUtils::alert(qmap.value("msg").toString(), this);
		}

	}
  else {
		UiUtils::alert(WORDING_KONEKSI_GAGAL, this);
	}
}

MenuUangElektronik::~MenuUangElektronik() {
	// TODO Auto-generated destructor stub
}

