/*
 * MenuTransferOperatorLain.h
 *
 *  Created on: Feb 1, 2015
 *      Author: yogi
 */

/*
 * MenuTransferOperatorLain.h
 *
 *  Created on: Dec 14, 2014
 *      Author: yogi
 */

/*
 * MenuTransferOperatorLain.h
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#ifndef MenuTransferOperatorLain_H_
#define MenuTransferOperatorLain_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include <bb/cascades/ListView>
#include <bb/cascades/TextField>
#include "DompetKuPage.h"
#include "LoadingPopup.h"
#include "HttpRequest.h"
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/pickers/ContactPicker>
#include <bb/pim/contacts/ContactService>
#include <bb/pim/contacts/Contact>
#include <bb/pim/contacts/ContactAttribute>

using namespace bb::data;
using namespace bb::cascades;
using namespace bb::pim::contacts;
using namespace bb::cascades::pickers;

class MenuTransferOperatorLain : public DompetKuPage{
	Q_OBJECT
public:
	MenuTransferOperatorLain();
	virtual ~MenuTransferOperatorLain();

	Q_INVOKABLE void kirimUang();
	Q_INVOKABLE void searchTujuan();

private:
	LoadingPopup *loadingPopup;
	TextField *pinField;
	TextField *jumlahField;
	TextField *tujuanField;
	HttpRequest *httpRequest;
	ContactPicker *contactPicker;

	bool validateTujuan(QString text);

private slots:
	void onContactsSelected(int);
	void onHttpReply(QByteArray data);
	void onTextPinChanging(QString text);
	void onTextTujuanChanging(QString text);
};

#endif /* MenuTransferOperatorLain_H_ */


