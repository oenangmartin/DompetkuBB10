/*
 * MenuRekeningBank.h
 *
 *  Created on: Dec 14, 2014
 *      Author: yogi
 */

/*
 * MenuRekeningBank.h
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#ifndef MenuRekeningBank_H_
#define MenuRekeningBank_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include <bb/cascades/ListView>
#include <bb/cascades/TextField>
#include "DompetKuPage.h"
#include "LoadingPopup.h"
#include "HttpRequest.h"
#include "CariKodeBank.h"
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>

using namespace bb::data;
using namespace bb::cascades;

class MenuRekeningBank : public DompetKuPage{
	Q_OBJECT
public:
	MenuRekeningBank();
	virtual ~MenuRekeningBank();

	Q_INVOKABLE void kirimUang();
	Q_INVOKABLE void searchBank();

private:
	LoadingPopup *loadingPopup;
	TextField *pinField;
	TextField *kodeField;
	TextField *tujuanField;
	TextField *jumlahField;
	HttpRequest *httpRequest;
	CariKodeBank *cariKodeBank;
	QString kodeBankSelected;

private slots:
	void onTextPinChanging(QString text);
	void onHttpReply(QByteArray data);
	void onBankSelected(QString kode, QString nama);
};

#endif /* MenuRekeningBank_H_ */

