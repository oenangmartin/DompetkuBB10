/*
 * MenuToken.cpp
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#include "MenuToken.h"
#include "Global.h"
#include "UiUtils.h"
#include "applicationui.hpp"
#include "InboxData.h"

MenuToken::MenuToken() {
	httpRequest = NULL;
	loadingPopup = NULL;

	setTitle("Minta Token");
	bottomMenuController->getButtonToken()->setSelected(true);
	QmlDocument *qml = QmlDocument::create("asset:///bottomMenu/MenuToken.qml").parent(this);
	qml->setContextProperty("page", this);

	Container* pageContent = qml->createRootObject<Container>();
	pageContent->setImplicitLayoutAnimationsEnabled(false);

	contentContainer->add(pageContent);

	pinField = pageContent->findChild<TextField*>("pin");
	connect(pinField, SIGNAL(textChanging(QString) ), this,
								SLOT(onTextPinChanging(QString)));
}

void MenuToken::onTextPinChanging(QString text){
	if(text.length() > 6){
		pinField->setText(text.left(6));
	}
}

void MenuToken::requestToken(){
	if (pinField->text().trimmed().length() == 0) {
			UiUtils::toast(
					"Mohon isi PIN",this);
			pinField->requestFocus();
			return;
		}

	if(httpRequest == NULL){
		httpRequest = new HttpRequest(this);
			connect(httpRequest, SIGNAL(httpReply(QByteArray)), this,
					SLOT(onHttpReply(QByteArray)));
	}
	if (loadingPopup == NULL) {
		loadingPopup = new LoadingPopup(this, "Processing");
	}
	loadingPopup->showLoading();

	/*QString url = API_GET_TOKEN + "?signature="
					+ ApplicationUI::accManager->getEncodedSignature() + "&userid=" + USER_ID;
			qDebug() << ">>url: " << url;
			httpRequest->get(url);*/

	QUrl postData;
			postData.addQueryItem("userid", ApplicationUI::accManager->getUser()->getUserId());
			postData.addQueryItem("signature", ApplicationUI::accManager->generateSignature(pinField->text().trimmed()));
			httpRequest->post(API_GET_TOKEN, postData);

			qDebug() << ">>d: " << postData;
}

/*
 status: 0,
  trxid: "24448807",
  msg: "Success",
  couponId: "2312524339",
  billpayQueryState: false,
  billpayState: false
 */
void MenuToken::onHttpReply(QByteArray data) {
	loadingPopup->hideLoading();
	if (!data.isNull()) {
		QString dataStr(data);
		qDebug() << ">>onHttpReply: " << dataStr;
		JsonDataAccess jda;
		QVariantMap qmap = jda.loadFromBuffer(data).value<QVariantMap>();
		int status = qmap.value("status").toInt();
		if(status == 0)
		{
			QDateTime now = QDateTime::currentDateTime();
			QString expired = now.addSecs(3600).toString("dd/MM/yyyy hh:mm");

			InboxData data;
			data.setRefId(qmap.value("trxid").toString());
			data.setToken(qmap.value("couponId").toString());
			data.setExpired(expired);
			data.setTanggal(now.toString("dddd MMM yyyy hh:mm"));
			data.setAction("Minta Token");
			data.setMerchant("-");
			data.setTujuan("-");
			data.setJumlah("-");
			ApplicationUI::inboxController->add(data);

			ApplicationUI::showBerandaPage("Transaksi berhasil! \nToken Anda: " + qmap.value("couponId").toString() +" akan kadaluarsa pada: " + expired + ". Ref: " + qmap.value("trxid").toString());
		}
		else if(status == 1012){
			UiUtils::alert("Pin anda telah terblokir. Silahkan menghubungi Customer Service Indosat di 100/111.", this);
		}
		else if(status == 1014){
			UiUtils::alert("Pin Salah, Silahkan Periksa kembali Pin anda", this);
		}
		else
		{
			UiUtils::alert(qmap.value("msg").toString(), this);
		}

	}
  else {
		UiUtils::alert(WORDING_KONEKSI_GAGAL, this);
	}
}

MenuToken::~MenuToken() {
	// TODO Auto-generated destructor stub
}

