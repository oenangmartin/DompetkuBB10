/*
 * MenuUangElektronik.h
 *
 *  Created on: Dec 14, 2014
 *      Author: yogi
 */

/*
 * MenuUangElektronik.h
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#ifndef MenuUangElektronik_H_
#define MenuUangElektronik_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include <bb/cascades/ListView>
#include <bb/cascades/TextField>
#include "DompetKuPage.h"
#include "LoadingPopup.h"
#include "HttpRequest.h"
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>

using namespace bb::data;
using namespace bb::cascades;

class MenuUangElektronik : public DompetKuPage{
	Q_OBJECT
public:
	MenuUangElektronik();
	virtual ~MenuUangElektronik();

	Q_INVOKABLE void kirimUang();

private:
	LoadingPopup *loadingPopup;
	TextField *pinField;
	TextField *jumlahField;
	TextField *tujuanField;
	HttpRequest *httpRequest;

private slots:
	void onHttpReply(QByteArray data);
};

#endif /* MenuUangElektronik_H_ */

