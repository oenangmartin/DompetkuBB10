/*
 * MenuBeli.h
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#ifndef MENUBELI_H_
#define MENUBELI_H_
#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include <bb/cascades/ListView>
#include "DompetKuPage.h"

#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>

using namespace bb::data;
using namespace bb::cascades;

class MenuBeli : public DompetKuPage{
	Q_OBJECT
public:
	MenuBeli();
	virtual ~MenuBeli();

private:
	ListView *listView;
	void setData(QVariantList list);

	GroupDataModel *dataModel;

private slots:
	void onListClicked(const QVariantList indexPath);

	void onBayarListLoaded(bool success);
};

#endif /* MENUBELI_H_ */
