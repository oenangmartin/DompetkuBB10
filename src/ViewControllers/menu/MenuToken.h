/*
 * MenuToken.h
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#ifndef MENUTOKEN_H_
#define MENUTOKEN_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include <bb/cascades/ListView>
#include <bb/cascades/TextField>
#include "DompetKuPage.h"
#include "LoadingPopup.h"
#include "HttpRequest.h"
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>

using namespace bb::data;
using namespace bb::cascades;

class MenuToken : public DompetKuPage{
	Q_OBJECT
public:
	MenuToken();
	virtual ~MenuToken();

	Q_INVOKABLE void requestToken();

private:
	LoadingPopup *loadingPopup;
	TextField *pinField;
	HttpRequest *httpRequest;

private slots:
	void onHttpReply(QByteArray data);
	void onTextPinChanging(QString text);
};

#endif /* MENUTOKEN_H_ */
