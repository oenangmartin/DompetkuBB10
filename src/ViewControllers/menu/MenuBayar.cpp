/*
 * MenuBayar.cpp
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#include "MenuBayar.h"
#include "MerchantManager.h"
#include "applicationui.hpp"
#include "SubMenu.h"
#include "Global.h"

MenuBayar::MenuBayar() {
	setTitle("Bayar");
	bottomMenuController->getButtonBayar()->setSelected(true);
	QmlDocument *qml =
			QmlDocument::create("asset:///bottomMenu/MenuBayar.qml").parent(
					this);
	Container* pageContent = qml->createRootObject<Container>();
	contentContainer->add(pageContent);

	listView = pageContent->findChild<ListView*>("list");
	connect(listView, SIGNAL(triggered(const QVariantList)), this,SLOT(onListClicked(const QVariantList)));
	/*if (listView) {
		connect(listView, SIGNAL(triggered(const QVariantList)), this,SLOT(onListClicked(const QVariantList)));
		JsonDataAccess jda;
		QVariantList list = jda.load(
				QDir::currentPath()
						+ "/app/native/assets/models/BayarMenu.json").value<
				QVariantList>();
		dataModel = new GroupDataModel();
		dataModel->setParent(this);
		dataModel->insertList(list);
		dataModel->setGrouping(ItemGrouping::None);
		dataModel->setSortedAscending(false);
		listView->setDataModel(dataModel);
	}*/

	MerchantManager *merchantManager = MerchantManager::getInstance();

	if(merchantManager->isDataLoaded())
	{
		setData(merchantManager->getBayarList());
	}
	else{
		connect(merchantManager, SIGNAL(dataLoaded(bool)), this,
						SLOT(onBayarListLoaded(bool)));
		merchantManager->getMerchantList();
	}



}

void MenuBayar::onListClicked(QVariantList indexPath) {
	QVariantMap map = dataModel->data(indexPath).toMap();
	QString title = map.value("mnama").toString();
	qDebug() << ">>map: " << map;
	/*JsonDataAccess jda;
				QVariantList list = jda.load(
						QDir::currentPath()
								+ "/app/native/assets/models/" + title+"Menu.json").value<
						QVariantList>();*/

	SubMenu *subMenu = new SubMenu(title, map.value("sub_merchant").toList(), TYPE_BAYAR);
	ApplicationUI::nav->push(subMenu);
}

void MenuBayar::onBayarListLoaded(bool success){
	if(success) setData(MerchantManager::getInstance()->getBayarList());
}

void MenuBayar::setData(QVariantList list){
	qDebug() << ">>list: " << list;
	if (listView) {
		//connect(listView, SIGNAL(triggered(const QVariantList)), this,SLOT(onListClicked(const QVariantList)));
		JsonDataAccess jda;
		/*QVariantList list = jda.load(
				QDir::currentPath()
						+ "/app/native/assets/models/BayarMenu.json").value<
				QVariantList>();*/
		dataModel = new GroupDataModel();
		dataModel->setParent(this);
		dataModel->insertList(list);
		dataModel->setGrouping(ItemGrouping::None);
		dataModel->setSortedAscending(false);
		listView->setDataModel(dataModel);
	}
}

MenuBayar::~MenuBayar() {
	// TODO Auto-generated destructor stub
}

