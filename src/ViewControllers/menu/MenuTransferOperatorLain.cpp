/*
 * MenuTransferOperatorLain.cpp
 *
 *  Created on: Feb 1, 2015
 *      Author: yogi
 */

/*
 * MenuTransferOperatorLain.cpp
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#include "MenuTransferOperatorLain.h"
#include "Global.h"
#include "UiUtils.h"
#include "applicationui.hpp"

MenuTransferOperatorLain::MenuTransferOperatorLain() {
	httpRequest = NULL;
	loadingPopup = NULL;
	contactPicker = NULL;

	setTitle("Operator Lain");
	bottomMenuController->getButtonTransfer()->setSelected(true);
	QmlDocument *qml = UiUtils::LoadQml("bottomMenu/MenuTransferOperatorLain.qml",this);
	qml->setContextProperty("page", this);

	Container* pageContent = qml->createRootObject<Container>();
	pageContent->setImplicitLayoutAnimationsEnabled(false);

	contentContainer->add(pageContent);

	pinField = pageContent->findChild<TextField*>("pinField");
	connect(pinField, SIGNAL(textChanging(QString) ), this,
							SLOT(onTextPinChanging(QString)));

	jumlahField = pageContent->findChild<TextField*>("jumlahField");
	tujuanField = pageContent->findChild<TextField*>("tujuanField");
	connect(tujuanField, SIGNAL(textChanged(QString) ), this,
								SLOT(onTextTujuanChanging(QString)));

}

void MenuTransferOperatorLain::onTextPinChanging(QString text){
	if(text.length() > 6){
		pinField->setText(text.left(6));
	}
}
void MenuTransferOperatorLain::onTextTujuanChanging(QString text){
	//remove 62 or +62
	if(text.length() > 2 && text.left(2).compare("62") == 0)
	{
		text = "0" + text.right(text.length() - 2);
	}
	else if(text.length() > 3 && text.left(3).compare("+62") == 0)
	{
		text = "0" + text.right(text.length() - 3);
	}

	text.replace('-', "", Qt::CaseInsensitive);
	text.replace(' ', "", Qt::CaseInsensitive);
	tujuanField->setText(text);

	if(text.length() > 4 && !validateTujuan(text)){
		UiUtils::toast(
						"Nomor Tujuan harus operator Telkomsel atau XL",this);
		tujuanField->requestFocus();
	}
}

bool MenuTransferOperatorLain::validateTujuan(QString text){
	//tsel / xl saja
	QRegExp rx("[;]");
	QString allowedNumber = "0811;0812;0813;0817;0818;0819;0821;0822;0823;0852;0853;0859;0877;0878;0879";
	QStringList list = allowedNumber.split(rx, QString::SkipEmptyParts);
	qDebug() << ">>list: " << list;
	for (int i = 0; i < list.length(); i++)
	{
		if(text.left(4).compare(list.at(i)) == 0) return true;
	}

	return false;
}

void MenuTransferOperatorLain::searchTujuan(){
	if(contactPicker == NULL)
		{
			contactPicker = new ContactPicker();
			contactPicker->setMode(ContactSelectionMode::Single);
			contactPicker->setKindFilters(QSet<bb::pim::contacts::AttributeKind::Type>()
				 << bb::pim::contacts::AttributeKind::Phone);

			QObject::connect(contactPicker,
				 SIGNAL(contactSelected(int)),
				 this,
				 SLOT(onContactsSelected(int)));

			/*QObject::connect(contactPicker,
				 SIGNAL(canceled()),
				 this,
				 SLOT(onCanceled()));**/
		}
		contactPicker->open();
	}

	void MenuTransferOperatorLain::onContactsSelected(int contactId){
		Contact contact = ContactService().contactDetails(contactId);
		QList<ContactAttribute> list = contact.phoneNumbers();
		ContactAttribute selectedContact = list.at(0);
		if(selectedContact.kind() == AttributeKind::Phone){
			tujuanField->setText(selectedContact.value());
		}
		else{
			UiUtils::toast("Kontak yang dipilih tidak ada nomor telepon", this);
		}

	}
void MenuTransferOperatorLain::kirimUang(){
	if (tujuanField->text().trimmed().length() == 0) {
		UiUtils::toast(
				"Mohon isi Nomor Tujuan",this);
		tujuanField->requestFocus();
		return;
	}
	if (jumlahField->text().trimmed().length() == 0) {
		UiUtils::toast(
				"Mohon isi Jumlah",this);
		jumlahField->requestFocus();
		return;
	}
	if (pinField->text().trimmed().length() == 0) {
		UiUtils::toast(
				"Mohon isi PIN",this);
		pinField->requestFocus();
		return;
	}

	if(httpRequest == NULL){
		httpRequest = new HttpRequest(this);
			connect(httpRequest, SIGNAL(httpReply(QByteArray)), this,
					SLOT(onHttpReply(QByteArray)));
	}
	if (loadingPopup == NULL) {
		loadingPopup = new LoadingPopup(this, "Loading");
	}
	loadingPopup->showLoading();

	//signature=dCikmtZ90C6jBN88aTxTsIb9VrKmHU2BP2ReXf1VRQ+p5b6eVq4SbA==&userid=web_api_test&msisdn=085814047024&amount=3000

	/*QString url = API_MONEY_TRANSFER + "?signature="
					+ ApplicationUI::accManager->getEncodedSignature() + "&userid=" + USER_ID;
			qDebug() << ">>url: " << url;
			httpRequest->get(url);*/

	QUrl postData;
		postData.addQueryItem("userid", ApplicationUI::accManager->getUser()->getUserId());
		postData.addQueryItem("signature", ApplicationUI::accManager->generateSignature(pinField->text().trimmed()));
		postData.addQueryItem("to", tujuanField->text().trimmed());
		postData.addQueryItem("amount", jumlahField->text().trimmed());
		httpRequest->post(API_TRANSFER_OP_LAIN, postData);
}

void MenuTransferOperatorLain::onHttpReply(QByteArray data) {
	loadingPopup->hideLoading();
	if (!data.isNull()) {
		QString dataStr(data);
		qDebug() << ">>onHttpReply: " << dataStr;
		JsonDataAccess jda;
		QVariantMap qmap = jda.loadFromBuffer(data).value<QVariantMap>();
		int status = qmap.value("status").toInt();
		if (status == 0)
		{
			ApplicationUI::showBerandaPage("Anda akan menerima konfirmasi pop-up di handphone Anda.");
			//UiUtils::alert("Anda akan menerima konfirmasi pop-up di handphone Anda." , this);
		}
		else if(status == 1012){
			UiUtils::alert("Pin anda telah terblokir. Silahkan menghubungi Customer Service Indosat di 100/111.", this);
		}
		else if(status == 1014){
			UiUtils::alert("Pin Salah, Silahkan Periksa kembali Pin anda", this);
		}
		else {
			UiUtils::alert("Transaksi gagal", this);
			//UiUtils::alert(qmap.value("msg").toString(), this);
		}

	}
  else {
		UiUtils::alert(WORDING_KONEKSI_GAGAL, this);
	}
}

MenuTransferOperatorLain::~MenuTransferOperatorLain() {
	// TODO Auto-generated destructor stub
}



