/*
 * MenuBeli.cpp
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#include "MenuBeli.h"
#include "MerchantManager.h"
#include "applicationui.hpp"
#include "SubMenu.h"
#include "Global.h"

MenuBeli::MenuBeli() {
	setTitle("Beli");
	bottomMenuController->getButtonBeli()->setSelected(true);
		QmlDocument *qml =
				QmlDocument::create("asset:///bottomMenu/MenuBeli.qml").parent(
						this);
		Container* pageContent = qml->createRootObject<Container>();
		contentContainer->add(pageContent);

		listView = pageContent->findChild<ListView*>("list");
			connect(listView, SIGNAL(triggered(const QVariantList)), this,SLOT(onListClicked(const QVariantList)));
			/*if (listView) {
				connect(listView, SIGNAL(triggered(const QVariantList)), this,SLOT(onListClicked(const QVariantList)));
				JsonDataAccess jda;
				QVariantList list = jda.load(
						QDir::currentPath()
								+ "/app/native/assets/models/BayarMenu.json").value<
						QVariantList>();
				dataModel = new GroupDataModel();
				dataModel->setParent(this);
				dataModel->insertList(list);
				dataModel->setGrouping(ItemGrouping::None);
				dataModel->setSortedAscending(false);
				listView->setDataModel(dataModel);
			}*/

			MerchantManager *merchantManager = MerchantManager::getInstance();

			if(merchantManager->isDataLoaded())
			{
				setData(merchantManager->getBeliList());
			}
			else{
				indicator->start();
				connect(merchantManager, SIGNAL(dataLoaded(bool)), this,
								SLOT(onBayarListLoaded(bool)));
				merchantManager->getMerchantList();
			}

}

void MenuBeli::onListClicked(QVariantList indexPath) {
	QVariantMap map = dataModel->data(indexPath).toMap();
	QString title = map.value("mnama").toString();
	qDebug() << ">>map: " << map;
	/*JsonDataAccess jda;
				QVariantList list = jda.load(
						QDir::currentPath()
								+ "/app/native/assets/models/" + title+"Menu.json").value<
						QVariantList>();*/

	SubMenu *subMenu = new SubMenu(title, map.value("sub_merchant").toList(), TYPE_BELI);
	ApplicationUI::nav->push(subMenu);
}

void MenuBeli::onBayarListLoaded(bool success){
	indicator->stop();
	if(success) setData(MerchantManager::getInstance()->getBeliList());
}

void MenuBeli::setData(QVariantList list){
	qDebug() << ">>list: " << list;
	if (listView) {
		//connect(listView, SIGNAL(triggered(const QVariantList)), this,SLOT(onListClicked(const QVariantList)));
		JsonDataAccess jda;
		/*QVariantList list = jda.load(
				QDir::currentPath()
						+ "/app/native/assets/models/BayarMenu.json").value<
				QVariantList>();*/
		dataModel = new GroupDataModel();
		dataModel->setParent(this);
		dataModel->insertList(list);
		dataModel->setGrouping(ItemGrouping::None);
		dataModel->setSortedAscending(false);
		listView->setDataModel(dataModel);
	}
}

MenuBeli::~MenuBeli() {
	// TODO Auto-generated destructor stub
}

