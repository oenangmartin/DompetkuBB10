/*
 * InformasiViewController.cpp
 *
 *  Created on: Dec 8, 2014
 *      Author: yogi
 */

#include "HistoryViewController.h"
#include "InformasiViewController.h"
#include "DaftarMerchantViewController.h"

#include "applicationui.hpp"
#include "UiUtils.h"
#include "HubungiKami.h"
#include "Terms.h"
#include "InformasiDetail.h"

InformasiViewController::InformasiViewController() {

	QmlDocument *qml = UiUtils::LoadQml("InformasiPage.qml", this);
	qml->setContextProperty("page", this);
	Container *root = qml->createRootObject<Container>();
	setContent(root);

	listView = root->findChild<ListView*>("list");
	if (listView) {
		qDebug() << ">>inf";
		connect(listView, SIGNAL(triggered(const QVariantList)), this,SLOT(onListClicked(const QVariantList)));
		JsonDataAccess jda;
		QVariantList list = jda.load(
				QDir::currentPath()
						+ "/app/native/assets/models/InformasiMenu.json").value<
				QVariantList>();
		dataModel = new GroupDataModel();
		dataModel->setParent(this);
		dataModel->insertList(list);
		dataModel->setGrouping(ItemGrouping::None);
		dataModel->setSortedAscending(false);
		listView->setDataModel(dataModel);
	}

}

void InformasiViewController::onListClicked(QVariantList indexPath) {
	QVariantMap map = dataModel->data(indexPath).toMap();
	if(map.value("title").toString().compare("Histori Transaksi") == 0)
	{
		ApplicationUI::nav->push(new HistoryViewController);
	}
	else if(map.value("title").toString().compare("Daftar Merchant") == 0)
	{
		ApplicationUI::nav->push(new DaftarMerchantViewController);
	}
	else{
		ApplicationUI::nav->push(new InformasiDetail(map.value("source").toString()));
	}
}

void InformasiViewController::back(){
	ApplicationUI::nav->pop();
}

InformasiViewController::~InformasiViewController() {
	// TODO Auto-generated destructor stub
}


