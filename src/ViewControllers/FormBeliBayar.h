/*
 * FormBeliBayar.h
 *
 *  Created on: Nov 16, 2014
 *      Author: yogi
 */

#ifndef FORMBELIBAYAR_H_
#define FORMBELIBAYAR_H_


#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/ImageView>
#include <bb/cascades/Container>
#include <bb/cascades/ImageToggleButton>
#include <bb/cascades/Control>
#include <bb/cascades/TextField>
#include "DompetKuPage.h"
#include "LoadingPopup.h"
#include "HttpRequest.h"
#include "FavoritController.h"
#include <bb/data/JsonDataAccess>
#include <bb/system/SystemDialog>
#include <bb/cascades/pickers/ContactPicker>
#include <bb/pim/contacts/ContactService>
#include <bb/pim/contacts/Contact>
#include <bb/pim/contacts/ContactAttribute>
#include <bb/cascades/DateTimePicker>

using namespace bb::data;
using namespace bb::cascades;
using namespace bb::system;
using namespace bb::cascades::pickers;
using namespace bb::pim::contacts;

class FormBeliBayar : public DompetKuPage{
	Q_OBJECT
public:
	FormBeliBayar(QVariantMap qmap, int formType, QString namaItem);
	virtual ~FormBeliBayar();
	Q_INVOKABLE void submitForm();
	void setFavoritNumber(QString number);


private:
	QVariantList listField;
	Container *formContainer;
	QVector<Control*> fieldVector;
	HttpRequest *submit1HttpRequest;
	HttpRequest *submit2HttpRequest;
	LoadingPopup *loadingPopup;
	QString urlAction1;
	QString urlAction2;
	QString trxid;
	int formType;
	QString msisdn;
	QString mTo;
	QString mDenom;
	QString mAmount;
	QString pin;
	QString price;
	QString nominal;
	QString voucherCode;
	bool fieldToExist;
	SystemDialog *prompt;
	ContactPicker *contactPicker;
	TextField *phoneNumberField;
	TextField *pinTextField;
	ImageButton *favoriteButton;
	FavoritController *favoritController;
	QString namaItem;
	QString jsonData;
	QString favoritNumber;
	QString favoritLabel;

	bool onlyOneUrl;

	void alert(QString message);


	QString promptOneUrlMessage;
	void showPrompt(QString message);
	void hitUrl1();
	void hitUrl2();
	void showSuccessMessage();


private slots:
	void onListFieldReturned(QByteArray data);
	void onUrlAction1Returned(QByteArray data);
	void onUrlAction2Returned(QByteArray data);
	void onPromptAnswered(bb::system::SystemUiResult::Type);
	void onFavoriteClicked();
	void onBtnSearchClicked();
	void onAlertDismiss();
	void onContactsSelected(int);
	void onTextPinChanging(QString text);
};

#endif /* FORMBELIBAYAR_H_ */
