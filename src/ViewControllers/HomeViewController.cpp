/*
 * HomeViewController.cpp
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#include "HomeViewController.h"
#include "UiUtils.h"
#include "applicationui.hpp"
#include "Global.h"
#include "TutorialViewController.h"
#include "BottomMenuController.h"
#include "HistoryViewController.h"
#include "PromoDetailPage.h"
#include <bb/cascades/LayoutUpdateHandler>
#include <bb/cascades/DockLayout>

HomeViewController::HomeViewController() {
	bottomMenuHeight = 0;
	headerHeight = 0;
	promoHeight = 0;
	saldoHeight = 0;
	profileHeight = 0;
	transaksiHeight = 0;
	loadCount = 0;
	timer = NULL;
	currPromoIdx = 0;

	QmlDocument *qml = UiUtils::LoadQml("pages/HomeView.qml", this);
	leftMenuWidth = UiUtils::DisplayW - 100;
	qml->setContextProperty("page", this);
	root = qml->createRootObject<Container>();
	setContent(root);


	leftMenuController = new LeftMenuViewController(
			root->findChild<Label*>("lblUser"), root->findChild<ListView*>("menuList"));
	qml->setContextProperty("leftMenu", leftMenuController);

	Container *headerContainer = root->findChild<Container*>("headerContainer");
	LayoutUpdateHandler::create(headerContainer).onLayoutFrameChanged(this,
			SLOT(headerUpdateHandler(QRectF)));

	contentContainer = root->findChild<Container*>("contentContainer");
	menuContainer = root->findChild<Container*>("menuContainer");
	transaksiContainer = root->findChild<Container*>("transaksiContainer");
	indicator = root->findChild<ActivityIndicator*>("indicator");
	refreshButton = root->findChild<ImageView*>("refreshButton");
	//rightMenuController = new RightMenuController;
	//rightMenuController->getMenuContainer()->setPreferredWidth(leftMenuWidth);
	//rightMenuController->getMenuContainer()->setTranslationX(-leftMenuWidth);

	//menuContainer->add(rightMenuController);

	//rightMenuController->getMenuContainer()->setVisible(false);

	//bottomButtonContainer
	Container *bottomButtonContainer = root->findChild<Container*>(
			"bottomButtonContainer");
	LayoutUpdateHandler::create(bottomButtonContainer).onLayoutFrameChanged(
			this, SLOT(bottomMenuUpdateHandler(QRectF)));

	BottomMenuController *bottomMenu = new BottomMenuController(this);
	bottomMenu->disableSelectionIcon(true);
	bottomButtonContainer->add(bottomMenu);

	//profileContainer
	/*Container *profileContainer = root->findChild<Container*>("profileContainer");
	 LayoutUpdateHandler::create(profileContainer)
	 .onLayoutFrameChanged(this, SLOT(profileUpdateHandler(QRectF)));

	 //saldoContainer
	 Container *saldoContainer = root->findChild<Container*>("saldoContainer");
	 LayoutUpdateHandler::create(saldoContainer)
	 .onLayoutFrameChanged(this, SLOT(saldoUpdateHandler(QRectF)));

	 //transaksiContainer
	 Container *transaksiContainer = root->findChild<Container*>("transaksiContainer");
	 LayoutUpdateHandler::create(transaksiContainer)
	 .onLayoutFrameChanged(this, SLOT(transaksiUpdateHandler(QRectF)));

	 //promoContainer
	 Container *promoContainer = root->findChild<Container*>("promoContainer");
	 LayoutUpdateHandler::create(promoContainer)
	 .onLayoutFrameChanged(this, SLOT(promoUpdateHandler(QRectF)));
	 */
	scroll = root->findChild<ScrollView*>("scroll");

	//promo
	promoManager = new PromoManager;
	promoManager->setParent(this);
	connect(promoManager, SIGNAL(promoLoaded(bool, QVariantList)), this,
			SLOT(onPromoLoaded(bool, QVariantList)));

	//load balance
	balanceManager = new BalanceManager;
	balanceManager->setParent(this);
	connect(balanceManager, SIGNAL(balanceLoaded(QVariantMap)), this,
			SLOT(onBalanceLoaded(QVariantMap)));
	connect(balanceManager, SIGNAL(failedLoadBalance()), this,
			SLOT(onFailedLoadBalance()));
	//balanceManager->getBalance();

	historyManager = NULL;
	//loadHistoryTransaction("4");

	TutorialViewController *tutorialView = new TutorialViewController(this);
	tutorialView->show();

	refresh();

}

void HomeViewController::refresh() {
	loadCount = 2;
	setLoading(true);
	loadHistoryTransaction("3");
	balanceManager->getBalance();
	promoManager->getPromo();

}

void HomeViewController::setLoading(bool isLoad) {
	if (isLoad) {
		if (indicator)
			indicator->start();
		if (refreshButton)
			refreshButton->setVisible(false);
	} else {
		if (indicator)
			indicator->stop();
		if (refreshButton)
			refreshButton->setVisible(true);
	}
}

void HomeViewController::loadHistoryTransaction(QString count) {
	if (historyManager == NULL) {
		historyManager = new HistoryManager;
		historyManager->setParent(this);
		connect(historyManager, SIGNAL(historyLoaded(QVariantMap)), this,
				SLOT(onHistoryLoaded(QVariantMap)));
		connect(historyManager, SIGNAL(failedLoadHistory()), this,
				SLOT(onFailedLoadHistory()));
	}
	historyManager->getHistory(count);
}

/*
 //sample list
 Container {
 preferredWidth: Infinity
 background: Color.LightGray
 topPadding: 10
 bottomPadding: topPadding
 Container {
 layout: DockLayout {
 }
 preferredWidth: Infinity
 Label {
 translationX: 20
 text: "10/03/2014   Beli Pulsa"
 horizontalAlignment: HorizontalAlignment.Left
 verticalAlignment: VerticalAlignment.Center
 textStyle {
 fontSize: FontSize.Small
 color: Color.Black
 fontWeight: FontWeight.W100
 }
 }
 Label {
 translationX: -10
 text: "(25.000)"
 horizontalAlignment: HorizontalAlignment.Right
 verticalAlignment: VerticalAlignment.Center
 textStyle {
 fontSize: FontSize.Small
 color: Color.Black
 fontWeight: FontWeight.W100
 }
 }
 }

 } //sample list
 */
void HomeViewController::onHistoryLoaded(QVariantMap qmap) {
	loadCount--;
	if (loadCount == 0)
		setLoading(false);
	qDebug() << ">>onHistoryLoaded: " << qmap;
	transaksiContainer->setVisible(true);
	Container *historyContainer = root->findChild<Container*>(
			"historyContainer");

	historyContainer->removeAll();
	QVariantList transList = qmap.value("trxList").toList();
	for (int i = 0; i < transList.size(); i++) {

		QVariantMap itemTrans = transList.at(i).toMap();
		Container *base = Container::create();
		if (i % 2 == 0)
			base->setBackground(Color::fromARGB(0xffD7D7D9));
		base->setPreferredWidth(99999);
		base->setTopPadding(10);
		base->setBottomPadding(10);
		base->setLeftPadding(10);
		base->setRightPadding(10);

		Container *dock = Container::create().layout(DockLayout::create());
		dock->setPreferredWidth(99999);
		base->add(dock);

		Label *labelTrans = Label::create();
		labelTrans->setHorizontalAlignment(HorizontalAlignment::Left);
		labelTrans->setVerticalAlignment(VerticalAlignment::Center);
		//labelTrans->textStyle()->setColor(Color::fromRGBA(2.42, 1.67, 0.46));
		QString date = itemTrans.value("date").toString();
		if (date.indexOf(' ') > 0)
			date = date.left(date.indexOf(' '));
		labelTrans->setText(date + "   " + itemTrans.value("type").toString());
		labelTrans->textStyle()->setColor(Color::fromARGB(0xffF29257));
		dock->add(labelTrans);


		Label *labelValue = Label::create();
		labelValue->setHorizontalAlignment(HorizontalAlignment::Right);
		labelValue->setVerticalAlignment(VerticalAlignment::Center);

		QString amount;
		if(itemTrans.value("amount").toInt() < 0){
			amount = "(Rp " + QString::number(itemTrans.value("amount").toInt() * -1) + ")";
			labelValue->textStyle()->setColor(Color::fromARGB(0xffF29257));
		}
		else amount = itemTrans.value("amount").toString();

		labelValue->setText(amount);
		dock->add(labelValue);

		historyContainer->add(base);

	}

}

void HomeViewController::onPromoLoaded(bool status, QVariantList promoList) {
	if (!status)
		return;

	qDebug() << ">>promolist: " << promoList;
	ListView *listView = root->findChild<ListView*>("list");
	connect(listView, SIGNAL(triggered(const QVariantList)), this,
			SLOT(onPromoClicked(const QVariantList)));
	if (listView) {
	    QVariantList dataList;

	    /*for(int i = 0; i < promoList.size(); i++)
	    {
	       QString text = promoList.at(i).toMap().value("promo_text").toString() ;
           if(text.length() > 30)
           {
               QVariantMap qmap = promoList.at(i).toMap();
               qDebug() << "after: ";
               int cutIdx = text.indexOf(' ', 30, Qt::CaseInsensitive);
               if(cutIdx > 30)
               {
                   qDebug() << "cutIdx: " << QString::number(cutIdx);
                           //if(text.length()-cutIdx+1 )

                   int rightIdx = text.length() - cutIdx - 1;
                   if(rightIdx > 0)
                   {
                       QString newLine = text.right(rightIdx);
                       qDebug() << "text: " << text.left(cutIdx);
                       qDebug() << "newLine: "<< newLine;
                       qmap.insert("text1", text.left(cutIdx));
                       qmap.insert("text2", newLine);
                       dataList.append(qmap);
                   }

               }

           }
           else{
               dataList.append(promoList.at(i).toMap());
           }
	    }*/
		promoDataModel = new GroupDataModel();
		promoDataModel->setParent(this);
		promoDataModel->insertList(promoList);
		promoDataModel->setGrouping(ItemGrouping::None);
		promoDataModel->setSortedAscending(false);
		listView->setDataModel(promoDataModel);
	}
	if (timer == NULL) {
		timer = new Timer(this);
		timer->setInterval(3000);
		connect(timer, SIGNAL(timeout()), this, SLOT(onTimerPromoTimeout()));
		timer->start();
	}
}
void HomeViewController::onTimerPromoTimeout() {
	int moveIdx = (currPromoIdx++) % promoDataModel->size();
	//qDebug() << ">>timeout: " << QString::number(moveIdx);
	QVariantList indexPath;
	indexPath.append(moveIdx);

	ListView *listView = root->findChild<ListView*>("list");
	listView->scrollToItem(indexPath, ScrollAnimation::Smooth);
	timer->start();
}
void HomeViewController::onPromoClicked(QVariantList indexPath) {
	QVariantMap map = promoDataModel->data(indexPath).toMap();
	ApplicationUI::nav->push(
			new PromoDetailPage(this, map.value("promo_url").toString()));
}
void HomeViewController::onFailedLoadHistory() {
	loadCount--;
	if (loadCount == 0)
		setLoading(false);
}
void HomeViewController::onBalanceLoaded(QVariantMap qmap) {
	loadCount--;
	if (loadCount == 0)
		setLoading(false);
	qDebug() << ">>balance: " << qmap;
	leftMenuController->setAccountName(qmap.value("name").toString());
	//{"status":0,"trxid":"22585638","balance":"77000","name":"08551436505","billpayQueryState":false,"billpayState":false}"
	Label *labelNama = root->findChild<Label*>("labelNama");
	labelNama->setText(qmap.value("name").toString());
	//rightMenuController->setAccoutName(qmap.value("name").toString());
	Label *labelSaldo = root->findChild<Label*>("labelSaldo");
	if(qmap.value("balance").toInt() > 0){
		labelSaldo->setText(
				"Rp "
						+ UiUtils::AddThousandSeparator(
								qmap.value("balance").toString()));
	}
	else {
	    if(labelSaldo->text().compare("Rp ") == 0){
	        labelSaldo->setText("Rp 0");
	    }
	}
	Label *labelHP = root->findChild<Label*>("labelHP");
	labelHP->setText(ApplicationUI::accManager->getUser()->getNoTlp());

	//Per 10/03/2014, 15:20
	Label *currentDateLabel = root->findChild<Label*>("currentDateLabel");
	currentDateLabel->setText(
			QDateTime::currentDateTime().toString("dd/mm/yyyy, hh:mm"));
}

void HomeViewController::onFailedLoadBalance() {
	loadCount--;
	if (loadCount == 0)
		setLoading(false);
}
void HomeViewController::closeLeftMenu() {
	if (contentContainer)
		contentContainer->setTranslationX(0);
	//rightMenuController->getMenuContainer()->setTranslationX(-leftMenuWidth);
	//rightMenuController->getMenuContainer()->setVisible(false);
	//menuContainer->setOpacity(1);
}
void HomeViewController::showMoreHistory() {
	ApplicationUI::nav->push(new HistoryViewController);
}
void HomeViewController::rightMenuTapped() {
	qDebug() << ">>menu show";
	//menuContainer->setOpacity(0.5);
	//rightMenuController->getMenuContainer()->setVisible(true);
	//rightMenuController->getMenuContainer()->setTranslationX(0);
}

void HomeViewController::headerUpdateHandler(QRectF rect) {
	headerHeight = rect.height();
	//rightMenuController->getMenuContainer()->setPreferredHeight(UiUtils::DisplayH - headerHeight - bottomMenuHeight);
}
void HomeViewController::bottomMenuUpdateHandler(QRectF rect) {
	bottomMenuHeight = rect.height();
	//rightMenuController->getMenuContainer()->setPreferredHeight(UiUtils::DisplayH - headerHeight - bottomMenuHeight);
}

void HomeViewController::saldoHandler(QRectF rect) {
	saldoHeight = rect.height();
	scroll->setPreferredHeight(saldoHeight + transaksiHeight + promoHeight);
}
void HomeViewController::transaksiUpdateHandler(QRectF rect) {
	transaksiHeight = rect.height();
	scroll->setPreferredHeight(saldoHeight + transaksiHeight + promoHeight);
}
void HomeViewController::promoUpdateHandler(QRectF rect) {
	promoHeight = rect.height();
	scroll->setPreferredHeight(saldoHeight + transaksiHeight + promoHeight);
}
void HomeViewController::profileHandler(QRectF rect) {
	profileHeight = rect.height();
	//rightMenuController->getMenuContainer()->setPreferredHeight(UiUtils::DisplayH - headerHeight - bottomMenuHeight);
}

HomeViewController::~HomeViewController() {
	// TODO Auto-generated destructor stub
}

