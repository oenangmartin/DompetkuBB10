/*
 * HubungiKami.cpp
 *
 *  Created on: Nov 30, 2014
 *      Author: yogi
 */

#include "HubungiKami.h"
#include "applicationui.hpp"
#include "UiUtils.h"

HubungiKami::HubungiKami() {

	QmlDocument *qml = UiUtils::LoadQml("HubungiKami.qml", this);
		qml->setContextProperty("page", this);
		Container *container = qml->createRootObject<Container>();
		setContent(container);

}

void HubungiKami::back(){
	ApplicationUI::nav->pop();
}

HubungiKami::~HubungiKami() {
	// TODO Auto-generated destructor stub
}

