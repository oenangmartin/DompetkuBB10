/*
 * HistoryViewController.h
 *
 *  Created on: Nov 4, 2014
 *      Author: yogi
 */

#ifndef HistoryViewController_H_
#define HistoryViewController_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include <bb/cascades/ListView>
#include <bb/cascades/TextField>
#include "DompetKuPage.h"
#include "LoadingPopup.h"
#include "HttpRequest.h"
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>

using namespace bb::data;
using namespace bb::cascades;

class HistoryViewController : public DompetKuPage{
	Q_OBJECT
public:
	HistoryViewController();
	virtual ~HistoryViewController();

	Q_INVOKABLE void getHistory();

private:
	HttpRequest *httpRequest;
	ListView *listView;
	GroupDataModel *dataModel;

private slots:
	void onHttpReply(QByteArray data);
	void onListClicked(const QVariantList indexPath);
};

#endif /* HistoryViewController_H_ */
