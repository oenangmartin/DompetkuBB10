/*
 * Terms.cpp
 *
 *  Created on: Nov 30, 2014
 *      Author: yogi
 */

#include "Terms.h"
#include "applicationui.hpp"
#include "UiUtils.h"

Terms::Terms() {

	QmlDocument *qml = UiUtils::LoadQml("login/Terms.qml", this);
		qml->setContextProperty("page", this);
		Container *container = qml->createRootObject<Container>();
		setContent(container);

}

void Terms::back(){
	ApplicationUI::nav->pop();
}

Terms::~Terms() {
	// TODO Auto-generated destructor stub
}

