/*
 * AdministrasiViewController.h
 *
 *  Created on: Jan 11, 2015
 *      Author: yogi
 */


/*
 * AdministrasiViewController.h
 *
 *  Created on: Dec 8, 2014
 *      Author: yogi
 */

#ifndef AdministrasiViewController_H_
#define AdministrasiViewController_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include "DompetKuPage.h"
#include <bb/cascades/ListView>
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>

using namespace bb::data;
using namespace bb::cascades;

class AdministrasiViewController : public Page{
	Q_OBJECT

public:
	AdministrasiViewController();
	virtual ~AdministrasiViewController();
	Q_INVOKABLE void back();

private:
	ListView *listView;
	void setData(QVariantList list);

	GroupDataModel *dataModel;

private slots:
	void onListClicked(const QVariantList indexPath);
	void onListActionDelete();
};

#endif /* AdministrasiViewController_H_ */


