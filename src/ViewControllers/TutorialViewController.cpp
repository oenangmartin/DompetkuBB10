/*
 * TutorialViewController.cpp
 *
 *  Created on: Jan 8, 2013
 *      Author: ASUS
 */

#include "TutorialViewController.h"
#include <bb/cascades/QmlDocument>
#include <bb/cascades/Dialog>
#include "UiUtils.h"
#include "Global.h"
TutorialViewController::TutorialViewController(QObject *parent){
	QmlDocument *dialogQml = UiUtils::LoadQml("TutorialView.qml", parent);
	dialog = dialogQml->createRootObject<Dialog>();

}

void TutorialViewController::show(){
	QSettings settings;

	if (settings.value(FIRST_OPEN_APPS).isNull()) {
		settings.setValue(FIRST_OPEN_APPS, QVariant("1"));
		dialog->open();
	}
}

void TutorialViewController::close(){
	if(dialog->isOpened())dialog->close();
}


TutorialViewController::~TutorialViewController() {
	// TODO Auto-generated destructor stub
}

