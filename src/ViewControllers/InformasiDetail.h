/*
 * InformasiDetail.h
 *
 *  Created on: Dec 21, 2014
 *      Author: yogi
 */

/*
 * InformasiDetail.h
 *
 *  Created on: Nov 30, 2014
 *      Author: yogi
 */

#ifndef InformasiDetail_H_
#define InformasiDetail_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include "DompetKuPage.h"


using namespace bb::cascades;

class InformasiDetail : public Page{
	Q_OBJECT
public:
	InformasiDetail(QString qmlSource);
	virtual ~InformasiDetail();
	Q_INVOKABLE void back();
};

#endif /* InformasiDetail_H_ */

