
/*
 * InboxViewController.h
 *
 *  Created on: Dec 8, 2014
 *      Author: yogi
 */

#ifndef InboxViewController_H_
#define InboxViewController_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include "DompetKuPage.h"
#include <bb/cascades/ListView>
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>

using namespace bb::data;
using namespace bb::cascades;

class InboxViewController : public Page{
	Q_OBJECT

public:
	InboxViewController();
	virtual ~InboxViewController();
	Q_INVOKABLE void back();
	void load();

private:
	ListView *listView;


	GroupDataModel *dataModel;

private slots:
	void onListClicked(const QVariantList indexPath);
	void onListActionDelete();
};

#endif /* InboxViewController_H_ */

