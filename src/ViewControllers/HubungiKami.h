/*
 * HubungiKami.h
 *
 *  Created on: Dec 21, 2014
 *      Author: yogi
 */

/*
 * HubungiKami.h
 *
 *  Created on: Nov 30, 2014
 *      Author: yogi
 */

#ifndef HubungiKami_H_
#define HubungiKami_H_

#include <bb/cascades/QmlDocument>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include "DompetKuPage.h"


using namespace bb::cascades;

class HubungiKami : public Page{
	Q_OBJECT
public:
	HubungiKami();
	virtual ~HubungiKami();
	Q_INVOKABLE void back();
};

#endif /* HubungiKami_H_ */

