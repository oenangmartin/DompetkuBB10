/*
 * InboxData.cpp
 *
 *  Created on: Jan 11, 2015
 *      Author: yogi
 */

#include "InboxData.h"

InboxData::InboxData() {

}
InboxData::InboxData(QVariantMap qmap) {
	setRefId(qmap.value("refId").toString());
	setTanggal(qmap.value("tanggal").toString());
	setAction(qmap.value("action").toString());
	setMerchant(qmap.value("merchant").toString());
	setTujuan(qmap.value("tujuan").toString());
	setJumlah(qmap.value("jumlah").toString());
	setToken(qmap.value("token").toString());
	setExpired(qmap.value("expired").toString());
	setId(qmap.value("id").toInt());
}

InboxData::~InboxData() {
}


QVariantMap InboxData::toMap() const
{
    QVariantMap map;
    map["refId"] = getRefId();
    map["tanggal"] = getTanggal();
    map["action"] = getAction();

    map["merchant"] = getMerchant();
    map["tujuan"] = getTujuan();
    map["jumlah"] = getJumlah();
    map["token"] = getToken();
    map["expired"] = getExpired();

    map["id"] = getId();

    return map;
}

const QString& InboxData::getAction() const {
	return action;
}

void InboxData::setAction(const QString& action) {
	this->action = action;
}

const QString& InboxData::getExpired() const {
	return expired;
}

void InboxData::setExpired(const QString& expired) {
	this->expired = expired;
}

int InboxData::getId() const {
	return id;
}

void InboxData::setId(int id) {
	this->id = id;
}

const QString& InboxData::getJumlah() const {
	return jumlah;
}

void InboxData::setJumlah(const QString& jumlah) {
	this->jumlah = jumlah;
}

const QString& InboxData::getMerchant() const {
	return merchant;
}

void InboxData::setMerchant(const QString& merchant) {
	this->merchant = merchant;
}

const QString& InboxData::getRefId() const {
	return refId;
}

void InboxData::setRefId(const QString& refId) {
	this->refId = refId;
}

const QString& InboxData::getTanggal() const {
	return tanggal;
}

void InboxData::setTanggal(const QString& tanggal) {
	this->tanggal = tanggal;
}

const QString& InboxData::getToken() const {
	return token;
}

void InboxData::setToken(const QString& token) {
	this->token = token;
}

const QString& InboxData::getTujuan() const {
	return tujuan;
}

void InboxData::setTujuan(const QString& tujuan) {
	this->tujuan = tujuan;
}
