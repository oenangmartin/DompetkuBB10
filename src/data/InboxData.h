/*
 * InboxData.h
 *
 *  Created on: Jan 11, 2015
 *      Author: yogi
 */

#ifndef INBOXDATA_H_
#define INBOXDATA_H_

class InboxData {
public:
	InboxData();
	InboxData(QVariantMap qmap);
	virtual ~InboxData();


	QVariantMap toMap() const;
	const QString& getAction() const;
	void setAction(const QString& action);
	const QString& getExpired() const;
	void setExpired(const QString& expired);
	int getId() const;
	void setId(int id);
	const QString& getJumlah() const;
	void setJumlah(const QString& jumlah);
	const QString& getMerchant() const;
	void setMerchant(const QString& merchant);
	const QString& getRefId() const;
	void setRefId(const QString& refId);
	const QString& getTanggal() const;
	void setTanggal(const QString& tanggal);
	const QString& getToken() const;
	void setToken(const QString& token);
	const QString& getTujuan() const;
	void setTujuan(const QString& tujuan);

private:
	QString refId, tanggal, action, merchant, tujuan, jumlah, token, expired;
	int id;
};

#endif /* INBOXDATA_H_ */
