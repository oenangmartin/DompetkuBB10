/*
 * FavoritData.cpp
 *
 *  Created on: Dec 25, 2014
 *      Author: yogi
 */

#include "FavoritData.h"

FavoritData::FavoritData() {
	// TODO Auto-generated constructor stub

}

FavoritData::~FavoritData() {
	// TODO Auto-generated destructor stub
}

const QString& FavoritData::getNama() const {
	return nama;
}

void FavoritData::setNama(const QString& nama) {
	this->nama = nama;
}

int FavoritData::getType() const {
	return type;
}

void FavoritData::setId(int id) {
	this->id = id;
}
void FavoritData::setType(int type) {
	this->type = type;
}

const QString& FavoritData::getTypeLabel() const {
	return typeLabel;
}

void FavoritData::setTypeLabel(const QString& typeLabel) {
	this->typeLabel = typeLabel;
}

const QString& FavoritData::getValue() const {
	return value;
}

void FavoritData::setValue(const QString& value) {
	this->value = value;
}

QVariantMap FavoritData::toMap() const
{
    QVariantMap map;
    map["nama"] = getNama();
    map["number"] = getValue();
    map["type"] = getType();
    map["typeLabel"] = getTypeLabel();
    map["jsonData"] = getJsonData();
    map["namaItem"] = getNamaItem();
    map["id"] = id;

    return map;
}

const QString& FavoritData::getJsonData() const {
	return jsonData;
}

void FavoritData::setJsonData(const QString& jsonData) {
	this->jsonData = jsonData;
}

const QString& FavoritData::getNamaItem() const {
	return namaItem;
}

void FavoritData::setNamaItem(const QString& namaItem) {
	this->namaItem = namaItem;
}
