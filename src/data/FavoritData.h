/*
 * FavoritData.h
 *
 *  Created on: Dec 25, 2014
 *      Author: yogi
 */

#ifndef FAVORITDATA_H_
#define FAVORITDATA_H_

class FavoritData {
public:
	FavoritData();
	virtual ~FavoritData();
	const QString& getNama() const;
	void setNama(const QString& nama);
	int getType() const;
	void setType(int type);
	void setId(int id);
	const QString& getTypeLabel() const;
	void setTypeLabel(const QString& typeLabel);
	const QString& getValue() const;
	void setValue(const QString& value);

	QVariantMap toMap() const;
	const QString& getJsonData() const;
	void setJsonData(const QString& jsonData);
	const QString& getNamaItem() const;
	void setNamaItem(const QString& namaItem);

private:
	QString nama;
	QString value;
	QString typeLabel;
	QString jsonData;
	QString namaItem;
	int type;
	int id;

};

#endif /* FAVORITDATA_H_ */
