/*
 * Merchant.cpp
 *
 *  Created on: Nov 8, 2014
 *      Author: yogi
 */

#include "Merchant.h"

Merchant::Merchant() {
	// TODO Auto-generated constructor stub

}

Merchant::~Merchant() {
	// TODO Auto-generated destructor stub
}

const QString& Merchant::getMid() const {
	return mid;
}

void Merchant::setMid(const QString& mid) {
	this->mid = mid;
}

const QString& Merchant::getMimage() const {
	return mimage;
}

void Merchant::setMimage(const QString& mimage) {
	this->mimage = mimage;
}

const QString& Merchant::getMnama() const {
	return mnama;
}

void Merchant::setMnama(const QString& mnama) {
	this->mnama = mnama;
}

const QVariantList& Merchant::getSubMerchant() const {
	return subMerchant;
}

void Merchant::setSubMerchant(const QVariantList& subMerchant) {
	this->subMerchant = subMerchant;
}
