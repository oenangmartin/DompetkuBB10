/*
 * MerchanDataModel.cpp
 *
 *  Created on: Feb 12, 2013
 *      Author: ASUS
 */


#include "MerchanDataModel.h"
#include "Global.h"
#include "MyIndexMapper.h"
#include "AccountManager.h"
MerchanDataModel::MerchanDataModel(QString apiUrl) :
		ListDataModel(apiUrl) {
	total_page = 0;
}

void MerchanDataModel::loadData(bool _isRefresh) {
		if (!noData || _isRefresh) {
			if(!_isRefresh ){
				if(total_page != 0 && start > total_page){
					noData = true;
					 emit dataLoaded(0);
					return;
				}
			}
			if (!isLoading) {
				isRefresh = _isRefresh;
				isLoading = true;
				noData = false;
				QVariantMap map;
				map["loadingitem"] = true;
				int addIndex = modelList->size();
				if (isRefresh) {
					addIndex = 0;
					modelList->insert(0, map);
					start = 1;
					emit refresh();
				} else
					modelList->append(map);

				emit itemsChanged(DataModelChangeType::AddRemove,
						QSharedPointer<MyIndexMapper>(
								new MyIndexMapper(addIndex, 1, false)));

				QString url = API_INFORMASI_MERCHANT + AccountManager::getInstance()->getUser()->getToken() + "/10/" + QString::number(start);
							qDebug() << ">>url: " << url;
							httpRequest->get(url);

			} else {
				qDebug() << ">>loading..";
			}
		}
		else{
			if(noData) emit dataLoaded(0);
		}
}

void MerchanDataModel::removeAllData() {
	int totalDeleted = modelList->length();
	for(int i=0; i <totalDeleted; i++) modelList->removeAt(i);
	emit itemsChanged(DataModelChangeType::AddRemove,
							QSharedPointer<MyIndexMapper>(
									new MyIndexMapper(0, totalDeleted, true)));


}

void MerchanDataModel::httpReply(QByteArray data) {
	removeLoadingItem();
	if (!data.isNull()) {
		QString dataStr(data);
		qDebug() << ">>data search: " << dataStr;

		JsonDataAccess jda;
		QVariantMap qmap = jda.loadFromBuffer(data).value<QVariantMap>();
		int status = qmap.value("status").toInt();
		emit dataLoaded(status);
		int addIndex;
		switch (status) {
		case 0: {
			total_page = qmap.value("total_page").toInt();
			QVariantList dataList = qmap.value("detail").toList();
			start++;
			if(dataList.size() % 10 != 0) noData = true;
			if (dataList.length() > 0) {
				if (isRefresh) {
					delete modelList;
					modelList = new QVariantList;
					emit itemsChanged(DataModelChangeType::AddRemove,
							QSharedPointer<MyIndexMapper>(
									new MyIndexMapper(0, 0, true)));
				}
				addIndex = modelList->size();
				modelList->append(dataList);

			} else {
				QVariantMap map;
				if(modelList->size() == 0)
					map["noitem"] = true;
				else
					map["nomoreitem"] = true;
				addIndex = modelList->size();
				modelList->append(map);
				noData = true;
			}
			emit itemsChanged(DataModelChangeType::AddRemove,
					QSharedPointer<MyIndexMapper>(
							new MyIndexMapper(addIndex, dataList.size(),
									false)));
		}
			break;
		case 3: {
			if (isRefresh) {
				qDebug() << ">>is refresh nodata";
				delete modelList;
				modelList = new QVariantList;
				emit itemsChanged(DataModelChangeType::AddRemove,
						QSharedPointer<MyIndexMapper>(
								new MyIndexMapper(0, 0, true)));
			}
			QVariantMap map;
			map["noitem"] = true;
			addIndex = modelList->size();
			modelList->append(map);
			emit itemsChanged(DataModelChangeType::AddRemove,
					QSharedPointer<MyIndexMapper>(
							new MyIndexMapper(addIndex, 1, false)));
			noData = true;
		}
			break;
		default:
			break;
		}
	} else
	{
		qDebug() << ">>data null";
		emit connectionError();
	}
	isLoading = false;
}

MerchanDataModel::~MerchanDataModel() {
	// TODO Auto-generated destructor stub
}

