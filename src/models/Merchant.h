/*
 * Merchant.h
 *
 *  Created on: Nov 8, 2014
 *      Author: yogi
 */

#ifndef MERCHANT_H_
#define MERCHANT_H_

class Merchant {
public:
	Merchant();
	virtual ~Merchant();
	const QString& getMid() const;
	void setMid(const QString& mid);
	const QString& getMimage() const;
	void setMimage(const QString& mimage);
	const QString& getMnama() const;
	void setMnama(const QString& mnama);
	const QVariantList& getSubMerchant() const;
	void setSubMerchant(const QVariantList& subMerchant);

/*
 {
      mid: "7",
      mnama: "Telepon",
      mimage: "",
      sub_merchant: [
        {
          sub_merchant_id: "25",
          sub_merchant_nama: "Matrix",
          img_path: "http://fb.elasitas.com/dompetku/images/sub_merchant/brand-matrix.png"
        },
        {
          sub_merchant_id: "26",
          sub_merchant_nama: "Telkom PSTN",
          img_path: "http://fb.elasitas.com/dompetku/images/sub_merchant/brand-telkom.png"
        }
      ]
    }
 */

private:
	QString mid, mnama, mimage;
	QVariantList subMerchant;

};

#endif /* MERCHANT_H_ */
