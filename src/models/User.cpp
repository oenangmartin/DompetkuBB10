/*
 * User.cpp
 *
 *  Created on: Nov 8, 2014
 *      Author: yogi
 */

#include "User.h"

User::User() {
	// TODO Auto-generated constructor stub

}

User::~User() {
	// TODO Auto-generated destructor stub
}

const QString& User::getNama() const {
	return nama;
}

void User::setNama(const QString& nama) {
	this->nama = nama;
}

const QString& User::getNoTlp() const {
	return noTlp;
}

void User::setNoTlp(const QString& noTlp) {
	this->noTlp = noTlp;
}

const QString& User::getPin() const {
	return pin;
}

void User::setPin(const QString& pin) {
	this->pin = pin;
}

const QString& User::getSaldo() const {
	return saldo;
}

void User::setSaldo(const QString& saldo) {
	this->saldo = saldo;
}

const QString& User::getToken() const {
	return token;
}

void User::setToken(const QString& token) {
	this->token = token;
}

QString User::getSignature() {

	return "";
}

const QString& User::getUserId() const {
	return userId;
}

void User::setUserId(const QString& userId) {
	this->userId = userId;
}
