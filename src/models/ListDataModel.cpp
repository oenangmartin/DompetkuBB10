/*
 * ListDataModel.cpp
 *
 *  Created on: Feb 12, 2013
 *      Author: ASUS
 */

#include "ListDataModel.h"

#include "MyIndexMapper.h"

ListDataModel::ListDataModel(QString _urlApi) {
	urlApi = _urlApi;
	httpRequest = new HttpRequest(this);
	connect(httpRequest, SIGNAL(httpReply(QByteArray)), this,
			SLOT(httpReply(QByteArray)));

	start = 0;
	isLoading = false;
	isRefresh = false;
	noData = false;
	modelList = new QVariantList;

}

void ListDataModel::loadData(QString keyword, bool _isRefresh) {}

void ListDataModel::loadData(bool _isRefresh) {}

void ListDataModel::loadData(QVariantList *data) {}

void ListDataModel::httpReply(QByteArray data) {
}


void ListDataModel::onClicked(QVariantList indexPath) {
}

void ListDataModel::removeAllData(){

}

void ListDataModel::removeLoadingItem() {
	QVariantList indexPath;
	if(isRefresh || modelList->size() == 1)
	{
		modelList->removeFirst();
		indexPath.append(0);
	}
	else {
		modelList->removeLast();
		indexPath.append(modelList->size());
	}
	emit itemRemoved(indexPath);
}

int ListDataModel::childCount(const QVariantList &indexPath) {
	if (indexPath.isEmpty())
		return modelList->size();
	else
		return 0;
}

bool ListDataModel::hasChildren(const QVariantList &indexPath) {
	if (indexPath.isEmpty()) {
		qDebug() << ">>empty data";
		return !modelList->isEmpty();
	} else {
		return false;
	}
}

QVariant ListDataModel::data(const QVariantList &indexPath) {
	//int currIdx = indexPath.at(0).toInt();
	//emit visibleIndex(currIdx);
	return modelList->at(indexPath.back().toInt());
}

ListDataModel::~ListDataModel() {
	// TODO Auto-generated destructor stub
}

