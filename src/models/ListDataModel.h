/*
 * ListDataModel.h
 *
 *  Created on: Feb 12, 2013
 *      Author: ASUS
 */

#ifndef LISTDATAMODEL_H_
#define LISTDATAMODEL_H_


#include <bb/cascades/QmlDocument>
#include <bb/cascades/DataModel>
#include <bb/data/JsonDataAccess>
#include "HttpRequest.h"

using namespace bb::cascades;
using namespace bb::data;

class ListDataModel: public DataModel {
Q_OBJECT
public:
	ListDataModel(QString apiUrl);
	virtual ~ListDataModel();
	HttpRequest *httpRequest;
	int start;
	bool isLoading;
	bool isRefresh;
	bool noData;

	QString urlApi;

	QVariantList *modelList;
	Q_INVOKABLE virtual void loadData(QString keyword, bool _isRefresh);
	Q_INVOKABLE virtual void loadData(bool _isRefresh);
	Q_INVOKABLE virtual void loadData(QVariantList *data);

	void removeLoadingItem();
	Q_INVOKABLE
	int childCount(const QVariantList &indexPath);
	Q_INVOKABLE
	bool hasChildren(const QVariantList &indexPath);
	Q_INVOKABLE
	QVariant data(const QVariantList &indexPath);

	virtual void removeAllData();

public slots:
	virtual void httpReply(QByteArray data);
	virtual void onClicked(QVariantList indexPath);

signals:
	void clicked(QString photoid);
	void connectionError();
	void refresh();
	void dataLoaded(int status);
	//void visibleIndex(int idx);
};

#endif /* LISTDATAMODEL_H_ */
