/*
 * MerchanDataModel.h
 *
 *  Created on: Feb 12, 2013
 *      Author: ASUS
 */

#ifndef MerchanDataModel_H_
#define MerchanDataModel_H_

#include "ListDataModel.h"

class MerchanDataModel : public ListDataModel {
public:
	MerchanDataModel(QString apiUrl);
	virtual ~MerchanDataModel();

	Q_INVOKABLE void loadData(bool isRefresh);
	void removeAllData();
	int total_page;

public slots:
	void httpReply(QByteArray data);

private:
	//virtual void onClicked(QVariantList indexPath);
};

#endif /* MerchanDataModel_H_ */
