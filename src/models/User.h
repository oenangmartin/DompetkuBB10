/*
 * User.h
 *
 *  Created on: Nov 8, 2014
 *      Author: yogi
 */

#ifndef USER_H_
#define USER_H_

class User {
public:
	User();
	virtual ~User();

	const QString& getNama() const;
	void setNama(const QString& nama);

	const QString& getNoTlp() const;
	void setNoTlp(const QString& noTlp);

	const QString& getSaldo() const;
	void setSaldo(const QString& saldo);

	const QString& getToken() const;
	void setToken(const QString& token);

	const QString& getPin() const;
	void setPin(const QString& pin);

	QString getSignature();

	const QString& getUserId() const;
	void setUserId(const QString& userId);

private:
	QString nama, noTlp, saldo, token, pin, userId;
};

#endif /* USER_H_ */
