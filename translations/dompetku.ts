<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>AdministrasiPage</name>
    <message>
        <location filename="../assets/AdministrasiPage.qml" line="64"/>
        <source>Administrasi</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BeliBayar</name>
    <message>
        <location filename="../assets/informasi/BeliBayar.qml" line="66"/>
        <source>Beli / bayar</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CariKodeBank</name>
    <message>
        <location filename="../assets/CariKodeBank.qml" line="62"/>
        <source>Kode Bank</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FavoritPage</name>
    <message>
        <location filename="../assets/FavoritPage.qml" line="64"/>
        <source>Favorit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GantiPinPage</name>
    <message>
        <location filename="../assets/GantiPinPage.qml" line="10"/>
        <source>PIN Lama</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/GantiPinPage.qml" line="32"/>
        <source>PIN Baru</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/GantiPinPage.qml" line="54"/>
        <source>Konfirmasi Pin Baru</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HubungiKami</name>
    <message>
        <location filename="../assets/informasi/HubungiKami.qml" line="63"/>
        <source>Hubungi Kami</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InboxDetailPage</name>
    <message>
        <location filename="../assets/InboxDetailPage.qml" line="47"/>
        <source>Inbox Detail</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InboxPage</name>
    <message>
        <location filename="../assets/InboxPage.qml" line="46"/>
        <source>Inbox</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InformasiPage</name>
    <message>
        <location filename="../assets/InformasiPage.qml" line="64"/>
        <source>Informasi</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KirimUang</name>
    <message>
        <location filename="../assets/informasi/KirimUang.qml" line="66"/>
        <source>Kirim Uang</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginView</name>
    <message>
        <location filename="../assets/login/LoginView.qml" line="60"/>
        <source>No HP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/login/LoginView.qml" line="90"/>
        <source>PIN</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LupaPinView</name>
    <message>
        <location filename="../assets/login/LupaPinView.qml" line="54"/>
        <source>Lupa PIN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/login/LupaPinView.qml" line="73"/>
        <source>Silahkan hubungi 111 dari nomor Matrix dan 100 dari nomor Mentari &amp; IM3 untuk permintaan reset PIN. PIN baru akan dikirimkan melalui SMS setelah kami verifikasi.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/login/LupaPinView.qml" line="78"/>
        <source>Masukan No HP account Anda, sistem akan megirimkan PIN baru ke nomor tersebut. Silahkan coba login kembali setelah mendapatkan PIN baru.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MenuRekeningBank</name>
    <message>
        <location filename="../assets/bottomMenu/MenuRekeningBank.qml" line="14"/>
        <source>Kode Bank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/bottomMenu/MenuRekeningBank.qml" line="51"/>
        <source>No Rekening Tujuan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/bottomMenu/MenuRekeningBank.qml" line="79"/>
        <source>Jumlah</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/bottomMenu/MenuRekeningBank.qml" line="107"/>
        <source>PIN</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MenuToken</name>
    <message>
        <location filename="../assets/bottomMenu/MenuToken.qml" line="10"/>
        <source>PIN</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MenuTransferDompetku</name>
    <message>
        <location filename="../assets/bottomMenu/MenuTransferDompetku.qml" line="17"/>
        <source>No Tujuan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/bottomMenu/MenuTransferDompetku.qml" line="65"/>
        <source>Jumlah</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/bottomMenu/MenuTransferDompetku.qml" line="93"/>
        <source>PIN</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MenuTransferOperatorLain</name>
    <message>
        <location filename="../assets/bottomMenu/MenuTransferOperatorLain.qml" line="16"/>
        <source>No Tujuan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/bottomMenu/MenuTransferOperatorLain.qml" line="63"/>
        <source>Jumlah</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/bottomMenu/MenuTransferOperatorLain.qml" line="91"/>
        <source>PIN</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MenuUangElektronik</name>
    <message>
        <location filename="../assets/bottomMenu/MenuUangElektronik.qml" line="12"/>
        <source>ID Tujuan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/bottomMenu/MenuUangElektronik.qml" line="40"/>
        <source>Jumlah</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/bottomMenu/MenuUangElektronik.qml" line="68"/>
        <source>PIN</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OneTimePass</name>
    <message>
        <location filename="../assets/login/OneTimePass.qml" line="59"/>
        <source>Kode Verifikasi</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegistrasiView</name>
    <message>
        <location filename="../assets/login/RegistrasiView.qml" line="63"/>
        <source>Registrasi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/login/RegistrasiView.qml" line="152"/>
        <source>Nama Depan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/login/RegistrasiView.qml" line="186"/>
        <source>Nama Belakang</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/login/RegistrasiView.qml" line="217"/>
        <source>Jenis Kelamin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/login/RegistrasiView.qml" line="258"/>
        <source>Pria</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/login/RegistrasiView.qml" line="289"/>
        <source>Wanita</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/login/RegistrasiView.qml" line="313"/>
        <source>Tanggal Lahir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/login/RegistrasiView.qml" line="369"/>
        <source>Alamat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/login/RegistrasiView.qml" line="395"/>
        <source>Tipe identitas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/login/RegistrasiView.qml" line="427"/>
        <source>No Identitas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/login/RegistrasiView.qml" line="454"/>
        <source>Nama Ibu Kandung</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/login/RegistrasiView.qml" line="494"/>
        <source>PIN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/login/RegistrasiView.qml" line="534"/>
        <source>Konfirmasi PIN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/login/RegistrasiView.qml" line="88"/>
        <source>No HP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/login/RegistrasiView.qml" line="124"/>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetorTunai</name>
    <message>
        <location filename="../assets/informasi/SetorTunai.qml" line="66"/>
        <source>Setor Tunai</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SyaratDanKetentuan</name>
    <message>
        <location filename="../assets/informasi/SyaratDanKetentuan.qml" line="66"/>
        <source>Syarat &amp; Ketentuan</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TarikTunai</name>
    <message>
        <location filename="../assets/informasi/TarikTunai.qml" line="66"/>
        <source>Tarik Tunai</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TentangDompetku</name>
    <message>
        <location filename="../assets/informasi/TentangDompetku.qml" line="66"/>
        <source>Tentang Dompetku</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Terms</name>
    <message>
        <location filename="../assets/login/Terms.qml" line="65"/>
        <source>Registrasi</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VerifikasiNo</name>
    <message>
        <location filename="../assets/login/VerifikasiNo.qml" line="61"/>
        <source>Verifikasi Nomor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/login/VerifikasiNo.qml" line="83"/>
        <source>No HP</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
