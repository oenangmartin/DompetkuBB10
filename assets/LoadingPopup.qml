import bb.cascades 1.0

Dialog {
    id: dialog
    objectName: "dialog"
    Container {
        // The Dialog does not automatically fill the entire screen
        // like a Page does, so in order for it to be possible to center
        // the dialog box on screen, the width and height must be set.
        preferredWidth: UiUtils.ScreenWidth
        preferredHeight: UiUtils.ScreenHeight

        // The background is set to semi-transparent to indicate
        // that it is not possible to interact with the screen
        // behind the dialog box.
        background: Color.create(0.0, 0.0, 0.0, 0.5)
        layout: DockLayout {
        }
        Container {
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
            topPadding: 20
            bottomPadding: 20
            leftPadding: 20
            rightPadding: 20
            preferredWidth: UiUtils.ScreenWidth
            layout: DockLayout {
            }
            ImageView {
                imageSource: "asset:///images/border/border_popup.amd"
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
            }
            Container {
                topPadding: 40
                bottomPadding: 40
                leftPadding: 40
                rightPadding: 40
                horizontalAlignment: HorizontalAlignment.Center
                ActivityIndicator {
                    id: indicator
                    objectName: "indicator"
                    preferredHeight: 100
                    preferredWidth: 100
                    horizontalAlignment: HorizontalAlignment.Center
                }
                Label {
                    id: loadingText
                    objectName: "loadingText"
                    text: "Loading"
                    textStyle.base: SystemDefaults.TextStyles.TitleText
                    textStyle.color: Color.create("#fafafa")
                    horizontalAlignment: HorizontalAlignment.Center
                }
            } // Container
        } // Container
        /*gestureHandlers: [
            TapHandler {
                onTapped: {
                    dialog.close();
                }
            }
        ]*/
            
    } // Container
}// Dialog
