import bb.cascades 1.0

Container {
    layout: DockLayout {
    }
    ScrollView {
        id: scrollView
        scrollViewProperties {
            scrollMode: ScrollMode.Vertical
            //scrollMode: ScrollMode.Both
            //pinchToZoomEnabled: true
        }
        layoutProperties: StackLayoutProperties {
            spaceQuota: 1.0
        }
        Container {
            objectName: "webViewContainer"
            background: Color.LightGray
        }

    }

    ActivityIndicator {
        objectName: "indicator"
        preferredHeight: 200
        preferredWidth: 200
        verticalAlignment: VerticalAlignment.Center
        horizontalAlignment: HorizontalAlignment.Center
    }
}
