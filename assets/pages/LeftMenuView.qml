import bb.cascades 1.0
Container {
    background: Color.create("#5A5A5A")
    layout: DockLayout {

    }
    property int menuW: leftMenuW
    property int menuH: UiUtils.ScreenHeight
    preferredWidth: menuW
    preferredHeight: menuH
    Container {
        property alias currentUser: currentUserLabel.text
        verticalAlignment: VerticalAlignment.Top
        minWidth: menuW

        Container {
            layout: DockLayout {

            }
            minHeight: 100
            preferredWidth: Infinity
            //background: Color.Blue
            ImageView {
                imageSource: "asset:///images/tab_title.png"
                verticalAlignment: VerticalAlignment.Fill
                horizontalAlignment: HorizontalAlignment.Fill
            }
            Container {
                topPadding: 25
                bottomPadding: topPadding
                leftPadding: 40
                rightPadding: leftPadding
                verticalAlignment: VerticalAlignment.Center
                Label {
                    bottomMargin: 0
                    text: "Beranda"
                    textStyle {
                        fontWeight: FontWeight.Bold
                        fontSize: FontSize.PointValue
                        fontSizeValue: 8
                        color: Color.White
                    }
                }
                Label {
                    objectName: "lblUser"
                    text: "User"
                    topMargin: 0
                    id: currentUserLabel
                    textStyle {
                        fontWeight: FontWeight.Normal
                        fontSize: FontSize.PointValue
                        fontSizeValue: 7
                        color: Color.White
                    }
                }
            }
            
            gestureHandlers: [
                TapHandler {
                    onTapped: {
                        leftMenu.beranda();
                    }
                }
            ]

        }
        ListView {
            objectName: "menuList"
            listItemComponents: [
                ListItemComponent {
                    type: "item"
                    Container {
                        layout: DockLayout {
                        }
                        preferredWidth: Infinity
                        background: Color.create("#5A5A5A")
                        ImageView {
                            id: border
                            visible: false
                            imageSource: "asset:///images/border/menu-kiri-h.amd"
                            horizontalAlignment: HorizontalAlignment.Fill
                            verticalAlignment: VerticalAlignment.Fill
                        }
                        Container {
                            topPadding: 30
                            leftPadding: 40
                            bottomPadding: 30
                            Label {
                                id: label
                                text: ListItemData.title
                                
                                textStyle {
                                    fontWeight: FontWeight.Normal
                                    fontSize: FontSize.PointValue
                                    fontSizeValue: 8
                                    color: Color.White
                                }
                            }
                        }
                        
                        function setHighlight(highlighted) {
                            if (highlighted) {
                                border.setVisible(true);
                                label.textStyle.setColor(Color.create("#f36e1f"));
                            } else {
                                border.setVisible(false);
                                label.textStyle.setColor(Color.create("#ffffff"));
                            }
                        }
                        
                        ListItem.onActivationChanged: {
                            setHighlight(ListItem.active);
                        }
                        
                        // Signal handler for ListItem selection
                        ListItem.onSelectionChanged: {
                            setHighlight(ListItem.selected);
                        }
                    }
                },
                ListItemComponent {
                    type: "item2"
                    Container {
                        layout: DockLayout {
                        }
                        preferredWidth: Infinity
                        background: Color.create("#4a4a4a")
                        ImageView {
                            id: border2
                            visible: false
                            imageSource: "asset:///images/border/menu-kiri-h.amd"
                            horizontalAlignment: HorizontalAlignment.Fill
                            verticalAlignment: VerticalAlignment.Fill
                        }
                        Container {
                            topPadding: 30
                            leftPadding: 40
                            bottomPadding: 30
                            Label {
                                id: label2
                                text: ListItemData.title
                                
                                textStyle {
                                    fontWeight: FontWeight.Normal
                                    fontSize: FontSize.PointValue
                                    fontSizeValue: 8
                                    color: Color.White
                                }
                            }
                        }
                        
                        function setHighlight(highlighted) {
                            if (highlighted) {
                                border2.setVisible(true);
                                label2.textStyle.setColor(Color.create("#f36e1f"));
                            } else {
                                border2.setVisible(false);
                                label2.textStyle.setColor(Color.create("#ffffff"));
                            }
                        }
                        
                        ListItem.onActivationChanged: {
                            setHighlight(ListItem.active);
                        }
                        
                        // Signal handler for ListItem selection
                        ListItem.onSelectionChanged: {
                            setHighlight(ListItem.selected);
                        }
                    }
                    
                    
                }
                
            ]
            function itemType(data, indexPath) {
                if (data.index == 1) {
                    return "item2";
                } 
                else {
                    return "item";
                }
            }
            
        }
        /*ScrollView {
            scrollViewProperties.scrollMode: ScrollMode.Vertical
            scrollViewProperties {
                //overScrollEffectMode: OverScrollEffectMode.None
            }
            Container {
                objectName: "tabItemContainer"
                Divider {
                    topPadding: 0
                    topMargin: 0
                    bottomMargin: 0
                    bottomPadding: 0
                }
                Container {
                    layout: DockLayout {
                    }
                    background: Color.create("#5A5A5A")
                    preferredWidth: Infinity
                    Container {
                        topPadding: 30
                        leftPadding: 40
                        bottomPadding: 30
                        Label {
                            text: "Inbox"
                            textStyle {
                                color: Color.White
                            }
                        }
                    }
                    gestureHandlers: [
                        TapHandler {
                            onTapped: {
                                leftMenu.inbox();
                            }
                        }
                    ]
                }
                Divider {
                    topPadding: 0
                    topMargin: 0
                    bottomMargin: 0
                    bottomPadding: 0
                }
                Container {
                    layout: DockLayout {
                    }
                    background: Color.create("#4A4A4A")
                    preferredWidth: 999999
                    Container {
                        topPadding: 30
                        leftPadding: 40
                        bottomPadding: 30
                        Label {
                            text: "Favorit"
                            textStyle {
                                color: Color.White
                            }
                        }
                    }
                    gestureHandlers: [
                        TapHandler {
                            onTapped: {
                                leftMenu.favorit();
                            }
                        }
                    ]
                }
                Divider {
                    topPadding: 0
                    topMargin: 0
                    bottomMargin: 0
                    bottomPadding: 0
                }
                Container {
                    layout: DockLayout {
                    }
                    background: Color.create("#5A5A5A")
                    preferredWidth: 999999
                    Container {
                        topPadding: 30
                        leftPadding: 40
                        bottomPadding: 30
                        Label {
                            text: "Administrasi"
                            textStyle {
                                color: Color.White
                            }
                        }
                    }
                    gestureHandlers: [
                        TapHandler {
                            onTapped: {
                                leftMenu.administrasi();
                            }
                        }
                    ]
                }
                Divider {
                    topPadding: 0
                    topMargin: 0
                    bottomMargin: 0
                    bottomPadding: 0
                }
                Container {
                    layout: DockLayout {
                    }
                    background: Color.create("#4A4A4A")
                    preferredWidth: 999999
                    Container {
                        topPadding: 30
                        leftPadding: 40
                        bottomPadding: 30
                        Label {
                            text: "Informasi"
                            textStyle {
                                color: Color.White
                            }
                        }
                    }
                    gestureHandlers: [
                        TapHandler {
                            onTapped: {
                                leftMenu.informasi();
                            }
                        }
                    ]
                }
                Divider {
                    topPadding: 0
                    topMargin: 0
                    bottomMargin: 0
                    bottomPadding: 0
                }

                Container {
                    layout: DockLayout {
                    }
                    background: Color.create("#5A5A5A")
                    preferredWidth: Infinity
                    Container {
                        topPadding: 30
                        leftPadding: 40
                        bottomPadding: 30
                        Label {
                            text: "Logout"
                            textStyle {
                                color: Color.White
                            }
                        }
                    }
                    gestureHandlers: [
                        TapHandler {
                            onTapped: {
                                leftMenu.logout();
                            }
                        }
                    ]
                }
            }
        }*/
    }
    
    ImageView {
        touchPropagationMode: TouchPropagationMode.None
        imageSource: "asset:///images/left-menu-bg.png"
        verticalAlignment: VerticalAlignment.Fill
        horizontalAlignment: HorizontalAlignment.Right    
    }
}
