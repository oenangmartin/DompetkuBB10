import bb.cascades 1.0

Container {
    id: leftMenu
    objectName: "menuContainer"
    background: Color.create("#5A5A5A")
    preferredWidth: UiUtils.ScreenWidth - 100
    //implicitLayoutAnimationsEnabled: false
    
    onVisibleChanged: {
        if(visible){
        	//leftMenu.setTranslationX(0);
        }
    }
    Container {
        layout: DockLayout {
        }
        background: Color.create("#FF7D14")
        preferredWidth: 999999
        Container {
            topPadding: 30
            leftPadding: 40
            bottomPadding: 30
            Label {
                bottomMargin: 0
                text: "Beranda"
                textStyle{
                    color: Color.White
                }            
            }    
            Label {
                topMargin: 0
                topPadding: 0
                objectName: "labelNama"
                text: ""
                textStyle{
                    color: Color.White
                }            
            }    
        }
        gestureHandlers: [
            TapHandler {
                onTapped: {
                    
                }
            }
        ]
    }
    Divider {
        topPadding: 0
        topMargin: 0
        bottomMargin: 0
        bottomPadding: 0
    }
    Container {
        layout: DockLayout {
        }
        background: Color.create("#5A5A5A")
        preferredWidth: Infinity
        Container {
            topPadding: 30
            leftPadding: 40
            bottomPadding: 30
            Label {
                text: "Inbox"
                textStyle{
                    color: Color.White
                }            
            }    
        }
        gestureHandlers: [
            TapHandler {
                onTapped: {
                    page.inbox();
                }
            }
        ]
    }
    Divider {
        topPadding: 0
        topMargin: 0
        bottomMargin: 0
        bottomPadding: 0
    }
    Container {
        layout: DockLayout {
        }
        background: Color.create("#4A4A4A")
        preferredWidth: 999999
        Container {
            topPadding: 30
            leftPadding: 40
            bottomPadding: 30
            Label {
                text: "Informasi"
                textStyle{
                    color: Color.White
                }            
            }    
        }
        gestureHandlers: [
            TapHandler {
                onTapped: {
                    page.informasi();
                }
            }
        ]
    }
    Divider {
        topPadding: 0
        topMargin: 0
        bottomMargin: 0
        bottomPadding: 0
    }
    
    Container {
        layout: DockLayout {
        }
        background: Color.create("#5A5A5A")
        preferredWidth: Infinity
        Container {
            topPadding: 30
            leftPadding: 40
            bottomPadding: 30
            Label {
                text: "Logout"
                textStyle{
                    color: Color.White
                }            
            }    
        }
        gestureHandlers: [
            TapHandler {
                onTapped: {
                	page.logout();
                }
            }
        ]
    }
    
    /*ListView {
        objectName: "list"
        listItemComponents: [

            ListItemComponent {
                type: "item"

                Container {
                    function setHighlight(highlighted) {
                        console.log("setHighlight");
                        if (highlighted) {
                            border.setImageSource(("asset:///images/border/arrow_border_sel.amd"));
                        } else {
                            border.setImageSource(("asset:///images/border/arrow_border.amd"));
                        }
                    }

                    // Signal handler for ListItem activation
                    ListItem.onActivationChanged: {
                        setHighlight(ListItem.active);
                    }

                    // Signal handler for ListItem selection
                    ListItem.onSelectionChanged: {
                        setHighlight(ListItem.selected);
                    }
                    layout: DockLayout {
                    }
                    preferredWidth: Infinity
                    ImageView {
                        id: border
                        imageSource: "asset:///images/border/arrow_border.amd"
                        horizontalAlignment: HorizontalAlignment.Fill
                        verticalAlignment: VerticalAlignment.Fill
                    }
                    Container {
                        topPadding: 30
                        bottomPadding: 30
                        horizontalAlignment: HorizontalAlignment.Left
                        verticalAlignment: VerticalAlignment.Center
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        Label {
                            id: title
                            text: ListItemData.title
                        }
                    }
                }
            }
        ]
    }*/

    property int leftMenuWidth: UiUtils.ScreenWidth - 100
    property int menuWidth: UiUtils.ScreenWidth - 100
    property int leftToRight
    property double touchDownX
    property int moveThreshold: 250
    onTranslationXChanged: {
        if (translationX == 0) {
            //fadeContainer.setVisible(true);
        }
        if (translationX == - leftMenuWidth) {
            //fadeContainer.setVisible(false);
        }
    }

    onTouch: {
        if (event.isUp()) {

            //cek pergeseran apakah signifikan utk tutup / buka
            if (touchDownX - event.windowX > 5) {
                //cek nilai translate leftmenu
                if (leftMenu.translationX * -1 > (leftMenuWidth / 2)) {
                    //lebih besar dari stga -> tutup menu
                    leftMenu.setTranslationX(- leftMenuWidth);
                    leftMenu.setVisible(false);
                } else {
                    //buka
                    leftMenu.setTranslationX(0);

                }
            }

            /*
             * if (leftToRight == 0) {
             * console.log(">>event up " + (event.windowX - touchDownX));
             * //cek harus tutup apa tidak
             * if(touchDownX - event.windowX > 5)
             * {
             * console.log(">>Tutup menu ");
             * var x = menuWidth + leftMenu.translationX;
             * console.log(">>x: " + x);
             * leftMenu.setTranslationX(-menuWidth);
             * }
             * } else if (leftToRight == 1) {
             * console.log(">>scroll to: ");
             * leftMenu.setTranslationX(0);
             * }
             * leftToRight = -1*/
        } else if (event.isMove()) {
            var x;
            if (event.windowX > (touchDownX )) {
                leftToRight = 1
                console.log(">>leftToRight: " + event.windowX + " : " + touchDownX)
            } else if (event.windowX <= (touchDownX )) {
                leftToRight = 0
                console.log(">>Right to left: " + event.windowX + " : " + touchDownX)
            }
            x = event.windowX - touchDownX;
            console.log(">>x1 = " + x);
            if (x > 0) {
                x = 0;
            } else if (x < - menuWidth) x = - menuWidth;
            console.log(">>x2 = " + x);
            leftMenu.setTranslationX(x);
            console.log(">>translate to: " + x);
            console.log(">>-menuWidth: " + - menuWidth);
        } else if (event.isDown()) {
            touchDownX = event.windowX
        } else {
            console.log(">>else event");
            //cek nilai translate leftmenu
            if (leftMenu.translationX * -1 > (leftMenuWidth / 2)) {
                //lebih besar dari stga -> tutup menu
                leftMenu.setTranslationX(- leftMenuWidth);
                leftMenu.setVisible(false);
            } else {
                //buka
                leftMenu.setTranslationX(0);

            }
        }
    }

}
