/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.0
Container {
    touchPropagationMode: TouchPropagationMode.Full
    background: Color.create("#F2F2F2")
    implicitLayoutAnimationsEnabled: false
    layout: DockLayout {

    }

    //property int screenW : 768
    property int screenW: UiUtils.ScreenWidth
    property int listW: screenW - 80
    property int listH: 100
    property int leftMenuW: 570//UiUtils.ScreenWidth/5 * 4
    
    property double initialWindowX
    property double startPosition
    property double finalPosition
    property bool dragHappening: false
    property double dragFactor: 1.25
    
    onCreationCompleted: {
        Qt.listW = listW;
    }
    
    //left drawer
    LeftMenuView {
        menuW: leftMenuW
        menuH: UiUtils.ScreenHeight
    }
    
    
    
    //left drawer

    Container {
        id: contentCont
        objectName: "contentContainer"
        background: Color.White
        
        Container {
            Container {
                objectName: "headerContainer"
                background: Color.Black
                implicitLayoutAnimationsEnabled: false
                verticalAlignment: VerticalAlignment.Top
                horizontalAlignment: HorizontalAlignment.Fill
                preferredWidth: Infinity
                layout: DockLayout {
                }
                minHeight: 100
                ImageView {
                    imageSource: "asset:///images/header_bg.jpg"
                    horizontalAlignment: HorizontalAlignment.Fill
                    verticalAlignment: VerticalAlignment.Fill
                }
                ImageView {
                    translationX: 20
                    imageSource: "asset:///images/icon-mobile-menu.png"
                    horizontalAlignment: HorizontalAlignment.Left
                    verticalAlignment: VerticalAlignment.Center
                    preferredHeight: 52
                    preferredWidth: 52
                    scalingMethod: ScalingMethod.AspectFit
                    gestureHandlers: [
                        TapHandler {
                            onTapped: {
                                //page.rightMenuTapped();
                                if (contentCont.translationX == 0) {
                                    contentCont.translationX = leftMenuW;
                                    startPosition = leftMenuW;
                                    finalPosition = leftMenuW;

                                } else {
                                    contentCont.translationX = 0;
                                    startPosition = 0;
                                    finalPosition = 0;
                                }
                                fadeContainer.opacity = (contentCont.translationX / 1000) * 0.5;
                            }
                        }
                    ]
                }
                Container {
                    horizontalAlignment: HorizontalAlignment.Right
                    verticalAlignment: VerticalAlignment.Center
                    preferredHeight: 60
                    preferredWidth: 60
                    layout: DockLayout {
                    }
                    ImageView {
                        objectName: "refreshButton"
                        translationX: -10
                        imageSource: "asset:///images/icon-refresh.png"
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center
                        preferredHeight: 50
                        preferredWidth: 50
                        scalingMethod: ScalingMethod.AspectFit
                        gestureHandlers: [
                            TapHandler {
                                onTapped: {
                                    page.refresh();
                                }
                            }
                        ]

                    }
                    ActivityIndicator {
                        objectName: "indicator"
                        preferredHeight: 50
                        preferredWidth: 50
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center
                    }
                }

                Container {
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Center
                    implicitLayoutAnimationsEnabled: false
                    topPadding: 10
                    bottomPadding: 10
                    ImageView {
                        imageSource: "asset:///images/logo-white.png"
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center
                        scalingMethod: ScalingMethod.AspectFit
                    }
                }
            }
            Container {
                objectName: "menuContainer"
                layout: DockLayout {
                }
                gestureHandlers: [
                    TapHandler {
                        onTapped: {
                            page.closeLeftMenu();
                        }
                    }
                ]

                Container {
                    objectName: "contentContainer"
                    preferredWidth: Infinity

                    //nama & telepon
                    Container {
                        preferredWidth: Infinity
                        layout: DockLayout {

                        }
                        background: Color.Black
                        objectName: "profileContainer"
                        ImageView {
                            imageSource: "asset:///images/bg-gradasi.png"
                            horizontalAlignment: HorizontalAlignment.Fill
                            verticalAlignment: VerticalAlignment.Fill
                        }
                        Label {
                            objectName: "labelNama"
                            translationX: 10
                            verticalAlignment: VerticalAlignment.Center
                            text: ""
                            textStyle {
                                fontWeight: FontWeight.Bold
                                fontSize: FontSize.PointValue
                                fontSizeValue: 8
                                color: Color.White
                            }
                        }
                        Label {
                            objectName: "labelHP"
                            translationX: -10
                            horizontalAlignment: HorizontalAlignment.Right
                            verticalAlignment: VerticalAlignment.Center
                            text: ""
                            textStyle {
                                fontWeight: FontWeight.Normal
                                fontSize: FontSize.PointValue
                                fontSizeValue: 8
                                color: Color.White
                            }
                        }
                    } //nama & telepon

                    ScrollView {
                        scrollViewProperties {
                            scrollMode: ScrollMode.Vertical
                        }
                        objectName: "scroll"
                        maxWidth: screenW
                        Container {

                            //saldo
                            Container {
                                objectName: "saldoContainer"
                                preferredWidth: Infinity
                                topPadding: 30
                                leftPadding: 10
                                rightPadding: leftPadding
                                Container {
                                    preferredWidth: Infinity
                                    layout: DockLayout {

                                    }

                                    ImageView {
                                        imageSource: "asset:///images/border/borderbelah_atas.amd"
                                        horizontalAlignment: HorizontalAlignment.Fill
                                        verticalAlignment: VerticalAlignment.Fill
                                    }
                                    Container {
                                        topPadding: 10
                                        bottomPadding: topPadding
                                        leftPadding: 20
                                        rightPadding: leftPadding
                                        Label {
                                            text: "Saldo"
                                            textStyle {
                                                fontWeight: FontWeight.Normal
                                                fontSize: FontSize.PointValue
                                                fontSizeValue: 9
                                                color: Color.White
                                            }
                                        }
                                    }

                                }

                                Container {
                                    preferredWidth: Infinity
                                    layout: DockLayout {
                                    }
                                    ImageView {
                                        imageSource: "asset:///images/border/borderbelah_bawah.amd"
                                        horizontalAlignment: HorizontalAlignment.Fill
                                        verticalAlignment: VerticalAlignment.Fill
                                    }
                                    Container {
                                        topPadding: 40
                                        bottomPadding: 2 * topPadding + 10
                                        leftPadding: 20
                                        rightPadding: leftPadding
                                        horizontalAlignment: HorizontalAlignment.Center
                                        Label {
                                            objectName: "labelSaldo"
                                            text: "Rp "
                                            textStyle {
                                                fontWeight: FontWeight.Bold
                                                fontSize: FontSize.PointValue
                                                fontSizeValue: 10
                                                color: Color.create("#F4971D")
                                            }
                                        }
                                    }
                                    Container {
                                        preferredWidth: Infinity
                                        background: Color.LightGray
                                        verticalAlignment: VerticalAlignment.Bottom
                                        topPadding: 10
                                        bottomPadding: topPadding
                                        Label {
                                            objectName: "currentDateLabel"
                                            translationX: -10
                                            text: ""
                                            horizontalAlignment: HorizontalAlignment.Right
                                            textStyle {
                                                fontSize: FontSize.Small
                                                color: Color.Black
                                                fontWeight: FontWeight.W100
                                            }
                                        }
                                    }
                                }

                            }
                            //saldo

                            ////transaksi terakhir
                            Container {
                                objectName: "transaksiContainer"
                                visible: false
                                preferredWidth: Infinity
                                topPadding: 30
                                leftPadding: 10
                                rightPadding: leftPadding
                                Container {
                                    preferredWidth: Infinity
                                    layout: DockLayout {

                                    }

                                    ImageView {
                                        imageSource: "asset:///images/border/borderbelah_atas.amd"
                                        horizontalAlignment: HorizontalAlignment.Fill
                                        verticalAlignment: VerticalAlignment.Fill
                                    }
                                    Container {
                                        topPadding: 10
                                        bottomPadding: topPadding
                                        leftPadding: 20
                                        rightPadding: leftPadding
                                        preferredWidth: Infinity
                                        layout: DockLayout {
                                            
                                        }
                                        Label {
                                            text: "Transaksi Terakhir"
                                            horizontalAlignment: HorizontalAlignment.Left
                                            verticalAlignment: VerticalAlignment.Center
                                            textStyle {
                                                fontWeight: FontWeight.Normal
                                                fontSize: FontSize.PointValue
                                                fontSizeValue: 9
                                                color: Color.White
                                            }
                                        }
                                        Label {
                                            text: "<a href='#' style=\"color:#ffffff;\">more</a>"
                                            translationX: -5
                                            horizontalAlignment: HorizontalAlignment.Right
                                            verticalAlignment: VerticalAlignment.Center
                                            textStyle {
                                                fontWeight: FontWeight.Normal
                                                fontSize: FontSize.PointValue
                                                fontSizeValue: 7
                                                color: Color.White
                                            }
                                            textFormat: TextFormat.Html
                                            activeTextHandler: ActiveTextHandler {
                                                onTriggered: {
                                                    page.showMoreHistory();
                                                }
                                            }
                                        }
                                    }

                                }

                                Container {
                                    preferredWidth: Infinity
                                    layout: DockLayout {
                                    }
                                    ImageView {
                                        imageSource: "asset:///images/border/borderbelah_bawah.amd"
                                        horizontalAlignment: HorizontalAlignment.Fill
                                        verticalAlignment: VerticalAlignment.Fill
                                    }

                                    //list
                                    Container {
                                        objectName: "historyContainer"
                                    } //list
                                }

                            }
                            //transaksi terakhir

                            //divider
                            Container {
                                topPadding: 30
                                preferredWidth: Infinity
                                leftPadding: 10
                                rightPadding: leftPadding
                                Divider {
                                }
                            } //divider

                            //promo
                            Container {
                                layout: DockLayout {

                                }
                                preferredWidth: Infinity
                                preferredHeight: listH
                                background: Color.White
                                leftPadding: 10
                                rightPadding: leftPadding
                                ImageButton {
                                    defaultImageSource: "asset:///images/slide-prev.png"
                                    verticalAlignment: VerticalAlignment.Center
                                    horizontalAlignment: HorizontalAlignment.Left
                                }
                                ImageButton {
                                    defaultImageSource: "asset:///images/slide-next.png"
                                    verticalAlignment: VerticalAlignment.Center
                                    horizontalAlignment: HorizontalAlignment.Right
                                }
                                ListView {
                                    preferredWidth: listW
                                    objectName: "list"
                                    //preferredHeight: listH / 2
                                    preferredHeight: listH
                                    horizontalAlignment: HorizontalAlignment.Center
                                    verticalAlignment: VerticalAlignment.Center
                                    layout: StackListLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    scrollIndicatorMode: ScrollIndicatorMode.None
                                    snapMode: SnapMode.LeadingEdge
                                    flickMode: FlickMode.SingleItem
                                    listItemComponents: [
                                        ListItemComponent {
                                            type: "item"
                                            Container {
                                                verticalAlignment: VerticalAlignment.Center
                                                preferredWidth: Qt.listW
                                                preferredHeight: 200
                                                Label {
                                                    topMargin: 2
                                                    bottomMargin: 2
                                                    leftMargin: 0
                                                    rightMargin: 0
                                                    multiline: true
                                                    autoSize{
                                                        maxLineCount: 3
                                                    }
                                                    text: ListItemData.promo_text
                                                    verticalAlignment: VerticalAlignment.Center
                                                    horizontalAlignment: HorizontalAlignment.Left
                                                    textStyle {
                                                        fontWeight: FontWeight.Normal
                                                        fontSize: FontSize.PointValue
                                                        fontSizeValue: 8
                                                        color: Color.Black
                                                    }
                                                }
                                            }
                                        },
                                        ListItemComponent {
                                            type: "item2line"
                                            Container {
                                                verticalAlignment: VerticalAlignment.Center
                                                preferredWidth: Qt.listW
                                                Label {
                                                    topMargin: 2
                                                    bottomMargin: 2
                                                    leftMargin: 0
                                                    rightMargin: 0
                                                    text: ListItemData.text1
                                                    horizontalAlignment: HorizontalAlignment.Left
                                                    textStyle {
                                                        fontWeight: FontWeight.Normal
                                                        fontSize: FontSize.PointValue
                                                        fontSizeValue: 8
                                                        color: Color.Black
                                                    }
                                                }
                                                Label {
                                                    topMargin: 2
                                                    bottomMargin: 2
                                                    leftMargin: 0
                                                    rightMargin: 0
                                                    text: ListItemData.text2
                                                    horizontalAlignment: HorizontalAlignment.Left
                                                    textStyle {
                                                        fontWeight: FontWeight.Normal
                                                        fontSize: FontSize.PointValue
                                                        fontSizeValue: 8
                                                        color: Color.Black
                                                    }
                                                }
                                            }
                                        }
                                    ]
                                    
                                    function itemType(data, indexPath) {
                                        /*if (data.text1) {
                                            return "item2line";
                                        } 
                                        else*/ 
                                        {
                                            return "item";
                                        }
                                    }
                                }
                            }
                            //promo
                        }
                    }

                } //content container

            }

        }
        Container {
            preferredWidth: Infinity
            verticalAlignment: VerticalAlignment.Bottom
            layout: DockLayout {

            }
            ImageView {
                imageSource: "asset:///images/bg-gradasi.png"
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
            }
            Container {
                objectName: "bottomButtonContainer"

            }
        }
        
        Container {
            id: fadeContainer
            objectName: "fadeContainer"
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
            background: Color.Black
            opacity: 0
            overlapTouchPolicy: OverlapTouchPolicy.Allow
            touchPropagationMode: TouchPropagationMode.PassThrough
        
        }
        onTouch: {
            // Determine the location inside the image that was touched,
            // relative to the container, and move it accordingly
            
            if (event.isDown()) {
                // Start a dragging gesture
                dragHappening = true
                startPosition = contentCont.translationX;
                initialWindowX = event.windowX
            
            } else if (dragHappening && event.isMove()) {
                // Move the image and record its new position
                contentCont.translationX += (event.windowX - initialWindowX) * dragFactor
                fadeContainer.opacity = (contentCont.translationX / 1000) * 0.5;
                if (contentCont.translationX <= 0) {
                    dragHappening = false;
                    contentCont.translationX = 0;
                }
                if (contentCont.translationX >= leftMenuW) {
                    dragHappening = false;
                    contentCont.translationX = leftMenuW;
                }
                finalPosition = contentCont.translationX;
                initialWindowX = event.windowX;
            
            } else {
                // Event type is Up or Cancel
                // Interrupt any ongoing drag gesture
                dragHappening = false
                if (finalPosition < 75) {
                    contentCont.translationX = 0;
                    startPosition = 0;
                    finalPosition = 0;
                    initialWindowX = 0;
                } else if (finalPosition > 520) {
                    contentCont.translationX = leftMenuW;
                    startPosition = leftMenuW;
                    finalPosition = leftMenuW;
                    initialWindowX = leftMenuW;
                } else {
                    if (finalPosition < startPosition) {
                        contentCont.translationX = 0;
                        startPosition = 0;
                        finalPosition = 0;
                        initialWindowX = 0;
                    } else if (finalPosition > startPosition) {
                        contentCont.translationX = leftMenuW;
                        startPosition = leftMenuW;
                        finalPosition = leftMenuW;
                        initialWindowX = leftMenuW;
                    }
                }
                fadeContainer.opacity = (contentCont.translationX / 1000) * 0.5;
            }
            if (event.isUp()) {
                if (finalPosition < leftMenuW/10) {
                    contentCont.translationX = 0;
                    startPosition = 0;
                    finalPosition = 0;
                    initialWindowX = 0;
                } else if (finalPosition > leftMenuW-70) {
                    contentCont.translationX = leftMenuW;
                    startPosition = leftMenuW;
                    finalPosition = leftMenuW;
                    initialWindowX = leftMenuW;
                } else {
                    if (finalPosition < startPosition) {
                        contentCont.translationX = 0;
                        startPosition = 0;
                        finalPosition = 0;
                        initialWindowX = 0;
                    } else if (finalPosition > startPosition) {
                        contentCont.translationX = leftMenuW;
                        startPosition = leftMenuW;
                        finalPosition = leftMenuW;
                        initialWindowX = leftMenuW;
                    }
                }
                fadeContainer.opacity = (contentCont.translationX / 1000) * 0.5;
            }
        }
    } //contentCont

}
