import bb.cascades 1.0

Dialog {
    id: dialog
    objectName: "dialog"
    Container {
        preferredWidth: UiUtils.ScreenWidth
        preferredHeight: UiUtils.ScreenHeight
        layout: DockLayout {
        }
        Container {
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
            preferredWidth: UiUtils.ScreenWidth
            layout: DockLayout {
            }
            ImageView {
                imageSource: "asset:///images/tutorial.png"
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
            }
        } // Container
        gestureHandlers: [
            TapHandler {
                onTapped: {
                    dialog.close();
                }
            }
        ]
            
    } // Container
}// Dialog
