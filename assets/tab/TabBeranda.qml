
import bb.cascades 1.0
Container {
    property int screenW : UiUtils.ScreenWidth
    property int listW: screenW - 80
    property int listH: 300
    onCreationCompleted: {
        Qt.listW = listW;
    }
    
    objectName: "contentContainer"
    preferredWidth: Infinity
    
    //nama & telepon
    Container {
        preferredWidth: Infinity
        layout: DockLayout {
        
        }
        background: Color.Black
        objectName: "profileContainer"
        ImageView {
            imageSource: "asset:///images/bg-gradasi.png"
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
        }
        Label {
            objectName: "labelNama"
            translationX: 10
            verticalAlignment: VerticalAlignment.Center
            text: ""
            textStyle {
                fontWeight: FontWeight.Bold
                fontSize: FontSize.Medium
                color: Color.White
            }
        }
        Label {
            objectName: "labelHP"
            translationX: -10
            horizontalAlignment: HorizontalAlignment.Right
            verticalAlignment: VerticalAlignment.Center
            text: "0"
            textStyle {
                fontSize: FontSize.Medium
                color: Color.White
            }
        }
    } //nama & telepon
    
    ScrollView {
        scrollViewProperties {
            scrollMode: ScrollMode.Vertical
        }
        objectName: "scroll"
        maxWidth: screenW
        Container {
            
            //saldo
            Container {
                objectName: "saldoContainer"
                preferredWidth: Infinity
                topPadding: 30
                leftPadding: 10
                rightPadding: leftPadding
                Container {
                    preferredWidth: Infinity
                    layout: DockLayout {
                    
                    }
                    
                    ImageView {
                        imageSource: "asset:///images/border/borderbelah_atas.amd"
                        horizontalAlignment: HorizontalAlignment.Fill
                        verticalAlignment: VerticalAlignment.Fill
                    }
                    Container {
                        topPadding: 10
                        bottomPadding: topPadding
                        leftPadding: 20
                        rightPadding: leftPadding
                        Label {
                            text: "Saldo"
                            textStyle {
                                fontSize: FontSize.Large
                                color: Color.Black
                            }
                        }
                    }
                
                }
                
                Container {
                    preferredWidth: Infinity
                    layout: DockLayout {
                    }
                    ImageView {
                        imageSource: "asset:///images/border/borderbelah_bawah.amd"
                        horizontalAlignment: HorizontalAlignment.Fill
                        verticalAlignment: VerticalAlignment.Fill
                    }
                    Container {
                        topPadding: 40
                        bottomPadding: 2 * topPadding + 10
                        leftPadding: 20
                        rightPadding: leftPadding
                        horizontalAlignment: HorizontalAlignment.Center
                        Label {
                            objectName: "labelSaldo"
                            text: "Rp "
                            textStyle {
                                fontSize: FontSize.XXLarge
                                fontWeight: FontWeight.Bold
                                color: Color.create("#F4971D")
                            }
                        }
                    }
                    Container {
                        preferredWidth: Infinity
                        background: Color.LightGray
                        verticalAlignment: VerticalAlignment.Bottom
                        topPadding: 10
                        bottomPadding: topPadding
                        Label {
                            objectName: "currentDateLabel"
                            translationX: -10
                            text: "Per 10/03/2014, 15:20"
                            horizontalAlignment: HorizontalAlignment.Right
                            textStyle {
                                fontSize: FontSize.Small
                                color: Color.Black
                                fontWeight: FontWeight.W100
                            }
                        }
                    }
                }
            
            }
            //saldo
            
            ////transaksi terakhir
            Container {
                objectName: "transaksiContainer"
                visible: false
                preferredWidth: Infinity
                topPadding: 30
                leftPadding: 10
                rightPadding: leftPadding
                Container {
                    preferredWidth: Infinity
                    layout: DockLayout {
                    
                    }
                    
                    ImageView {
                        imageSource: "asset:///images/border/borderbelah_atas.amd"
                        horizontalAlignment: HorizontalAlignment.Fill
                        verticalAlignment: VerticalAlignment.Fill
                    }
                    Container {
                        topPadding: 10
                        bottomPadding: topPadding
                        leftPadding: 20
                        rightPadding: leftPadding
                        Label {
                            text: "Transaksi Terakhir"
                            textStyle {
                                fontSize: FontSize.Large
                                color: Color.Black
                            }
                        }
                    }
                
                }
                
                Container {
                    preferredWidth: Infinity
                    layout: DockLayout {
                    }
                    ImageView {
                        imageSource: "asset:///images/border/borderbelah_bawah.amd"
                        horizontalAlignment: HorizontalAlignment.Fill
                        verticalAlignment: VerticalAlignment.Fill
                    }
                    
                    //list
                    Container {
                        objectName: "historyContainer"
                    } //list
                }
            
            }
            //transaksi terakhir
            
            //divider
            Container {
                topPadding: 30
                preferredWidth: Infinity
                leftPadding: 10
                rightPadding: leftPadding
                Divider {
                }
            } //divider
            
            //promo
            Container {
                layout: DockLayout {
                
                }
                preferredWidth: Infinity
                preferredHeight: 300
                background: Color.White
                leftPadding: 10
                rightPadding: leftPadding
                ImageButton {
                    defaultImageSource: "asset:///images/slide-prev.png"
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Left
                }
                ImageButton {
                    defaultImageSource: "asset:///images/slide-next.png"
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Right
                }
                ListView {
                    preferredWidth: listW
                    objectName: "list"
                    preferredHeight: listH
                    horizontalAlignment: HorizontalAlignment.Center
                    layout: StackListLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    scrollIndicatorMode: ScrollIndicatorMode.None
                    snapMode: SnapMode.LeadingEdge
                    flickMode: FlickMode.SingleItem
                    listItemComponents: [
                        ListItemComponent {
                            type: "item"
                            Container {
                                preferredWidth: Qt.listW
                                Label {
                                    topMargin: 2
                                    bottomMargin: 2
                                    text: ListItemData.title
                                    textStyle{
                                        fontSize: FontSize.Medium
                                    }
                                }
                                Label {
                                    topMargin: 2
                                    bottomMargin: 2
                                    text: ListItemData.detail
                                    multiline: true
                                    textStyle{
                                        fontSize: FontSize.Small
                                    }
                                }
                            }
                        }
                    ]
                }
            }
            //promo
        }
    }

} //content container