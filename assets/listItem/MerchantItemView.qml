import bb.cascades 1.0
import com.dompetku.WebImage 1.0
Container {
    minHeight: 100
   // topPadding: 10
    layout: DockLayout {
    }
    attachedObjects: [
        TextStyleDefinition {
            id: bodyText
            fontWeight: FontWeight.Normal
            color: Color.Black
            fontSize: FontSize.PointValue
            fontSizeValue: 8
        }
        
    ]
    ImageView {
        id: border
        objectName: "border"
        imageSource: "asset:///images/border/arrow_border.amd"
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
    }
    Container {
        objectName: "contentContainer"
        layout: DockLayout {
        }
        verticalAlignment: VerticalAlignment.Center
        preferredWidth: 9999
        Container {
            rightPadding: 10
            horizontalAlignment: HorizontalAlignment.Right
            verticalAlignment: VerticalAlignment.Center
            ImageView {
                imageSource: "asset:///images/icon-arrow-right.png"
            }
        }
        Container {
            rightPadding: 15
            leftPadding: 10
            horizontalAlignment: HorizontalAlignment.Left
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            verticalAlignment: VerticalAlignment.Center
            WebImage {
                objectName: "icon"
                horizontalAlignment: HorizontalAlignment.Right
                verticalAlignment: VerticalAlignment.Center
            }
            Label {
                objectName: "lblName"
                horizontalAlignment: HorizontalAlignment.Left
                verticalAlignment: VerticalAlignment.Center
                translationX: 10
                textStyle.base: bodyText.style

                leftMargin: 0
            }
        }

    }
}
