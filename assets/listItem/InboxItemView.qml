import bb.cascades 1.0
Container {
    function setHighlight(highlighted) {
        console.log("setHighlight");
        if (highlighted) {
            border.setImageSource(("asset:///images/border/arrow_border_sel.amd"));
            title.textStyle.color = Color.White;
        } else {
            border.setImageSource(("asset:///images/border/arrow_border.amd"));
            title.textStyle.color = Color.Black;
        }
    }
    
    // Signal handler for ListItem activation
    ListItem.onActivationChanged: {
        setHighlight(ListItem.active);
    }
    
    // Signal handler for ListItem selection
    ListItem.onSelectionChanged: {
        setHighlight(ListItem.selected);
    }
    layout: DockLayout {
    }
    preferredWidth: Infinity
    ImageView {
        id: border
        imageSource: "asset:///images/border/arrow_border.amd"
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
    }
    Container {
        topPadding: 30
        bottomPadding: 30
        leftPadding: 30
        rightPadding: leftPadding
        horizontalAlignment: HorizontalAlignment.Left
        verticalAlignment: VerticalAlignment.Center
        layout: StackLayout {
        }
        Label {
            objectName: "lblTanggal"
            textStyle {
                fontWeight: FontWeight.Normal
                fontSize: FontSize.PointValue
                fontSizeValue: 8
                color: Color.DarkGray
            }
        }
        Label {
            objectName: "lblNama"
            textStyle {
                fontWeight: FontWeight.Normal
                fontSize: FontSize.PointValue
                fontSizeValue: 8
                color: Color.Black
            }
        }
        
    }
    ImageButton {
        objectName: "deleteButton"
        translationX: -10
        horizontalAlignment: HorizontalAlignment.Right
        verticalAlignment: VerticalAlignment.Center
        defaultImageSource: "asset:///images/icon-delete.png"
    }


}
