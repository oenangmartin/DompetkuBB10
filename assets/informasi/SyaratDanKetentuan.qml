import bb.cascades 1.0

/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.0

Container {
    property int screenW: UiUtils.ScreenWidth
    property int screenH: UiUtils.ScreenHeight
    property int fieldBottomPadding: 40
    background: Color.create("#F2F2F2")
    preferredWidth: screenW
    preferredHeight: screenH
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    layout: DockLayout {

    }
    Container {

        Container {
            background: Color.create("#F37022")
            preferredWidth: Infinity
            topPadding: 30
            bottomPadding: 30
            Container {
                layout: DockLayout {
                }
                preferredWidth: Infinity
                Container {
                    leftPadding: 20
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    verticalAlignment: VerticalAlignment.Center
                    ImageButton {
                        objectName: "buttonBack"
                        defaultImageSource: "asset:///images/back_button.png"
                        verticalAlignment: VerticalAlignment.Center
                        onClicked: {
                            page.back();
                        }
                    }
                    onTouch: {

                    }
                }

                Label {
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    text: qsTr("Syarat & Ketentuan") + Retranslate.onLocaleOrLanguageChanged
                    textStyle {
                        fontWeight: FontWeight.Bold
                        fontSize: FontSize.Large
                        color: Color.White
                    }
                }
            }

        }

        Container {
            topPadding: 20
            bottomPadding: topPadding
            leftPadding: 40
            rightPadding: leftPadding
            ScrollView {

                Container {

                    ImageView {
                        imageSource: "asset:///images/logo.png"
                        horizontalAlignment: HorizontalAlignment.Center
                    }

                    Label {
                        topMargin: 50
                        multiline: true
                        text: "Ketentuan Umum dan Syarat-Syarat Layanan Dompetku
                        
Ketentuan umum dan syarat-syarat layanan Dompetku  ini (“Ketentuan dan Syarat ”) berlaku dan mengikat bagi  Pelanggan serta PT Indosat Tbk (“Indosat”). 

1. DEFINISI 

1.1. “Batas Dana” adalah jumlah maksimal dana yang ditetapkan Indosat untuk ditampung dalam setiap Rekening Pelanggan, termasuk setiap perubahannya dari waktu ke waktu. 

1.2.“Layanan Dompetku” adalah layanan yang memungkinkan pelanggan melakukan pembayaran Transaksi dengan menggunakan fasilitas telekomunikasi berupa nomor MSISDN Pelanggan untuk mendebet dana yang ada di Rekening Pelanggan. 

1.3. Rekening Pelanggan adalah nomor rekening Layanan Dompetku dengan menggunakan nomor MSISDN Pelanggan sebagai rekening virtual untuk menampung Dana Pelanggan yang akan digunakan untuk Transaksi. 

1.4. “Pelanggan” adalah pelanggan jasa telekomunikasi Indosat baik pasca bayar maupun prabayar yang sudah mendaftarkan diri untuk menggunakan Layanan Dompetku. 

1.5. “Masa Berlaku” adalah masa atau periode dimana Pelanggan dapat menggunakan Layanan Dompetku yaitu sejak Layanan Dompetku diaktifkan sampai dengan berakhirnya Layanan Dompetku. 

1.6. “Transaksi” adalah transaksi jual-beli barang dan/atau jasa yang pembayarannya dilakukan dengan menggunakan fasilitas Layanan Dompetku, di tempat-tempat yang telah menjalin kerjasama dengan Indosat (”Merchant”). 

1.7. “Pengisian Dana” adalah penyetoran dana ke Rekening Pelanggan oleh Pelanggan atau pihak lainnya. 

1.8. “Pemberitahuan Tertulis” adalah pemberitahuan terkait Layanan Dompetku yang disampaikan secara tertulis baik langsung dikirim ke alamat Pelanggan maupun disampaikan melalui media komunikasi lainnya. 

1.9. “PIN” adalah nomor sandi pribadi yang dikeluarkan oleh Indosat untuk setiap Rekening Pelanggan untuk tujuan keamanan Rekening Pelanggan. 

1.10. “Force Majeure” adalah kejadian‐kejadian di luar kekuasaan Indosat yang mengakibatkan terhenti atau tertundanya pelaksanaan kewajiban Indosat berdasarkan Ketentuan dan Syarat ini, termasuk : petir, gempa bumi, topan, kebakaran, perang, ledakan, banjir, tsunami, sabotase, huru‐hara atau kebijakan pemerintah/regulator atau tindakan pihak ketiga.


2. PENGGUNAAN LAYANAN DOMPETKU

2.1 Layanan Dompetku hanya boleh digunakan oleh Pelanggan dan tidak dapat dipindahtangankan kepada pihak lain tanpa persetujuan Indosat. Segala akibat atau kerugian yang timbul karena kesalahan, kelalaian, ketidakhati–hatian Pelanggan sendiri dalam menggunakan layanan ini adalah merupakan beban dan tanggung jawab Pelanggan sepenuhnya. 

2.2 Layanan Dompetku tidak dapat digunakan untuk transaksi Pembelanjaan/Pembayaran di Merchant apabila :

(a) Dana dalam Rekening Pelanggan tidak mencukupi;

(b) Kartu Prabayar Pelanggan dalam masa tenggang atau sudah kadaluarsa;

(c) Kartu Pasca Bayar Pelanggan dalam keadaan terblokir baik sebagian (outgoing) maupun seluruhnya (incoming & outgoing). 

2.3 Dalam hal Pelanggan melanggar sebagian atau seluruh Ketentuan dan Syarat ini, maka Indosat berhak untuk: 

(a) Menolak setiap Transaksi yang dilakukan Pelanggan. 

(b) Menutup dan/atau mengakhiri dan/atau memblokir Layanan Dompetku untuk Pelanggan bersangkutan. 


3. PIN 

3.1 Indosat akan menerbitkan PIN atas setiap nomor Rekening Pelanggan. Penjelasan lebih lanjut mengenai fungsi PIN diatur di dalam buku/brosur/leaflet/pamflet/flyer pedoman Layanan Dompetku. 

3.2 Pelanggan harus selalu menjaga kerahasiaan PIN dan tidak boleh memberitahukannya kepada pihak lain dalam keadaan atau dengan cara apapun. Segala akibat yang timbul karena kelalaian, ketidakhati–hatian, atau atas penggunaan atau penyalahgunaan PIN oleh Pelanggan atau orang lain dengan atau tanpa izin dari Pelanggan yang bersangkutan, adalah merupakan beban dan tanggung jawab sepenuhnya dari Pelanggan. 


4. BATAS DANA 

Indosat, dengan tetap memperhatikan ketentuan perundang–undangan yang berlaku, berhak menetapkan serta merubah Batas Dana sewaktu-waktu melalui Pemberitahuan Tertulis. Untuk saat ini Batas Dana Rekening Layanan Dompetku adalah :

4.1 Untuk pelanggan Reguler maksimum Rp 1.000.000,- (Satu Juta Rupiah) 

4.2 Untuk pelanggan Premium maksimum Rp 5.000.000,- (Lima Juta Rupiah) 


5. PENGISIAN DANA 

5.1 Pengisian Dana ke Rekening Pelanggan dapat dilakukan melalui fasilitas sebagai berikut:

(a) Galeri Indosat

(b) Melalui Merchant / Agent yang ditunjuk

(c) ATM Bank;

(d) Mobile Banking / SMS Banking;

(e) Internet Banking;

(f) Telephone Banking; 

(g) Tunai melalui Bank yang ditunjuk; atau

(h) Fasilitas lainnya yang akan diberitahukan oleh Indosat dari waktu ke waktu.

5.2 Indosat sewaktu–waktu berhak melakukan perubahan atas fasilitas–fasilitas tersebut di atas. Pelanggan mengetahui dan setuju bahwa atas penggunaan Layanan Dompetku ini, Indosat atau mitranya mengenakan biaya administrasi bulanan sebagaimana ditentukan di Formulir Pendaftaran yang akan dibebankan dan didebet dari Rekening Pelanggan secara bulanan. Biaya administrasi bulanan dapat berubah sesuai kebijakan yang berlaku di Indosat.

5.3 Pelanggan tidak mendapatkan bunga atas penempatan Dananya di Rekening Pelanggan. 

5.4 Apabila Masa Berlaku Layanan Dompetku telah berakhir atau ditutup atau Kartu SIM diblokir atau dalam masa tenggang atau kadaluarsa dan masih terdapat sisa Dana di Rekening Pelanggan maka apabila Pelanggan tidak mengambil Dananya, Indosat berhak motong biaya administrasi bulanan sampai dengan habisnya sisa Dana yang tersimpan di Rekening Pelanggan atau ditutupnya Rekening Pelanggan. 

6. PENGAKHIRAN LAYANAN DOMPETKU 

6.1 Layanan Dompetku akan berakhir karena sebab-sebab sebagai berikut : 

(a) Diakhiri oleh Pelanggan setiap saat dengan mengisi form penutupan yang tersedia di galeri-galeri Indosat. 

(b) Dakhiri secara seketika oleh Indosat setiap saat, apabila menurut pertimbangan Indosat, Pelanggan telah melanggar Ketentuan dan Syarat ini dan/atau melanggar ketentuan dan syarat berlangganan jasa telekomunikasi Indosat pasca bayar atau ketentuan dan syarat-syarat pemakaian kartu prabayar Indosat dan/atau peraturan perundang-undangan yang berlaku. 

(c) Kartu Prabayar Pelanggan dalam keadaan kadaluarsa.

(d) Kartu pascabayar Pelanggan dalam keadaan terblokir baik sebagian (outgoing) maupun seluruhnya (incoming & outgoing). 

6.2 Setiap penutupan atau pembatalan Layanan Dompetku baik atas kehendak Pelanggan maupun atas kehendak Indosat akan dikenakan biaya administrasi penutupan yang besarnya sebagaimana tercantum dalam Formulir Berlangganan dan dapat berubah sesuai kebijakan yang berlaku di Indosat.

6.3 Pelanggan dengan ini menyatakan bertanggung jawab sepenuhnya dan karenanya membebaskan Indosat dari segala tuntutan dan/atau gugatan dalam bentuk apapun dari pihak ketiga manapun termasuk suami/istri/ahli waris Pelanggan sehubungan dengan proses pendebetan, penutupan dan/atau pembatalan dan/atau pemblokiran Layanan Dompetku. Pelanggan berjanji tidak akan melakukan suatu tindakan apapun yang membatasi atau mengurangi hak–hak Indosat berdasarkan Ketentuan dan Syarat ini. 


7. BATAS TANGGUNG JAWAB INDOSAT

7.1 Pelanggan berjanji tidak akan menuntut Indosat atas segala kerugian tidak langsung termasuk namun tidak terbatas pada kehilangan keuntungan, kehilangan pendapatan atau kehilangan kesempatan maupun kerugian immateriil lainnya termasuk karena tuntutan dari pihak manapun yang timbul karena penggunaan Layanan Dompetku oleh Pelanggan. 

7.2 Indosat hanya berkewajiban untuk menanggung kerugian langsung yang diderita Pelanggan sepanjang dapat dibuktikan karena kesalahan Indosat, dengan jumlah maksimum sebesar jumlah nominal transaksi yang bermasalah.

7.3 Indosat dibebaskan dari kewajibannya berdasarkan Ketentuan dan Syarat ini apabila terjadi Force Majeure atau karena tindakan pihak ketiga atau karena peraturan pemerintah yang menyebabkan Indosat tidak bisa memberikan Layanan Dompetku. 


8 HUKUM YANG BERLAKU 

8.1 Syarat dan Ketentuan ini dibuat dan dilaksanakan berdasarkan ketentuan hukum dan perundang-undangan yang berlaku di negara Republik Indonesia.

8.2 Segala perselisihan yang timbul berkenaan dengan layanan Dompetku yang tidak dapat diselesaikan secara musyawarah akan diselesaikan melalui Pengadilan Negeri Jakarta Pusat."
                    }
                }
            }
        }

    }
}
