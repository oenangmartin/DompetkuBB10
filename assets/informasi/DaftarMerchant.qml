import bb.cascades 1.0
import  com.dompetku.WebImage 1.0

Container {
    topPadding: 20
    leftPadding: 20
    rightPadding: 20
    bottomPadding: 100
    horizontalAlignment: HorizontalAlignment.Center
    onCreationCompleted: {
        Qt.iconSize = UiUtils.ScreenWidth/3
    }
    ListView {
        objectName: "list"
        layout: GridListLayout {
            columnCount: 2
            headerMode: ListHeaderMode.None
            //cellAspectRatio: 1.0
            spacingAfterHeader: 10
            verticalCellSpacing: 0
            horizontalCellSpacing: 0
        }
        listItemComponents: [
            ListItemComponent {
                type: "activityItem"
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        layout: DockLayout {
                        }
                    }
                }
            },
            ListItemComponent {
                type: "item"
                Container {
                    layout: DockLayout {
                    }
                    ImageView {
                        id: border
                        imageSource: "asset:///images/border/arrow_border.amd"
                        horizontalAlignment: HorizontalAlignment.Fill
                        verticalAlignment: VerticalAlignment.Fill
                    }
                    WebImage {
                        width: Qt.iconSize
                        height: Qt.iconSize
                        placeholder: false
                        imagePath: ListItemData.list_merchant_img
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center
                    }
                }
                
            },
            ListItemComponent {
                type: "itemText"
                Container {
                    layout: DockLayout {
                    }
                    ImageView {
                        imageSource: "asset:///images/border/arrow_border.amd"
                        horizontalAlignment: HorizontalAlignment.Fill
                        verticalAlignment: VerticalAlignment.Fill
                    }
                    Label {
                        text: ListItemData.list_merchant_nama
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center
                        textStyle{
                            fontWeight: FontWeight.Bold
                            fontSize: FontSize.Large
                            color: Color.create("#F9A200")
                        }
                    }
                }
            }
            
        ]
        function itemType(data, indexPath) {
            if (data.loadingitem == true) {
                return "activityItem";
            }
            else if (data.list_merchant_img == "") {
                return "itemText";
            } 
            else {
                return "item";
            }
        }
        attachedObjects: [
            ListScrollStateHandler {
                id: scrollStateHandler
                onScrollingChanged: {
                    if (atEnd) {
                        console.log(">>Loadmore");
                        if (dataModel) dataModel.loadData(false);
                    }
                }
            }
        ]
    }

}
