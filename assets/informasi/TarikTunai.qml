import bb.cascades 1.0

/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.0

Container {
    property int screenW: UiUtils.ScreenWidth
    property int screenH: UiUtils.ScreenHeight
    property int fieldBottomPadding: 40
    background: Color.create("#F2F2F2")
    preferredWidth: screenW
    preferredHeight: screenH
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    layout: DockLayout {

    }
    Container {

        Container {
            background: Color.create("#F37022")
            preferredWidth: Infinity
            topPadding: 30
            bottomPadding: 30
            Container {
                layout: DockLayout {
                }
                preferredWidth: Infinity
                Container {
                    leftPadding: 20
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    verticalAlignment: VerticalAlignment.Center
                    ImageButton {
                        objectName: "buttonBack"
                        defaultImageSource: "asset:///images/back_button.png"
                        verticalAlignment: VerticalAlignment.Center
                        onClicked: {
                            page.back();
                        }
                    }
                    onTouch: {

                    }
                }

                Label {
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    text: qsTr("Tarik Tunai") + Retranslate.onLocaleOrLanguageChanged
                    textStyle {
                        fontWeight: FontWeight.Bold
                        fontSize: FontSize.Large
                        color: Color.White
                    }
                }
            }

        }

        Container {
            topPadding: 20
            bottomPadding: topPadding
            leftPadding: 40
            rightPadding: leftPadding
            ScrollView {

                Container {
                    ImageView {
                        imageSource: "asset:///images/logo.png"
                        horizontalAlignment: HorizontalAlignment.Center
                    }

                    Label {
                        topMargin: 50
                        multiline: true
                        text: "<strong>Tarik Tunai</strong><br/>
Lakukan permintaan token pada menu Minta Token pada bagian bawah aplikasi Dompetku. Lalu datang ke Agen/Merchant/Galeri Indosat terdekat dan lakukan permintaan Tarik Tunai. Informasikan nomor handphone serta token yang diterima kepada Kasir.
                        "
                        textStyle{
                            fontWeight: FontWeight.Normal
                        }
                        textFormat: TextFormat.Html
                    }
                    
                }
            }
        }

    }
}
