import bb.cascades 1.0

/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.0

Container {
    property int screenW: UiUtils.ScreenWidth
    property int screenH: UiUtils.ScreenHeight
    property int fieldBottomPadding: 40
    background: Color.create("#F2F2F2")
    preferredWidth: screenW
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    layout: DockLayout {

    }
    Container {
        Container {
            ListView {
                objectName: "list"
                listItemComponents: [
                    ListItemComponent {
                        type: "itemNegatif"
                        Container {
                            topPadding: 20
                            bottomPadding: topPadding
                            leftPadding: 20
                            rightPadding: leftPadding
                            Label {
                                topMargin: 0
                                bottomMargin: 0
                                text: ListItemData.type
                                textStyle{
                                    fontWeight: FontWeight.Bold
                                    fontSize: FontSize.Medium
                                }
                            }
                            
                            Container {
                                layout: DockLayout {
                                
                                }
                                preferredWidth: Infinity
                                Label {
                                    verticalAlignment: VerticalAlignment.Center
                                    topMargin: 0
                                    bottomMargin: 0
                                    text: ListItemData.agent
                                    textStyle{
                                        fontWeight: FontWeight.Bold
                                        color: Color.Gray
                                        fontSize: FontSize.Medium
                                    }
                                }
                                Container {
                                    horizontalAlignment: HorizontalAlignment.Right
                                    verticalAlignment: VerticalAlignment.Center
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    rightPadding: 10
                                    Label {
                                        topMargin: 0
                                        bottomMargin: 0
                                        text: "Rp (" + ListItemData.amount*-1 + ")"
                                        textStyle{
                                            fontWeight: FontWeight.Normal
                                            color: Color.create("#F29257")
                                            fontSize: FontSize.Medium
                                        }
                                    }
                                    /*ImageView {
                                        imageSource: "asset:///images/icon-history-right-h.png"
                                    }*/
                                }
                            }
                            
                            
                            Label {
                                topMargin: 0
                                bottomMargin: 0
                                text: ListItemData.date
                                textStyle{
                                    fontWeight: FontWeight.Normal
                                    color: Color.Gray
                                    fontSize: FontSize.Small
                                }
                            }
                            
                            
                            Divider {
                            
                            }
                        }
                    
                    },
                    ListItemComponent {
                        type: "item"
                        Container {
                            topPadding: 20
                            bottomPadding: topPadding
                            leftPadding: 20
                            rightPadding: leftPadding
                            Label {
                                topMargin: 0
                                bottomMargin: 0
                                text: ListItemData.type
                                textStyle{
                                    fontWeight: FontWeight.Bold
                                    fontSize: FontSize.Medium
                                }
                            }
                            
                            Container {
                                layout: DockLayout {
                                    
                                }
                                preferredWidth: Infinity
                                Label {
                                    verticalAlignment: VerticalAlignment.Center
                                    topMargin: 0
                                    bottomMargin: 0
                                    text: ListItemData.agent
                                    textStyle{
                                        fontWeight: FontWeight.Bold
                                        color: Color.Gray
                                        fontSize: FontSize.Medium
                                    }
                                }
                                Container {
                                    horizontalAlignment: HorizontalAlignment.Right
                                    verticalAlignment: VerticalAlignment.Center
                                    layout: StackLayout {
                                    	orientation: LayoutOrientation.LeftToRight
                                    }
                                    rightPadding: 10
                                    Label {
                                        topMargin: 0
                                        bottomMargin: 0
                                        text: "Rp " + ListItemData.amount
                                        textStyle{
                                            fontWeight: FontWeight.Normal
                                            color: Color.Black
                                            fontSize: FontSize.Medium
                                        }
                                    }
                                    /*ImageView {
                                        imageSource: "asset:///images/icon-history-left.png"
                                    }*/
                                }
                            }
                            
                            
                            Label {
                                topMargin: 0
                                bottomMargin: 0
                                text: ListItemData.date
                                textStyle{
                                    fontWeight: FontWeight.Normal
                                    color: Color.Gray
                                    fontSize: FontSize.Small
                                }
                            }
                            
                            
                            Divider {
                                
                            }
                        }

                    }
                ]
                function itemType(data, indexPath) {
                    if(data.amount < 0)
                    	return "itemNegatif"
                    else
                    	return "item"
                }
            }
        }

    }
}
