import bb.cascades 1.0

/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.0

Container {
    property int screenW: UiUtils.ScreenWidth
    property int screenH: UiUtils.ScreenHeight
    property int fieldBottomPadding: 40
    background: Color.create("#F2F2F2")
    preferredWidth: screenW
    preferredHeight: screenH
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    layout: DockLayout {

    }
    Container {

        Container {
            background: Color.create("#F37022")
            preferredWidth: Infinity
            topPadding: 30
            bottomPadding: 30
            Container {
                layout: DockLayout {
                }
                preferredWidth: Infinity
                Container {
                    leftPadding: 20
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    verticalAlignment: VerticalAlignment.Center
                    ImageButton {
                        objectName: "buttonBack"
                        defaultImageSource: "asset:///images/back_button.png"
                        verticalAlignment: VerticalAlignment.Center
                        onClicked: {
                            page.back();
                        }
                    }
                    onTouch: {

                    }
                }

                Label {
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    text: qsTr("Beli / bayar") + Retranslate.onLocaleOrLanguageChanged
                    textStyle {
                        fontWeight: FontWeight.Bold
                        fontSize: FontSize.PointValue
                        fontSizeValue: 9
                        color: Color.White
                    }
                }
            }

        }

        Container {
            topPadding: 20
            bottomPadding: topPadding
            leftPadding: 40
            rightPadding: leftPadding
            ScrollView {

                Container {
                    ImageView {
                        imageSource: "asset:///images/logo.png"
                        horizontalAlignment: HorizontalAlignment.Center
                    }

                    Label {
                        topMargin: 50
                        multiline: true
                        text: "<strong>Beli</strong><br/>
Lakukan pembelian pulsa, voucher games, asuransi, voucher TV, donasi, dan isi ulang kartu KRL dari menu Beli pada bagian bawah aplikasi Dompetku.
<br/><strong>Bayar Tagihan</strong><br/>
Lakukan pembayaran tagihan telepon, TV, air, pinjaman dan internet dari menu Bayar pada bagian bawah aplikasi Dompetku.
<br/><strong>Belanja di Merchant</strong><br/>
Lakukan permintaan token pada menu Minta Token pada bagian bawah aplikasi. Lalu serahkan token dan nomor handphone anda ke kasir Merchant.

Untuk transaksi di merchant online cukup pilih metode pembayaran Dompetku dan masukan nomor handphone anda yang terdaftar Dompetku. Notifikasi akan muncul di handphone anda lalu balas dengan memasukan PIN untuk konfirmasi transaksi.
                        "
                    textStyle {
                        fontSize: FontSize.PointValue
                        fontSizeValue: 8
                        color: Color.Black
                    }
                        textFormat: TextFormat.Html
                    }
                    
                }
            }
        }

    }
}
