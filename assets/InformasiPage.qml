import bb.cascades 1.0
import bb.cascades 1.0

/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.0

Container {
    property int screenW: UiUtils.ScreenWidth
    property int screenH: UiUtils.ScreenHeight
    property int fieldBottomPadding: 40
    background: Color.create("#F2F2F2")
    preferredWidth: screenW
    preferredHeight: screenH
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    layout: DockLayout {

    }
    Container {

        Container {
            background: Color.create("#F37022")
            preferredWidth: Infinity
            topPadding: 30
            bottomPadding: 30
            Container {
                layout: DockLayout {
                }
                preferredWidth: Infinity
                Container {
                    leftPadding: 20
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    ImageButton {
                        objectName: "buttonBack"
                        defaultImageSource: "asset:///images/back_button.png"
                        verticalAlignment: VerticalAlignment.Center
                        onClicked: {
                            page.back();
                        }
                    }

                }

                Label {
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    text: qsTr("Informasi")
                    textStyle {
                        fontWeight: FontWeight.Bold
                        fontSize: FontSize.Large
                        color: Color.White
                    }
                }
            }

        }

        Container {
            ListView {
                objectName: "list"
                listItemComponents: [
                    ListItemComponent {
                        type: "item"
                        Container {
                            function setHighlight(highlighted) {
                                console.log("setHighlight");
                                if (highlighted) {
                                    border.setImageSource(("asset:///images/border/arrow_border_sel.amd"));
                                    icon.setImageSource(ListItemData.iconH);
                                    title.textStyle.color = Color.White;
                                } else {
                                    border.setImageSource(("asset:///images/border/arrow_border.amd"));
                                    icon.setImageSource(ListItemData.icon);
                                    title.textStyle.color = Color.Black;
                                }
                            }
                            
                            // Signal handler for ListItem activation
                            ListItem.onActivationChanged: {
                                setHighlight(ListItem.active);
                            }
                            
                            // Signal handler for ListItem selection
                            ListItem.onSelectionChanged: {
                                setHighlight(ListItem.selected);
                            }
                            layout: DockLayout {
                            }
                            preferredWidth: Infinity
                            ImageView {
                                id: border
                                imageSource: "asset:///images/border/arrow_border.amd"
                                horizontalAlignment: HorizontalAlignment.Fill
                                verticalAlignment: VerticalAlignment.Fill
                            }
                            Container {
                                topPadding: 30
                                bottomPadding: 30
                                horizontalAlignment: HorizontalAlignment.Left
                                verticalAlignment: VerticalAlignment.Center
                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight
                                }
                                ImageView {
                                    id: icon
                                    imageSource: ListItemData.icon
                                    verticalAlignment: VerticalAlignment.Center
                                }
                                Label {
                                    id: title
                                    text: ListItemData.title
                                }
                            }
                            ImageView {
                                translationX: -10
                                horizontalAlignment: HorizontalAlignment.Right
                                verticalAlignment: VerticalAlignment.Center
                                imageSource: "asset:///images/icon-arrow-right.png"
                            }
                        
                        
                        }
                    }
                ]
                function itemType(data, indexPath) {
                    return "item"
                }
            }
        }

    }
}
