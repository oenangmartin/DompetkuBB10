import bb.cascades 1.0
import com.dompetku.InboxItem 1.0

Container {
    property int screenW: UiUtils.ScreenWidth
    property int screenH: UiUtils.ScreenHeight
    property int fieldBottomPadding: 40
    background: Color.create("#F2F2F2")
    preferredWidth: screenW
    preferredHeight: screenH
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    layout: DockLayout {
    
    }
    Container {
        
        Container {
            background: Color.create("#F37022")
            preferredWidth: Infinity
            topPadding: 30
            bottomPadding: 30
            Container {
                layout: DockLayout {
                }
                preferredWidth: Infinity
                Container {
                    verticalAlignment: VerticalAlignment.Center
                    leftPadding: 20
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    ImageButton {
                        objectName: "buttonBack"
                        defaultImageSource: "asset:///images/back_button.png"
                        verticalAlignment: VerticalAlignment.Center
                        onClicked: {
                            page.back();
                        }
                    }
                
                }
                
                Label {
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    text: qsTr("Inbox Detail")
                    textStyle {
                        fontWeight: FontWeight.Bold
                        fontSize: FontSize.Large
                        color: Color.White
                    }
                }
                
                ImageButton {
                    translationX: -10
                    defaultImageSource: "asset:///images/icon-delete.png"
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Right
                    onClicked: {
                        page.deleteInbox();
                    }
                }
            }
        
        }
        
        Container {
            topPadding: 20
            leftPadding: 20
            Label {
                objectName: "lblRefId"
            }
            Label {
                objectName: "lblTanggal"
            }
            Label {
                objectName: "lblAction"
            }
            Label {
                objectName: "lblMerchant"
            }
            Label {
                objectName: "lblTujuan"
            }
            Label {
                objectName: "lblJumlah"
            }
            Label {
                objectName: "lblToken"
                multiline: true
            }
        }
    
    }
}
