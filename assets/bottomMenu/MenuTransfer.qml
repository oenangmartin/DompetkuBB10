import bb.cascades 1.0

Container {
    ListView {
        objectName: "list"
        listItemComponents: [
            ListItemComponent {
                type: "item"
                Container {
                    layout: DockLayout {
                    }
                    preferredWidth: Infinity
                    ImageView {
                        imageSource: "asset:///images/border/arrow_border.amd"
                        horizontalAlignment: HorizontalAlignment.Fill
                        verticalAlignment: VerticalAlignment.Fill
                    }
                    Container {
                        topPadding: 30
                        bottomPadding: 30
                        horizontalAlignment: HorizontalAlignment.Left
                        verticalAlignment: VerticalAlignment.Center
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        ImageView {
                            imageSource: ListItemData.icon
                            verticalAlignment: VerticalAlignment.Center
                        }
                        Label {
                            text: ListItemData.title
                            textStyle {
                                fontSize: FontSize.PointValue
                                fontSizeValue: 8
                                color: Color.Black
                            }
                        }
                    }
                    ImageView {
                        translationX: -10
                        horizontalAlignment: HorizontalAlignment.Right
                        verticalAlignment: VerticalAlignment.Center
                        imageSource: "asset:///images/icon-arrow-right.png"
                    }
                
                
                }
            }
        ]
    }
}
