import bb.cascades 1.0

Container {
   
    topPadding: 80
    leftPadding: 80
    rightPadding: 80
    horizontalAlignment: HorizontalAlignment.Center
    Label {
        text: qsTr("PIN") + Retranslate.onLocaleOrLanguageChanged
        textStyle {
            fontSize: FontSize.PointValue
            fontSizeValue: 8
            color: Color.Black
        }
    }
    Container {
        layout: DockLayout {
        
        }
        ImageView {
            imageSource: "asset:///images/border/input.amd"
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
        }
        TextField {
            objectName: "pin"
            leftPadding: 40
            verticalAlignment: VerticalAlignment.Center
            backgroundVisible: false
            hintText: ""
            inputMode: TextFieldInputMode.NumericPassword
            
        }
    } 
    
    ImageButton {
        topMargin: 50
        horizontalAlignment: HorizontalAlignment.Center
        defaultImageSource: "asset:///images/button/button-kirim.png"
        onClicked: {
            page.requestToken();
        }
    }
    
}
