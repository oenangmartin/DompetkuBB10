import bb.cascades 1.0

Container {

    ScrollView {
        implicitLayoutAnimationsEnabled: false
        Container {

            topPadding: 80
            leftPadding: 80
            rightPadding: 80
            horizontalAlignment: HorizontalAlignment.Center

            //tujuan
            Label {
                text: qsTr("No Tujuan") + Retranslate.onLocaleOrLanguageChanged
                textStyle {
                    fontSize: FontSize.PointValue
                    fontSizeValue: 8
                    color: Color.Black
                }
            }
            Container {
                preferredWidth: Infinity
                layout: DockLayout {

                }
                ImageButton {
                    horizontalAlignment: HorizontalAlignment.Right
                    verticalAlignment: VerticalAlignment.Center
                    defaultImageSource: "asset:///images/icon-search.png"
                    pressedImageSource: "asset:///images/icon-search-h.png"
                    onClicked: {
                        page.searchTujuan();
                    }
                }
                Container {
                    preferredWidth: UiUtils.ScreenWidth - 70 - 80 - 80 - 20
                    horizontalAlignment: HorizontalAlignment.Left
                    verticalAlignment: VerticalAlignment.Center
                    layout: DockLayout {

                    }
                    ImageView {
                        imageSource: "asset:///images/border/input.amd"
                        verticalAlignment: VerticalAlignment.Fill
                        horizontalAlignment: HorizontalAlignment.Fill
                    }
                    TextField {
                        objectName: "tujuanField"
                        leftPadding: 40
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center
                        backgroundVisible: false
                        hintText: ""
                        inputMode: TextFieldInputMode.PhoneNumber
                    }
                }
            }

            //Jumlah
            Label {
                text: qsTr("Jumlah") + Retranslate.onLocaleOrLanguageChanged
                textStyle {
                    fontSize: FontSize.PointValue
                    fontSizeValue: 8
                    color: Color.Black
                }
            }
            Container {
                layout: DockLayout {

                }
                ImageView {
                    imageSource: "asset:///images/border/input.amd"
                    verticalAlignment: VerticalAlignment.Fill
                    horizontalAlignment: HorizontalAlignment.Fill
                }
                TextField {
                    objectName: "jumlahField"
                    leftPadding: 40
                    verticalAlignment: VerticalAlignment.Center
                    backgroundVisible: false
                    hintText: ""
                    inputMode: TextFieldInputMode.PhoneNumber
                }
            }

            //PIN
            Label {
                text: qsTr("PIN") + Retranslate.onLocaleOrLanguageChanged
                textStyle {
                    fontSize: FontSize.PointValue
                    fontSizeValue: 8
                    color: Color.Black
                }
            }
            Container {
                layout: DockLayout {

                }
                ImageView {
                    imageSource: "asset:///images/border/input.amd"
                    verticalAlignment: VerticalAlignment.Fill
                    horizontalAlignment: HorizontalAlignment.Fill
                }
                TextField {
                    objectName: "pinField"
                    leftPadding: 40
                    verticalAlignment: VerticalAlignment.Center
                    backgroundVisible: false
                    hintText: ""
                    inputMode: TextFieldInputMode.NumericPassword
                    input {
                        submitKey: SubmitKey.Submit
                        onSubmitted: {
                            page.kirimUang();
                        }
                    }
                }
            }

            ImageButton {
                topMargin: 50
                horizontalAlignment: HorizontalAlignment.Center
                defaultImageSource: "asset:///images/button/button-kirim.png"
                onClicked: {
                    page.kirimUang();
                }
            }

        }
    }
}
