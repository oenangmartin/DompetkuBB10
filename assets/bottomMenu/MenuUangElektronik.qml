import bb.cascades 1.0

Container {
   
    topPadding: 80
    leftPadding: 80
    rightPadding: 80
    horizontalAlignment: HorizontalAlignment.Center
    
    //tujuan
    Label {
        text: qsTr("ID Tujuan") + Retranslate.onLocaleOrLanguageChanged
        textStyle {
            fontSize: FontSize.PointValue
            fontSizeValue: 8
            color: Color.Black
        }
    }
    Container {
        layout: DockLayout {
        
        }
        ImageView {
            imageSource: "asset:///images/border/input.amd"
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
        }
        TextField {
            objectName: "tujuanField"
            leftPadding: 40
            verticalAlignment: VerticalAlignment.Center
            backgroundVisible: false
            hintText: ""
            inputMode: TextFieldInputMode.Default
        }
    } 
    
    //Jumlah
    Label {
        text: qsTr("Jumlah") + Retranslate.onLocaleOrLanguageChanged
        textStyle {
            fontSize: FontSize.PointValue
            fontSizeValue: 8
            color: Color.Black
        }
    }
    Container {
        layout: DockLayout {
        
        }
        ImageView {
            imageSource: "asset:///images/border/input.amd"
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
        }
        TextField {
            objectName: "jumlahField"
            leftPadding: 40
            verticalAlignment: VerticalAlignment.Center
            backgroundVisible: false
            hintText: ""
            inputMode: TextFieldInputMode.PhoneNumber
        }
    } 
    
    //PIN
    Label {
        text: qsTr("PIN") + Retranslate.onLocaleOrLanguageChanged
        textStyle {
            fontSize: FontSize.PointValue
            fontSizeValue: 8
            color: Color.Black
        }
    }
    Container {
        layout: DockLayout {
        
        }
        ImageView {
            imageSource: "asset:///images/border/input.amd"
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
        }
        TextField {
            objectName: "pinField"
            leftPadding: 40
            verticalAlignment: VerticalAlignment.Center
            backgroundVisible: false
            hintText: ""
            inputMode: TextFieldInputMode.NumericPassword
            input {
                submitKey: SubmitKey.Submit
                onSubmitted: {
                    page.kirimUang();
                }
            }
        }
    } 
    
    ImageButton {
        topMargin: 50
        horizontalAlignment: HorizontalAlignment.Center
        defaultImageSource: "asset:///images/button/button-kirim.png"
        onClicked: {
            page.kirimUang();
        }
    }
    
}
