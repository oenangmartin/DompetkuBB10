import bb.cascades 1.0
import com.dompetku.MerchantItem 1.0

Container {
    ListView {
        objectName: "list"
        listItemComponents: [
            
            ListItemComponent {
                type: "item"
                MerchantItem{
                    data: ListItemData
                }
                
            }
        ]
        
        function itemType(data, indexPath) {
            return "item"
        }
    }
}
