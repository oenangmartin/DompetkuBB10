import bb.cascades 1.0
//import com.dompetku.MerchantItem 1.0

Container {
    implicitLayoutAnimationsEnabled: false
    ScrollView {
        implicitLayoutAnimationsEnabled: false
        Container {
            implicitLayoutAnimationsEnabled: false
            leftPadding: 80
            topPadding: 50
            rightPadding: leftPadding
            bottomPadding: leftPadding
            Container {
                implicitLayoutAnimationsEnabled: false
                objectName: "formContainer"
            }
            ImageButton {
                objectName: "submitButton"
                implicitLayoutAnimationsEnabled: false
                visible: false
                topMargin: 50
                horizontalAlignment: HorizontalAlignment.Center
                defaultImageSource: "asset:///images/button/button-beli.png"
                onClicked: {
                    page.submitForm();
                }
            }
        }
    }
}
