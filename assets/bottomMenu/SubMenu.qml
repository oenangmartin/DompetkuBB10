import bb.cascades 1.0
//import com.dompetku.MerchantItem 1.0
import com.dompetku.WebImage 1.0

Container {
    ListView {
        objectName: "list"
        listItemComponents: [
            
            ListItemComponent {
                type: "item"
                Container {
                    function setHighlight(highlighted) {
                        console.log("setHighlight");
                        if (highlighted) {
                            border.setImageSource(("asset:///images/border/arrow_border_sel.amd"));
                            icon.setImageSource(ListItemData.iconH);
                            title.textStyle.color = Color.White;
                        } else {
                            border.setImageSource(("asset:///images/border/arrow_border.amd"));
                            icon.setImageSource(ListItemData.icon);
                            title.textStyle.color = Color.Black;
                        }
                    }
                    
                    // Signal handler for ListItem activation
                    ListItem.onActivationChanged: {
                        setHighlight(ListItem.active);
                    }
                    
                    // Signal handler for ListItem selection
                    ListItem.onSelectionChanged: {
                        setHighlight(ListItem.selected);
                    }
                    layout: DockLayout {
                    }
                    preferredWidth: Infinity
                    ImageView {
                        id: border
                        imageSource: "asset:///images/border/arrow_border.amd"
                        horizontalAlignment: HorizontalAlignment.Fill
                        verticalAlignment: VerticalAlignment.Fill
                    }
                    Container {
                        topPadding: 30
                        bottomPadding: 30
                        horizontalAlignment: HorizontalAlignment.Left
                        verticalAlignment: VerticalAlignment.Center
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        ImageView {
                            id: icon
                            imageSource: ListItemData.icon
                            verticalAlignment: VerticalAlignment.Center
                        }
                        Label {
                            id: title
                            text: ListItemData.sub_merchant_nama
                            textStyle {
                                fontSize: FontSize.PointValue
                                fontSizeValue: 8
                                color: Color.Black
                            }
                        }
                    }
                    ImageView {
                        translationX: -10
                        horizontalAlignment: HorizontalAlignment.Right
                        verticalAlignment: VerticalAlignment.Center
                        imageSource: "asset:///images/icon-arrow-right.png"
                    }
                
                
                }
                
            },
            
            ListItemComponent {
                type: "imageItem"
                Container {
                    function setHighlight(highlighted) {
                        console.log("setHighlight");
                        if (highlighted) {
                            border.setImageSource(("asset:///images/border/arrow_border_sel.amd"));
                            icon.setImageSource(ListItemData.iconH);
                            title.textStyle.color = Color.White;
                        } else {
                            border.setImageSource(("asset:///images/border/arrow_border.amd"));
                            icon.setImageSource(ListItemData.icon);
                            title.textStyle.color = Color.Black;
                        }
                    }
                    
                    // Signal handler for ListItem activation
                    ListItem.onActivationChanged: {
                        setHighlight(ListItem.active);
                    }
                    
                    // Signal handler for ListItem selection
                    ListItem.onSelectionChanged: {
                        setHighlight(ListItem.selected);
                    }
                    layout: DockLayout {
                    }
                    preferredWidth: Infinity
                    ImageView {
                        imageSource: "asset:///images/border/arrow_border.amd"
                        horizontalAlignment: HorizontalAlignment.Fill
                        verticalAlignment: VerticalAlignment.Fill
                    }
                    Container {
                        topPadding: 30
                        bottomPadding: 30
                        leftPadding: 30
                        horizontalAlignment: HorizontalAlignment.Left
                        verticalAlignment: VerticalAlignment.Center
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        WebImage {
                            imagePath: ListItemData.img_path
                        }
                    }
                    ImageView {
                        translationX: -10
                        horizontalAlignment: HorizontalAlignment.Right
                        verticalAlignment: VerticalAlignment.Center
                        imageSource: "asset:///images/icon-arrow-right.png"
                    }
                
                
                }
            }
        ]
        
        function itemType(data, indexPath) {
            if (data.img_path) {
                return "imageItem";
            } 
            else {
                return "item";
            }
        }
        
    }
}
