import bb.cascades 1.0

Container {
   
    topPadding: 80
    leftPadding: 80
    rightPadding: 80
    horizontalAlignment: HorizontalAlignment.Center
    Label {
        text: qsTr("PIN Lama") + Retranslate.onLocaleOrLanguageChanged
    }
    Container {
        layout: DockLayout {
        
        }
        ImageView {
            imageSource: "asset:///images/border/input.amd"
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
        }
        TextField {
            objectName: "pinLamaField"
            leftPadding: 40
            verticalAlignment: VerticalAlignment.Center
            backgroundVisible: false
            hintText: ""
            inputMode: TextFieldInputMode.NumericPassword
            
        }
    } 
    Label {
        text: qsTr("PIN Baru") + Retranslate.onLocaleOrLanguageChanged
    }
    Container {
        layout: DockLayout {
        
        }
        ImageView {
            imageSource: "asset:///images/border/input.amd"
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
        }
        TextField {
            objectName: "pinBaruField"
            leftPadding: 40
            verticalAlignment: VerticalAlignment.Center
            backgroundVisible: false
            hintText: ""
            inputMode: TextFieldInputMode.NumericPassword
            
        }
    } 
    Label {
        text: qsTr("Konfirmasi Pin Baru") + Retranslate.onLocaleOrLanguageChanged
    }
    Container {
        layout: DockLayout {
        
        }
        ImageView {
            imageSource: "asset:///images/border/input.amd"
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
        }
        TextField {
            objectName: "pinBaruUlangField"
            leftPadding: 40
            verticalAlignment: VerticalAlignment.Center
            backgroundVisible: false
            hintText: ""
            inputMode: TextFieldInputMode.NumericPassword
            
        }
    } 
    
    ImageButton {
        topMargin: 50
        horizontalAlignment: HorizontalAlignment.Center
        defaultImageSource: "asset:///images/button/button-submit.png"
        onClicked: {
            page.submit();
        }
    }
    
}
