/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.0

Container {
    property int screenW: UiUtils.ScreenWidth
    property int screenH: UiUtils.ScreenHeight
    property int fieldBottomPadding: 40
    background: Color.create("#F2F2F2")
    preferredWidth: screenW
    preferredHeight: screenH
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    layout: DockLayout {

    }
    Container {

        Container {
            background: Color.create("#F37022")
            preferredWidth: Infinity
            topPadding: 30
            bottomPadding: 30
            Container {
                layout: DockLayout {
                }
                preferredWidth: Infinity
                Container {
                    leftPadding: 20
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    ImageButton {
                        defaultImageSource: "asset:///images/back_button.png"
                        verticalAlignment: VerticalAlignment.Center
                        onClicked: {
                            page.back();
                        }
                    }
                }

                Label {
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    text: qsTr("Kode Verifikasi") + Retranslate.onLocaleOrLanguageChanged
                    textStyle {
                        fontWeight: FontWeight.Bold
                        fontSize: FontSize.Large
                        color: Color.White
                    }
                }
            }

        }
        ScrollView {

            Container {
                topPadding: 100
                bottomPadding: 50
                leftPadding: 50
                rightPadding: 50

                ImageView {
                    imageSource: "asset:///images/logo.png"
                    horizontalAlignment: HorizontalAlignment.Center
                }

                ActivityIndicator {
                    objectName: "indicator"
                    horizontalAlignment: HorizontalAlignment.Center
                    preferredHeight: 100
                    preferredWidth: 100
                }
                Label {
                    objectName: "lblTimer"
                    horizontalAlignment: HorizontalAlignment.Center
                    text: "5:00"
                    textStyle {
                        color: Color.Gray
                    }
                }
                Container {
                    visible: false
                    topPadding: 50
                    objectName: "doneContainer"
                    horizontalAlignment: HorizontalAlignment.Center
                    Label {
                        topMargin: 50
                        bottomMargin: 0
                        horizontalAlignment: HorizontalAlignment.Center
                        text: "Done!"
                        multiline: true
                        textStyle {
                            color: Color.Gray
                        }
                    }
                    Label {
                        topMargin: 50
                        horizontalAlignment: HorizontalAlignment.Center
                        text: "Sistem kami tidak dapat mendeteksi kode verifikasi dari HP anda. Silahkan klik Resend untuk kirim ulang kode verifikasi atau masukan manual Kode Verifikasi . Pastikan nomor Hp yang anda masukan benar."
                        multiline: true
                        textStyle {
                            color: Color.Gray
                        }
                    }
                }
                Container {
                    visible: true
                    topPadding: 50
                    objectName: "waitContainer"
                    horizontalAlignment: HorizontalAlignment.Center
                    Label {
                        topMargin: 50
                        bottomMargin: 0
                        horizontalAlignment: HorizontalAlignment.Center
                        text: "Mohon tunggu Kode Verifikasi Anda"
                        multiline: true
                        textStyle {
                            color: Color.Gray
                        }
                    }
                }
                

                Container {
                	topPadding: 100
                    Label {
                        topMargin: 0
                        horizontalAlignment: HorizontalAlignment.Center
                        text: "Masukan Kode Verifikasi dari SMS"
                        multiline: true
                        textStyle {
                            color: Color.Gray
                            fontSize: FontSize.Small
                        }
                    }
                    Container {
                        bottomPadding: fieldBottomPadding
                        layout: DockLayout {

                        }
                        ImageView {
                            imageSource: "asset:///images/border/input.amd"
                            verticalAlignment: VerticalAlignment.Fill
                            horizontalAlignment: HorizontalAlignment.Fill
                        }
                        TextField {
                            objectName: "noHpField"
                            leftPadding: 40
                            verticalAlignment: VerticalAlignment.Center
                            backgroundVisible: false
                            hintText: ""
                            inputMode: TextFieldInputMode.NumericPassword
                            input {
                                submitKey: SubmitKey.Submit
                                onSubmitted: {
                                    page.kirimSms();
                                }
                            }
                        }
                    } //np hp

					Container {
					    layout: StackLayout {
					        orientation: LayoutOrientation.LeftToRight
					    }
					    horizontalAlignment: HorizontalAlignment.Center
                        ImageButton {
                            horizontalAlignment: HorizontalAlignment.Center
                            defaultImageSource: "asset:///images/button/button-resend.png"
                            pressedImageSource: "asset:///images/button/button-resend_aktif.png"
                            onClicked: {
                                page.requestOtp();
                            }
                        }
                        ImageButton {
                            horizontalAlignment: HorizontalAlignment.Center
                            defaultImageSource: "asset:///images/button/button-oke.png"
                            pressedImageSource: "asset:///images/button/button-oke_aktif.png"
                            onClicked: {
                                page.confirmOtp();
                            }
                        }
					}
                    
                }

            }
        }
    }

}
