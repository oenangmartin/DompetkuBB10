/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.0

Container {
    property int screenW: UiUtils.ScreenWidth
    property int screenH: UiUtils.ScreenHeight
    property int fieldBottomPadding: 40
    background: Color.create("#F2F2F2")
    preferredWidth: screenW
    preferredHeight: screenH
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    layout: DockLayout {

    }
    Container {

        Container {
            background: Color.create("#F37022")
            preferredWidth: Infinity
            topPadding: 30
            bottomPadding: 30
            Container {
                layout: DockLayout {
                }
                preferredWidth: Infinity
                Container {
                    leftPadding: 20
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    verticalAlignment: VerticalAlignment.Center
                    ImageButton {
                        visible: false
                        defaultImageSource: "asset:///images/back_button.png"
                        verticalAlignment: VerticalAlignment.Center
                        onClicked: {
                            page.back();
                        }
                    }
                }

                Label {
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    text: qsTr("Verifikasi Nomor") + Retranslate.onLocaleOrLanguageChanged
                    textStyle {
                        fontWeight: FontWeight.Bold
                        fontSize: FontSize.PointValue
                        fontSizeValue: 8
                        color: Color.White
                    }
                }
            }

        }
        ScrollView {

            Container {
                topPadding: 50
                bottomPadding: 50
                leftPadding: 50
                rightPadding: 50


                //np HP
                Label {
                    text: qsTr("No HP") + Retranslate.onLocaleOrLanguageChanged
                    textStyle {
                        fontSize: FontSize.PointValue
                        fontSizeValue: 8
                        color: Color.Black
                    }
                }
                Container {
                    bottomPadding: fieldBottomPadding
                    layout: DockLayout {

                    }
                    ImageView {
                        imageSource: "asset:///images/border/input.amd"
                        verticalAlignment: VerticalAlignment.Fill
                        horizontalAlignment: HorizontalAlignment.Fill
                    }
                    TextField {
                        objectName: "noHpField"
                        leftPadding: 40
                        verticalAlignment: VerticalAlignment.Center
                        backgroundVisible: false
                        hintText: ""
                        inputMode: TextFieldInputMode.PhoneNumber
                        input {
                            submitKey: SubmitKey.Submit
                            onSubmitted: {
                                page.kirimSms();
                            }
                        }
                    }
                } //np hp

                ImageButton {
                    horizontalAlignment: HorizontalAlignment.Center
                    defaultImageSource: "asset:///images/button-submit.png"
                    onClicked: {
                        page.kirimSms();
                    }
                }

            }
        }
    }

}
