/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.0

Container {
    property int screenW: UiUtils.ScreenWidth
    property int screenH: UiUtils.ScreenHeight
    property int fieldBottomPadding: 40
    background: Color.create("#F2F2F2")
    preferredWidth: screenW
    preferredHeight: screenH
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    layout: DockLayout {

    }
    Container {

        Container {
            background: Color.create("#F37022")
            preferredWidth: Infinity
            topPadding: 30
            bottomPadding: 30
            Container {
                layout: DockLayout {
                }
                preferredWidth: Infinity
                ImageButton {
                    objectName: "buttonBack"
                    defaultImageSource: "asset:///images/back_button.png"
                    verticalAlignment: VerticalAlignment.Center
                    onClicked: {
                        page.back();
                    }
                }

                Label {
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    text: qsTr("Lupa PIN") + Retranslate.onLocaleOrLanguageChanged
                    textStyle {
                        fontWeight: FontWeight.Bold
                        fontSize: FontSize.Large
                        color: Color.White
                    }
                }
            }

        }
        ScrollView {

            Container {
                topPadding: 50
                bottomPadding: 50
                leftPadding: 50
                rightPadding: 50

                Label {
                    text: qsTr("Silahkan hubungi 111 dari nomor Matrix dan 100 dari nomor Mentari & IM3 untuk permintaan reset PIN. PIN baru akan dikirimkan melalui SMS setelah kami verifikasi.") + Retranslate.onLocaleOrLanguageChanged
                    multiline: true
                }
                Label {
                    visible: false
                    text: qsTr("Masukan No HP account Anda, sistem akan megirimkan PIN baru ke nomor tersebut. Silahkan coba login kembali setelah mendapatkan PIN baru.") + Retranslate.onLocaleOrLanguageChanged
                    multiline: true
                }
                Container {
                    visible: false
                    bottomPadding: fieldBottomPadding
                    layout: DockLayout {

                    }
                    ImageView {
                        imageSource: "asset:///images/border/input.amd"
                        verticalAlignment: VerticalAlignment.Fill
                        horizontalAlignment: HorizontalAlignment.Fill
                    }
                    TextField {
                        leftPadding: 40
                        verticalAlignment: VerticalAlignment.Center
                        backgroundVisible: false
                        hintText: ""
                    }
                } 
                ImageButton {
                    visible: false
                    horizontalAlignment: HorizontalAlignment.Center
                    defaultImageSource: "asset:///images/button-submit.png"
                }

            }
        }
    }

}
