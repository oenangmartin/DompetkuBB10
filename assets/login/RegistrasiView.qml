/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.0

Container {
    property int screenW: UiUtils.ScreenWidth
    property int screenH: UiUtils.ScreenHeight
    property int fieldBottomPadding: 40
    background: Color.create("#F2F2F2")
    preferredWidth: screenW
    preferredHeight: screenH
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    layout: DockLayout {

    }
    Container {

        Container {
            background: Color.create("#F37022")
            preferredWidth: Infinity
            topPadding: 30
            bottomPadding: 30
            Container {
                layout: DockLayout {
                }
                preferredWidth: Infinity
                Container {
                    verticalAlignment: VerticalAlignment.Center
                    leftPadding: 20
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    ImageButton {
                        defaultImageSource: "asset:///images/back_button.png"
                        verticalAlignment: VerticalAlignment.Center
                        onClicked: {
                            page.back();
                        }
                    }
                    onTouch: {

                    }
                }

                Label {
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    text: qsTr("Registrasi") + Retranslate.onLocaleOrLanguageChanged
                    textStyle {
                        fontWeight: FontWeight.Bold
                        fontSize: FontSize.Large
                        color: Color.White
                    }
                }
            }

        }
        ScrollView {

            Container {
                topPadding: 50
                bottomPadding: 50
                leftPadding: 50
                rightPadding: 50

				
                //np HP
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Label {
                        text: qsTr("No HP") + Retranslate.onLocaleOrLanguageChanged
                        verticalAlignment: VerticalAlignment.Center
                        rightMargin: 0
                    }
                    Label {
                        text: "*"
                        leftMargin: 2
                        verticalAlignment: VerticalAlignment.Top
                        textStyle{
                            color: Color.Red
                        }
                    }
                }
                
                Container {
                    bottomPadding: fieldBottomPadding
                    layout: DockLayout {

                    }
                    ImageView {
                        imageSource: "asset:///images/border/input.amd"
                        verticalAlignment: VerticalAlignment.Fill
                        horizontalAlignment: HorizontalAlignment.Fill
                    }
                    TextField {
                        objectName: "noHpField"
                        leftPadding: 40
                        verticalAlignment: VerticalAlignment.Center
                        backgroundVisible: false
                        hintText: ""
                        inputMode: TextFieldInputMode.PhoneNumber
                    }
                } //np hp

                //email
                Label {
                    text: qsTr("Email") + Retranslate.onLocaleOrLanguageChanged
                }
                Container {
                    bottomPadding: fieldBottomPadding
                    layout: DockLayout {
                    
                    }
                    ImageView {
                        imageSource: "asset:///images/border/input.amd"
                        verticalAlignment: VerticalAlignment.Fill
                        horizontalAlignment: HorizontalAlignment.Fill
                    }
                    TextField {
                        objectName: "emailField"
                        leftPadding: 40
                        verticalAlignment: VerticalAlignment.Center
                        backgroundVisible: false
                        hintText: ""
                        inputMode: TextFieldInputMode.EmailAddress
                    }
                } //email
                
                //nama depan
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Label {
                        text: qsTr("Nama Depan") + Retranslate.onLocaleOrLanguageChanged
                        verticalAlignment: VerticalAlignment.Center
                        rightMargin: 0
                    }
                    Label {
                        text: "*"
                        leftMargin: 2
                        verticalAlignment: VerticalAlignment.Top
                        textStyle{
                            color: Color.Red
                        }
                    }
                }
                Container {
                    bottomPadding: fieldBottomPadding
                    layout: DockLayout {

                    }
                    ImageView {
                        imageSource: "asset:///images/border/input.amd"
                        verticalAlignment: VerticalAlignment.Fill
                        horizontalAlignment: HorizontalAlignment.Fill
                    }
                    TextField {
                        objectName: "namaDepanField"
                        leftPadding: 40
                        verticalAlignment: VerticalAlignment.Center
                        backgroundVisible: false
                        hintText: ""
                    }
                } //nama depan

                //nama belakang
                Label {
                    text: qsTr("Nama Belakang") + Retranslate.onLocaleOrLanguageChanged
                }
                Container {
                    bottomPadding: fieldBottomPadding
                    layout: DockLayout {

                    }
                    ImageView {
                        imageSource: "asset:///images/border/input.amd"
                        verticalAlignment: VerticalAlignment.Fill
                        horizontalAlignment: HorizontalAlignment.Fill
                    }
                    TextField {
                        objectName: "namaBelakangField"
                        leftPadding: 40
                        verticalAlignment: VerticalAlignment.Center
                        backgroundVisible: false
                        hintText: ""
                    }
                } //nama belakang

                //jenis kelamin
                Container {
                    
                    preferredWidth: Infinity
                    bottomPadding: fieldBottomPadding
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        Label {
                            text: qsTr("Jenis Kelamin") + Retranslate.onLocaleOrLanguageChanged
                            verticalAlignment: VerticalAlignment.Center
                            rightMargin: 0
                        }
                        Label {
                            text: "*"
                            leftMargin: 2
                            verticalAlignment: VerticalAlignment.Top
                            textStyle{
                                color: Color.Red
                            }
                        }
                    }
                    Container {
                        topPadding: fieldBottomPadding / 2
                        horizontalAlignment: HorizontalAlignment.Left
                        verticalAlignment: VerticalAlignment.Center
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        //option pria
                        Container {
                            rightPadding: 30
                            verticalAlignment: VerticalAlignment.Center
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }
                            ImageToggleButton {
                                id: priaField
                                objectName: "priaField"
                                verticalAlignment: VerticalAlignment.Center
                                imageSourceChecked: "asset:///images/icon-option-gender-h.png"
                                imageSourceDefault: "asset:///images/icon-option-gender.png"
                                onCheckedChanged: {
                                    if(checked)
                                        wanitaField.setChecked(false);
                                
                                }
                            }
                            Label {
                                verticalAlignment: VerticalAlignment.Center
                                text: qsTr("Pria") + Retranslate.onLocaleOrLanguageChanged
                            }
                            gestureHandlers:
                                [
                                    TapHandler {
                                        onTapped: {
                                            priaField.setChecked(!priaField.checked)
                                        }
                                    }
                                ]
                        } //option wanita
                        //option pria
                        Container {
                            verticalAlignment: VerticalAlignment.Center
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }
                            ImageToggleButton {
                                id: wanitaField
                                objectName: "wanitaField"
                                verticalAlignment: VerticalAlignment.Center
                                imageSourceChecked: "asset:///images/icon-option-gender-h.png"
                                imageSourceDefault: "asset:///images/icon-option-gender.png"
                                onCheckedChanged: {
                                    if(checked)
                                    	priaField.setChecked(false);
                                    	
                                }
                            }
                            Label {
                                verticalAlignment: VerticalAlignment.Center
                                text: qsTr("Wanita") + Retranslate.onLocaleOrLanguageChanged
                            }
                            gestureHandlers:
                            [
                                TapHandler {
                                    onTapped: {
                                        wanitaField.setChecked(!wanitaField.checked)
                                    }
                                }
                            ]
                        } //option wanita

                    }
                } //jenis kelamin

                //tanggal lahir
                Container {
                    topPadding: fieldBottomPadding
                    bottomPadding: fieldBottomPadding
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        Label {
                            text: qsTr("Tanggal Lahir") + Retranslate.onLocaleOrLanguageChanged
                            verticalAlignment: VerticalAlignment.Center
                            rightMargin: 0
                        }
                        Label {
                            text: "*"
                            leftMargin: 2
                            verticalAlignment: VerticalAlignment.Top
                            textStyle{
                                color: Color.Red
                            }
                        }
                    }
                    Container {
                        bottomPadding: fieldBottomPadding
                        layout: DockLayout {

                        }
                        ImageView {
                            imageSource: "asset:///images/border/input.amd"
                            verticalAlignment: VerticalAlignment.Fill
                            horizontalAlignment: HorizontalAlignment.Fill
                        }
                        /*TextField {
                            objectName: "tglLahirField"
                            leftPadding: 40
                            verticalAlignment: VerticalAlignment.Center
                            backgroundVisible: false
                            hintText: ""
                            enabled: false
                        }*/
                        Container {
                            DateTimePicker {
                                id: datePicker
                                objectName: "datePicker"
                                title: ""
                                mode: DateTimePickerMode.Date
                                value: { new Date(); }
                            }
                        }
                        gestureHandlers: [
                            TapHandler {
                                onTapped: {
                                    page.openDatePicker()
                                }
                            }
                        ]
                    }

                }
                //tanggal lahir

                //alamat
                Container {
                    
                    Label {
                        text: qsTr("Alamat") + Retranslate.onLocaleOrLanguageChanged
                    }
                    Container {
                        bottomPadding: fieldBottomPadding
                        layout: DockLayout {

                        }
                        ImageView {
                            imageSource: "asset:///images/border/input.amd"
                            verticalAlignment: VerticalAlignment.Fill
                            horizontalAlignment: HorizontalAlignment.Fill
                        }
                        TextArea {
                            objectName: "alamatField"
                            leftPadding: 40
                            verticalAlignment: VerticalAlignment.Center
                            backgroundVisible: false
                            minHeight: 170
                            hintText: ""

                        }
                    } //alamat
                }

                //Tipe identitas
                Label {
                    text: qsTr("Tipe identitas") + Retranslate.onLocaleOrLanguageChanged
                }
                Container {
                    bottomPadding: fieldBottomPadding
                    layout: DockLayout {
                    
                    }
                    ImageView {
                        imageSource: "asset:///images/border/input.amd"
                        verticalAlignment: VerticalAlignment.Fill
                        horizontalAlignment: HorizontalAlignment.Fill
                    }
                    TextField {
                        objectName: "identitasField"
                        leftPadding: 40
                        verticalAlignment: VerticalAlignment.Center
                        backgroundVisible: false
                        hintText: ""
                        enabled: false
                    }
                    gestureHandlers: [
                        TapHandler {
                            onTapped: {
                                page.openTipeIdentitas()
                            }
                        }
                    ]
                } 
                //Tipe identitas
                
                //No Identitas
                Label {
                    text: qsTr("No Identitas") + Retranslate.onLocaleOrLanguageChanged
                }
                Container {
                    bottomPadding: fieldBottomPadding
                    layout: DockLayout {
                    
                    }
                    ImageView {
                        imageSource: "asset:///images/border/input.amd"
                        verticalAlignment: VerticalAlignment.Fill
                        horizontalAlignment: HorizontalAlignment.Fill
                    }
                    TextField {
                        objectName: "noIdentitasField"
                        leftPadding: 40
                        verticalAlignment: VerticalAlignment.Center
                        backgroundVisible: false
                        hintText: ""
                    }
                } //No Identitas

                //nama ibu kandung
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Label {
                        text: qsTr("Nama Ibu Kandung") + Retranslate.onLocaleOrLanguageChanged
                        verticalAlignment: VerticalAlignment.Center
                        rightMargin: 0
                    }
                    Label {
                        text: "*"
                        leftMargin: 2
                        verticalAlignment: VerticalAlignment.Top
                        textStyle{
                            color: Color.Red
                        }
                    }
                }
                Container {
                    bottomPadding: fieldBottomPadding
                    layout: DockLayout {

                    }
                    ImageView {
                        imageSource: "asset:///images/border/input.amd"
                        verticalAlignment: VerticalAlignment.Fill
                        horizontalAlignment: HorizontalAlignment.Fill
                    }
                    TextField {
                        leftPadding: 40
                        objectName: "namaIbuField"
                        verticalAlignment: VerticalAlignment.Center
                        backgroundVisible: false
                        hintText: ""
                    }
                } //nama ibu kandung

                

                //PIN
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Label {
                        text: qsTr("PIN") + Retranslate.onLocaleOrLanguageChanged
                        verticalAlignment: VerticalAlignment.Center
                        rightMargin: 0
                    }
                    Label {
                        text: "*"
                        leftMargin: 2
                        verticalAlignment: VerticalAlignment.Top
                        textStyle{
                            color: Color.Red
                        }
                    }
                }
                Container {
                    bottomPadding: fieldBottomPadding
                    layout: DockLayout {

                    }
                    ImageView {
                        imageSource: "asset:///images/border/input.amd"
                        verticalAlignment: VerticalAlignment.Fill
                        horizontalAlignment: HorizontalAlignment.Fill
                    }
                    TextField {
                        objectName: "pinField"
                        leftPadding: 40
                        verticalAlignment: VerticalAlignment.Center
                        backgroundVisible: false
                        hintText: ""
                        inputMode: TextFieldInputMode.NumericPassword
                        
                    }
                } //pin
                
                //PIN
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Label {
                        text: qsTr("Konfirmasi PIN") + Retranslate.onLocaleOrLanguageChanged
                        verticalAlignment: VerticalAlignment.Center
                        rightMargin: 0
                    }
                    Label {
                        text: "*"
                        leftMargin: 2
                        verticalAlignment: VerticalAlignment.Top
                        textStyle{
                            color: Color.Red
                        }
                    }
                }
                Container {
                    bottomPadding: fieldBottomPadding
                    layout: DockLayout {

                    }
                    ImageView {
                        imageSource: "asset:///images/border/input.amd"
                        verticalAlignment: VerticalAlignment.Fill
                        horizontalAlignment: HorizontalAlignment.Fill
                    }
                    TextField {
                        objectName: "pin2Field"
                        leftPadding: 40
                        verticalAlignment: VerticalAlignment.Center
                        backgroundVisible: false
                        hintText: ""
                        inputMode: TextFieldInputMode.NumericPassword
                    }
                } //pin

                //tnc
                Container {
                    bottomPadding: fieldBottomPadding
                    horizontalAlignment: HorizontalAlignment.Center
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    ImageToggleButton {
                        id: agreeField
                        objectName: "agreeField"
                        imageSourceDefault: "asset:///images/icon-checked.png"
                        imageSourceChecked: "asset:///images/icon-checked-h.png"
                        verticalAlignment: VerticalAlignment.Center
                        minHeight: 50
                        minWidth: 50
                    }
                    Label {
                        text: "Saya setuju dengan <a href='#'>Syarat dan ketentuan layanan Dompetku</a> "
                        textStyle {
                            fontSize: FontSize.XSmall
                        }
                        textFormat: TextFormat.Html
                        activeTextHandler: ActiveTextHandler {
                            onTriggered: {
                                page.showTermsView();
                            }
                        }
                        multiline: true
                    }
                    gestureHandlers: [
                        TapHandler {
                            onTapped: {
                                agreeField.setChecked(!agreeField.checked);
                            }
                        }
                    ]
                }
                //tnc

                ImageButton {
                    horizontalAlignment: HorizontalAlignment.Center
                    defaultImageSource: "asset:///images/button-submit.png"
                    onClicked: {
                        page.daftar();
                    }
                }

            }
        }
    }

}
