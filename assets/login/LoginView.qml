/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.0

Container {
    property int screenW: 768 //UiUtils.ScreenWidth
    property int screenH: 1280 //UiUtils.ScreenHeight

    background: Color.create("#F2F2F2")
    preferredWidth: screenW
    preferredHeight: screenH
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    layout: DockLayout {

    }
    ScrollView {
        //translationY: -50
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center
        //login input
        Container {
            layout: DockLayout {

            }
            preferredWidth: screenW
            preferredHeight: screenH
            Container {

                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                ImageView {
                    imageSource: "asset:///images/logo.png"
                    horizontalAlignment: HorizontalAlignment.Center
                }

                Container {
                    preferredWidth: screenW
                    topPadding: 100
                    horizontalAlignment: HorizontalAlignment.Center
                    Container {
                        leftPadding: 80
                        rightPadding: 80
                        horizontalAlignment: HorizontalAlignment.Center
                        Label {
                            text: qsTr("No HP") + Retranslate.onLocaleOrLanguageChanged
                            textStyle {
                                fontWeight: FontWeight.Normal
                                fontSize: FontSize.PointValue
                                fontSizeValue: 8
                                color: Color.Black
                            }
                        }
                        //nomor akun
                        Container {
                            layout: DockLayout {

                            }
                            ImageView {
                                imageSource: "asset:///images/border/input.amd"
                                verticalAlignment: VerticalAlignment.Fill
                                horizontalAlignment: HorizontalAlignment.Fill
                            }
                            TextField {
                                objectName: "nomorAkun"
                                leftPadding: 40
                                verticalAlignment: VerticalAlignment.Center
                                backgroundVisible: false
                                hintText: ""
                                text: ""
                            }
                        } //nomor akun

                        Label {
                            topMargin: 40
                            text: qsTr("PIN") + Retranslate.onLocaleOrLanguageChanged
                            textStyle {
                                fontWeight: FontWeight.Normal
                                fontSize: FontSize.PointValue
                                fontSizeValue: 8
                                color: Color.Black
                            }
                        }
                        //PIN
                        Container {
                            layout: DockLayout {

                            }
                            ImageView {
                                imageSource: "asset:///images/border/input.amd"
                                verticalAlignment: VerticalAlignment.Fill
                                horizontalAlignment: HorizontalAlignment.Fill
                            }
                            TextField {
                                objectName: "pin"
                                text: ""
                                leftPadding: 40
                                verticalAlignment: VerticalAlignment.Center
                                backgroundVisible: false
                                hintText: ""
                                inputMode: TextFieldInputMode.NumericPassword
                                input {
                                    submitKey: SubmitKey.Submit
                                    onSubmitted: {
                                        page.login();
                                    }
                                }
                            }
                        } //PIN
                    }
                    ImageButton {
                        topMargin: 50
                        horizontalAlignment: HorizontalAlignment.Center
                        defaultImageSource: "asset:///images/button/button-masuk.png"
                        onClicked: {
                            page.login();
                        }
                    }
                    Label {
                        topMargin: 40
                        horizontalAlignment: HorizontalAlignment.Center
                        text: "<a href='#' style=\"color:#A1A1A1;\">Lupa pin?</a>"
                        //<div style=\"width: 67px;height: 40px; background-color: #DBDBDB;moz-border-radius: 15px; -webkit-border-radius: 15px; border: 5px solid #009900;\"> <a href='#' style=\"color:#A1A1A1;\">Lupa pin?</a> </div>
                        textFormat: TextFormat.Html
                        textStyle {
                            fontWeight: FontWeight.Normal
                            fontSize: FontSize.PointValue
                            fontSizeValue: 8
                            color: Color.create("#A1A1A1")
                        }
                        activeTextHandler: ActiveTextHandler {
                          onTriggered: {
                          page.showLupaPinView();
                          }
                          }
                    }
                }


            } //login input
            
            Container {
                visible: false
                bottomPadding: 50
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Bottom
                //link registrasi
                Container {
                    topPadding: 50
                    horizontalAlignment: HorizontalAlignment.Center
                    Divider {
                        preferredWidth: 500
                    }
                
                } //link registrasi
                
                Label {
                    topMargin: 40
                    horizontalAlignment: HorizontalAlignment.Center
                    text: "Buat akun baru? <a href='#' style=\"color:#A1A1A1;\"> Registrasi </a> "
                    textFormat: TextFormat.Html
                    textStyle {
                        
                    }
                    textStyle {
                        fontWeight: FontWeight.Normal
                        fontSize: FontSize.PointValue
                        fontSizeValue: 8
                        color: Color.create("#A1A1A1")
                    }
                    activeTextHandler: ActiveTextHandler {
                      onTriggered: {
                      page.showRegisterView();
                      }
                      }
                    multiline: true
                }
            }
        }
    } //scroll view

}
