/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.0
Container {
    background: Color.create("#F2F2F2")
    implicitLayoutAnimationsEnabled: false
   
        Container {
            implicitLayoutAnimationsEnabled: false
            verticalAlignment: VerticalAlignment.Top
            horizontalAlignment: HorizontalAlignment.Fill
            preferredWidth: Infinity
            layout: DockLayout {
            }
            minHeight: 100
            ImageView {
                imageSource: "asset:///images/header_bg.jpg"
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
            }
            ImageButton {
                objectName: "buttonBack"
                defaultImageSource: "asset:///images/back_button.png"
                verticalAlignment: VerticalAlignment.Center
            }
            Container {
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                implicitLayoutAnimationsEnabled: false
                topPadding: 10
                bottomPadding: 10
                Label {
                    objectName: "labelTitle"
                    text: "Header"
                    implicitLayoutAnimationsEnabled: false
                    horizontalAlignment: HorizontalAlignment.Center
                    textStyle {
                        fontWeight: FontWeight.Bold
                        fontSize: FontSize.PointValue
                        fontSizeValue: 8
                        color: Color.White
                    }
                }
            }
        }
        Container {
            layout: DockLayout {
                
            }
            preferredHeight: UiUtils.ScreenHeight
            preferredWidth: UiUtils.ScreenWidth
            
            ActivityIndicator {
                objectName: "indicator"
                preferredHeight: 200
                preferredWidth: 200
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
            }
            Container {
                objectName: "contentContainer"
                
            }
        }
        

    Container {
        minHeight: 150
        objectName: "bottomContainer"
        preferredWidth: Infinity
        verticalAlignment: VerticalAlignment.Bottom
        layout: DockLayout {

        }
        ImageView {
            imageSource: "asset:///images/bg-gradasi.png"
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
        }
        Container {
            objectName: "bottomButtonContainer"

        }
    }

    
}
