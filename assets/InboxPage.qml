import bb.cascades 1.0
import com.dompetku.InboxItem 1.0

Container {
    property int screenW: UiUtils.ScreenWidth
    property int screenH: UiUtils.ScreenHeight
    property int fieldBottomPadding: 40
    background: Color.create("#F2F2F2")
    preferredWidth: screenW
    preferredHeight: screenH
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    layout: DockLayout {
    
    }
    Container {
        
        Container {
            background: Color.create("#F37022")
            preferredWidth: Infinity
            topPadding: 30
            bottomPadding: 30
            Container {
                layout: DockLayout {
                }
                preferredWidth: Infinity
                Container {
                    leftPadding: 20
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    ImageButton {
                        objectName: "buttonBack"
                        defaultImageSource: "asset:///images/back_button.png"
                        verticalAlignment: VerticalAlignment.Center
                        onClicked: {
                            page.back();
                        }
                    }
                
                }
                
                Label {
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    text: qsTr("Inbox")
                    textStyle {
                        fontWeight: FontWeight.Bold
                        fontSize: FontSize.Large
                        color: Color.White
                    }
                }
            }
        
        }
        
        Container {
            ListView {
                objectName: "list"
                listItemComponents: [
                    ListItemComponent {
                        type: "item"
                        InboxItem{
                            data: ListItemData
                        }
                    }
                ]
                function itemType(data, indexPath) {
                    return "item"
                }
            }
        }
    
    }
}
